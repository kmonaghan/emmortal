var setTime;
var counter=15;
$(document).ready(function()
{
    var base_url=$('#base_url').val();
    var session_user_id=$('#session_user_id').val();
    var session_pass=$('#session_pass').val();
    var is_logged_in=$('#is_logged_in').val();
    var session_email_id=$('#session_email_id').val();
    setInterval(function ()
    {
        if(is_logged_in || is_logged_in==true)
        {
            $.ajax({

                url: base_url + 'Users/checkSessionLogin',
                data: {user_id: session_user_id},
                type: 'post',
                success: function (data)
                {
                    var jsObject = JSON.parse(data);
                   if(jsObject.session_data.session_email_id!=session_email_id || jsObject.session_data.session_pass!=session_pass)
                   {

                       $('#new_pass').val(jsObject.session_data.session_pass);
                       $('#new_email').val(jsObject.session_data.session_email_id);
                       setTime=setTimeout(function()
                       {
                           if(counter>0)
                           {
                               $('#logoutTimer').modal('show');
                                $('#logoutMessage').html('You will be logout in '+counter+' seconds!');
                           }
                           if(counter==0)
                           {
                               clearTimeout(setTime);
                               window.location.href=base_url+'Users/logout';
                           }
                           counter--;
                       }, 1000)
                   }
                }
            });
        }
        else
        {
            window.location.href=base_url;
        }
    }, 1000);
});
function signMeOut()
{
    var base_url=$('#base_url').val();
    window.location.href=base_url+'Users/logout';
}
function keepMeSignnedIn()
{
    clearTimeout(setTime);
    var base_url=$('#base_url').val();
    var password=$('#new_pass').val();
    var email=$('#new_email').val();
    $.ajax({

        url: base_url + 'Users/keepMeSignnedIn',
        data: {email:email,password:password},
        type: 'post',
        success: function (data)
        {
            location.reload();
        }
    });
}