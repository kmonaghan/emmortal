var dragged = 0;
// Choose image file
$(document).on('click', '.image_browse_icon', function()
{
    $(".image_file").click();

});
$('document').ready(function ()
{
    /* Send Button Enable or disable for support */
    $('#comment_form textarea').on('keyup blur', function () { // fires on every keyup & blur
        var comment_text = $('#comment_text').val().trim();
        if (comment_text != '')  {                   // checks form for validity
            $('#btn_comment_relationships').prop('disabled', false);        // enables button
            $('#btn_comment_relationships').css('cursor', 'pointer');

        } else {
            $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
            $('#btn_comment_relationships').css('cursor', 'not-allowed');

        }
    });
    $('.image_file').change(function ()
    {
        if (this.files && this.files[0] &&  this.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
        {

        }
        else
        {
            $(this).val('');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    });
    $("#comment_form").validate(
        {

            submitHandler: function (form)
            {
                var session_user_id=$('#session_user_id').val();
                if(session_user_id)
                {
                    var base_url=$('#base_url').val();
                    var url = $('#base_url').val() + 'Users/commentProfilePage';
                    var comments_type_id = $('#comments_type_id').val();
                    var user_id=$('#profile_user_id').val();
                    var session_user_id=$('#session_user_id').val();
                    var comment_text = $('#comment_text').val().trim();
                    var tagger_id=$('#relationshipsModalShow_'+comments_type_id).find('.tagger_id').val();
                    if (comment_text != '')
                    {
                        var comments_form_data = new FormData();
                        comments_form_data.append('comments_type_id',comments_type_id);
                        comments_form_data.append('comment_text',comment_text);
                        comments_form_data.append('tagger_id',tagger_id);
                        if($('.image_file')[0].files[0])
                        {
                            comments_form_data.append('comment_file', $('.image_file')[0].files[0]);
                        }
                        Pace.track(function() {
                            $.ajax({
                                url: url,
                                type: 'post',
                                data: comments_form_data,
                                contentType: false,
                                processData: false,
                                cache: false,
                                success: function (data) {
                                    $('#btn_comment').prop('disabled', 'disabled');   // disables button
                                    $('#btn_comment').css('cursor', 'not-allowed');
                                    var appendHtml = '';
                                    var jsObject = JSON.parse(data);
                                    if (jsObject.commentsData.length > 0) {
                                        for (var i = 0; i < jsObject.commentsData.length; i++) {
                                            var flag=0;
                                            appendHtml += '<div class="use-padding  use-border">';
                                            appendHtml += '<div class="left-box">';
                                            appendHtml += '<p><span class="square-icon">';
                                            if (jsObject.commentsData[i].profileimage) {

                                                appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + jsObject.commentsData[i].profileimage + '" />';
                                            }
                                            else {
                                                appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                                            }
                                            appendHtml += '</span>';
                                            appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                                            appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                                            appendHtml += '</div>';
                                            appendHtml += '<div class="right-box">';
                                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                                            {
                                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                                {
                                                    flag=1;
                                                    break;
                                                }
                                                else
                                                {
                                                    flag=0;
                                                }
                                            }
                                            if(flag==1)
                                            {
                                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                if(session_user_id!=jsObject.commentsData[i].userid)
                                                {
                                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                                }

                                            }
                                            var flag1=0;
                                            var count=0;
                                            for(var j=0;j<jsObject.allLikecomment.length;j++)
                                            {
                                                if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                                                {
                                                    flag1=1;
                                                    break;
                                                }

                                            }
                                            for(var k=0;k<jsObject.allLikecomment.length;k++)
                                            {
                                                if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                                                {
                                                    count++;
                                                }
                                            }
                                            if(flag1==1)
                                            {
                                                appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                            }
                                            else
                                            {
                                                appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)" ><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                            }
                                            appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                                            appendHtml += '</div>';
                                            if (jsObject.commentsData[i].image_comments) {
                                                appendHtml += '<img class="men-in-black" src="' + base_url + jsObject.commentsData[i].image_comments + '" />';
                                            }
                                            appendHtml += '</div>';

                                        }
                                    }
                                    $('.count_comment').html(jsObject.commentsData.length);
                                    $('.comment_body').html(appendHtml);
                                    $('#comment_text').val('');
                                    $('.image_file').val('');
                                    $('#comment_perticular_' + comments_type_id).html(jsObject.commentsData.length);
                                }
                            });
                        });
                    }
                }
                else
                {
                    $.notify
                    ({

                            message: 'You must Sign In'
                        }
                        , {
                            type: 'danger',
                            offset:
                                {
                                    x: 10,
                                    y: 50
                                },
                            z_index: 1050,
                        });
                    $('#comment_text').val('');
                    $('.image_file').val('');
                    $('.relationshipsModal').modal('hide');
                    $('.relationshipsModal1').modal('hide');
                    $("#comment_div").modal('hide');
                    $('#signinModal').modal('show');
                }
            }

        });
});


$(document).ready(function()
{
   /* var start = parseInt($('#row_count').val());
    var page_id=$('#page_id').val();
    if ($('#row_count').val()< 15)
    {
        $('#row_count').val(start + 15);
        sendData(page_id,start);
    }
    $(window).scroll(function ()
    {
        if($(window).scrollTop() + $(window).height() >= $(document).height())
        {

            var start = parseInt($('#row_count').val());
            if ($('#total_row').val() >= start)
            {
                $('#row_count').val(start + 15);
                sendData(page_id,start);
            }

        }
    });*/
    sendData($('#page_id').val());
});
function showPage(page_id)
{
   /* $('#row_count').val(0);
    var start = 0;
    if ($('#row_count').val()< 15)
    {
        $('#row_count').val(start + 15);
        sendData1(page_id,start);

    }
    $(window).scroll(function ()
    {
        if($(window).scrollTop() + $(window).height() >= $(document).height())
        {

            var start = parseInt($('#row_count').val());
            if ($('#total_row').val() >= start)
            {
                $('#row_count').val(start + 15);
                sendData(page_id,start);
            }

        }
    });*/
    sendData1(page_id);
}
function  sendData1(page_id)
{
    $('#page_content').html('');
    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?page_id='+page_id;
    var sessionUserId=$('#session_user_id').val();
   // history.replaceState({path:newurl},'',newurl);
    window.location.replace(newurl);
    var url=$('#base_url').val();
    Pace.track(function()
    {
        $.ajax({
            type: "post",
            url: url + "Users/pageContent",
            data: {page_id: page_id},
            success: function (data)
            {
                $("#page_content").html('');
                var jsObject = JSON.parse(data);
                if (jsObject.page_content.page_style_id)
                {
                    var image = url + jsObject.page_content.upload_path + '/' + jsObject.page_content.image_name;
                    $('body').css("background-image", "url(" + image + ")");
                }
                if (jsObject.page_content.page_style_id==0)
                {
                    if(jsObject.page_content.backgroundimage=='')
                    {
                        if(jsObject.fetchBackgroundStyle.image_name)
                        {
                            var image = url + jsObject.fetchBackgroundStyle.upload_path + '/' +escape(jsObject.fetchBackgroundStyle.image_name);
                            $('body').css("background-image", "url(" + image + ")");
                        }
                        else
                        {
                            var image = url + 'assets/img/bg2.jpg';
                            $('body').css("background-image", "url(" + image + ")");
                        }
                    }
                    else
                    {
                        var image = url + escape(jsObject.page_content.backgroundimage);
                        $('body').css("background-image", "url(" + image + ")");
                    }
                }
                if(jsObject.page_content.default_page==0)
                {

                    var headHtml = '';
                    var appendHtml='';
                    headHtml += '<h1>' + jsObject.page_content.page_title + '</h1>';
                    if(jsObject.pageAllContent.length>0)
                    {
                        for (var i = 0; i < jsObject.pageAllContent.length; i++)
                        {

                            var flag_particular_album_total_like=0;
                            var flag_particular_album_total_comment=0;
                            var particular_album_total_like=0;
                            var particular_album_total_comment=0;
                            var flag_particular_album_total_content=0;
                            var particular_album_total_content=0;

                            var flag_particular_image_total_like=0;
                            var flag_particular_image_total_comment=0;
                            var particular_image_total_like=0;
                            var particular_image_total_comment=0;

                            var flag_particular_text_total_like=0;
                            var flag_particular_text_total_comment=0;
                            var particular_text_total_like=0;
                            var particular_text_total_comment=0;

                            var flag_particular_video_total_like=0;
                            var flag_particular_video_total_comment=0;
                            var particular_video_total_like=0;
                            var particular_video_total_comment=0;

                            var flag_particular_tribute_total_like=0;
                            var flag_particular_tribute_total_comment=0;
                            var particular_tribute_total_like=0;
                            var particular_tribute_total_comment=0;

                            if(jsObject.pageAllContent[i].page_album_id)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showAlbumdetails(\''+url+'ImageUpload/albumdetails/'+jsObject.pageAllContent[i].user_id+'/'+jsObject.pageAllContent[i].page_album_id+'\')">';
                                if(jsObject.pageAllContent[i].image_name=="")
                                {
                                    appendHtml +='<img src="'+url+'assets/img/no_cover.jpg" alt="img not found">';
                                }
                                else
                                {
                                    appendHtml +='<img src="'+url+jsObject.pageAllContent[i].uploadPath+jsObject.pageAllContent[i].image_name+'" alt="img not found">';
                                }
                                if(jsObject.totalCommentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalCommentParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_comment=1;
                                            particular_album_total_comment=jsObject.totalCommentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalLikeParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_like=1;
                                            particular_album_total_like=jsObject.totalLikeParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_like=0;
                                        }
                                    }
                                }
                                if(jsObject.totalContentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalContentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalContentParticularAlbum[j].albumid)
                                        {
                                            flag_particular_album_total_content=1;
                                            particular_album_total_content=jsObject.totalContentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_content=0;
                                        }
                                    }
                                }
                                appendHtml += '<div class="album_triangle_bg"><div class="album_content_count">';
                                appendHtml += ' <i class="fa fa-file-image-o" aria-hidden="true"></i>';
                                if(flag_particular_album_total_content==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_content+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</div></div>';
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';
                                if(flag_particular_album_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_album_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_image_id) {


                                var image_id = 'image_' + jsObject.pageAllContent[i].page_image_id;
                                var title=jsObject.pageAllContent[i].image_title;
                                var desc=jsObject.pageAllContent[i].image_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                if(jsObject.pageAllContent[i].thumbnail_upload_path!="" && jsObject.pageAllContent[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.pageAllContent[i].thumbnail_upload_path + jsObject.pageAllContent[i].thumbnail_image_name;
                                }
                               else
                                {
                                    var image = jsObject.pageAllContent[i].uploadPath + jsObject.pageAllContent[i].image_name;
                                }
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
 		                        appendHtml += '<div class="gridster-box">';
 		                        appendHtml += ' <div class="handle-resize"></div>';
 		                        appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';

                                }
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_' + image_id + '" onclick="showModal(\'' + image_id + '\')">';
                                appendHtml += '<img src="' + url + image + '" alt="img not found">';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }

                                if(jsObject.totalCommentParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalCommentParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_comment=1;
                                            particular_image_total_comment=jsObject.totalCommentParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalLikeParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_like=1;
                                            particular_image_total_like=jsObject.totalLikeParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_image_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">'+particular_image_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_image_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">'+particular_image_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_text_id) {

                                var text_id = 'text_' + jsObject.pageAllContent[i].page_text_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                var str=jsObject.pageAllContent[i].text_title;
                                var n = jsObject.pageAllContent[i].text_title.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + text_id + '" onclick="showModal(\'' + text_id + '\')">';
                                appendHtml += '<p class="text_description">' + str + '</p>';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_description + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].fullname + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].title + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularText)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalCommentParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_comment=1;
                                            particular_text_total_comment=jsObject.totalCommentParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularText)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalLikeParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_like=1;
                                            particular_text_total_like=jsObject.totalLikeParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_text_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">'+particular_text_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_text_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">'+particular_text_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_video_id) {
                                var video_id = 'video_' + jsObject.pageAllContent[i].page_video_id;
                                var title=jsObject.pageAllContent[i].video_title;
                                var desc=jsObject.pageAllContent[i].video_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                var video = 'https://player.vimeo.com/video/' + jsObject.pageAllContent[i].vimeo_video_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0"></iframe>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + video_id + '" onclick="showModal(\'' + video_id + '\')">';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalCommentParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_comment=1;
                                            particular_video_total_comment=jsObject.totalCommentParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalLikeParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_like=1;
                                            particular_video_total_like=jsObject.totalLikeParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_video_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">'+particular_video_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_video_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">'+particular_video_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if(jsObject.pageAllContent[i].page_tribute_id)
                            {
                                var str=jsObject.pageAllContent[i].tribute_description;
                                var n = jsObject.pageAllContent[i].tribute_description.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showTributedetails(\''+url+'Users/tributeDetails/'+jsObject.pageAllContent[i].user_id+'?tribute_id='+jsObject.pageAllContent[i].page_tribute_id+'\')">';
                                appendHtml +='<p class="text_description">'+str+'</p>';
                                appendHtml +='</a>';
                                if(jsObject.totalCommentParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalCommentParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_comment=1;
                                            particular_tribute_total_comment=jsObject.totalCommentParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalLikeParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_like=1;
                                            particular_tribute_total_like=jsObject.totalLikeParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_tribute_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_tribute_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                        }
                    }
                    $('.profile_page_head').html(headHtml);
                    $('#page_content').append(appendHtml);
                    $('.actscrl').toggleClass('actscrl');
                    $('#' + page_id).find('li').toggleClass('actscrl');
                }
                else
                {
                    var headHtml = '';
                    var appendHtml='';
                    headHtml += '<h1>' + jsObject.page_content.page_title + '</h1>';
                    if(jsObject.pageAllContent.length>0)
                    {
                        for(var i=0;i<jsObject.pageAllContent.length;i++)
                        {
                            if(jsObject.pageAllContent[i].user_profile_id!=0)
                            {
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_profile_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="'+url+'Users/settings#parentVerticalTab1" data-original-title="Edit!" data-toggle="tooltip">';
                                    appendHtml +='<i class="glyphicon glyphicon-pencil EditText"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_profile_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="javascript:void(0);" data-original-title="Delete!" data-toggle="tooltip" id="profileImage_'+jsObject.pageAllContent[i].user_profile_id+'" onclick="deleteContentPage(id)">';
                                    appendHtml +='<i class="fa fa-trash-o" aria-hidden="true"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                if (jsObject.pageAllContent[i].profileimage) {

                                    var profileimage=jsObject.pageAllContent[i].profileimage;
                                }
                                else
                                {
                                    var profileimage="assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg";
                                }
                                appendHtml +='<a onclick="fullProfileImage(\'' + url + profileimage + '\')" class="example-image-link fullProfileImage">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img src="' + url + jsObject.pageAllContent[i].profileimage + '"/>';
                                } else {
                                    appendHtml += '<img src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>';
                                }
                                appendHtml +='</a>';
                                appendHtml +='</div>'
                            }
                            if(jsObject.pageAllContent[i].user_description_id!=0)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {

                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';

                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_description_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="'+url+'Users/settings#parentVerticalTab1" title="Edit!" data-toggle="tooltip">';
                                    appendHtml +='<i class="glyphicon glyphicon-pencil EditText"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_description_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="javascript:void(0);" data-original-title="Delete!" data-toggle="tooltip" id="userDescription_'+jsObject.pageAllContent[i].user_description_id+'" onclick="deleteContentPage(id)">';
                                    appendHtml +='<i class="fa fa-trash-o" aria-hidden="true"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<p class="text_description">Date Of Birth: '+jsObject.pageAllContent[i].dob+'<br>'+jsObject.pageAllContent[i].fullname+'<br><br></p>'
                                appendHtml +='</div>'
                            }
                            var flag_particular_album_total_like=0;
                            var flag_particular_album_total_comment=0;
                            var particular_album_total_like=0;
                            var particular_album_total_comment=0;

                            var flag_particular_image_total_like=0;
                            var flag_particular_image_total_comment=0;
                            var particular_image_total_like=0;
                            var particular_image_total_comment=0;

                            var flag_particular_text_total_like=0;
                            var flag_particular_text_total_comment=0;
                            var particular_text_total_like=0;
                            var particular_text_total_comment=0;

                            var flag_particular_video_total_like=0;
                            var flag_particular_video_total_comment=0;
                            var particular_video_total_like=0;
                            var particular_video_total_comment=0;

                            var flag_particular_tribute_total_like=0;
                            var flag_particular_tribute_total_comment=0;
                            var particular_tribute_total_like=0;
                            var particular_tribute_total_comment=0;

                            if(jsObject.pageAllContent[i].page_album_id)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showAlbumdetails(\''+url+'ImageUpload/albumdetails/'+jsObject.pageAllContent[i].user_id+'/'+jsObject.pageAllContent[i].page_album_id+'\')">';
                                if(jsObject.pageAllContent[i].image_name=="")
                                {
                                    appendHtml +='<img src="'+url+'assets/img/no_cover.jpg" alt="img not found">';
                                }
                                else
                                {
                                    appendHtml +='<img src="'+url+jsObject.pageAllContent[i].uploadPath+jsObject.pageAllContent[i].image_name+'" alt="img not found">';
                                }
                                if(jsObject.totalCommentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalCommentParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_comment=1;
                                            particular_album_total_comment=jsObject.totalCommentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalLikeParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_like=1;
                                            particular_album_total_like=jsObject.totalLikeParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_like=0;
                                        }
                                    }
                                }
                                if(jsObject.totalContentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalContentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalContentParticularAlbum[j].albumid)
                                        {
                                            flag_particular_album_total_content=1;
                                            particular_album_total_content=jsObject.totalContentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_content=0;
                                        }
                                    }
                                }
                                appendHtml += '<div class="album_triangle_bg"><div class="album_content_count">';
                                appendHtml += ' <i class="fa fa-file-image-o" aria-hidden="true"></i>';
                                if(flag_particular_album_total_content==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_content+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</div></div>';
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';
                                if(flag_particular_album_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_album_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_image_id) {


                                var image_id = 'image_' + jsObject.pageAllContent[i].page_image_id;
                                var title=jsObject.pageAllContent[i].image_title;
                                var desc=jsObject.pageAllContent[i].image_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                if(jsObject.pageAllContent[i].thumbnail_upload_path!="" && jsObject.pageAllContent[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.pageAllContent[i].thumbnail_upload_path + jsObject.pageAllContent[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.pageAllContent[i].uploadPath + jsObject.pageAllContent[i].image_name;
                                }
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_' + image_id + '" onclick="showModal(\'' + image_id + '\')">';
                                appendHtml += '<img src="' + url + image + '" alt="img not found">';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }

                                if(jsObject.totalCommentParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalCommentParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_comment=1;
                                            particular_image_total_comment=jsObject.totalCommentParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalLikeParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_like=1;
                                            particular_image_total_like=jsObject.totalLikeParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_image_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">'+particular_image_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_image_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">'+particular_image_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_text_id) {

                                var text_id = 'text_' + jsObject.pageAllContent[i].page_text_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                var str=jsObject.pageAllContent[i].text_title;
                                var n = jsObject.pageAllContent[i].text_title.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + text_id + '" onclick="showModal(\'' + text_id + '\')">';
                                appendHtml += '<p class="text_description">' + str + '</p>';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_description + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].fullname + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].title + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularText)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalCommentParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_comment=1;
                                            particular_text_total_comment=jsObject.totalCommentParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularText)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalLikeParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_like=1;
                                            particular_text_total_like=jsObject.totalLikeParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_text_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">'+particular_text_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_text_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">'+particular_text_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_video_id) {
                                var video_id = 'video_' + jsObject.pageAllContent[i].page_video_id;
                                var title=jsObject.pageAllContent[i].video_title;
                                var desc=jsObject.pageAllContent[i].video_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                var video = 'https://player.vimeo.com/video/' + jsObject.pageAllContent[i].vimeo_video_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0"></iframe>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + video_id + '" onclick="showModal(\'' + video_id + '\')">';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalCommentParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_comment=1;
                                            particular_video_total_comment=jsObject.totalCommentParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalLikeParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_like=1;
                                            particular_video_total_like=jsObject.totalLikeParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_video_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">'+particular_video_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_video_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">'+particular_video_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if(jsObject.pageAllContent[i].page_tribute_id)
                            {
                                var str=jsObject.pageAllContent[i].tribute_description;
                                var n = jsObject.pageAllContent[i].tribute_description.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showTributedetails(\''+url+'Users/tributeDetails/'+jsObject.pageAllContent[i].user_id+'?tribute_id='+jsObject.pageAllContent[i].page_tribute_id+'\')">';
                                appendHtml +='<p class="text_description">'+str+'</p>';
                                appendHtml +='</a>';
                                if(jsObject.totalCommentParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalCommentParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_comment=1;
                                            particular_tribute_total_comment=jsObject.totalCommentParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalLikeParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_like=1;
                                            particular_tribute_total_like=jsObject.totalLikeParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_tribute_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_tribute_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }

                        }
                    }
                    $('.profile_page_head').html(headHtml);
                    $('#page_content').append(appendHtml);
                    $('.actscrl').toggleClass('actscrl');
                    $('#' + page_id).find('li').toggleClass('actscrl');
                }
                if(jsObject.page_content.userid==parseInt(sessionUserId))
                {
                    $('#user_page_id').val(page_id);
                }
                $(document).ready(function ()
                {
                    if($('#session_user_id').val()==$('#page_user_id').val())
                    {
                        var gridster = null;
                        gridster= $(".gridster ul").gridster({
                            namespace: '.gridster',
                            widget_base_dimensions: ['auto', 200],
                            avoid_overlapped_widgets: true,
                            autogenerate_stylesheet: true,
                            min_cols: 1,
                            max_cols: 5,
                            widget_margins: [10, 10],
                            draggable:
                                {
                                    start: function(event, ui) {
                                        dragged = 1;
                                    },
                                    stop: function (event, ui) {
                                        var arr = [];
                                        arr.sizex = [];
                                        arr.sizey = [];
                                        arr.col = [];
                                        arr.row = [];
                                        arr.id = [];
                                        var i = 0;
                                        $(".content_li").each(function () {
                                            var id = $(this).attr('id');
                                            if (id != undefined) {
                                                var content_id = parseInt(id.split('_')[1]);
                                                var sizex = $('#' + id).attr('data-sizex');
                                                var sizey = $('#' + id).attr('data-sizey');
                                                var col = $('#' + id).attr('data-col');
                                                var row = $('#' + id).attr('data-row');
                                                arr.id.push(content_id);
                                                arr.sizex.push(sizex);
                                                arr.sizey.push(sizex);
                                                arr.col.push(col);
                                                arr.row.push(row);

                                            }
                                        });
                                        $.ajax({
                                            type:'post',
                                            url:url+'Users/updatePageContentResizableDraggable',
                                            data: {id:arr.id, sizex:arr.sizex,sizey:arr.sizey,col:arr.col,row:arr.row}

                                        }).done(function( data )
                                        {
                                            dragged = 0;
                                        });

                                    }

                                },
                            resize: {
                                enabled: true,
                                handles: ['n', 'e', 's', 'w', 'se', 'sw'],
                                max_size: [2, 2],
                                min_size: [1, 1],
                                stop: function (event, ui) {
                                    var arr = [];
                                    arr.sizex = [];
                                    arr.sizey = [];
                                    arr.col = [];
                                    arr.row = [];
                                    arr.id = [];
                                    var i = 0;
                                    $(".content_li").each(function () {
                                        var id = $(this).attr('id');
                                        if (id != undefined) {
                                            var content_id = parseInt(id.split('_')[1]);
                                            var sizex = $('#' + id).attr('data-sizex');
                                            var sizey = $('#' + id).attr('data-sizey');
                                            var col = $('#' + id).attr('data-col');
                                            var row = $('#' + id).attr('data-row');
                                            arr.id.push(content_id);
                                            arr.sizex.push(sizex);
                                            arr.sizey.push(sizey);
                                            arr.col.push(col);
                                            arr.row.push(row);

                                        }
                                    });
                                    $.ajax({
                                        type:'post',
                                        url:url+'Users/updatePageContentResizableDraggable',
                                        data: {id:arr.id, sizex:arr.sizex,sizey:arr.sizey,col:arr.col,row:arr.row}

                                    }).done(function( data )
                                    {

                                    });

                                }

                            }


                        }).data('gridster');
                        $('.gridster  ul').css({'padding': '0'});

                    }
                    else
                    {
                        var gridster = $(".gridster ul").gridster({
                            namespace: '.gridster',
                            widget_base_dimensions: ['auto', 200],
                            avoid_overlapped_widgets: true,
                            autogenerate_stylesheet: true,
                            min_cols: 1,
                            max_cols: 5,
                            widget_margins: [10, 10],
                            draggable:false,
                            resize:false
                        }).data('gridster').disable();
                        $('.gridster  ul').css({'padding': '0'});
                    }
                });
            }
        });
    });
}
function sendData(page_id)
{
    $('#page_content').html('');
    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?page_id='+page_id;
    var sessionUserId=$('#session_user_id').val();
    history.replaceState({path:newurl},'',newurl);
    var url=$('#base_url').val();
    Pace.track(function()
    {
        $.ajax({
            type: "post",
            url: url + "Users/pageContent",
            data: {page_id: page_id},
            success: function (data)
            {
                var jsObject = JSON.parse(data);
                if (jsObject.page_content.page_style_id)
                {
                    var image = url + jsObject.page_content.upload_path + '/' + jsObject.page_content.image_name;
                    $('body').css("background-image", "url(" + image + ")");
                }
                if (jsObject.page_content.page_style_id==0)
                {
                    if(jsObject.page_content.backgroundimage=='')
                    {
                        if(jsObject.fetchBackgroundStyle.image_name)
                        {
                            var image = url + jsObject.fetchBackgroundStyle.upload_path + '/' +escape(jsObject.fetchBackgroundStyle.image_name);
                            $('body').css("background-image", "url(" + image + ")");
                        }
                        else
                        {
                            var image = url + 'assets/img/bg2.jpg';
                            $('body').css("background-image", "url(" + image + ")");
                        }
                    }
                    else
                    {
                        var image = url + escape(jsObject.page_content.backgroundimage);
                        $('body').css("background-image", "url(" + image + ")");
                    }
                }
                if(jsObject.page_content.default_page==0)
                {

                    var headHtml = '';
                    var appendHtml='';
                    headHtml += '<h1>' + jsObject.page_content.page_title + '</h1>';
                    if(jsObject.pageAllContent.length>0) {
                        for (var i = 0; i < jsObject.pageAllContent.length; i++)
                        {

                            var flag_particular_album_total_like=0;
                            var flag_particular_album_total_comment=0;
                            var particular_album_total_like=0;
                            var particular_album_total_comment=0;
                            var flag_particular_album_total_content=0;
                            var particular_album_total_content=0;


                            var flag_particular_image_total_like=0;
                            var flag_particular_image_total_comment=0;
                            var particular_image_total_like=0;
                            var particular_image_total_comment=0;

                            var flag_particular_text_total_like=0;
                            var flag_particular_text_total_comment=0;
                            var particular_text_total_like=0;
                            var particular_text_total_comment=0;

                            var flag_particular_video_total_like=0;
                            var flag_particular_video_total_comment=0;
                            var particular_video_total_like=0;
                            var particular_video_total_comment=0;

                            var flag_particular_tribute_total_like=0;
                            var flag_particular_tribute_total_comment=0;
                            var particular_tribute_total_like=0;
                            var particular_tribute_total_comment=0;

                            if(jsObject.pageAllContent[i].page_album_id)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showAlbumdetails(\''+url+'ImageUpload/albumdetails/'+jsObject.pageAllContent[i].user_id+'/'+jsObject.pageAllContent[i].page_album_id+'\')">';
                                if(jsObject.pageAllContent[i].image_name=="")
                                {
                                    appendHtml +='<img src="'+url+'assets/img/no_cover.jpg" alt="img not found">';
                                }
                                else
                                {
                                    appendHtml +='<img src="'+url+jsObject.pageAllContent[i].uploadPath+jsObject.pageAllContent[i].image_name+'" alt="img not found">';
                                }
                                if(jsObject.totalCommentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalCommentParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_comment=1;
                                            particular_album_total_comment=jsObject.totalCommentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalLikeParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_like=1;
                                            particular_album_total_like=jsObject.totalLikeParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_like=0;
                                        }
                                    }
                                }
                                if(jsObject.totalContentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalContentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalContentParticularAlbum[j].albumid)
                                        {
                                            flag_particular_album_total_content=1;
                                            particular_album_total_content=jsObject.totalContentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_content=0;
                                        }
                                    }
                                }
                                appendHtml += '<div class="album_triangle_bg"><div class="album_content_count">';
                                appendHtml += ' <i class="fa fa-file-image-o" aria-hidden="true"></i>';
                                if(flag_particular_album_total_content==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_content+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</div></div>';
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';
                                if(flag_particular_album_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_album_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_image_id) {


                                var image_id = 'image_' + jsObject.pageAllContent[i].page_image_id;
                                var title=jsObject.pageAllContent[i].image_title;
                                var desc=jsObject.pageAllContent[i].image_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                if(jsObject.pageAllContent[i].thumbnail_upload_path!="" && jsObject.pageAllContent[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.pageAllContent[i].thumbnail_upload_path + jsObject.pageAllContent[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.pageAllContent[i].uploadPath + jsObject.pageAllContent[i].image_name;
                                }
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_' + image_id + '" onclick="showModal(\'' + image_id + '\')">';
                                appendHtml += '<img src="' + url + image + '" alt="img not found">';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }

                                if(jsObject.totalCommentParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalCommentParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_comment=1;
                                            particular_image_total_comment=jsObject.totalCommentParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalLikeParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_like=1;
                                            particular_image_total_like=jsObject.totalLikeParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_image_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">'+particular_image_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_image_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">'+particular_image_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_text_id) {

                                var text_id = 'text_' + jsObject.pageAllContent[i].page_text_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                var str=jsObject.pageAllContent[i].text_title;
                                var n = jsObject.pageAllContent[i].text_title.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + text_id + '" onclick="showModal(\'' + text_id + '\')">';
                                appendHtml += '<p class="text_description">' + str + '</p>';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_description + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].fullname + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].title + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularText)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalCommentParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_comment=1;
                                            particular_text_total_comment=jsObject.totalCommentParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularText)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalLikeParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_like=1;
                                            particular_text_total_like=jsObject.totalLikeParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_text_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">'+particular_text_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_text_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">'+particular_text_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_video_id) {
                                var video_id = 'video_' + jsObject.pageAllContent[i].page_video_id;
                                var title=jsObject.pageAllContent[i].video_title;
                                var desc=jsObject.pageAllContent[i].video_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                var video = 'https://player.vimeo.com/video/' + jsObject.pageAllContent[i].vimeo_video_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0"></iframe>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + video_id + '" onclick="showModal(\'' + video_id + '\')">';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalCommentParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_comment=1;
                                            particular_video_total_comment=jsObject.totalCommentParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalLikeParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_like=1;
                                            particular_video_total_like=jsObject.totalLikeParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_video_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">'+particular_video_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_video_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">'+particular_video_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if(jsObject.pageAllContent[i].page_tribute_id)
                            {
                                var str=jsObject.pageAllContent[i].tribute_description;
                                var n = jsObject.pageAllContent[i].tribute_description.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showTributedetails(\''+url+'Users/tributeDetails/'+jsObject.pageAllContent[i].user_id+'?tribute_id='+jsObject.pageAllContent[i].page_tribute_id+'\')">';
                                appendHtml +='<p class="text_description">'+str+'</p>';
                                appendHtml +='</a>';
                                if(jsObject.totalCommentParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalCommentParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_comment=1;
                                            particular_tribute_total_comment=jsObject.totalCommentParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalLikeParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_like=1;
                                            particular_tribute_total_like=jsObject.totalLikeParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_tribute_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_tribute_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                        }
                    }
                    $('.profile_page_head').html(headHtml);
                    $('#page_content').append(appendHtml);
                    $('.actscrl').toggleClass('actscrl');
                    $('#' + page_id).find('li').toggleClass('actscrl');
                }
                else
                {
                    var headHtml = '';
                    var appendHtml='';
                    headHtml += '<h1>' + jsObject.page_content.page_title + '</h1>';
                    if(jsObject.pageAllContent.length>0)
                    {

                        for(var i=0;i<jsObject.pageAllContent.length;i++)
                        {

                            if(jsObject.pageAllContent[i].user_profile_id!=0)
                            {
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_profile_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="'+url+'Users/settings#parentVerticalTab1" data-original-title="Edit!" data-toggle="tooltip">';
                                    appendHtml +='<i class="glyphicon glyphicon-pencil EditText"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_profile_id)
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<a  href="javascript:void(0);" data-original-title="Delete!" data-toggle="tooltip" id="profileImage_'+jsObject.pageAllContent[i].user_profile_id+'" onclick="deleteContentPage(id)">';
                                    appendHtml +='<i class="fa fa-trash-o" aria-hidden="true"></i>';
                                    appendHtml +='</a>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                if (jsObject.pageAllContent[i].profileimage) {

                                    var profileimage=jsObject.pageAllContent[i].profileimage;
                                }
                                else
                                {
                                    var profileimage="assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg";
                                }
                                appendHtml +='<a onclick="fullProfileImage(\'' + url + profileimage + '\')" class="example-image-link fullProfileImage">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img src="' + url + jsObject.pageAllContent[i].profileimage + '"/>';
                                } else {
                                    appendHtml += '<img src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>';
                                }
                                appendHtml +='</a>';
                                appendHtml +='</div>'
                            }
                            if(jsObject.pageAllContent[i].user_description_id!=0)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_description_id)
                                {
                                    appendHtml += '<div>';
                                    appendHtml +='<a  href="'+url+'Users/settings#parentVerticalTab1" title="Edit!" data-toggle="tooltip">';
                                    appendHtml +='<i class="glyphicon glyphicon-pencil EditText"></i>';
                                    appendHtml +='</a>';
                                    appendHtml += '</div>';
                                }
                                if(sessionUserId==jsObject.pageAllContent[i].user_description_id)
                                {
                                    appendHtml += '<div>';
                                    appendHtml +='<a  href="javascript:void(0);" data-original-title="Delete!" data-toggle="tooltip" id="userDescription_'+jsObject.pageAllContent[i].user_description_id+'" onclick="deleteContentPage(id)">';
                                    appendHtml +='<i class="fa fa-trash-o" aria-hidden="true"></i>';
                                    appendHtml +='</a>';
                                    appendHtml += '</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<p class="text_description">Date Of Birth: '+jsObject.pageAllContent[i].dob+'<br>'+jsObject.pageAllContent[i].fullname+'<br><br></p>'
                                appendHtml +='</div>'
                            }
                            var flag_particular_album_total_like=0;
                            var flag_particular_album_total_comment=0;
                            var particular_album_total_like=0;
                            var particular_album_total_comment=0;
                            var flag_particular_album_total_content=0;
                            var particular_album_total_content=0;

                            var flag_particular_image_total_like=0;
                            var flag_particular_image_total_comment=0;
                            var particular_image_total_like=0;
                            var particular_image_total_comment=0;

                            var flag_particular_text_total_like=0;
                            var flag_particular_text_total_comment=0;
                            var particular_text_total_like=0;
                            var particular_text_total_comment=0;

                            var flag_particular_video_total_like=0;
                            var flag_particular_video_total_comment=0;
                            var particular_video_total_like=0;
                            var particular_video_total_comment=0;

                            var flag_particular_tribute_total_like=0;
                            var flag_particular_tribute_total_comment=0;
                            var particular_tribute_total_like=0;
                            var particular_tribute_total_comment=0;

                            if(jsObject.pageAllContent[i].page_album_id)
                            {

                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showAlbumdetails(\''+url+'ImageUpload/albumdetails/'+jsObject.pageAllContent[i].user_id+'/'+jsObject.pageAllContent[i].page_album_id+'\')">';
                                if(jsObject.pageAllContent[i].image_name=="")
                                {
                                    appendHtml +='<img src="'+url+'assets/img/no_cover.jpg" alt="img not found">';
                                }
                                else
                                {
                                    appendHtml +='<img src="'+url+jsObject.pageAllContent[i].uploadPath+jsObject.pageAllContent[i].image_name+'" alt="img not found">';
                                }
                                if(jsObject.totalCommentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalCommentParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_comment=1;
                                            particular_album_total_comment=jsObject.totalCommentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalLikeParticularAlbum[j].album_id)
                                        {
                                            flag_particular_album_total_like=1;
                                            particular_album_total_like=jsObject.totalLikeParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_like=0;
                                        }
                                    }
                                }
                                if(jsObject.totalContentParticularAlbum)
                                {
                                    for(var j=0;j<jsObject.totalContentParticularAlbum.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_album_id==jsObject.totalContentParticularAlbum[j].albumid)
                                        {
                                            flag_particular_album_total_content=1;
                                            particular_album_total_content=jsObject.totalContentParticularAlbum[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_album_total_content=0;
                                        }
                                    }
                                }
                                appendHtml += '<div class="album_triangle_bg"><div class="album_content_count">';
                                appendHtml += ' <i class="fa fa-file-image-o" aria-hidden="true"></i>';
                                if(flag_particular_album_total_content==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_content+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</div></div>';
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';
                                if(flag_particular_album_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_album_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_album_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_image_id) {


                                var image_id = 'image_' + jsObject.pageAllContent[i].page_image_id;
                                var title=jsObject.pageAllContent[i].image_title;
                                var desc=jsObject.pageAllContent[i].image_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                if(jsObject.pageAllContent[i].thumbnail_upload_path!="" && jsObject.pageAllContent[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.pageAllContent[i].thumbnail_upload_path + jsObject.pageAllContent[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.pageAllContent[i].uploadPath + jsObject.pageAllContent[i].image_name;
                                }
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_' + image_id + '" onclick="showModal(\'' + image_id + '\')">';
                                appendHtml += '<img src="' + url + image + '" alt="img not found">';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }

                                if(jsObject.totalCommentParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalCommentParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_comment=1;
                                            particular_image_total_comment=jsObject.totalCommentParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularImage)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularImage.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_image_id==jsObject.totalLikeParticularImage[j].image_id)
                                        {
                                            flag_particular_image_total_like=1;
                                            particular_image_total_like=jsObject.totalLikeParticularImage[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_image_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_image_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">'+particular_image_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + image_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_image_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">'+particular_image_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + image_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_text_id) {

                                var text_id = 'text_' + jsObject.pageAllContent[i].page_text_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                var str=jsObject.pageAllContent[i].text_title;
                                var n = jsObject.pageAllContent[i].text_title.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml +='</div>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + text_id + '" onclick="showModal(\'' + text_id + '\')">';
                                appendHtml += '<p class="text_description">' + str + '</p>';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].text_description + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].fullname + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].title + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularText)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalCommentParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_comment=1;
                                            particular_text_total_comment=jsObject.totalCommentParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularText)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularText.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_text_id==jsObject.totalLikeParticularText[j].text_id)
                                        {
                                            flag_particular_text_total_like=1;
                                            particular_text_total_like=jsObject.totalLikeParticularText[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_text_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_text_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">'+particular_text_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + text_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_text_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">'+particular_text_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + text_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if (jsObject.pageAllContent[i].page_video_id) {
                                var video_id = 'video_' + jsObject.pageAllContent[i].page_video_id;
                                var title=jsObject.pageAllContent[i].video_title;
                                var desc=jsObject.pageAllContent[i].video_desc;
                                var full_name=jsObject.pageAllContent[i].fullname;
                                var album_name=jsObject.pageAllContent[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                var video = 'https://player.vimeo.com/video/' + jsObject.pageAllContent[i].vimeo_video_id;
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0"></iframe>';
                                appendHtml += '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' + video_id + '" onclick="showModal(\'' + video_id + '\')">';
                                appendHtml += '<iframe src="' + video + '" width="100%" height="100" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="' + title + '" class="content_title">';
                                appendHtml += '<input type="hidden" value="' + desc + '" class="content_description">';
                                appendHtml += '<input type="hidden" value="' + full_name + '" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].userid + '" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].pageid + '" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="' + jsObject.pageAllContent[i].albumid + '" class="album_id">';
                                appendHtml += '<input type="hidden" value="' + album_name + '" class="album_name">';
                                if (jsObject.pageAllContent[i].profileimage) {
                                    appendHtml += '<img class="tagger_image" src="' + url + jsObject.pageAllContent[i].profileimage + '" style="display: none" />';
                                } else {
                                    appendHtml += '<img class="tagger_image" src="' + url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                                }
                                if(jsObject.totalCommentParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalCommentParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_comment=1;
                                            particular_video_total_comment=jsObject.totalCommentParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularVideo)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularVideo.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_video_id==jsObject.totalLikeParticularVideo[j].video_id)
                                        {
                                            flag_particular_video_total_like=1;
                                            particular_video_total_like=jsObject.totalLikeParticularVideo[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_video_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_video_total_comment==1)
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">'+particular_video_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="comment_perticular_' + video_id + '">0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_video_total_like==1)
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">'+particular_video_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span id="like_particular_' + video_id + '">0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                            if(jsObject.pageAllContent[i].page_tribute_id)
                            {
                                var str=jsObject.pageAllContent[i].tribute_description;
                                var n = jsObject.pageAllContent[i].tribute_description.length;
                                if(n>140)
                                    str=str.substring(0,140)+'...';
                                appendHtml += '<li class="content_li" data-sizey="'+jsObject.pageAllContent[i].sizey+'" data-sizex="'+jsObject.pageAllContent[i].sizex+'" data-col="'+jsObject.pageAllContent[i].col+'" data-row="'+jsObject.pageAllContent[i].row+'" id="dragdrop_'+jsObject.pageAllContent[i].dragdrop_id+'">';
                                appendHtml += '<div class="gridster-box">';
                                appendHtml += ' <div class="handle-resize"></div>';
                                appendHtml += ' </div>';
                                appendHtml +='<div class="prorel">';
                                if(sessionUserId==$('#page_user_id').val())
                                {
                                    appendHtml +='<div>';
                                    appendHtml +='<i class="fa fa-arrows"></i>';
                                    appendHtml +='</div>';
                                }
                                appendHtml +='</div>';
                                appendHtml +='<a class="content_link" onclick="showTributedetails(\''+url+'Users/tributeDetails/'+jsObject.pageAllContent[i].user_id+'?tribute_id='+jsObject.pageAllContent[i].page_tribute_id+'\')">';
                                appendHtml +='<p class="text_description">'+str+'</p>';
                                appendHtml +='</a>';
                                if(jsObject.totalCommentParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalCommentParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalCommentParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_comment=1;
                                            particular_tribute_total_comment=jsObject.totalCommentParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_comment=0;
                                        }
                                    }
                                }
                                if(jsObject.totalLikeParticularTribute)
                                {
                                    for(var j=0;j<jsObject.totalLikeParticularTribute.length;j++)
                                    {
                                        if(jsObject.pageAllContent[i].page_tribute_id==jsObject.totalLikeParticularTribute[j].tribute_id)
                                        {
                                            flag_particular_tribute_total_like=1;
                                            particular_tribute_total_like=jsObject.totalLikeParticularTribute[j].total;
                                            break;
                                        }
                                        else
                                        {
                                            flag_particular_tribute_total_like=0;
                                        }
                                    }
                                }
                                appendHtml += '<small>';
                                appendHtml += '<i class="fa fa-comment" aria-hidden="true"></i>';

                                if(flag_particular_tribute_total_comment==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_comment+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += ' <i class="fa fa-heart" aria-hidden="true"></i>';
                                if(flag_particular_tribute_total_like==1)
                                {
                                    appendHtml += ' <span>'+particular_tribute_total_like+'</span>';
                                }
                                else
                                {
                                    appendHtml += ' <span>0</span>';
                                }
                                appendHtml += '</small>';
                                appendHtml += '</a>';
                                appendHtml += '</li>';
                            }
                        }
                    }
                    $('.profile_page_head').html(headHtml);
                    $('#page_content').append(appendHtml);
                    $('.actscrl').toggleClass('actscrl');
                    $('#' + page_id).find('li').toggleClass('actscrl');
                }
                if(jsObject.page_content.userid==parseInt(sessionUserId))
                {
                    $('#user_page_id').val(page_id);
                }
                $(document).ready(function ()
                {
                    if($('#session_user_id').val()==$('#page_user_id').val())
                    {
                        var gridster = null;
                        gridster= $(".gridster ul").gridster({
                            namespace: '.gridster',
                            widget_base_dimensions: ['auto', 200],
                            avoid_overlapped_widgets: true,
                            autogenerate_stylesheet: true,
                            min_cols: 1,
                            max_cols: 5,
                            widget_margins: [10, 10],
                            draggable:
                            {
                                start: function(event, ui) {
                                    dragged = 1;
                                },
                                stop: function (event, ui) {
                                    var arr = [];
                                    arr.sizex = [];
                                    arr.sizey = [];
                                    arr.col = [];
                                    arr.row = [];
                                    arr.id = [];
                                    var i = 0;
                                    $(".content_li").each(function () {
                                        var id = $(this).attr('id');
                                        if (id != undefined) {
                                            var content_id = parseInt(id.split('_')[1]);
                                            var sizex = $('#' + id).attr('data-sizex');
                                            var sizey = $('#' + id).attr('data-sizey');
                                            var col = $('#' + id).attr('data-col');
                                            var row = $('#' + id).attr('data-row');
                                            arr.id.push(content_id);
                                            arr.sizex.push(sizex);
                                            arr.sizey.push(sizex);
                                            arr.col.push(col);
                                            arr.row.push(row);

                                        }
                                    });
                                    $.ajax({
                                        type:'post',
                                        url:url+'Users/updatePageContentResizableDraggable',
                                        data: {id:arr.id, sizex:arr.sizex,sizey:arr.sizey,col:arr.col,row:arr.row}

                                    }).done(function( data )
                                    {
                                        dragged = 0;
                                    });

                                }

                             },
                            resize: {
                                enabled: true,
                                handles: ['n', 'e', 's', 'w', 'se', 'sw'],
                                max_size: [2, 2],
                                min_size: [1, 1],
                                stop: function (event, ui) {
                                    var arr = [];
                                    arr.sizex = [];
                                    arr.sizey = [];
                                    arr.col = [];
                                    arr.row = [];
                                    arr.id = [];
                                    var i = 0;
                                    $(".content_li").each(function () {
                                        var id = $(this).attr('id');
                                        if (id != undefined) {
                                            var content_id = parseInt(id.split('_')[1]);
                                            var sizex = $('#' + id).attr('data-sizex');
                                            var sizey = $('#' + id).attr('data-sizey');
                                            var col = $('#' + id).attr('data-col');
                                            var row = $('#' + id).attr('data-row');
                                            arr.id.push(content_id);
                                            arr.sizex.push(sizex);
                                            arr.sizey.push(sizey);
                                            arr.col.push(col);
                                            arr.row.push(row);

                                        }
                                    });
                                    $.ajax({
                                        type:'post',
                                        url:url+'Users/updatePageContentResizableDraggable',
                                        data: {id:arr.id, sizex:arr.sizex,sizey:arr.sizey,col:arr.col,row:arr.row}

                                    }).done(function( data )
                                    {

                                    });

                                }

                            }


                        }).data('gridster');
                        $('.gridster  ul').css({'padding': '0'});

                    }
                    else
                    {
                        var gridster = $(".gridster ul").gridster({
                            namespace: '.gridster',
                            widget_base_dimensions: ['auto', 200],
                            avoid_overlapped_widgets: true,
                            autogenerate_stylesheet: true,
                            min_cols: 1,
                            max_cols: 5,
                            widget_margins: [10, 10],
                            draggable:false,
                            resize:false
                        }).data('gridster').disable();
                        $('.gridster  ul').css({'padding': '0'});
                    }
                });
            }
        });
    });
}
function fullProfileImage(url)
{
    if(!dragged)
    {
        $('.fullProfileImage').attr('data-lightbox','example-1');
        $('.fullProfileImage').attr('href',url);
    }
   else
    {
        $('.fullProfileImage').removeAttr('data-lightbox');
        $('.fullProfileImage').removeAttr('href');
    }
}

function showModal(id)
{
    if(!dragged)
    {
        var base_url = $('#base_url').val();
        var check_type = id.split('_')[0];
        var check_id = id.split('_')[1];

        var content_title = $('#relationshipsModalShow_' + id).find('.content_title').val();
        var content_description = $('#relationshipsModalShow_' + id).find('.content_description').val();
        var tagger_name = $('#relationshipsModalShow_' + id).find('.tagger_name').val();
        var tagger_image = $('#relationshipsModalShow_' + id).find('.tagger_image').attr('src');
        var album_name = $('#relationshipsModalShow_' + id).find('.album_name').val();
        var tagger_id = $('#relationshipsModalShow_' + id).find('.tagger_id').val();
        var tagger_page_id = $('#relationshipsModalShow_' + id).find('.tagger_page_id').val();
        var album_id = $('#relationshipsModalShow_' + id).find('.album_id').val();
        var tagger_profile = base_url + 'Users/profile/' + tagger_id + '?page_id=' + tagger_page_id;
        var tagger_album = base_url + 'ImageUpload/albumdetails/' + tagger_id + '/' + album_id;
        $('.tagger_user_id_modal').val(tagger_id);
        $('#comment_form').find('#comments_type_id').val(id);
        $('.relationshipsModal').find('.comment_icon').attr('id', id);

        if (check_type == 'image') {
            var img = $('#relationshipsModalShow_' + id).find('img').attr('src');
            $('.relationshipsModal').find('.content_image_modal').attr('src', img);
            $('.relationshipsModal').find('.full_image_view ').attr('href',img);
            $('.relationshipsModal').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href', tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal').find('.content_text_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_video_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_image_modal').css('display', 'block');

            $('.relationshipsModal').modal('show');


        }
        if (check_type == 'video') {
            var video = $('#relationshipsModalShow_' + id).find('iframe').attr('src');
            $('.relationshipsModal').find('.content_video_modal').attr('src', video);

            $('.relationshipsModal').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href', tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal').find('.content_text_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_video_modal').css('display', 'block');
            $('.relationshipsModal').find('.content_image_modal').css('display', 'none');

            $('.relationshipsModal').modal('show');

        }
        if (check_type == 'text') {
            var text = $('#relationshipsModalShow_' + id).find('.content_description').val();
            $('.relationshipsModal1').find('.content_description_modal').html('');

            $('.relationshipsModal1').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal1').find('.tagger_album_link').attr('href', tagger_album);
            /*$('.relationshipsModal1').find('.content_title_modal').html(content_title);*/
            $('.relationshipsModal1').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal1').find('.album_name_modal').html(album_name);
            $('.relationshipsModal1').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal1').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal1').find('.content_text_modal').html(text);
            $('.relationshipsModal1').find('.content_image_modal').css('display', 'none');
            $('.relationshipsModal1').find('.content_video_modal').css('display', 'none');
            $('.relationshipsModal1').find('.content_text_modal').css('display', 'block');

            $('.relationshipsModal1').modal('show');
        }

        var session_user_id = $('#session_user_id').val();
        $('.editContentId').attr('id', id);
        $('.deleteContentId').attr('id', id);
        $('.pinModal').attr('id', id);
        if (tagger_id == session_user_id) {
            $('.editContentId').show();
            $('.deleteContentPage').show();
            $('.pinModal').show();
        }
        else {
            $('.editContentId').hide();
            $('.deleteContentId').hide();
            $('.pinModal').hide();
        }
        openComment1(id);
    }
}
function openComment1(id)
{
    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#session_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        var flag1=0;
                        var flag=0;
                        var count=0;
                        if(session_user_id)
                        {
                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                            {
                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                {
                                    flag=1;
                                    break;
                                }
                                else
                                {
                                    flag=0;
                                }
                            }
                            if(flag==1)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }
                            else
                            {
                                if(session_user_id!=jsObject.commentsData[i].userid)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                }

                            }
                        }
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img class="men-in-black" src="' + base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }

                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }

                var seeTaggedHtml='';
                if(jsObject.seeTagged.length>0)
                {
                    for(var p=0;p<jsObject.seeTagged.length;p++)
                    {
                        seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                    }

                }
                $('.see_tagged_name').html(seeTaggedHtml);
                $('.count_album_content').html(jsObject.count_album_content);
                $('.count_tag').html(jsObject.seeTagged.length);
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
}
function openComment(id)
{

    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#session_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img class="img-circle" style="height: 30px;width: 30px" src="' + base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        var flag1=0;
                        var flag=0;
                        var count=0;
                        if(session_user_id)
                        {
                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                            {
                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                {
                                    flag=1;
                                    break;
                                }
                                else
                                {
                                    flag=0;
                                }
                            }
                            if(flag==1)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }
                            else
                            {
                                if(session_user_id!=jsObject.commentsData[i].userid)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img src="' + base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                }

                            }
                        }
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img class="men-in-black" src="' + base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }

                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }

                var seeTaggedHtml='';
                if(jsObject.seeTagged.length>0)
                {
                    for(var p=0;p<jsObject.seeTagged.length;p++)
                    {
                        seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                    }

                }
                $('.see_tagged_name').html(seeTaggedHtml);
                $('.count_album_content').html(jsObject.count_album_content);
                $('.count_tag').html(jsObject.seeTagged.length);
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
    $("#comment_div").modal('show');
    $("#comment_div").css('width',"315");
}
function closeComment() {
    $("#comment_div").modal('hide');
    $("#comment_div").css('width',"0");
}
function comment_like(tagger_id,id)
{
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        var url = $('#base_url').val() + 'Users/commentLike';
        var base_url = $('#base_url').val();
        var comments_type_id = $('#comments_type_id').val();
        Pace.track(function () {
            $.ajax({
                url: url,
                type: 'post',
                data: {comment_id: id,tagger_id:tagger_id,comments_type_id:comments_type_id},
                success: function (data) {
                    var jsObject = JSON.parse(data);
                    if (jsObject.existLike == true) {
                        $('#' + id).attr('data-original-title', 'Like!');
                        $('#' + id).attr('title', 'Like!');
                        $('#' + id).find('.comment_like_dislike').css('color', '#5F5F5F');

                    }
                    else {
                        $('#' + id).attr('data-original-title', 'Dislike!');
                        $('#' + id).attr('title', 'Dislike!');
                        $('#' + id).find('.comment_like_dislike').css('color', 'red');
                    }
                    $('#count_comment_like_' + id).html(jsObject.totalCommentLike.total);

                }
            });
        });
    }
    else
    {
        $.notify
        ({

                message: 'You must Sign In'
            }
            , {
                type: 'danger',
                offset:
                    {
                        x: 10,
                        y: 50
                    },
                z_index: 1050,
            });
        $('.relationshipsModal').modal('hide');
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        $('#signinModal').modal('show');
    }
}
function like(id)
{
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        var url=$('#base_url').val()+'Users/likeContent';
        var tagger_id=$('.tagger_user_id_modal').val();
        var base_url=$('#base_url').val();
        Pace.track(function() {
            $.ajax({
                url: url,
                type: 'post',
                data: {id: id,tagger_id:tagger_id},
                success: function (data) {

                    var jsObject = JSON.parse(data);
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');
                    }
                    $('.count_like').html(jsObject.totalLikePerticular.total);
                    $('#like_particular_' + id).html(jsObject.totalLikePerticular.total);
                }
            });
        });
    }
    else
    {
        $.notify
        ({

                message: 'You must Sign In'
            }
            , {
                type: 'danger',
                offset:
                    {
                        x: 10,
                        y: 50
                    },
                z_index: 1050,
            });
        $('.relationshipsModal').modal('hide');
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        $('#signinModal').modal('show');
    }

}
function editContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {


        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');

        Pace.track(function()
        {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/edit_inner_image",
                data: {'album_id':album_id,'image_id':check_id },
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#single_image_album_id').val(album_id);
                    $('#editSingleImageUpload').modal('show');
                    var img_src=base_url+obj.uploadPath+obj.image_name;
                    $('#edit_imagePreview2').attr('src',img_src);
                    $('.croppr-container img').attr('src', img_src);

                    $('.croppr-container').css('display','block');
                    $('.rotation').css('display','block');
                    if(img_src!='')
                    {
                        $('#edit_Remove_uploadFile2').show();
                        $('#edit_imagePreview2').show();
                        $('#edit_choosephoto2').hide();
                    }

                    $('#edit_title').val(obj.image_title);
                    $('.edit_txtEditor').val(obj.image_desc);
                    $('#edit_inner_image_id').val(obj.image_id);
                    if(obj.image_id)
                    {
                        $(".friend_list_edit_single").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_single").append(option);
                            });
                            if(obj.tag_user_id)
                            {
                                for (var i=0;i<obj.tag_user_id.length;i++){
                                    $(".friend_list_edit_single option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                                }
                            }

                        }

                    }
                    var croppr = new Croppr('#edit_imagePreview2',
                        {
                            onCropStart: function (data) {
                                updateValue(data.x, data.y, data.width, data.height);
                            },
                            onCropMove: function (data) {
                                updateValue(data.x, data.y, data.width, data.height);
                            },
                            onCropEnd: function (data) {
                                updateValue(data.x, data.y, data.width, data.height);
                            },
                            returnMode: 'real',
                        });

                }
            });
        });

    }
    if(check_type == 'text')
    {
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_text",
                data: {'album_id': album_id, 'edit_text_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.albumTitle[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.albumTitle[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#show_edit_text_title').val(obj.text_title);
                    $('.edit_txtEditor').html(obj.text_description);
                    $('#edit_text_id').val(obj.textid);
                    $('#edittextcontent').modal('show');
                    if (obj.textid) {
                        $(".friend_list_edit_text").empty();
                        if (obj.friend_fullName != null) {
                            $.each(obj.friend_fullName, function (index, value) {
                                option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_text").append(option);
                            });
                            for (i = 0; i < obj.tag_user_id.length; i++) {
                                $(".friend_list_edit_text option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                            }
                        }
                    }
                }
            });
        });
    }
    if(check_type == 'video')
    {
        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');
        $('#album_owner_id').val($('.tagger_user_id_modal').val());
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_video",
                data: {'album_id': album_id, 'edit_video_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#editvideoUpload').modal('show');
                    $('#edit_video_id').val(obj.video_id);
                    $('.show_edit_video').html(obj.video_url);
                    if(obj.video_id)
                    {
                        $(".friend_list_edit_video").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_video").append(option);
                            });

                            for (i=0;i<obj.tag_user_id.length;i++){
                                $(".friend_list_edit_video option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                            }
                        }

                    }
                }
            });
        });
    }
}
function deleteContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {
        var image_url = base_url + "ImageUpload/removeAlbumImage/" + album_id + '/' + check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            window.location.replace(image_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'text')
    {
        var text_url=base_url+"ImageUpload/removeAlbumText/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Text Content?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(text_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'video')
    {
        var video_url=base_url+"ImageUpload/removeAlbumVideo/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(video_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
}
function pinModalEditDelete(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    $('.updateContentPage').attr('id',id);
    $('.deleteContentPage').attr('id',id);
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/imagePinEditDelete",
                data: {'image_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                }
                                else
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/textPinEditDelete",
                data: {'text_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                }
                                else
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/videoPinEditDelete",
                data: {'video_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                }
                                else
                                {
                                    appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }

}
function updateContentPage(id)
{
    var base_url=$('#base_url').val();
    var session_user_id=$('#session_user_id').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var selected = $('.selectPageId option:selected');
    var page_id=selected.val();
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageImage",
                data: {'image_id':check_id,page_id:page_id},
                success: function (data)
                {
                   window.location.href=base_url+"Users/profile/"+session_user_id+"?page_id="+page_id;
                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageText",
                data: {'text_id':check_id,page_id:page_id},
                success: function (data)
                {
                    window.location.href=base_url+"Users/profile/"+session_user_id+"?page_id="+page_id;
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageVideo",
                data: {'video_id':check_id,page_id:page_id},
                success: function (data)
                {
                    window.location.href=base_url+"Users/profile/"+session_user_id+"?page_id="+page_id;
                }
            });
        });
    }
}
function deleteContentPage(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];

    if(check_type=='profileImage')
    {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin your Profile Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageProfileImage",
                                    data: {'user_profile_id':check_id},
                                    success: function (data)
                                    {
                                        location.reload();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type=='userDescription')
    {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin profile description?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageUserDescription",
                                    data: {'user_description_id':check_id},
                                    success: function (data)
                                    {
                                        location.reload();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type=='image')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageImage",
                                    data: {'image_id':check_id},
                                    success: function (data)
                                    {
                                        location.reload();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='video')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageVideo",
                                    data: {'video_id':check_id},
                                    success: function (data)
                                    {
                                        location.reload();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='text')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Text?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageText",
                                    data: {'text_id':check_id},
                                    success: function (data)
                                    {
                                        location.reload();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }

}
$(document).ready(function ()
{
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="dropdown"]').tooltip();
});
function showAlbumdetails(url)
{
    if(!dragged) {
        window.location.href = url;
    }
}
function showTributedetails(url)
{
    if(!dragged) {
        window.location.href = url;
    }
}
function editPage(page_id,page_style_id,page_foreground_id)
{
    var base_url=$('#base_url').val();
    $.ajax({
            url: base_url+"ImageUpload/select_page_background_images",
            data:{pageBackgroundStyleId:page_style_id},
            type: 'post',
            success: function(data)
            {
                var obj = JSON.parse(data);
                if(obj.select_page_background_images.length>0)
                {
                    var option='';
                    var option1='';
                    for(var i=0;i<obj.select_page_background_images.length;i++)
                    {
                        if(obj.select_page_background_images[i].id==page_style_id)
                        {
                            option +='<div class="page-style-item hli" id="pageInnerDiv_'+obj.select_page_background_images[i].id+'" onclick="selectPageStyle(id)">';
                            option +='<img class="album_back_image" src="'+base_url+obj.select_page_background_images[i].upload_path+'/'+obj.select_page_background_images[i].image_name+'" /></div>';
                            $('#edit_page_style_id').val(page_style_id);
                        }
                        else
                        {
                            option +='<div class="page-style-item" id="pageInnerDiv_'+obj.select_page_background_images[i].id+'" onclick="selectPageStyle(id)">';
                            option +='<img class="album_back_image" src="'+base_url+obj.select_page_background_images[i].upload_path+'/'+obj.select_page_background_images[i].image_name+'" /></div>';
                        }
                    }
                    $('#edit_page_style').html(option);
                }
                if (obj.select_page_foreground_images.length>0) {
                    $('.edit_page_foreground_label').html('Page Foreground Style');
                    for (var i = 0; i < obj.select_page_foreground_images.length; i++) {
                        if (obj.select_page_foreground_images[i].id == page_foreground_id) {
                            option1 += '<div class="page-style-item toggle" id="edit_page_foreground_image_inner_div_' + obj.select_page_background_images[i].id + '" onclick="editPageForegroundImage(' + obj.select_page_foreground_images[i].id + ');"><img alt="image"  class="album_foreground_image" data-pid="' + obj.select_page_foreground_images[i].id + '" src="' + base_url + obj.select_page_foreground_images[i].upload_path+ '/' + obj.select_page_foreground_images[i].image_name + '" /></div>';
                            $('.editPageForegroundStyleId').val(page_foreground_id);
                        }
                        else {
                            option1 += '<div class="page-style-item" id="edit_page_foreground_image_inner_div_' + obj.select_page_foreground_images[i].id + '" onclick="editPageForegroundImage(' + obj.select_page_foreground_images[i].id + ')";><img alt="image"  class="album_foreground_image" data-pid="' + obj.select_page_foreground_images[i].id + '" src="' + base_url + obj.select_page_foreground_images[i].upload_path+ '/' + obj.select_page_foreground_images[i].image_name + '" /></div>';
                        }
                    }
                }
                else
                {
                    $('.edit_page_foreground_label').html('');
                }
                $('.edit_page_foreground_style').html(option1);
            }
        });
    $('#edit_page_title').val($('#page_title').val());
    $('#edit_page_id').val(page_id);
    $('#editPageModal').modal('show');

}
function selectPageStyle(id)
{
    var page_style_id=id.split('_')[1];
    $('#edit_page_style_id').val(page_style_id);
    $('.hli').toggleClass('hli');
    $('#'+id).toggleClass('hli');
    var base_url=$('#base_url').val();
    $.ajax({
        url: base_url+"ImageUpload/select_page_foreground_images",
        type: 'post',
        data:{pageBackgroundStyleId:page_style_id},
        success: function(data)
        {
            var obj2 = JSON.parse(data);
            var total_foreground_images=obj2.total_foreground_images;
            var option=new Array();
            if(total_foreground_images>0)
            {
                $('.edit_page_foreground_label').html('Page Foreground Style');
            }
            else
            {
                $('.edit_page_foreground_label').html('');
            }
            for(var i=0;i<total_foreground_images;i++)
            {
                option[i]='<div class="page-style-item" id="edit_page_foreground_image_inner_div_'+obj2.id[i]+'" onclick="editPageForegroundImage('+obj2.id[i]+')";><img alt="image" alt="image" class="page_foreground_image" data-pid="'+obj2.id[i]+'" src="'+base_url+obj2.upload_path[i]+'/'+obj2.image_name[i]+'" /></div>';
            }
            $('.edit_page_foreground_style').html(option);
        }
    });
}
function editPageForegroundImage(id) {
    $('.editPageForegroundStyleId').val(id);
    $('.toggle').toggleClass('toggle');
    $('#edit_page_foreground_image_inner_div_'+id).toggleClass('toggle');
}
function deletePage(page_id)
{
    var base_url=$('#base_url').val();
    var page_title=$('#page_title').val();
    var default_page_id=$('#default_page_id').val();
    var session_user_id=$('#session_user_id').val();
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure want to delete your page '+page_title+'?',
        buttons: {
            confirm:
                {
                    btnClass: 'btn-danger',
                    action: function ()
                    {
                        Pace.track(function() {
                            $.ajax({
                                type: 'post',
                                url: base_url + "Users/deletePage",
                                data:{page_id:page_id},
                                success: function (data)
                                {
                                    window.location.href=base_url+'Users/profile/'+session_user_id+'?page_id='+default_page_id;
                                }
                            });
                        });
                    }
                },
            cancel: {
                btnClass: 'btn-info'
            }
        }
    });

}
function addFriends(id)
{

    var base_url=$('#base_url').val();
    $.ajax({
        url: base_url+"Users/addFriends",
        type: 'post',
        data:{friends_id:id},
        success: function(data)
        {
            var jsObject = JSON.parse(data);
            if(jsObject)
            {
                $('#addFriends').html('<button onclick="requestSentFunction()" class="btn btn-success dropdown-btn">\n' +
                    '                        <div class="fa fa-clock-o"></div>\n' +
                    '                        Request Sent &nbsp;<span class="caret"></span>\n' +
                    '                    </button>\n' +
                    '                    <div id="cancelRequestDropdown" class="dropdown-content">\n' +
                    '                        <button onclick="cancelRequest('+id+')" class="btn btn-warning"><i class="fa fa-times"></i> Cancel Request</button>\n' +
                    '                    </div>');
            }

        }
    });
}
function unfriend(id)
{

    var base_url=$('#base_url').val();
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure want to unfriend?',
        buttons: {
            confirm:
                {
                    btnClass: 'btn-danger',
                    action: function ()
                    {
                        Pace.track(function() {
                            $.ajax({
                                url: base_url+"Users/unfriend",
                                type: 'post',
                                data:{friends_id:id},
                                success: function(data)
                                {
                                    if(data)
                                    {
                                        location.reload();
                                    }

                                }
                            });
                        });
                    }
                },
            cancel: {
                btnClass: 'btn-info'
            }
        }
    });
}
function cancelRequest(id)
{

    var base_url=$('#base_url').val();
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure want to cancel sent request?',
        buttons: {
            confirm:
                {
                    btnClass: 'btn-danger',
                    action: function ()
                    {
                        Pace.track(function() {
                            $.ajax({
                                url: base_url+"Users/cancelSentRequest",
                                type: 'post',
                                data:{friends_id:id},
                                success: function(data)
                                {
                                    if(data)
                                    {
                                        location.reload();
                                    }

                                }
                            });
                        });
                    }
                },
            cancel: {
                btnClass: 'btn-info'
            }
        }
    });
}
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function ConnectedFunction() {
    document.getElementById("unfriendDropdown").classList.toggle("show");
}
function requestSentFunction()
{
    document.getElementById("cancelRequestDropdown").classList.toggle("show");
}
// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropdown-btn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}
function updateValue(x, y, w, h)
{
    $('#x').val(x);
    $('#y').val(y);
    $('#width').val(w);
    $('#height').val(h);
}




