$(document).ready(function()
{
    $("#chooseProfileBackgroundImage").validate(
        {
            submitHandler: function(form) {

                var url = $('#base_url').val() + 'Users/chooseProfileBackgroundImage';
                var generalSettingsData = new FormData();

                if (($("#img_choose_avatar1").attr("src")) == "")// check image has src
                {
                    generalSettingsData.append('blankprofileimage', 'blank');

                }
                if (($("#img_choose_background1").attr("src")) == "")// check image has src
                {
                    generalSettingsData.append('blankbackgroundimage', 'blank');

                }
                generalSettingsData.append('profileimage', $('#choose_avatar_file1')[0].files[0]);
                generalSettingsData.append('backgroundimage', $('#choose_background_file1')[0].files[0]);
                Pace.track(function() {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: generalSettingsData,
                        contentType: false,
                        processData: false,
                        cache: false,
                        success: function (data) {

                            if (data == 1) {
                                $.notify
                                ({

                                        message: 'Changes was successfully saved!'
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 50
                                            },
                                        z_index: 1050
                                    });
                                location.reload();
                            }
                        }

                    });
                });
            }


        });
    // Choose Avatar click
    $(document).on('click', '#choose_avatar1', function()
    {
        $("#choose_avatar_file1").click();

    });
    //show image
    function readURLAvataor1(input)
    {

        if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif)$/))
        {
            var reader = new FileReader();

            reader.onload = function(e)
            {
                $('#img_choose_avatar1').css('display','block');
                $('.choose_avatar').removeAttr('id');
                $('.choose_avatar').attr('id','remove_avatar1');
                $('.choose_avatar').removeClass('btn-success');
                $('.choose_avatar').addClass('btn-danger');
                $('.choose_avatar').html('<i class="glyphicon glyphicon-remove-circle"></i>  Remove avatar');
                $('#img_choose_avatar1').attr('src', e.target.result);
                $('#blank_avatar1').css('display','none');
                //$('#avatar_image_error1').css('display','none');
            }

            reader.readAsDataURL(input.files[0]);
        }
        else
        {
            //$('#avatar_image_error1').css('display','block');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    }
    //show image in choose avatar onchange
    $(document).on('change', '#choose_avatar_file1',function() {
        readURLAvataor1(this);
    });
    //remove image for avatar
    $(document).on('click', '#remove_avatar1', function()
    {
        $('#img_choose_avatar1').css('display','none');
        $('#img_choose_avatar1').attr('src','');
        $('#blank_avatar1').css('display','block');
        $('.choose_avatar').html('<i class="glyphicon glyphicon-upload"></i>  Choose avatar');
        $('.choose_avatar').removeAttr('id');
        $('.choose_avatar').attr('id','choose_avatar1');
        $('.choose_avatar').removeClass('btn-danger');
        $('.choose_avatar').addClass('btn-success');
    });


    // Choose background click
    $(document).on('click', '#choose_background1', function()
    {
        $("#choose_background_file1").click();

    });
    //show image
    function readURLBackground1(input)
    {

        if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif)$/))
        {
            var reader = new FileReader();

            reader.onload = function(e)
            {
                $('#img_choose_background1').css('display','block');
                $('.choose_background').removeAttr('id');
                $('.choose_background').attr('id','remove_background1');
                $('.choose_background').removeClass('btn-success');
                $('.choose_background').addClass('btn-danger');
                $('.choose_background').html('<i class="glyphicon glyphicon-remove-circle"></i>  Remove background');
                $('#img_choose_background1').attr('src', e.target.result);
                $('#blank_background1').css('display','none');
                //$('#background_image_error1').css('display','none');
            }

            reader.readAsDataURL(input.files[0]);
        }
        else
        {
            //$('#background_image_error1').css('display','block');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    }
    //show image in choose background onchange
    $(document).on('change', '#choose_background_file1',function() {
        readURLBackground1(this);
    });
    //remove image for background
    $(document).on('click', '#remove_background1', function()
    {
        $('#img_choose_background1').css('display','none');
        $('#img_choose_background1').attr('src','');
        $('#blank_background1').css('display','block');
        $('.choose_background').html('<i class="glyphicon glyphicon-upload"></i>  Choose background');
        $('.choose_background').removeAttr('id');
        $('.choose_background').attr('id','choose_background1');
        $('.choose_background').removeClass('btn-danger');
        $('.choose_background').addClass('btn-success');
    });

});