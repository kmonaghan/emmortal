var cloudfront_base_url=$('#cloudfront_base_url').val();
$(document).ready(function()
{
    var start = parseInt($('#row_count').val());
    if ($('#row_count').val()< 25)
    {
        $('#row_count').val(start + 25);
        send_Data(start);
    }
    $(window).scroll(function ()
    {

        if ($(window).scrollTop() == $(document).height() - $(window).height())
        {

            var start = parseInt($('#row_count').val());
            if ($('#total_row').val() >= start)
            {
                $('#row_count').val(start + 25);
                send_Data(start);
            }

        }
    });
});

function send_Data(start) {
    var url = $('#base_url').val();
    var user_id=$('#Album_user_id').val();
    var album_id=$('#present_album_id').val();
    Pace.track(function()
    {
        $.ajax({
            type: "post",
            url: url + "ImageUpload/scrollLoadAlbumContent",
            data: {start: start,user_id:user_id,album_id:album_id},
            success: function (data)
            {
                $('#albumContent').append(data);
            }
        });
    });
}
$(document).ready(function(){
	//for Pining Text Section
$('.text_pining').click(function(e){
	e.preventDefault();
   var text_id=$('.popup_text_id').val();
   var albumId=$('.popup_album_id').val();
   var userId=$('#session_user_id').val();
   var base_url=$('#base_url').val();
   var text_pining_url=base_url+'ImageUpload/selectAllPageDetailsText';

	  var data="user_id="+userId+"&album_id="+albumId+"&text_id="+text_id;
    Pace.track(function() {
        $.ajax({
            url: text_pining_url,
            type: 'post',
            data: data,
            success: function (data) {
                var obj2 = JSON.parse(data);
                $('.PingalbumId').val(albumId);
                $('.PinguserId').val(userId);
                $('.ping_text_id').val(text_id);

                var option = new Array();
                if (obj2.page_name) {
                    //$.each(obj2.page_name, function(index, value) {
                    for (i = 0; i < obj2.page_name.length; i++) {
                        option[i] = $('<option></option>').attr("value", obj2.page_id[i]).text(obj2.page_name[i]);
                    }
                    $("#PiningToPageforText").html(option);
                    //});

                    for (i = 0; i < obj2.page_id.length; i++) {
                        $("#PiningToPageforText option[value='" + obj2.current_page_id + "']").attr('selected', 'selected');
                    }
                }
                if (obj2.current_page_id == 0) {
                    $('.unPin_submit_Text').css('display', 'none');
                }
                else {
                    $('.unPin_submit_Text').css('display', 'block');
                }

            }
        });
    });
        $('#myModalInnerText').modal('show');

});
//Pin_submit_text
$('.Pin_submit_Text').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforText').val();
  var textId=$('.ping_text_id').val();
  var base_url=$('#base_url').val();
  //alert(textId);
  var data="text_id="+textId+"&album_id="+albumid+"&page_id="+page_id;
  var Pin_submit_Text_url=base_url+'ImageUpload/InsertPageIdInTextDetails';
    Pace.track(function() {
        $.ajax({
            url: Pin_submit_Text_url,
            type: 'post',
            data: data,
            success: function (data) {
                $('#myModalInnerText').modal('hide');
                $('#text_' + textId).css('color', 'red');
            }
        });
    });

});

//unPin_submit_text
$('.unPin_submit_Text').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforText').val();
  var textId=$('.ping_text_id').val();
  var base_url=$('#base_url').val();
  //alert(textId);
  var data="text_id="+textId+"&album_id="+albumid+"&page_id="+page_id;
  //alert(data);
  var unPin_submit_Text_url=base_url+'ImageUpload/deletePageIdInTextDetails';
$.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to Unpin your Text?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            Pace.track(function() {
                                $.ajax({
                                    url: unPin_submit_Text_url,
                                    type: 'post',
                                    data: data,
                                    success: function (data) {
                                        $('#myModalInnerText').modal('hide');
                                        $('#text_' + textId).css('color', '#fff');
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
});



//for Pining Video Section
$('.video_pining').click(function(e){
	e.preventDefault();
   var video_id=$('.popup_video_id').val();
   var albumId=$('.popup_album_id').val();
   var userId=$('#session_user_id').val();
   var base_url=$('#base_url').val();
   var data="user_id="+userId+"&album_id="+albumId+"&video_id="+video_id;
   var video_pining_url=base_url+'ImageUpload/selectAllPageDetailsvideo';
    Pace.track(function() {
        $.ajax({
            url: video_pining_url,
            type: 'post',
            data: data,
            success: function (data) {
                var obj2 = JSON.parse(data);
                $('.PingalbumId').val(albumId);
                $('.PinguserId').val(userId);
                $('.ping_video_id').val(video_id);

                var option = new Array();
                if (obj2.page_name) {
                    //$.each(obj2.page_name, function(index, value) {
                    for (i = 0; i < obj2.page_name.length; i++) {
                        option[i] = $('<option></option>').attr("value", obj2.page_id[i]).text(obj2.page_name[i]);
                    }
                    $("#PiningToPageforvideo").html(option);
                    //});

                    for (i = 0; i < obj2.page_id.length; i++) {
                        $("#PiningToPageforvideo option[value='" + obj2.current_page_id + "']").attr('selected', 'selected');
                    }
                }
                if (obj2.current_page_id == 0) {
                    $('.unPin_submit_video').css('display', 'none');
                }
                else {
                    $('.unPin_submit_video').css('display', 'block');
                }
            }
        });
    });
   $('#myModalInnervideo').modal('show');
});
//Pin_submit_video
$('.Pin_submit_video').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforvideo').val();
  var videoId=$('.ping_video_id').val();
  var base_url=$('#base_url').val();
  var data="video_id="+videoId+"&album_id="+albumid+"&page_id="+page_id;
  //alert(data);
  var Pin_submit_video_url=base_url+'ImageUpload/InsertPageIdInVideoDetails';
    Pace.track(function() {
        $.ajax({
            url: Pin_submit_video_url,
            type: 'post',
            data: data,
            success: function (data) {
                $('#myModalInnervideo').modal('hide');
                $('#video_' + videoId).css('color', 'red');
            }
        });
    });

});

//unPin_submit_video
$('.unPin_submit_video').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforvideo').val();
  var videoId=$('.ping_video_id').val();
  var base_url=$('#base_url').val();
  var data="video_id="+videoId+"&album_id="+albumid+"&page_id="+page_id;
  //alert(data);
  var unPin_submit_video_url=base_url+'ImageUpload/deletePageIdInVideoDetails';
 $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to Unpin your Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            Pace.track(function() {
                                $.ajax({
                                    url: unPin_submit_video_url,
                                    type: 'post',
                                    data: data,
                                    success: function (data) {
                                        $('#myModalInnervideo').modal('hide');
                                        $('#video_' + videoId).css('color', '#fff');
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
});





//for Pining Image Section
$('.image_pining').click(function(e){
	e.preventDefault();
   var image_id=$('.popup_image_id').val();
   var albumId=$('.popup_album_id').val();
   var userId=$('#session_user_id').val();
   var base_url=$('#base_url').val();
   var data="user_id="+userId+"&album_id="+albumId+"&image_id="+image_id;
   var image_pining_url=base_url+'ImageUpload/selectAllPageDetailsimage';
    Pace.track(function() {
        $.ajax({
            url: image_pining_url,
            type: 'post',
            data: data,
            success: function (data) {
                var obj2 = JSON.parse(data);
                $('.PingalbumId').val(albumId);
                $('.PinguserId').val(userId);
                $('.ping_image_id').val(image_id);
                var option = new Array();
                if (obj2.page_name) {
                    //$.each(obj2.page_name, function(index, value) {
                    for (i = 0; i < obj2.page_name.length; i++) {
                        option[i] = $('<option></option>').attr("value", obj2.page_id[i]).text(obj2.page_name[i]);
                    }
                    $("#PiningToPageforImage").html(option);
                    //});

                    for (i = 0; i < obj2.page_id.length; i++) {
                        $("#PiningToPageforImage option[value='" + obj2.current_page_id + "']").attr('selected', 'selected');
                    }
                }

                if (obj2.current_page_id == 0) {
                    $('.unPin_submit_image').css('display', 'none');
                }
                else {
                    $('.unPin_submit_image').css('display', 'block');
                }

            }
        });
    });
		$('#myModalInnerImage').modal('show');
});
//Pin_submit_image
$('.Pin_submit_image').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforImage').val();
  var ImageId=$('.ping_image_id').val();
  var base_url=$('#base_url').val();
  var data="image_id="+ImageId+"&album_id="+albumid+"&page_id="+page_id;
  var Pin_submit_image_url=base_url+'ImageUpload/InsertPageIdInImageDetails';
    Pace.track(function() {
        $.ajax({
            url: Pin_submit_image_url,
            type: 'post',
            data: data,
            success: function (data) {
                $('#myModalInnerImage').modal('hide');
                $('#image_' + ImageId).css('color', 'red');
            }
        });
    });

});

//unPin_submit_image
$('.unPin_submit_image').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPageforImage').val();
  var ImageId=$('.ping_image_id').val();
  var base_url=$('#base_url').val();
  var data="image_id="+ImageId+"&album_id="+albumid+"&page_id="+page_id;
  //alert(data);
  var unPin_submit_image_url=base_url+'ImageUpload/deletePageIdInImageDetails';
   $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to Unpin your Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            Pace.track(function()
                            {
                                $.ajax({
                                    url: unPin_submit_image_url,
                                    type: 'post',
                                    data: data,
                                    success: function (data) {
                                        $('#myModalInnerImage').modal('hide');
                                        $('#image_' + ImageId).css('color', '#fff');
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
});
//for Pining Album section
$('.pinging').click(function(e){
	e.preventDefault();
   var userId=$('#Album_user_id').val();
   var albumId = $(this).attr('data-pid');
   var data="user_id="+userId+"&album_id="+albumId;
   var base_url=$('#base_url').val();
   var pinging_url=base_url+'ImageUpload/selectAllPageDetails';
    Pace.track(function() {
        $.ajax({
            url: pinging_url,
            type: 'post',
            data: data,
            success: function (data) {
                var obj2 = JSON.parse(data);
                $('.PingalbumId').val(albumId);
                $('.PinguserId').val(userId);

                var option = new Array();
                if (obj2.page_name) {
                    //$.each(obj2.page_name, function(index, value) {
                    for (i = 0; i < obj2.page_name.length; i++) {
                        option[i] = $('<option></option>').attr("value", obj2.page_id[i]).text(obj2.page_name[i]);
                    }
                    $("#PiningToPage").html(option);
                    //});

                    for (i = 0; i < obj2.page_id.length; i++) {
                        $("#PiningToPage option[value='" + obj2.current_page_id + "']").attr('selected', 'selected');
                    }
                }
                if (obj2.current_page_id == 0) {
                    $('.unpin_submit').css('display', 'none');
                }
                else {
                    $('.unpin_submit').css('display', 'block');
                }
            }
        });
    });
$('#myModal').modal('show');
});
$('.Pin_submit').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPage').val();
  var userId=$('.PinguserId').val();
  var base_url=$('#base_url').val();
  var data="user_id="+userId+"&album_id="+albumid+"&page_id="+page_id;
  var Pin_submit_url=base_url+'ImageUpload/InsertPageIdInAlbumDetails';
    Pace.track(function() {
        $.ajax({
            url: Pin_submit_url,
            type: 'post',
            data: data,
            success: function (data) {
                $('#myModal').modal('hide');
                $('.pinging').html('<i class="glyphicon glyphicon-pushpin" aria-hidden="true" style="color:red"></i> Pinned').css('color', 'red');
            }
        });
    });

});

//unpin_submit
$('.unpin_submit').click(function(){
  var albumid=$('.PingalbumId').val();
  var page_id=$('#PiningToPage').val();
  var userId=$('.PinguserId').val();
  var base_url=$('#base_url').val();
  var data="user_id="+userId+"&album_id="+albumid+"&page_id="+page_id;
  var un_pin_submit_url=base_url+'ImageUpload/deletePageIdInAlbumDetails';

  $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to Unpin your Album?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            Pace.track(function() {
                                $.ajax({
                                    url: un_pin_submit_url,
                                    type: 'post',
                                    data: data,
                                    success: function (data) {
                                        $('#myModal').modal('hide');
                                        $('.pinging').html('<i class="glyphicon glyphicon-pushpin"></i> Pin').css('color', '#428bca');
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
});
  var session_user_id=$('#session_user_id').val();
    var present_album_owner_id=$('#Album_user_id').val();
  if((session_user_id==present_album_owner_id)&&(session_user_id!=''))
  {
      var alltagid = [];
      var base_url=$('#base_url').val();
      var currentlyScrolling = false;

      var SCROLL_AREA_HEIGHT = 40;
      $(".album-list").sortable({
          scroll: true,
          scrollSensitivity: 80,
          scrollSpeed: 3,
          sort: function(event, ui) {

              if (currentlyScrolling) {
                  return;
              }

              var windowHeight = $(window).height();
              var mouseYPosition = event.clientY;

              if (mouseYPosition < SCROLL_AREA_HEIGHT) {
                  currentlyScrolling = true;

                  $('html, body').animate({
                          scrollTop: "-=" + windowHeight / 2 + "px"
                      },
                      400,
                      function() {
                          currentlyScrolling = false;
                      });
              } else if (mouseYPosition > (windowHeight - SCROLL_AREA_HEIGHT)) {

                  currentlyScrolling = true;

                  $('html, body').animate({
                          scrollTop: "+=" + windowHeight / 2 + "px"
                      },
                      400,
                      function() {
                          currentlyScrolling = false;
                      });
              }
          },
          stop: function(event, ui)
          {
              var alltagid = [];
              $(".image_li").each(function () {
                  var id = $(this).attr('id');
                  if (id != undefined) {
                      var content_id = id.split('_')[2];
                      alltagid.push(content_id);
                  }

              });
              Pace.track(function ()
              {
                  $.ajax({
                      type: 'post',
                      url: base_url + "ImageUpload/updateImageOrder",
                      data: {ids: alltagid}

                  }).
                  done(function (msg) {

                  });
              });
          }
      });
 }
  else
  {
      $(".album-list").sortable({ tolerance: 'pointer' });
      $(".album-list").sortable( "option", "disabled",true);
  }
});


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    //countlike
    $(function(){
        $(".albumCountLike").on("click", function(){
            var this_elem = $(this);
            var user_id=$('#session_user_id').val();
			var base_url=$('#base_url').val();
            var album_id =$('#present_album_id').val();
			var album_user_id=$('#Album_user_id').val();
			if(user_id!='')
			{
			    Pace.track(function () {
                    $.ajax({
                        type: 'post',
                        url: base_url + 'ImageUpload/addAlbumlike',
                        data: {'user_id': user_id, 'album_id': album_id, 'album_user_id': album_user_id},
                        success: function (res) {
                            var obj = JSON.parse(res);
                            if (obj.isUserAlbumLike == 'yes') {
                                this_elem.find('.fa-heart').css('color', 'red');
                                this_elem.find('.total_album_like').text(obj.totalLike);
                                this_elem.attr('data-original-title', 'Dislike!');
                            }
                            else {
                                this_elem.find('.fa-heart').css('color', '#fff');
                                this_elem.find('.total_album_like').text(obj.totalLike);
                                this_elem.attr('data-original-title', 'Like!');
                            }

                        }
                    });
                });
			}
			else
			{
			    $.notify
				({
                        message: 'You must Sign In'
				}
				, {
					type: 'danger',
                    offset:
                    {
                        x: 10,
                        y: 50
                    },
				    z_index: 1050,
				});
			    $('#signinModal').modal('show');
			}
        });
    });

    //Image Like
    $(function(){
        $(".user_like_dislike_image").on("click", function(e){
            var this_elem = $(this);
            var userid=$('#session_user_id').val();
            var imageid=$('.popup_image_id').val();
			var base_url=$('#base_url').val();
			var album_user_id=$('#Album_user_id').val();
           if(userid!='')
           {
               Pace.track(function () {
                   $.ajax({
                       type: 'post',
                       url: base_url + 'ImageUpload/addImagelike',
                       data: {'user_id': userid, 'image_id': imageid, 'album_user_id': album_user_id},
                       success: function (res) {
                           var obj = JSON.parse(res);
                           if (obj.isUserImageLike == 'yes') {
                               this_elem.find('.total_like_dislike_image').css('color', 'red');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Dislike!');
                               $('#like_particular_image_' + imageid).html(obj.totalLike);
                           }
                           else {
                               this_elem.find('.total_like_dislike_image').css('color', '#5F5F5F');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Like!');
                               $('#like_particular_image_' + imageid).html(obj.totalLike);
                           }
                       }
                   });
               });

        }
        else
		{
            $.notify
            ({
                    message: 'You must Sign In'
                }
                , {
                    type: 'danger',
                    offset:
                        {
                            x: 10,
                            y: 50
                        },
                    z_index: 1050,
                });
			$('#basicModalPicture').modal('hide');
			$('#signinModal').modal('show');
		}
        });
    });
    //Text Like
     $(function(){
        $(".user_like_dislike_text").on("click", function(e){
            var this_elem = $(this);
            var userid=$('#session_user_id').val();
			var base_url=$('#base_url').val();
            //alert(userid);
            var textid=$('.popup_text_id').val();
			var album_user_id=$('#Album_user_id').val();
           if(userid!='') {
               Pace.track(function () {
                   $.ajax({
                       type: 'post',
                       url: base_url + 'ImageUpload/addTextlike',
                       data: {'popup_user_id': userid, 'popup_text_id': textid, 'album_user_id': album_user_id},
                       success: function (res) {
                           var obj = JSON.parse(res);
                           if (obj.isUserTextLike == 'yes') {
                               this_elem.find('.total_like_dislike_text').css('color', 'red');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Dislike!');
                               $('#like_particular_text_' + textid).html(obj.totalLike);
                           }
                           else {
                               this_elem.find('.total_like_dislike_text').css('color', '#5F5F5F');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Like!');
                               $('#like_particular_text_' + textid).html(obj.totalLike);
                           }
                       }
                   });
               });
        }
        else
		{
            $.notify
            ({
                    message: 'You must Sign In'
                }
                , {
                    type: 'danger',
                    offset:
                        {
                            x: 10,
                            y: 50
                        },
                    z_index: 1050,
                });
			$('#showtextcontent').modal('hide');
			$('#signinModal').modal('show');
		}
        });
    });

    //video Like
     $(function(){
        $(".user_like_dislike_video").on("click", function(e){
            var this_elem = $(this);
            var userid=$('#session_user_id').val();
            var videoid=$('.popup_video_id').val();
			var base_url=$('#base_url').val();
			var album_user_id=$('#Album_user_id').val();
           if(userid!='') {
               Pace.track(function () {
                   $.ajax({
                       type: 'post',
                       url: base_url + 'ImageUpload/addvideolike',
                       data: {'user_id': userid, 'videoid': videoid, 'album_user_id': album_user_id},
                       success: function (res) {
                           var obj = JSON.parse(res);
                           if (obj.isUserVideoLike == 'yes') {
                               this_elem.find('.total_like_dislike_video').css('color', 'red');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Dislike!');
                               $('#like_particular_video_' + videoid).html(obj.totalLike);
                           }
                           else {
                               this_elem.find('.total_like_dislike_video').css('color', '#5F5F5F');
                               this_elem.find('.count_like').text(obj.totalLike);
                               this_elem.attr('data-original-title', 'Like!');
                               $('#like_particular_video_' + videoid).html(obj.totalLike);
                           }
                       }
                   });
               });
           }
           else {
               $.notify
               ({
                       message: 'You must Sign In'
                   }
                   , {
                       type: 'danger',
                       offset:
                           {
                               x: 10,
                               y: 50
                           },
                       z_index: 1050,
                   });
			$('#basicModalvideo').modal('hide');
			$('#signinModal').modal('show');
		}
    });
  });
});
//This is use for show large the inner image in a pop up
$(document).ready(function(){
	//for image pop up
    $(document).on('click', '.album_inner_image', function(e){
        e.preventDefault();
        var this_elem = $(this);
        var album_id = $('#present_album_id').val();
        //alert(album_id);
        var user_id = $('#Album_user_id').val();
        var image_id = $(this).attr("data-pid");
        var base_url = $('#base_url').val();
        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url + 'ImageUpload/image_details',
                data: {'image_id': image_id, 'album_id': album_id, 'user_id': user_id},
                success: function (res) {
                    var obj = JSON.parse(res);
                    var total_records = $('#total_records').val();
                    var album_url = base_url + 'ImageUpload/albumdetails/' + user_id + '/' + album_id;
                    var user_profile = base_url + 'Users/profile/' + user_id + '?page_id=' + obj.page_id;
                    $('.full_image_view').attr('href', cloudfront_base_url+obj.image_src);
                    $('.popup_image_id').val(obj.image_id);
                    $('.popup_album_id').val(obj.albumid);

                    var title=obj.image_title;
                    var desc=obj.image_desc;
                    var full_name=obj.user_name;
                    var album_name=obj.album_title;
                    if(title.length>40)
                    {
                        title=title.substr(0, 40)+'...';
                    }
                    if(desc.length>40)
                    {
                        desc=desc.substr(0, 40)+'...';
                    }
                    if(full_name.length>13)
                    {
                        full_name=full_name.substr(0, 13)+'...';
                    }
                    if(album_name.length>13)
                    {
                        album_name=album_name.substr(0, 13)+'...';
                    }

                    $('#popup_image').attr('src',cloudfront_base_url+obj.image_src)
                    $('#album_title').html(title);
                    $('#album_desciption').html(desc);
                    $('#photo_upload_user_name').html(full_name);
                    $('.tagger_profile_link').attr("href", user_profile);
                    $('#photo_upload_album_name').html(album_name);
                    $('#photo_upload_album_name').attr("href", album_url);
                    $('.tagger_album_link').attr("href", album_url);
                    $('.img-circle').attr("src", cloudfront_base_url + obj.profileimage);
                    $('.count_tag').html(obj.total_frd_tag);
                    $('.count_album_content').html(total_records);
                    $('.count_comment').html(obj.total_comment);
                    if (obj.isUserImageLike == 'yes') {
                        $('.total_like_dislike_image').css('color', 'red');
                        $('.count_like').text(obj.total_like);
                        $('.user_like_dislike_image').attr('data-original-title', 'Dislike!');
                    }
                    else {
                        $('.total_like_dislike_image').css('color', '#5F5F5F');
                        $('.count_like').text(obj.total_like);
                        $('.user_like_dislike_image').attr('data-original-title', 'Like!');

                    }

                    $('.image_pining').attr("id", 'image_' + obj.image_id);
                    if (obj.page_ids != 0) {
                        $('#image_' + obj.image_id).css('color', 'red');
                    }
                    else {
                        $('#image_' + obj.image_id).css('color', '#fff');
                    }

                    var option = new Array();
                    if (obj.records) {
                        for (i = 0; i < obj.records.length; i++) {
                            option[i] = $("<li></li>", {}).html($("<a></a>", {href: obj.records[i].url}).text(obj.records[i].name));

                        }

                    }
                    if(obj.get_next_image_id)
                    {
                        $('.next_content').attr('data-pid',obj.get_next_image_id);
                        $('.next_file_show_modal').css('display','block');
                    }
                    else
                    {
                        $('.next_file_show_modal').css('display','none');
                    }
                    if(obj.get_prev_image_id)
                    {
                        $('.previous_content').attr('data-pid',obj.get_prev_image_id);
                        $('.prev_file_show_modal').css('display','block');
                    }
                    else
                    {
                        $('.prev_file_show_modal').css('display','none');
                    }
                    $(".tag_frd_name").html(option);
                    $('#basicModalPicture').modal('show');
                }
            });
        });
    });
    //for text pop up
    $(document).on('click', '.album_inner_text', function(e)
    {
         e.preventDefault();
      var this_elem_image = $(this);
      var edit_text_id = $(this).attr("data-pid");
      var album_id=$('#present_album_id').val();
	  var userId=$('#Album_user_id').val();
	  var base_url=$('#base_url').val();

        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url + 'ImageUpload/select_text_details',
                data: {'album_id': album_id, 'user_id': userId, 'edit_text_id': edit_text_id},
                success: function (res) {
                    var obj = JSON.parse(res);
                    var album_url = base_url + 'ImageUpload/albumdetails/' + obj.userid + '/' + obj.albumid;
                    var user_profile = base_url + 'Users/profile/' + obj.userid + '?page_id=' + obj.pageid;
                    var total_records = $('#total_records').val();
                    $('.popup_text_id').val(obj.textid);
                    $('.popup_album_id').val(obj.albumid);
                    $('#popup_text_id').val(obj.textid);
                    $('#popup_text_album_id').val(obj.albumid);
                    $('#show_text_desciption').html(obj.description);
                    $('#txtEditornew').html(obj.text_description);

                    $('#text_upload_user_name').html(obj.fullname);
                    $('.total_text_like').html(total_records);
                    $('.text_user_link').attr("href", user_profile);
                    $('.text_album_link').attr("href", album_url);
                    $('.text_author_image').attr('src', base_url + obj.profileimage);
                    $('.count_tag_text').html(obj.total_frd_tag);
                    $('#text_upload_album_name').html(obj.album_title);
                    $('.count_comment').html(obj.total_comment);
                    if (obj.isUserTextLike == 'yes') {
                        $('.total_like_dislike_text').css('color', 'red');
                        $('.count_like').text(obj.total_like);
                        $('.user_like_dislike_text').attr('data-original-title', 'Dislike!');
                    }
                    else {
                        $('.total_like_dislike_text').css('color', '#5F5F5F');
                        $('.count_like').text(obj.total_like);
                        $('.user_like_dislike_text').attr('data-original-title', 'Like!');

                    }

                    $('.text_pining').attr("id", 'text_' + obj.textid);
                    if (obj.page_ids != 0) {
                        $('#text_' + obj.textid).css('color', 'red');
                    }
                    else {
                        $('#text_' + obj.textid).css('color', '#fff');
                    }

                    var option = new Array();
                    if (obj.records) {
                        for (i = 0; i < obj.records.length; i++) {
                            option[i] = $("<li></li>", {}).html($("<a></a>", {href: obj.records[i].url}).text(obj.records[i].name));

                        }
                        $(".text_tagger_name").html(option);
                    }
                    if(obj.get_next_text_id)
                    {
                        $('.next_content_text').attr('data-pid',obj.get_next_text_id);
                        $('.next_file_show_modal').css('display','block');
                    }
                    else
                    {
                        $('.next_file_show_modal').css('display','none');
                    }
                    if(obj.get_prev_text_id)
                    {
                        $('.previous_content_text').attr('data-pid',obj.get_prev_text_id);
                        $('.prev_file_show_modal').css('display','block');
                    }
                    else
                    {
                        $('.prev_file_show_modal').css('display','none');
                    }
                    $('#showtextcontent').modal('show');
                }
            });
        });
    });
 //This for show video in popup
 $(document).on('click', '.show_inner_video', function(e){
     e.preventDefault();
     var this_elem_video = $(this);
     var video_id = $(this).attr("data-pid");
     var album_id=$('#present_album_id').val();
	 var user_id=$('#Album_user_id').val();
	 var base_url=$('#base_url').val();
     Pace.track(function () {
         $.ajax({
             type: 'post',
             url: base_url + 'ImageUpload/select_video_details',
             data: {'album_id': album_id, 'video_id': video_id, 'user_id': user_id},
             success: function (res) {
                 var obj = JSON.parse(res);
                 var album_url = base_url + 'ImageUpload/albumdetails/' + obj.user_id + '/' + obj.albumid;
                 var user_profile = base_url + 'Users/profile/' + obj.user_id + '?page_id=' + obj.pageid;
                 var total_records = $('#total_records').val();


                 var title=obj.video_title;
                 var desc=obj.video_desc;
                 var full_name=obj.user_fullname;
                 var album_name=obj.album_title;
                 if(title.length>40)
                 {
                     title=title.substr(0, 40)+'...';
                 }
                 if(desc.length>40)
                 {
                     desc=desc.substr(0, 40)+'...';
                 }
                 if(full_name.length>13)
                 {
                     full_name=full_name.substr(0, 13)+'...';
                 }
                 if(album_name.length>13)
                 {
                     album_name=album_name.substr(0, 13)+'...';
                 }


                 $('#video_title').html(obj.title);
                 $('#video_desciption').html(obj.desc);
                 $('.video_upload_user_img').attr('src', base_url + obj.profileimage);
                 $('#video_upload_user_name').html(full_name);
                 $('.tagger_profile_link').attr('href', user_profile);
                 $('.tagger_album_link').attr('href', album_url);
                 $('#video_upload_album_name').html(album_name);
                 $('.total_tag_frd').html(obj.total_frd_tag);
                 $('.show_video_popup').html(obj.video_url);
                 $('.popup_album_id').val(obj.albumid);
                 $('.popup_video_id').val(obj.video_id);
                 $('.count_comment').html(obj.total_comment);
                 if (obj.isUserVideoLike == 'yes') {
                     $('.total_like_dislike_video').css('color', 'red');
                     $('.count_like').text(obj.total_like);
                     $('.user_like_dislike_video').attr('data-original-title', 'Dislike!');
                 }
                 else {
                     $('.total_like_dislike_video').css('color', '#5F5F5F');
                     $('.count_like').text(obj.total_like);
                     $('.user_like_dislike_video').attr('data-original-title', 'Like!');

                 }
                 $('.total_video_like').html(total_records);

                 $('.video_pining').attr("id", 'video_' + video_id);
                 if (obj.page_ids != 0) {
                     $('.video_pining').css('color', 'red');
                 }
                 else {
                     $('.video_pining').css('color', '#fff');
                 }
                 var option = new Array();
                 if (obj.records) {
                     for (i = 0; i < obj.records.length; i++) {
                         option[i] = $("<li></li>", {}).html($("<a></a>", {href: obj.records[i].url}).text(obj.records[i].name));

                     }
                     $(".video_tag_frd_name").html(option);

                 }
                 if(obj.get_next_video_id)
                 {
                     $('.next_content_video').attr('data-pid',obj.get_next_video_id);
                     $('.next_file_show_modal').css('display','block');
                 }
                 else
                 {
                     $('.next_file_show_modal').css('display','none');
                 }
                 if(obj.get_prev_video_id)
                 {
                     $('.previous_content_video').attr('data-pid',obj.get_prev_video_id);
                     $('.prev_file_show_modal').css('display','block');
                 }
                 else
                 {
                     $('.prev_file_show_modal').css('display','none');
                 }
                 $('#basicModalvideo').modal('show');
             }
         });
     });

 });


//This is use for edit the album in a popup
    $('#EditAlbum').click(function(e){
      e.preventDefault();
       var album_id = $(this).attr("data-pid");
       //alert(album_id);
       var base_url=$('#base_url').val();
       var user_id=$('#Album_user_id').val();
        var background_image_id=$('.album_background_image_id').val();
        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url + 'ImageUpload/edit_album',
                data: {'album_id': album_id, 'user_id': user_id},
                success: function (res) {
                    var obj2 = JSON.parse(res);
                    var img_src =obj2.uploadPath + obj2.image_name;
                    if(img_src!="")
                    {
                        $('#edit_album_imagePreview1').attr('src',cloudfront_base_url+img_src);
                        $('#edit_album_imagePreview1').show();
                        $('#edit_Remove_uploadFile1').show();
                        $('#edit_choosephoto1').hide();
                    }
                    if(img_src==false)
                    {
                        $('#edit_album_imagePreview1').attr('src',"");
                        $('#edit_album_imagePreview1').hide();
                        $('#edit_Remove_uploadFile1').hide();
                        $('#edit_choosephoto1').show();
                    }
                    $('.edit_album_id').val(obj2.album_id);
                    $('#image_id').val(obj2.image_id);
                    $('#edit_album_title').val(obj2.title);
                    $('.edit_album_txtEditor1').val(obj2.description);

                    if (obj2.total_background_images != null && obj2.back_ground_image_id != null && obj2.back_ground_image_upload_path != null) {
                        var total_background_images = obj2.total_background_images;
                        var option = new Array();
                        for (i = 0; i < total_background_images; i++) {
                            if (obj2.back_ground_image_id[i] == background_image_id) {
                                option[i] = '<div class="page-style-item hli" id="edit_album_back_image_inner_div_' + obj2.back_ground_image_id[i] + '" onclick="edit_readme(' + obj2.back_ground_image_id[i] + ')";><img alt="image" class="album_back_image" data-pid="' + obj2.back_ground_image_id[i] + '" src="' + cloudfront_base_url + obj2.back_ground_image_upload_path[i] + '/' + obj2.back_ground_image_image_name[i] + '" /></div>';
                                $('.edit_background_image_id').val(background_image_id);
                            }
                            else {
                                option[i] = '<div class="page-style-item" id="edit_album_back_image_inner_div_' + obj2.back_ground_image_id[i] + '" onclick="edit_readme(' + obj2.back_ground_image_id[i] + ')";><img alt="image" class="album_back_image" data-pid="' + obj2.back_ground_image_id[i] + '" src="' + cloudfront_base_url + obj2.back_ground_image_upload_path[i] + '/' + obj2.back_ground_image_image_name[i] + '" /></div>';

                            }
                        }
                    }
                    if (obj2.foreground_image_id != null && obj2.foreground_image_name != null && obj2.foreground_upload_path != null) {
                        var total_foreground_images = obj2.total_foreground_images;
                        var option1 = new Array();
                        for (i = 0; i < total_foreground_images; i++) {
                            if (obj2.foreground_image_id[i] == obj2.album_foreground_id) {
                                option1[i] = '<div class="page-style-item toggle" id="edit_album_foreground_image_inner_div_' + obj2.foreground_image_id[i] + '" onclick="editAlbumForegroundImage(' + obj2.foreground_image_id[i] + ')";><img alt="image" class="album_foreground_image" data-pid="' + obj2.foreground_image_id[i] + '" src="' + cloudfront_base_url + obj2.foreground_upload_path[i] + '/' + obj2.foreground_image_name[i] + '" /></div>';
                                $('.editAlbumForegroundStyleId').val(obj2.album_foreground_id);
                            }
                            else {
                                option1[i] = '<div class="page-style-item" id="edit_album_foreground_image_inner_div_' + obj2.foreground_image_id[i] + '" onclick="editAlbumForegroundImage(' + obj2.foreground_image_id[i] + ')";><img alt="image" class="album_foreground_image" data-pid="' + obj2.foreground_image_id[i] + '" src="' + cloudfront_base_url + obj2.foreground_upload_path[i] + '/' + obj2.foreground_image_name[i] + '" /></div>';

                            }
                        }
                    }
                    if(total_foreground_images>0)
                    {
                        $('.album_foreground_header').html('Album Foreground Style');
                    }
                    else
                    {
                        $('.album_foreground_header').html('');
                    }
                    $('.album_view_permission option[value='+obj2.viewstatus+']').attr('selected','selected');
                    $('.edit_album_back_image_outr').html(option);
                    $('.edit_album_foreground_image_outr').html(option1);
                    $('#editalbumUpload').modal('show');
                }
            });
        });
    });
//This is use for edit the inner image in a popup
    $('.EditImage').click(function(e){
      e.preventDefault();

      $('#basicModalPicture').modal('hide');
      var edit_image_id = $('.popup_image_id').val();
      var album_id=$('.popup_album_id').val();
	  var base_url=$('#base_url').val();
        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url + 'ImageUpload/edit_inner_image',
                data: {'album_id': album_id, 'image_id': edit_image_id},
                success: function (res) {
                    var obj = JSON.parse(res);
                    $('#editSingleImageUpload').modal('show');
                    var img_src = cloudfront_base_url + obj.uploadPath + obj.image_name;
                    $('#edit_imagePreview2').attr('src', img_src);
                    $('.editImageIconForEdit').css('display','block');

                    if (img_src != '') {
                        $('#edit_Remove_uploadFile2').show();
                        $('#edit_imagePreview2').show();
                        $('#edit_choosephoto2').hide();
                    }
                    $('#edit_title').val(obj.image_title);
                    $('.edit_txtEditor').val(obj.image_desc);
                    $('#edit_inner_image_id').val(obj.image_id);
                    if (obj.image_id) {
                        $(".friend_list_edit_single").empty();
                        if (obj.friend_fullName != null) {
                            $.each(obj.friend_fullName, function (index, value) {
                                option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_single").append(option);
                            });
                            if (obj.tag_user_id) {
                                for (i = 0; i < obj.tag_user_id.length; i++) {
                                    $(".friend_list_edit_single option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                                }
                            }
                        }

                    }
                }
            });
        });
    });

//This is use for edit the text in a popup
    $('.EditText').click(function(e){
      e.preventDefault();
      var this_elem_image = $(this);
      $('#showtextcontent').modal('hide');
      var edit_text_id =$('.popup_text_id').val();
      //alert(edit_text_id);
      var base_url=$('#base_url').val();
      var album_id=$('#popup_text_album_id').val();
        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url + 'ImageUpload/select_edit_album_text',
                data: {'album_id':album_id,'edit_text_id':edit_text_id },
                success: function (res) {
                  var obj = JSON.parse(res);
                  $('#show_edit_text_title').val(obj.text_title);
                  $('.edit_txtEditor').html(obj.text_description);
                  $('#edit_text_id').val(obj.textid);
                  $('#edittextcontent').modal('show');
                  if(obj.textid)
                  {
                    $(".friend_list_edit_text").empty();
                  if(obj.friend_fullName!=null)
                    {
                        $.each(obj.friend_fullName, function(index, value) {
                            option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                            $(".friend_list_edit_text").append(option);
                        });
                        if(obj.tag_user_id) {
                            for (i = 0; i < obj.tag_user_id.length; i++) {
                                $(".friend_list_edit_text option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                            }
                        }
                    }

                  }
                }
            });
        });
    });
//This is use for edit the video in a popup
    $('.EditVideo').click(function(e){
      e.preventDefault();
	  var this_elem_image = $(this);
      var edit_video_id = $('.popup_video_id').val();
      var base_url=$('#base_url').val();
      var album_id=$('.popup_album_id').val();

        Pace.track(function () {
            $.ajax({
                type: 'post',
                url: base_url+'ImageUpload/select_edit_album_video',
                data: {'album_id':album_id,'edit_video_id':edit_video_id},
                success: function (res) {
                    var obj = JSON.parse(res);
                    $('#editvideoUpload').modal('show');
                    $('#basicModalvideo').modal('hide');
                    $('#edit_video_id').val(obj.video_id);
                    $('.show_edit_video').html(obj.video_url);
                    $('#album_owner_id').val(obj.album_owner_id);

                    if (obj.video_url) {
                        $('#edit_Remove_uploadFile2').show();

                    }
                    if (obj.video_id) {
                        $(".friend_list_edit_video").empty();
                        if (obj.friend_fullName != null) {
                            $.each(obj.friend_fullName, function (index, value) {
                                option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_video").append(option);
                            });
                            if (obj.tag_user_id) {
                                for (i = 0; i < obj.tag_user_id.length; i++) {
                                    $(".friend_list_edit_video option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                                }
                            }
                        }

                    }
                }
            });
        });

    });
    //delete Image option
    $('.deleteImage').click(function(e){
        e.preventDefault();
         var delete_image_id =$('.popup_image_id').val();
         var album_id=$('.popup_album_id').val();
		 var base_url=$('#base_url').val();
         var image_url=base_url+"ImageUpload/removeAlbumImage/"+album_id+'/'+delete_image_id;
         $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(image_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    });

    //delete Text option
    $('.deleteText').click(function(e){
        e.preventDefault();
        var delete_text_id =$('.popup_text_id').val();
         var base_url=$('#base_url').val();
         var album_id=$('.popup_album_id').val();
         var text_url=base_url+"ImageUpload/removeAlbumText/"+album_id+'/'+delete_text_id;
         $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Text Content?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(text_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    });

   //delete video option
    $('.deletevideo').click(function(e){
        e.preventDefault();
        var delete_video_id =$('.popup_video_id').val();
        var base_url=$('#base_url').val();
         var album_id=$('.popup_album_id').val();
         var video_url=base_url+"ImageUpload/removeAlbumVideo/"+album_id+'/'+delete_video_id;
         $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(video_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    });


 $(document).ready(function(){
  //this is for remove Album
 $('#RemoveAlbum').click(function(e){
     e.preventDefault();
     var this_elem_album = $(this);
      var base_url=$('#base_url').val();
      var album_id=$('#present_album_id').val();
	  //alert(album_id);
      var album_url=base_url+"ImageUpload/removeAlbum/"+album_id;
	  //alert(album_url);
      $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Album?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(album_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    });

});
//open the comment popup for the image
$('.user_comment_image').on('click',function(e){
    e.preventDefault();
    var base_url=$('#base_url').val();
    var userId=$('#Album_user_id').val();
	var user_id=$('#Album_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var image_id = $('.popup_image_id').val();
	var data="user_id="+userId+"&image_id="+image_id;
	var user_comment_image_url=base_url+'ImageUpload/showInnerImageComments';
    Pace.track(function() {
        $.ajax({
            url: user_comment_image_url,
            type: 'post',
            data: data,
            cache: false,
            processData: false,
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);

                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                            if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                flag = 1;
                                break;
                            }
                            else {
                                flag = 0;
                            }
                        }
                        if (flag == 1) {
                            appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                        }
                        else {
                            if (session_user_id != jsObject.commentsData[i].userid) {
                                appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }

                        }
                        var flag1 = 0;
                        var count = 0;
                        for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                            if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                flag1 = 1;
                                break;
                            }

                        }
                        for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                            if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                count++;
                            }
                        }
                        if (flag1 == 1) {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }

                }
                var show_comment_type_id = "image_" + image_id;
                $('#comments_type_id').val(show_comment_type_id);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
                $('.comment_perticular_' + show_comment_type_id).html(jsObject.commentsData.length);
            }
        });
    });
		$("#comment_div").modal('show');
		$("#comment_div").css('width',"315px");
});

//open the comment popup for the text
$('.user_comment_text').on('click',function(e){
    e.preventDefault();
   var base_url=$('#base_url').val();
    var userId=$('#Album_user_id').val();
	var user_id=$('#Album_user_id').val();
    var session_user_id=$('#session_user_id').val();;
    var text_id = $('.popup_text_id').val();
    var data="user_id="+userId+"&text_id="+text_id;
	var user_comment_text_url=base_url+'ImageUpload/showInnerTextComments';
    Pace.track(function() {
        $.ajax({
            url: user_comment_text_url,
            type: 'post',
            data: data,
            cache: false,
            processData: false,
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);

                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                            if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                flag = 1;
                                break;
                            }
                            else {
                                flag = 0;
                            }
                        }
                        if (flag == 1) {
                            appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                        }
                        else {
                            if (session_user_id != jsObject.commentsData[i].userid) {
                                appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }

                        }
                        var flag1 = 0;
                        var count = 0;
                        for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                            if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                flag1 = 1;
                                break;
                            }

                        }
                        for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                            if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                count++;
                            }
                        }
                        if (flag1 == 1) {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }

                }
                var show_comment_type_id = "text_" + text_id;
                $('#comments_type_id').val(show_comment_type_id);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
                $('.comment_perticular_' + show_comment_type_id).html(jsObject.commentsData.length);
            }
        });
    });
    $("#comment_div").modal('show');
    $("#comment_div").css('width',"315px");
});

//open the comment popup for the image
$('.user_comment_video').on('click',function(e){
    e.preventDefault();
    var base_url=$('#base_url').val();
    var userId=$('#Album_user_id').val();
	var user_id=$('#Album_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var video_id = $('.popup_video_id').val();
    var data="user_id="+userId+"&video_id="+video_id;
	var user_comment_video_url=base_url+'ImageUpload/showInnerVideoComments';
    Pace.track(function() {
        $.ajax({
            url: user_comment_video_url,
            type: 'post',
            data: data,
            cache: false,
            processData: false,
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);

                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                            if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                flag = 1;
                                break;
                            }
                            else {
                                flag = 0;
                            }
                        }
                        if (flag == 1) {
                            appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                        }
                        else {
                            if (session_user_id != jsObject.commentsData[i].userid) {
                                appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }

                        }
                        var flag1 = 0;
                        var count = 0;
                        for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                            if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                flag1 = 1;
                                break;
                            }

                        }
                        for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                            if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                count++;
                            }
                        }
                        if (flag1 == 1) {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }

                }
                var show_comment_type_id = "video_" + video_id;
                $('#comments_type_id').val(show_comment_type_id);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
                $('.comment_perticular_' + show_comment_type_id).html(jsObject.commentsData.length);

            }
        });
    });
    $("#comment_div").modal('show');
    $("#comment_div").css('width',"315px");
});

//open the comment pop-up and show the records for album
$( ".tribute_comment" ).on( "click", function(e) {
        e.preventDefault();
       var base_url=$('#base_url').val();
        //var userId=$('#session_user_id').val();
        var userId=$('#Album_user_id').val();
		var user_id=$('#Album_user_id').val();
        var session_user_id=$('#session_user_id').val();
        var albumId = $(this).attr('data-pid');
		var data="user_id="+userId+"&album_id="+albumId;
		var tribute_comment_url=base_url+'ImageUpload/showComments';
    Pace.track(function() {
        $.ajax({
            url: tribute_comment_url,
            type: 'post',
            data: data,
            cache: false,
            processData: false,
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);

                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                            if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                flag = 1;
                                break;
                            }
                            else {
                                flag = 0;
                            }
                        }
                        if (flag == 1) {
                            appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                        }
                        else {
                            if (session_user_id != jsObject.commentsData[i].userid) {
                                appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }

                        }
                        var flag1 = 0;
                        var count = 0;
                        for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                            if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                flag1 = 1;
                                break;
                            }

                        }
                        for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                            if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                count++;
                            }
                        }
                        if (flag1 == 1) {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else {
                            appendHtml += '<a href="javascript:void(0)" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }

                }
                $('#comments_type_id').val(albumId);
                $('.comment_body').html(appendHtml);
                $('.album_tribute_comment').html(jsObject.commentsData.length);

            }
        });
    });
            $("#comment_div").modal('show');
            $("#comment_div").css('width',"315px");
});
//comment submit
$('document').ready(function ()
    {
        /* Send Button Enable or disable for support */
        $('#album_comment_form textarea').on('keyup blur', function () { // fires on every keyup & blur
            var comment_text = $('#comment_text').val().trim();
            if (comment_text != '')  {                   // checks form for validity
                $('#btn_comment_relationships').prop('disabled', false);        // enables button
                $('#btn_comment_relationships').css('cursor', 'pointer');

            } else {
                $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
                $('#btn_comment_relationships').css('cursor', 'not-allowed');

            }
        });
        $('.image_file').change(function ()
        {
            if (this.files && this.files[0] &&  this.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
            {

            }
            else
            {
                $(this).val('');
                $.alert({
                    title: 'Alert!',
                    content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
                });
            }
        });
        $("#album_comment_form").validate(
        {

                submitHandler: function(form)
                {
                            var base_url=$('#base_url').val();
                            //alert(aaaaa);
                            var url = $('#base_url').val()+'ImageUpload/commentAlbum';
                            var comments_type_id = $('#comments_type_id').val();
                            //alert(comments_type_id);
                            //var user_id=$('#profile_user_id').val();
                            var user_id=$('#Album_user_id').val();
                            var session_user_id=$('#session_user_id').val();
                            var comment_text = $('#comment_text').val().trim();
                            if ((comment_text != '') && (session_user_id!=''))
                            {
                               var comments_form_data = new FormData();
                                comments_form_data.append('comments_type_id',comments_type_id);
                                comments_form_data.append('comment_text',comment_text);
								comments_form_data.append('album_user_id', user_id);
                                if($('.image_file')[0].files[0])
                                {
                                    comments_form_data.append('comment_file', $('.image_file')[0].files[0]);
                                }
                                Pace.track(function() {
                                    $.ajax({
                                        url: url,
                                        type: 'post',
                                        data: comments_form_data,
                                        contentType: false,
                                        processData: false,
                                        cache: false,
                                        success: function (data) {
                                            $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
                                            $('#btn_comment_relationships').css('cursor', 'not-allowed');
                                            var appendHtml = '';
                                            var jsObject = JSON.parse(data);
                                            if (jsObject.commentsData.length > 0) {
                                                for (var i = 0; i < jsObject.commentsData.length; i++) {
                                                    var flag=0;
                                                    appendHtml += '<div class="use-padding  use-border">';
                                                    appendHtml += '<div class="left-box">';
                                                    appendHtml += '<p><span class="square-icon">';
                                                    if (jsObject.commentsData[i].profileimage) {

                                                        appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                                                    }
                                                    else {
                                                        appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                                                    }
                                                    appendHtml += '</span>';
                                                    appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                                                    appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                                                    appendHtml += '</div>';
                                                    appendHtml += '<div class="right-box">';
                                                    for(var j=0;j<jsObject.relationshipUsers.length;j++)
                                                    {
                                                        if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                                        {
                                                            flag=1;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            flag=0;
                                                        }
                                                    }
                                                    if(flag==1)
                                                    {
                                                        appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                                    }
                                                    else
                                                    {
                                                        if(session_user_id!=jsObject.commentsData[i].userid)
                                                        {
                                                            appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                                        }

                                                    }
                                                    var flag1=0;
                                                    var count=0;
                                                    for(var j=0;j<jsObject.allLikecomment.length;j++)
                                                    {
                                                        if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                                                        {
                                                            flag1=1;
                                                            break;
                                                        }

                                                    }
                                                    for(var k=0;k<jsObject.allLikecomment.length;k++)
                                                    {
                                                        if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                                                        {
                                                            count++;
                                                        }
                                                    }
                                                    if(flag1==1)
                                                    {
                                                        appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                                    }
                                                    else
                                                    {
                                                        appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)" ><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                                    }
                                                    appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                                                    appendHtml += '</div>';
                                                    if (jsObject.commentsData[i].image_comments) {
                                                        appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                                                    }
                                                    appendHtml += '</div>';

                                                }
                                            }
                                            $('.comment_body').html(appendHtml);
                                            $('#comment_text').val('');
                                            $('.image_file').val('');
										     var comment_selection=$('#comments_type_id').val();
											 if(comment_selection.match('image'))
											 {
												$('.count_comment').html(jsObject.commentsData.length);
											 }
											 else if(comment_selection.match('text'))
											 {
												$('.count_comment').html(jsObject.commentsData.length);
											 }
											 else if(comment_selection.match('video'))
											 {
												$('.count_comment').html(jsObject.commentsData.length);
											 }
											 else
										     {
											   $('.album_tribute_comment').html(jsObject.commentsData.length);
											 }
                                            $('#comment_perticular_' + comments_type_id).html(jsObject.commentsData.length);
                                        }
                                    });
                                });
                            }
                            else
                            {
                                $.notify
                                ({

                                message: 'You must Sign In'
                                }
                                , {
                                    type: 'danger',
                                offset:
                                {
                                x: 10,
                                y: 50
                                },
                                z_index: 1050,
                                });

								$('#comment_div').modal('hide');
                                $('#showtextcontent').modal('hide');
                                $('#basicModalPicture').modal('hide');
                                $('#basicModalvideo').modal('hide');
                                $('#signinModal').modal('show');
                            }
            }
        });
    });
});

//closeComment
function closeComment() {
         $("#comment_div").modal('hide');
        $("#comment_div").css('width',"0");
    }
// Choose image file
    $(document).on('click', '.image_browse_icon', function()
    {
        $(".image_file").click();

    });
//comment like
function comment_like(id)
{
        var url=$('#base_url').val()+'ImageUpload/commentLike';
        var base_url=$('#base_url').val();
		var user_id=$('#session_user_id').val();
		if(user_id!='')
		{
				Pace.track(function() {
					$.ajax({
						url: url,
						type: 'post',
						data: {comment_id: id},
						success: function (data)
						{
							var jsObject = JSON.parse(data);

							if (jsObject.existLike == true) {
								$('#'+id).attr('data-original-title', 'Like!');
								$('#'+id).attr('title', 'Like!');
								$('#'+id).find('.comment_like_dislike').css('color', '#5F5F5F');

							}
							else {
								$('#'+id).attr('data-original-title', 'Dislike!');
								$('#'+id).attr('title', 'Dislike!');
								$('#'+id).find('.comment_like_dislike').css('color', 'red');
							}
							$('#count_comment_like_'+id).html(jsObject.totalCommentLike.total);

						}
					});
				});
		}
		else
        {
			$.notify
                                ({

                                message: 'You must Sign In'
                                }
                                , {
                                    type: 'danger',
                                offset:
                                {
                                x: 10,
                                y: 50
                                },
                                z_index: 1050,
                                });

								$('#comment_div').modal('hide');
								$('#showtextcontent').modal('hide');
                                $('#basicModalPicture').modal('hide');
                                $('#basicModalvideo').modal('hide');
                                $('#signinModal').modal('show');

		}
}
function showModal(id)
{

        var base_url = $('#base_url').val();
        var check_type = id.split('_')[0];
        var check_id = id.split('_')[1];

        var content_title = $('#relationshipsModalShow_' + id).find('.content_title').val();
        var content_description = $('#relationshipsModalShow_' + id).find('.content_description').val();
        var tagger_name = $('#relationshipsModalShow_' + id).find('.tagger_name').val();
        var tagger_image = $('#relationshipsModalShow_' + id).find('.tagger_image').attr('src');
        var album_name = $('#relationshipsModalShow_' + id).find('.album_name').val();
        var tagger_id = $('#relationshipsModalShow_' + id).find('.tagger_id').val();
        var tagger_page_id = $('#relationshipsModalShow_' + id).find('.tagger_page_id').val();
        var album_id = $('#relationshipsModalShow_' + id).find('.album_id').val();
        var tagger_profile = base_url + 'Users/profile/' + tagger_id + '?page_id=' + tagger_page_id;
        var tagger_album = base_url + 'ImageUpload/albumdetails/' + tagger_id + '/' + album_id;
        $('.tagger_user_id_modal').val(tagger_id);
        $('#comment_form').find('#comments_type_id').val(id);
        $('.relationshipsModal').find('.comment_icon').attr('id', id);

        if (check_type == 'image') {
            var img = $('#relationshipsModalShow_' + id).find('img').attr('src');
            $('.relationshipsModal').find('.content_image_modal').attr('src', img);
            $('.relationshipsModal').find('.full_image_view ').attr('href',img);
            $('.relationshipsModal').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href', tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal').find('.content_text_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_video_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_image_modal').css('display', 'block');

            $('.relationshipsModal').modal('show');


        }
        if (check_type == 'video') {
            var video = $('#relationshipsModalShow_' + id).find('iframe').attr('src');
            $('.relationshipsModal').find('.content_video_modal').attr('src', video);

            $('.relationshipsModal').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href', tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal').find('.content_text_modal').css('display', 'none');
            $('.relationshipsModal').find('.content_video_modal').css('display', 'block');
            $('.relationshipsModal').find('.content_image_modal').css('display', 'none');

            $('.relationshipsModal').modal('show');

        }
        if (check_type == 'text') {
            var text = $('#relationshipsModalShow_' + id).find('.content_description').val();
            $('.relationshipsModal1').find('.content_description_modal').html('');

            $('.relationshipsModal1').find('.tagger_profile_link').attr('href', tagger_profile);
            $('.relationshipsModal1').find('.tagger_album_link').attr('href', tagger_album);
            /*$('.relationshipsModal1').find('.content_title_modal').html(content_title);*/
            $('.relationshipsModal1').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal1').find('.album_name_modal').html(album_name);
            $('.relationshipsModal1').find('.tagger_image_modal').attr('src', tagger_image);
            $('.relationshipsModal1').find('.modal_comment_id').attr('id', id);

            $('.relationshipsModal1').find('.content_text_modal').html(text);
            $('.relationshipsModal1').find('.content_image_modal').css('display', 'none');
            $('.relationshipsModal1').find('.content_video_modal').css('display', 'none');
            $('.relationshipsModal1').find('.content_text_modal').css('display', 'block');

            $('.relationshipsModal1').modal('show');
        }

        var session_user_id = $('#session_user_id').val();
        $('.editContentId').attr('id', id);
        $('.deleteContentId').attr('id', id);
        $('.pinModal').attr('id', id);
        if (tagger_id == session_user_id) {
            $('.editContentId').show();
            $('.deleteContentPage').show();
            $('.pinModal').show();
        }
        else {
            $('.editContentId').hide();
            $('.deleteContentId').hide();
            $('.pinModal').hide();
        }
        openComment1(id);
}
function openComment1(id)
{
    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#session_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width: 30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width: 30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        var flag1=0;
                        var flag=0;
                        var count=0;
                        if(session_user_id)
                        {
                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                            {
                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                {
                                    flag=1;
                                    break;
                                }
                                else
                                {
                                    flag=0;
                                }
                            }
                            if(flag==1)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }
                            else
                            {
                                if(session_user_id!=jsObject.commentsData[i].userid)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"  src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                }

                            }
                        }
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }

                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }

                var seeTaggedHtml='';
                if(jsObject.seeTagged.length>0)
                {
                    for(var p=0;p<jsObject.seeTagged.length;p++)
                    {
                        seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                    }

                }
                $('.see_tagged_name').html(seeTaggedHtml);
                $('.count_album_content').html(jsObject.count_album_content);
                $('.count_tag').html(jsObject.seeTagged.length);
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
}
function openComment(id)
{

    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#profile_user_id').val();
    var session_user_id=$('#session_user_id').val();
    $('#comment_form').find('#comments_type_id').val(id);
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width: 30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width: 30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        for(var j=0;j<jsObject.relationshipUsers.length;j++)
                        {
                            if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                            {
                                flag=1;
                                break;
                            }
                            else
                            {
                                flag=0;
                            }
                        }
                        if(flag==1)
                        {
                            appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                        }
                        else
                        {
                            if(session_user_id!=jsObject.commentsData[i].userid)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                            }

                        }
                        var flag1=0;
                        var count=0;
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like('+jsObject.commentsData[i].userid +',id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }

                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
    $("#comment_div").modal('show');
    $("#comment_div").css('width',"315");
}
function like(id)
{
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        var url=$('#base_url').val()+'Users/likeContent';
        var tagger_id=$('.tagger_user_id_modal').val();
        var base_url=$('#base_url').val();
        Pace.track(function() {
            $.ajax({
                url: url,
                type: 'post',
                data: {id: id,tagger_id:tagger_id},
                success: function (data) {

                    var jsObject = JSON.parse(data);
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');
                    }
                    $('.count_like').html(jsObject.totalLikePerticular.total);
                    $('#like_particular_' + id).html(jsObject.totalLikePerticular.total);
                }
            });
        });
    }
    else
    {
        $.notify
        ({

                message: 'You must Sign In'
            }
            , {
                type: 'danger',
                offset:
                    {
                        x: 10,
                        y: 50
                    },
                z_index: 1050,
            });
        $('.relationshipsModal').modal('hide');
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        $('#signinModal').modal('show');
    }

}
function pinModalEditDelete(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    $('.updateContentPage').attr('id',id);
    $('.deleteContentPage').attr('id',id);
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/imagePinEditDelete",
                data: {'image_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/textPinEditDelete",
                data: {'text_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/videoPinEditDelete",
                data: {'video_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }

}
function updateContentPage(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var selected = $('.selectPageId option:selected');
    var page_id=selected.val();
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageImage",
                data: {'image_id':check_id,page_id:page_id},
                success: function (data)
                {

                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#pinModal').modal('hide');
                    $('.deleteContentPage').show();

                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageText",
                data: {'text_id':check_id,page_id:page_id},
                success: function (data)
                {
                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#pinModal').modal('hide');
                    $('.deleteContentPage').show();
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageVideo",
                data: {'video_id':check_id,page_id:page_id},
                success: function (data)
                {
                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#pinModal').modal('hide');
                    $('.deleteContentPage').show();
                }
            });
        });
    }
}
function deleteContentPage(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    if(check_type=='image')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageImage",
                                    data: {'image_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#pinModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='video')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageVideo",
                                    data: {'video_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#pinModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='text')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Text?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageText",
                                    data: {'text_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#pinModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }

}
function deleteContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {
        var image_url = base_url + "ImageUpload/removeAlbumImage/" + album_id + '/' + check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            window.location.replace(image_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'text')
    {
        var text_url=base_url+"ImageUpload/removeAlbumText/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Text Content?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(text_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'video')
    {
        var video_url=base_url+"ImageUpload/removeAlbumVideo/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(video_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
}
function editContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {


        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');

        Pace.track(function()
        {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/edit_inner_image",
                data: {'album_id':album_id,'image_id':check_id },
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#single_image_album_id').val(album_id);
                    $('#editSingleImageUpload').modal('show');
                    var img_src=cloudfront_base_url+obj.uploadPath+obj.image_name;
                    $('#edit_imagePreview2').attr('src',img_src);
                    $('.editImageIconForEdit').css('display','block');
                    if(img_src!='')
                    {
                        $('#edit_Remove_uploadFile2').show();
                        $('#edit_imagePreview2').show();
                        $('#edit_choosephoto2').hide();
                    }

                    $('#edit_title').val(obj.image_title);
                    $('.edit_txtEditor').val(obj.image_desc);
                    $('#edit_inner_image_id').val(obj.image_id);
                    if(obj.image_id)
                    {
                        $(".friend_list_edit_single").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_single").append(option);
                            });
                            if(obj.tag_user_id)
                            {
                                for (var i=0;i<obj.tag_user_id.length;i++){
                                    $(".friend_list_edit_single option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                                }
                            }

                        }

                    }

                }
            });
        });

    }
    if(check_type == 'text')
    {
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_text",
                data: {'album_id': album_id, 'edit_text_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.albumTitle[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.albumTitle[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#show_edit_text_title').val(obj.text_title);
                    $('.edit_txtEditor').html(obj.text_description);
                    $('#edit_text_id').val(obj.textid);
                    $('#edittextcontent').modal('show');
                    if (obj.textid) {
                        $(".friend_list_edit_text").empty();
                        if (obj.friend_fullName != null) {
                            $.each(obj.friend_fullName, function (index, value) {
                                option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_text").append(option);
                            });
                            for (i = 0; i < obj.tag_user_id.length; i++) {
                                $(".friend_list_edit_text option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                            }
                        }
                    }
                }
            });
        });
    }
    if(check_type == 'video')
    {
        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');
        $('#album_owner_id').val($('.tagger_user_id_modal').val());
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_video",
                data: {'album_id': album_id, 'edit_video_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#editvideoUpload').modal('show');
                    $('#edit_video_id').val(obj.video_id);
                    $('.show_edit_video').html(obj.video_url);
                    if(obj.video_id)
                    {
                        $(".friend_list_edit_video").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_video").append(option);
                            });

                            for (i=0;i<obj.tag_user_id.length;i++){
                                $(".friend_list_edit_video option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                            }
                        }

                    }
                }
            });
        });
    }
}



