
$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="dropdown"]').tooltip();

	/*add Tribute Modal */
    $('#addTribute').click(function ()
    {
        var url=$('#base_url').val();
        $.ajax({
            url:url+'ImageUpload/friendsList',
            type: 'post',
            success: function(data)
            {
                $('#tribute_friends_id').html(data);
            }
        });
        $('#tributeModal').modal('show');
        $('#basicModalEmmUpdated2').modal('hide');

    });
    $('#backTribute').click(function ()
    {
        $('#tributeModal').modal('hide');
        $('#basicModalEmmUpdated2').modal('show');
    });


    $("#formTribute").validate(
        {
            rules: {
                tributeDescription: "required",
                "tribute_friends_id[]": "required",
            },
            messages: {
                tributeDescription: "Tribute Description field can not be blank!",
                "tribute_friends_id[]": "Please specify at least one friend!"
            },
            submitHandler: function(form)
            {
                var url				 = $('#base_url').val();
                var tributeDescription	 = $('#tributeDescription').val();
                var tribute_friends_id=[];
                var i=0;
                $('#tribute_friends_id :selected').each(function(){

                    tribute_friends_id[i]=$(this).val();
                    i++;

                });
                var tributeData 		=
                    {
                        tributeDescription: tributeDescription,
                        tribute_friends_id:tribute_friends_id
                    };
                Pace.track(function()
                {
                    $.ajax({
                        url: url+'Users/addTribute',
                        type: 'post',
                        dataType: 'json',
                        data: tributeData,
                        success: function (data)
                        {

                            if (data==1) {

                                $('#tributeModal').modal('hide');
                                $.notify
                                ({

                                        message: 'Tribute was successfully created'
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 50
                                            }
                                    });
                            }

                        }
                    });
                });
            }
        });
});



