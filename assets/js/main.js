$(function() {
    $('.lazy').Lazy({
        // your configuration goes here
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        visibleOnly: true,
        onError: function(element) {
            console.log('error loading ' + element.data('src'));
        }
    });
});
$(document).ready(function()
{

    window.location.href.split('#')[0];
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        $('.hover-effect').removeClass('homedropdown');
    }
    else
    {
        $('.hover-effect').addClass('homedropdown');
    }

	$.validator.addMethod("email", function(value, element) 
	{ 
		return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value); 
	}, "Please enter a valid email address");
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Only alphabetical characters");
    $("#firstname").keypress(function (event) {
        if (event.charCode!=0) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });
    $("#lastname").keypress(function (event) {
        if (event.charCode!=0) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });
	/* Jquery Signup Validation */

   $("#signup").validate(
   {
		rules:
            {
                firstname: {
                    required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    minlength:2,
                    lettersonly: true
                },
			lastname: {
                    required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    minlength:2,
                    lettersonly: true

            },
			 email: 
        	{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
				email:true
          		
      		 },
       		 password:
			 {
           		 required: true,
           		 minlength: 6
          	},
        	 cpassword: 
         	{
              equalTo: "#password"
          	},
          dob:{
                required: {
                     depends:function(){
                      $(this).val($.trim($(this).val()));
                      return true;
                    }
                }
             },
		 },
		messages:
		{
		  firstname: {
              required: "First name field can not be blank!"
          },
		  lastname: {
              required: "Last name field can not be blank!"
          },
		  email: 
          {
         	 required: "Email field can not be blank!",
          	
          },
		  password:
		  {
			   required: "Password field can not be blank!",
          	   minlength: "Should be at least 6 characters length"
		  },
		  cpassword: "Enter confirm password same as password",
		  dob: {
              required: "DOB field can not be blank!"
          }
         
		},
	  submitHandler: function(form) 
	  {
		  var url			= $('#base_url').val()+'Users/signup';
		  var firstname		= $('#firstname').val();
		  var lastname		= $('#lastname').val();
		  var email			= $('#email').val();
		  var password		= $('#password').val();
		  var dob			= $('#dob').val(); 
		  var signupData = 
		  {
           	firstname	: firstname,
          	lastname 	: lastname,
		 	email		: email,
		  	password	: password,
		  	dob			:dob
       	 };
          Pace.track(function() {
              $.ajax({
                  url: url,
                  type: 'post',
                  dataType: 'json',
                  data: signupData,
                  beforeSend:function () {
                      $('#btn_signup').attr("disabled", "disabled");
                      $('.icon_signup_spinner').css('display','block');
                  },
                  complete:function () {
                      $('#btn_signup').removeAttr("disabled", "disabled");
                      $('.icon_signup_spinner').css('display','none');
                  },
                  success: function (data) {
                      //console.log(data);
                      if (data == 0) {

                          $('#signupModal').modal('hide');
                          $.notify
                          ({

                                  message: 'Verification mail has been sent. Please check your mail to activate your account.'
                              }
                              , {
                                  type: 'success',
                                  offset:
                                      {
                                          x: 10,
                                          y: 50
                                      }
                              });
                      }
                      if (data == 1) {
                          $('#email_alert').css('display', 'block');
                          $('#email').focus();
                          $('#email_alert_close').click(function () {
                              $('#email_alert').css('display', 'none');
                              $('#email').focus();

                          });

                      }
                  }
              });
          });
  	   }
  	});
	/* Signup Button Enable or disable */
	/*$('#signup input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#signup').valid()) {                   // checks form for validity
            $('#btn_signup').prop('disabled', false);        // enables button
			$('#btn_signup').addClass('btn-success');
        } else {
            $('#btn_signup').prop('disabled', 'disabled');   // disables button
			$('#btn_signup').removeClass('btn-success');
        }
    });*/
	
	/* Jquery Signin Validation */
	$("#signin").validate(
   {
		rules: 
		{
			 emailid: 
        	{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email:true
          		
      		 },
       		 pass:
			 {
           		 required: true,

          	}
     
		 },
		messages:
		{
		  emailid: 
          {
         	 required: "Email field can not be blank!",
          	
          },
		  pass:
		  {
			   required: "Password field can not be blank!",

		  }
		
		},
	  submitHandler: function(form) 
	  {
		  var url			= $('#base_url').val()+'Users/signin';
		
		  var email			= $('#emailid').val();
		  var password		= $('#pass').val();
		
		  var signinData = 
		  {
		 	email		: email,
		  	password	: password
       	 };
          Pace.track(function() {
              $.ajax({

                  url: url,
                  type: 'post',
                  dataType: 'json',
                  data: signinData,
                  beforeSend:function () {
                      $('#btn_signin').attr("disabled", "disabled");
                      $('.icon_signin_spinner').css('display','block');
                  },
                  complete:function () {
                      $('#btn_signin').removeAttr("disabled", "disabled");
                      $('.icon_signin_spinner').css('display','none');
                  },
                  success: function (data) {

                      if (data.activation == 0) {
                          $('#invalid_user_alert').html('You have to confirm your email address before continuing');
                          $('#user_alert').css('display', 'block');
                          $('#user_alert_close').click(function () {
                              $('#user_alert').css('display', 'none');
                          });
                      }
                      if (data.activation == 1 && data.userstype == 'user') {
                          window.location.href = $('#base_url').val() + 'Users/dashboard';
                      }
                      if (data.activation == 1 && data.userstype == 'admin') {
                          window.location.href = $('#base_url').val() + 'Admin/dashboard';
                      }
                      if (data == 1) {
                          $('#invalid_user_alert').html('Invalid Email or Password');
                          $('#user_alert').css('display', 'block');
                          $('#user_alert_close').click(function () {
                              $('#user_alert').css('display', 'none');
                          });

                      }
                  }
              });
          });
  	   }
  	});
	/* Signin Button Enable or disable */
	/*$('#signin input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#signin').valid()) {                   // checks form for validity
            $('#btn_signin').prop('disabled', false);        // enables button
			$('#btn_signin').addClass('btn-success');
			
			
        } else {
            $('#btn_signin').prop('disabled', 'disabled');   // disables button
			$('#btn_signin').removeClass('btn-success');
        }
    });*/
	
	/* Signup Modal Show for Signup link in signin modal */
	$('#sign-up-link').click(function()
	{
		$('#signinModal').modal('hide');
		$('#signupModal').modal('show');
		
	});
	
	/* Signin Modal Show for Signin link in Signup modal */
	$('#sign-in-link').click(function()
	{
		$('#signupModal').modal('hide');
		$('#signinModal').modal('show');
		
	});
	
	/* Terms & conditions  Modal Show for Terms & conditions link in Signup modal */
	$('#terms-conditions-link').click(function()
	{
		$('#terms_ConditionsModal').modal('show');
		
	});
    /* Forgot Password  Modal Show for Forgot Password link in Signin modal */
    $('#forgotPasswordLink').click(function()
    {
        $('#signinModal').modal('hide');
        $('#forgotPasswordModal').modal('show');

    });

    /* Jquery forgotPassword Validation */
    $("#forgotPassword").validate(
        {
            rules:
                {
                    forgot_email:
                        {
                            required: {
                                depends:function(){
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            },
                            email:true

                        },
                },
            messages:
                {
                    forgot_email:
                        {
                            required: "Email field can not be blank!",

                        },
                },
            submitHandler: function(form)
            {

                var url				 = $('#base_url').val() + 'Users/forgotPassword';
                var forgot_email	 = $('#forgot_email').val();
                var forgotData 		=
					{
                    				forgot_email: forgot_email
               		 };
                Pace.track(function() {
                    $.ajax({

                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: forgotData,
                        beforeSend:function () {
                            $('#btn_forgotPassword').attr("disabled", "disabled");
                            $('.icon_recovery_spinner').css('display','block');
                        },
                        complete:function () {
                            $('#btn_forgotPassword').removeAttr("disabled", "disabled");
                            $('.icon_recovery_spinner').css('display','none');
                        },
                        success: function (data) {

                            // console.log(data);
                            if (data == 0) {
                                $('#invalid_forgot_email_alert').html('You are not registered with us. Please sign up.');
                                $('#forgot_email_alert').css('display', 'block');
                                $('#forgot_email').focus();
                                $('#forgot_email_alert_close').click(function () {
                                    $('#forgot_email_alert').css('display', 'none');
                                    $('#forgot_email').focus();
                                });

                            }
                            if (data == 1) {
                                $('#forgotPasswordModal').modal('hide');
                                $.notify
                                ({

                                        message: 'Please check your mail: ' + forgot_email
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 50
                                            }
                                    });
                            }
                        }
                    });
                });
            }
        });
    /* forgotPassword Button Enable or disable */
  /*  $('#forgotPassword input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#forgotPassword').valid()) {                   // checks form for validity
            $('#btn_forgotPassword').prop('disabled', false);        // enables button
            $('#btn_forgotPassword').addClass('btn-success');


        } else {
            $('#btn_forgotPassword').prop('disabled', 'disabled');   // disables button
            $('#btn_forgotPassword').removeClass('btn-success');
        }
    });*/

    /* Back button in forgot password for back into sign modal */
    $('#btn_back_forgotPassword').click(function()
    {
        $('#forgotPasswordModal').modal('hide');
        $('#signinModal').modal('show');

    });
    //Reset Password
    $("#resetPassword").validate(
        {
            rules:
                {
                    npass:
						{
                            required: true,
                            minlength: 6
                        },
                    cpass:
                        {
                            equalTo: "#npass"
                        }

                },
            messages:
				{
                    npass:
                        {
                            required: "New Password field can not be blank!",
                            minlength: "Should be at least 6 characters length"
                        },
                    cpass: "Enter confirm password same as New password"
                },
            submitHandler: function(form)
            {

                var url				= $('#base_url').val()+'Users/resetPassword';
                var npassword		= $('#npass').val();
                var email			= $('#email_reset').val();
                var resetPasswordData =
                    {
                        email		: email,
                        npassword	: npassword
                    };
                Pace.track(function() {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: resetPasswordData,
                        beforeSend:function () {
                            $('#btn_resetPassword').attr("disabled", "disabled");
                            $('.icon_reset_spinner').css('display','block');
                        },
                        complete:function () {
                            $('#btn_resetPassword').removeAttr("disabled", "disabled");
                            $('.icon_reset_spinner').css('display','none');
                        },
                        success: function (data) {
                            if (data == 1) {
                                $('#resetPasswordModal').modal('hide');
                                $.notify
                                ({

                                        message: 'Password Reset Successfully please Sign in'
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 50
                                            }
                                    });

                            }
                            if (data == 0) {
                                $('#resetPasswordModal').modal('hide');
                                $.notify
                                ({

                                        message: 'Something error in password reset!!!'
                                    }
                                    , {
                                        type: 'danger',
                                        offset:
                                            {
                                                x: 10,
                                                y: 50
                                            }
                                    });
                            }
                        }

                    });
                });
            }
        });
    /* forgotPassword Button Enable or disable */
    /*$('#resetPassword input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#resetPassword').valid()) {                   // checks form for validity
            $('#btn_resetPassword').prop('disabled', false);        // enables button
            $('#btn_resetPassword').addClass('btn-success');


        } else {
            $('#btn_resetPassword').prop('disabled', 'disabled');   // disables button
            $('#btn_resetPassword').removeClass('btn-success');
        }
    });*/
});
 //clear modal cache, so that new content can be loaded
$(function ()
{
    $('.modal').on('hidden.bs.modal', function () {
        $(this).find("input").val('').end();
        $(this).find("#dob").val(moment().format('YYYY-MM-DD'));
        $(this).parent().find('label').remove();
        $('#email_alert').css('display', 'none');
        $('#user_alert').css('display', 'none');
        $('#signup_success_alert').css('display', 'none');
        $('#forgot_email_alert').css('display', 'none');
        $("body").css('overflow-y', 'scroll !important');
        $('html').css('overflow-y', 'scroll !important');
        var _this = this, vimeoSrc = $(_this).find("iframe").attr("src");
        if ($(_this).find("iframe").length > 0) {// checking if there is iframe only then it will go to next level
            $(_this).find("iframe").attr("src", "");                // removing src on runtime to stop video
            $(_this).find("iframe").attr("src", vimeoSrc);        // again passing vimeo src value to iframe
        }


    });
    $('.modal').on('show.bs.modal', function () {
        $("body").css('overflow-y', 'hidden !important');
        $('html').css('overflow-y', 'hidden !important');
    });
});



/*Date Picker*/
 $(function () {

    //Date picker
    $('.datepicker').datepicker
    ({
		format: 'yyyy-mm-dd',
      	autoclose: true,
    })
  });



