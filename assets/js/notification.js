$(document).ready(function()
{
    getNotification();
  	setInterval(function () {
  	    getNotification();
    }, 1000);
});
var url=$('#base_url').val();
var cloudfront_base_url=$('#cloudfront_base_url').val();
    //notification      
function getNotification()
{
	   $.ajax({
				url:url+'Users/getNotification',
		   		data:{userid:$('#session_user_id').val()},
				type: 'post',
				success: function(data)
				{
					var appendHtml='';
					var jsObject=JSON.parse(data);
					var notify_class='';
					if(jsObject.allNotification.length>0)
					{

						for(var i=0;i<jsObject.allNotification.length;i++) {
						    //console.log(jsObject.allNotification[i]);
                            if (jsObject.allNotification[i].notify_type == 'friendsRequest') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<div class="notfify_btn"><button class="btn btn-primary btn-xs" onclick="accept(' + jsObject.allNotification[i].userid + ')"><div class="fa fa-user-plus"></div> Accept</button>';
                                appendHtml += ' <button class="btn btn-warning btn-xs" onclick="decline(' + jsObject.allNotification[i].userid + ')"><div class="fa fa-user-plus"></div> Decline</button></div></div></li>';
                            }

                            if (jsObject.allNotification[i].notify_type == 'friendsRequestAccept') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'friendsAccept') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><strong>Accepted</strong></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'friendsRequestDecline') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'friendsDecline') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><strong>Decline</strong></div></li>';
                            }

                            if (jsObject.allNotification[i].notify_type == 'tagTribute') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openTagTribute(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'Users/tributeDetails/'+jsObject.allNotification[i].FID+'?tribute_id='+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-tag"></span></a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'deleteTributeRequest') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<div class="notify_btn"><button class="btn btn-success btn-xs" onclick="confirmDeleteTributeRequest(' + jsObject.allNotification[i].notify_type_id + ',' + jsObject.allNotification[i].userid + ')"><div class="fa fa-user-plus"></div> Confirm</button>';
                                appendHtml += ' <button class="btn btn-warning btn-xs" onclick="declineDeleteTributeRequest(' + jsObject.allNotification[i].notify_type_id + ',' + jsObject.allNotification[i].userid + ')"><div class="fa fa-user-plus"></div> Decline</button></div></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'deleteTributeRequestConfirm') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><strong>Confirm</strong></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'confirmDeleteTributeRequest') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'deleteTributeRequestDecline') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><strong>Decline</strong></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'declineDeleteTributeRequest') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'imageLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var image_id='image_'+jsObject.allNotification[i].notify_type_id;
                                if(jsObject.allNotification[i].thumbnail_upload_path!="" && jsObject.allNotification[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.allNotification[i].thumbnail_upload_path + jsObject.allNotification[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.allNotification[i].uploadPath+jsObject.allNotification[i].image_name;
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                var title=jsObject.allNotification[i].image_title;
                                var desc=jsObject.allNotification[i].image_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+image_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+image_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<img alt="image"  src="'+cloudfront_base_url+image+'" style="display: none"/>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'textLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var text_id='text_'+jsObject.allNotification[i].notify_type_id;
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                var title=jsObject.allNotification[i].text_title;
                                if(title.length>140)
                                {
                                    title=title.substr(0, 140)+'...';
                                }
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+text_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+text_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].text_description+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].title+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';

                            }
                            if (jsObject.allNotification[i].notify_type == 'videoLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var video_id='video_'+jsObject.allNotification[i].notify_type_id;
                                var video='https://player.vimeo.com/video/'+jsObject.allNotification[i].vimeo_video_id;
                                var title=jsObject.allNotification[i].video_title;
                                var desc=jsObject.allNotification[i].video_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+video_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+video_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<iframe src="'+video+'" width="100%" height="100px" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'tributeLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openTagTribute(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'Users/tributeDetails/'+jsObject.allNotification[i].FID+'?tribute_id='+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red"></span></a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'imageComment') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var image_id='image_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].image_title;
                                var desc=jsObject.allNotification[i].image_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                if(jsObject.allNotification[i].thumbnail_upload_path!="" && jsObject.allNotification[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.allNotification[i].thumbnail_upload_path + jsObject.allNotification[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.allNotification[i].uploadPath+jsObject.allNotification[i].image_name;
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+image_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+image_id+'\')"><span class="glyphicon glyphicon-comment"></span>';
                                appendHtml += '<img alt="image"  src="'+cloudfront_base_url+image+'" style="display: none"/>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'textComment') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var text_id='text_'+jsObject.allNotification[i].notify_type_id;

                                var title=jsObject.allNotification[i].text_title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+text_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+text_id+'\')"><span class="glyphicon glyphicon-comment"></span>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].text_description+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].title+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';

                            }
                            if (jsObject.allNotification[i].notify_type == 'videoComment') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var video_id='video_'+jsObject.allNotification[i].notify_type_id;
                                var video='https://player.vimeo.com/video/'+jsObject.allNotification[i].vimeo_video_id;
                                var title=jsObject.allNotification[i].video_title;
                                var desc=jsObject.allNotification[i].video_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+video_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+video_id+'\')"><span class="glyphicon glyphicon-comment"></span>';
                                appendHtml += '<iframe src="'+video+'" width="100%" height="100px" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'tributeComment') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openTagTribute(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'Users/tributeDetails/'+jsObject.allNotification[i].FID+'?tribute_id='+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-comment"></span></a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'tagImage') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var image_id='image_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].image_title;
                                var desc=jsObject.allNotification[i].image_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }

                                if(jsObject.allNotification[i].thumbnail_upload_path!="" && jsObject.allNotification[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.allNotification[i].thumbnail_upload_path + jsObject.allNotification[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.allNotification[i].uploadPath+jsObject.allNotification[i].image_name;
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userId + '?page_id=' + jsObject.allNotification[i].pageId + '"><h3>' + jsObject.allNotification[i].full_name + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+image_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+image_id+'\')"><span class="glyphicon glyphicon-tag"></span>';
                                appendHtml += '<img alt="image"  src="'+cloudfront_base_url+image+'" style="display: none"/>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'tagText') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var text_id='text_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].text_title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }

                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userId + '?page_id=' + jsObject.allNotification[i].pageId + '"><h3>' + jsObject.allNotification[i].full_name + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+text_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+text_id+'\')"><span class="glyphicon glyphicon-tag"></span>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].text_description+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].title+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';

                            }
                            if (jsObject.allNotification[i].notify_type == 'tagVideo') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var video_id='video_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].video_title;
                                var desc=jsObject.allNotification[i].video_desc;
                                var full_name=jsObject.allNotification[i].full_name;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                var video='https://player.vimeo.com/video/'+jsObject.allNotification[i].vimeo_video_id;
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userId + '?page_id=' + jsObject.allNotification[i].pageId + '"><h3>' + jsObject.allNotification[i].full_name + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+video_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+video_id+'\')"><span class="glyphicon glyphicon-tag"></span>';
                                appendHtml += '<iframe src="'+video+'" width="100%" height="100px" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profile_image) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profile_image + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }

                            if (jsObject.allNotification[i].notify_type == 'imageCommentLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var image_id='image_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].image_title;
                                var desc=jsObject.allNotification[i].image_desc;
                                var full_name=jsObject.allNotification[i].full_name_tag;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                if(jsObject.allNotification[i].thumbnail_upload_path!="" && jsObject.allNotification[i].thumbnail_image_name!="")
                                {
                                    var image = jsObject.allNotification[i].thumbnail_upload_path + jsObject.allNotification[i].thumbnail_image_name;
                                }
                                else
                                {
                                    var image = jsObject.allNotification[i].uploadPath+jsObject.allNotification[i].image_name;
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+image_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+image_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<img alt="image"  src="'+cloudfront_base_url+image+'" style="display: none"/>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId_tag+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId_tag+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'textCommentLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var text_id='text_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].text_title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+text_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+text_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].text_description+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].full_name_tag+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId_tag+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId_tag+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].title+'" class="album_name">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';

                            }
                            if (jsObject.allNotification[i].notify_type == 'videoCommentLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                var video_id='video_'+jsObject.allNotification[i].notify_type_id;
                                var title=jsObject.allNotification[i].video_title;
                                var desc=jsObject.allNotification[i].video_desc;
                                var full_name=jsObject.allNotification[i].full_name_tag;
                                var album_name=jsObject.allNotification[i].title;
                                if(title.length>40)
                                {
                                    title=title.substr(0, 40)+'...';
                                }
                                if(desc.length>40)
                                {
                                    desc=desc.substr(0, 40)+'...';
                                }
                                if(full_name.length>13)
                                {
                                    full_name=full_name.substr(0, 13)+'...';
                                }
                                if(album_name.length>13)
                                {
                                    album_name=album_name.substr(0, 13)+'...';
                                }
                                var video='https://player.vimeo.com/video/'+jsObject.allNotification[i].vimeo_video_id;
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div>';
                                appendHtml += '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_'+video_id+'" onClick="updateNotifySeen(\''+jsObject.allNotification[i].notificationid+'\',\''+video_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red;"></span>';
                                appendHtml += '<iframe src="'+video+'" width="100%" height="100px" frameborder="0" style="display:none;"></iframe>';
                                appendHtml += '<input type="hidden" value="'+title+'" class="content_title">';
                                appendHtml += '<input type="hidden" value="'+desc+'" class="content_description">';
                                appendHtml += '<input type="hidden" value="'+full_name+'" class="tagger_name">';
                                appendHtml += '<input type="hidden" value="' +jsObject.allNotification[i].userId_tag+'" class="tagger_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].pageId_tag+'" class="tagger_page_id">';
                                appendHtml += '<input type="hidden" value="'+jsObject.allNotification[i].albumid+'" class="album_id">';
                                appendHtml += '<input type="hidden" value="'+album_name+'" class="album_name">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none">';
                                }
                                else {
                                    appendHtml += '<img alt="image" class="tagger_image" src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '" style="display: none">';
                                }
                                appendHtml += '</a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'tributeCommentLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openTagTribute(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'Users/tributeDetails/'+jsObject.allNotification[i].FID+'?tribute_id='+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red"></span></a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'albumComment') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openAlbumList(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'ImageUpload/albumdetails/'+jsObject.allNotification[i].FID+'/'+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-comment"></span></a></div></li>';
                            }
                            if (jsObject.allNotification[i].notify_type == 'albumLike') {
                                if (jsObject.allNotification[i].notify_seen == '0') {
                                    notify_class = "not-seen";
                                }
                                else {
                                    notify_class = "";
                                }
                                appendHtml += '<li><div class="notifi_in ' + notify_class + '"><div class="notilft">';
                                if ((jsObject.allNotification[i].profileimage) == '') {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg">';
                                }
                                else {
                                    appendHtml += '<img alt="image"  src="' + cloudfront_base_url + jsObject.allNotification[i].profileimage + '">';
                                }
                                appendHtml += '<a href="' + url + 'Users/profile/' + jsObject.allNotification[i].userid + '?page_id=' + jsObject.allNotification[i].pageid + '"><h3>' + jsObject.allNotification[i].fullname + '</h3></a>';
                                appendHtml += '<h2>' + jsObject.allNotification[i].notify_details + '</h2><span class="notify_date">'+moment(jsObject.allNotification[i].notificationdate).format('MMM Do YYYY [at] h:mm:ss a')+'</span> ';
                                appendHtml += '</div><a href="javascript:void(0)" onclick="openAlbumList(\''+jsObject.allNotification[i].notificationid+'\',\''+url+'ImageUpload/albumdetails/'+jsObject.allNotification[i].FID+'/'+ jsObject.allNotification[i].notify_type_id+'\')"><span class="glyphicon glyphicon-heart" style="color:red"></span></a></div></li>';
                            }

                        }
                        $('#notification_body').html(appendHtml);
					}
					else
					{
						$('#notification_body').html('<li><div class="notifi_in"><div class="notilft">You have no notification yet</div></div></li>');
					}
					 $('.notification-count').html(jsObject.countNotication.total);
								
					
				}

			});
		
}
function markAllRead()
{
    $.ajax({

        url:url+'Users/markAllRead',
        data:{userid:$('#session_user_id').val()},
        type: 'post',
        success: function(data)
        {
            getNotification();

        }

    });
}
function accept(id)
{
    $.ajax({
				
				url:url+'Users/accept',
				data:{friends_id:id},
				type: 'post',
				success: function(data)
				{
                    getNotification();
					
				}

			});
}
function decline(id)
{
	 $.ajax({
				
				url:url+'Users/decline',
				data:{friends_id:id},
				type: 'post',
				success: function(data)
				{
                    getNotification();
				}

			});
}

function confirmDeleteTributeRequest(tribute_id,friends_id)
{
    $.ajax({

        url:url+'Users/confirmDeleteTributeRequest',
        data:{tribute_id:tribute_id,friends_id:friends_id},
        type: 'post',
        success: function(data)
        {
            getNotification();
        }

    });
}
function declineDeleteTributeRequest(tribute_id,friends_id)
{
    $.ajax({

        url:url+'Users/declineDeleteTributeRequest',
        data:{tribute_id:tribute_id,friends_id:friends_id},
        type: 'post',
        success: function(data)
        {
            getNotification();
        }

    });
}
function updateNotifySeen (notifyId,contentId)
{
    $.ajax({

        url:url+'Users/updateNotifySeen',
        data:{notifyId:notifyId},
        type: 'post',
        success: function(data)
        {
           // console.log(data);
        }

    });
    showModal(contentId);
}
function openTagTribute(notifyId,tagTributeUrl)
{
    $.ajax({

        url:url+'Users/updateNotifySeen',
        data:{notifyId:notifyId},
        type: 'post',
        success: function(data)
        {
            //console.log(data);
        }

    });
    window.location.href=tagTributeUrl;
}
function openAlbumList(notifyId,albumListUrl)
{
    $.ajax({

        url:url+'Users/updateNotifySeen',
        data:{notifyId:notifyId},
        type: 'post',
        success: function(data)
        {
            //console.log(data);
        }

    });
    window.location.href=albumListUrl;
}