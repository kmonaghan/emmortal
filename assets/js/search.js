var cloudfront_base_url=$('#cloudfront_base_url').val();
$('document').ready(function ()
{
    var url=$('#base_url').val();
    Pace.track(function() {
        $.ajax({
            type: "post",
            url: url + "Users/RelationshipUser",
            data: {userid:$("#get_user_id").val()},
            success: function (data) {
                $("#allRelationshipUser").html(data);
            }
        });
    });
    $( "#searchUser" ).keyup(function()
    {

        Pace.track(function() {
            $.ajax({
                type: "post",
                url: url + "Users/searchRelationshipUser",
                data: {search_users: $("#searchUser").val(),userid:$("#get_user_id").val()},
                success: function (data) {
                    $("#allRelationshipUser").html(data);
                }
            });
        });

    });
});
function sendData(start) {
    var url = $('#base_url').val();
    Pace.track(function()
    {
        $.ajax({
            type: "post",
            url: url + "Users/scrollLoad",
            data: {start: start},
            success: function (data) {
                $("#allUserList").html(data);
            }
        });
    });
}
function sendData1(start) {
    var url = $('#base_url').val();
    Pace.track(function()
    {
        $.ajax({
            type: "post",
            url: url + "Users/scrollLoad",
            data: {start: start},
            success: function (data) {
                $("#allUserList").append(data);
            }
        });
    });
}
$(document).ready(function ()
{
    /* All User */
    $("ul.tabs li[rel='tab1']").click(function ()
    {
        $('#row_count').val(0);
        var start = parseInt($('#row_count').val());
        if ($('#row_count').val()< 5)
        {
            $('#row_count').val(start + 5);
            sendData(start);
        }
        $(window).scroll(function ()
        {
           
            if($(window).scrollTop() + $(window).height() >= $(document).height())
            {
                if($('#search').val()=='')
                {
                    var start = parseInt($('#row_count').val());
                    if ($('#total_row').val() >= start)
                    {
                        $('#row_count').val(start + 5);
                        sendData1(start);
                    }
                }
            }
        });

        $( "#search" ).keyup(function()
        {

            var url=$('#base_url').val();
            Pace.track(function() {
                $.ajax({
                    type: "post",
                    url: url + "Users/searchAll",
                    data: {search_users: $("#search").val()},
                    success: function (data) {
                        $("#allUserList").html(data);
                    }
                });
            });

        });
    });
    /* Outgoing Users*/
    $("ul.tabs li[rel='tab3']").click(function ()
    {
        if($("#search").val()!='')
        {
            var url = $('#base_url').val();
            Pace.track(function () {
                $.ajax({
                    type: "post",
                    url: url + "Users/allOutgoingUsers",
                    success: function (data) {
                        $("#allOutgoingUsers").html(data);
                    }
                });
            });
        }
        else {
                var url = $('#base_url').val();
                Pace.track(function () {
                    $.ajax({
                        type: "post",
                        url: url + "Users/searchOutgoingUsers",
                        data: {search_users: $("#search").val()},
                        success: function (data) {
                            $("#allOutgoingUsers").html(data);
                        }
                    });
                });
        }
        $("#search").keyup(function () {

            var url = $('#base_url').val();
            Pace.track(function () {
                $.ajax({
                    type: "post",
                    url: url + "Users/searchOutgoingUsers",
                    data: {search_users: $("#search").val()},
                    success: function (data) {
                        $("#allOutgoingUsers").html(data);
                    }
                });
            });

        });
    });

    /* Incoming Users*/
    $("ul.tabs li[rel='tab4']").click(function ()
    {
        var url=$('#base_url').val();
        Pace.track(function() {
            $.ajax({
                type: "post",
                url: url + "Users/allIncomingUsers",
                success: function (data) {
                    $("#allIncomingUsers").html(data);
                }
            });
        });
    });

    /* Relatioship Users*/
    $("ul.tabs li[rel='tab2']").click(function ()
    {
        if($("#search").val()!='')
        {
            var url=$('#base_url').val();
            Pace.track(function() {
                $.ajax({
                    type: "post",
                    url: url + "Users/searchRelationship",
                    data: {search_users: $("#search").val()},
                    success: function (data) {
                        $("#allRelationshipUsers").html(data);
                    }
                });
            });
        }
        else
        {
            var url=$('#base_url').val();
            Pace.track(function() {
                $.ajax({
                    type: "post",
                    url: url + "Users/allRelationshipUsers",
                    success: function (data) {
                        $("#allRelationshipUsers").html(data);
                    }
                });
            });
        }
        $( "#search" ).keyup(function()
        {

            var url=$('#base_url').val();
            Pace.track(function() {
                $.ajax({
                    type: "post",
                    url: url + "Users/searchRelationship",
                    data: {search_users: $("#search").val()},
                    success: function (data) {
                        $("#allRelationshipUsers").html(data);
                    }
                });
            });

        });
    });

});
function  addFriend(id)
{
    var url=$('#base_url').val();
    Pace.track(function() {
        $.ajax({
            type: "post",
            url: url + "Users/addFriend",
            data: {friend_id: id},
            success: function (data) {
                $("#allUserList").html(data);
            }
        });
    });

}
function  acceptFriend(id)
{
    var url=$('#base_url').val();
    Pace.track(function() {
        $.ajax({
            type: "post",
            url: url + "Users/acceptFriend",
            data: {friend_id: id},
            success: function (data) {

                $("#allIncomingUsers").html(data);
            }
        });
    });

}

function declineFriend(id)
{
    var url=$('#base_url').val();
    Pace.track(function() {
        $.ajax({
            type: "post",
            url: url + "Users/declineFriends",
            data: {friends_id: id},
            success: function (data) {

                $("#allIncomingUsers").html(data);
            }
        });
    });
}
function deleteContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {
        var image_url = base_url + "ImageUpload/removeAlbumImage/" + album_id + '/' + check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            window.location.replace(image_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'text')
    {
        var text_url=base_url+"ImageUpload/removeAlbumText/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Text Content?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(text_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
    if(check_type == 'video')
    {
        var video_url=base_url+"ImageUpload/removeAlbumVideo/"+album_id+'/'+check_id;
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to delete your Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            window.location.replace(video_url);
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    }
}
function editContentPost(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    if(check_type == 'image')
    {


        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');

        Pace.track(function()
        {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/edit_inner_image",
                data: {'album_id':album_id,'image_id':check_id },
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#single_image_album_id').val(album_id);
                    $('#editSingleImageUpload').modal('show');
                    var img_src=cloudfront_base_url+obj.uploadPath+obj.image_name;
                    $('#edit_imagePreview2').attr('src',img_src);
                    $('.editImageIconForEdit').css('display','block');
                    if(img_src!='')
                    {
                        $('#edit_Remove_uploadFile2').show();
                        $('#edit_imagePreview2').show();
                        $('#edit_choosephoto2').hide();
                    }

                    $('#edit_title').val(obj.image_title);
                    $('.edit_txtEditor').val(obj.image_desc);
                    $('#edit_inner_image_id').val(obj.image_id);
                    if(obj.image_id)
                    {
                        $(".friend_list_edit_single").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_single").append(option);
                            });
                            if(obj.tag_user_id)
                            {
                                for (var i=0;i<obj.tag_user_id.length;i++){
                                    $(".friend_list_edit_single option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                                }
                            }

                        }

                    }

                }
            });
        });

    }
    if(check_type == 'text')
    {
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_text",
                data: {'album_id': album_id, 'edit_text_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.albumTitle[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.albumTitle[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#show_edit_text_title').val(obj.text_title);
                    $('.edit_txtEditor').html(obj.text_description);
                    $('#edit_text_id').val(obj.textid);
                    $('#edittextcontent').modal('show');
                    if (obj.textid) {
                        $(".friend_list_edit_text").empty();
                        if (obj.friend_fullName != null) {
                            $.each(obj.friend_fullName, function (index, value) {
                                option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_text").append(option);
                            });
                            for (i = 0; i < obj.tag_user_id.length; i++) {
                                $(".friend_list_edit_text option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                            }
                        }
                    }
                }
            });
        });
    }
    if(check_type == 'video')
    {
        $('.relationshipsModal').modal('hide');
        $("#comment_div").modal('hide');
        $('#album_owner_id').val($('.tagger_user_id_modal').val());
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "ImageUpload/select_edit_album_video",
                data: {'album_id': album_id, 'edit_video_id': check_id},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var appendHtml= '';
                    if(obj.albumeid.length>0)
                    {
                        for(var i=0;i<obj.albumeid.length;i++)
                        {
                            if(album_id==obj.albumeid[i])
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                            }

                            else
                            {
                                appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                            }

                        }
                    }
                    else
                    {
                        appendHtml += '<option value="-1">My Chronicles</option>'
                    }
                    $('.album_list_id').html(appendHtml);
                    $('#editvideoUpload').modal('show');
                    $('#edit_video_id').val(obj.video_id);
                    $('.show_edit_video').html(obj.video_url);
                    if(obj.video_id)
                    {
                        $(".friend_list_edit_video").empty();
                        if(obj.friend_fullName!=null)
                        {
                            $.each(obj.friend_fullName, function(index, value) {
                                option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                $(".friend_list_edit_video").append(option);
                            });

                            for (i=0;i<obj.tag_user_id.length;i++){
                                $(".friend_list_edit_video option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                            }
                        }

                    }
                }
            });
        });
    }
}
function showModal(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];

    var content_title=$('#relationshipsModalShow_'+id).find('.content_title').val();
    var content_description=$('#relationshipsModalShow_'+id).find('.content_description').val();
    var tagger_name=$('#relationshipsModalShow_'+id).find('.tagger_name').val();
    var tagger_image=$('#relationshipsModalShow_'+id).find('.tagger_image').attr('src');
    var album_name=$('#relationshipsModalShow_'+id).find('.album_name').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var tagger_page_id=$('#relationshipsModalShow_'+id).find('.tagger_page_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    var tagger_profile=base_url+'Users/profile/'+tagger_id+'?page_id='+tagger_page_id;
    var tagger_album=base_url+'ImageUpload/albumdetails/'+tagger_id+'/'+album_id;

    $('#comment_form').find('#comments_type_id').val(id);
    $('.relationshipsModal').find('.comment_icon').attr('id',id);
    $('.tagger_user_id_modal').val(tagger_id);
    if(check_type == 'image')
    {
        var img=$('#relationshipsModalShow_'+id).find('img').attr('src');
        $('.relationshipsModal').find('.content_image_modal').attr('src',img);
        $('.relationshipsModal').find('.full_image_view ').attr('href',img);
        $('.relationshipsModal').find('.tagger_profile_link').attr('href',tagger_profile);
        $('.relationshipsModal').find('.tagger_album_link').attr('href',tagger_album);
        $('.relationshipsModal').find('.content_title_modal').html(content_title);
        $('.relationshipsModal').find('.content_description_modal').html(content_description);
        $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
        $('.relationshipsModal').find('.album_name_modal').html(album_name);
        $('.relationshipsModal').find('.tagger_image_modal').attr('src',tagger_image);
        $('.relationshipsModal').find('.modal_comment_id').attr('id',id);

        $('.relationshipsModal').find('.content_text_modal').css('display','none');
        $('.relationshipsModal').find('.content_video_modal').css('display','none');
        $('.relationshipsModal').find('.content_image_modal').css('display','block');

        $('.relationshipsModal').modal('show');


    }
    if(check_type == 'video')
    {
        var video=$('#relationshipsModalShow_'+id).find('iframe').attr('src');
        $('.relationshipsModal').find('.content_video_modal').attr('src',video);

        $('.relationshipsModal').find('.tagger_profile_link').attr('href',tagger_profile);
        $('.relationshipsModal').find('.tagger_album_link').attr('href',tagger_album);
        $('.relationshipsModal').find('.content_title_modal').html(content_title);
        $('.relationshipsModal').find('.content_description_modal').html(content_description);
        $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
        $('.relationshipsModal').find('.album_name_modal').html(album_name);
        $('.relationshipsModal').find('.tagger_image_modal').attr('src',tagger_image);
        $('.relationshipsModal').find('.modal_comment_id').attr('id',id);

        $('.relationshipsModal').find('.content_text_modal').css('display','none');
        $('.relationshipsModal').find('.content_video_modal').css('display','block');
        $('.relationshipsModal').find('.content_image_modal').css('display','none');

        $('.relationshipsModal').modal('show');

    }
    if(check_type=='text')
    {
        var text=$('#relationshipsModalShow_'+id).find('.content_description').val();
        $('.relationshipsModal1').find('.content_description_modal').html('');

        $('.relationshipsModal1').find('.tagger_profile_link').attr('href',tagger_profile);
        $('.relationshipsModal1').find('.tagger_album_link').attr('href',tagger_album);
        /*$('.relationshipsModal1').find('.content_title_modal').html(content_title);*/
        $('.relationshipsModal1').find('.tagger_name_modal').html(tagger_name);
        $('.relationshipsModal1').find('.album_name_modal').html(album_name);
        $('.relationshipsModal1').find('.tagger_image_modal').attr('src',tagger_image);
        $('.relationshipsModal1').find('.modal_comment_id').attr('id',id);

        $('.relationshipsModal1').find('.content_text_modal').html(text);
        $('.relationshipsModal1').find('.content_image_modal').css('display','none');
        $('.relationshipsModal1').find('.content_video_modal').css('display','none');
        $('.relationshipsModal1').find('.content_text_modal').css('display','block');

        $('.relationshipsModal1').modal('show');
    }

    var session_user_id=$('#session_user_id').val();
    $('.editContentId').attr('id',id);
    $('.deleteContentId').attr('id',id);
    $('.pinModal').attr('id',id);
    if(tagger_id==session_user_id)
    {
        $('.editContentId').show();
        $('.deleteContentId').show();
        $('.pinModal').show();
    }
    else
    {
        $('.editContentId').hide();
        $('.deleteContentId').hide();
        $('.pinModal').hide();
    }
    openComment1(id);
}
function openComment1(id)
{
    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#session_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        var flag1=0;
                        var flag=0;
                        var count=0;
                        if(session_user_id)
                        {
                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                            {
                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                {
                                    flag=1;
                                    break;
                                }
                                else
                                {
                                    flag=0;
                                }
                            }
                            if(flag==1)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                            }
                            else
                            {
                                if(session_user_id!=jsObject.commentsData[i].userid)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                                }

                            }
                        }
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"> <i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"> <i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image"   class="men-in-black" src="' + cloudfront_base_url  + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }
                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }
                var seeTaggedHtml='';
                if(jsObject.seeTagged.length>0)
                {
                    for(var p=0;p<jsObject.seeTagged.length;p++)
                    {
                        seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                    }

                }
                $('.see_tagged_name').html(seeTaggedHtml);
                $('.count_album_content').html(jsObject.count_album_content);
                $('.count_tag').html(jsObject.seeTagged.length);
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
}
function openComment(id)
{

    var url=$('#base_url').val()+'Users/showCommentRelationships';
    var base_url=$('#base_url').val();
    var user_id=$('#session_user_id').val();
    var session_user_id=$('#session_user_id').val();
    var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
    var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
    $('#comment_form').find('#comments_type_id').val(id);
    Pace.track(function() {
        $.ajax({
            url: url,
            type: 'post',
            data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
            success: function (data) {
                var appendHtml = '';
                var jsObject = JSON.parse(data);
                if (jsObject.commentsData.length > 0) {
                    for (var i = 0; i < jsObject.commentsData.length; i++) {
                        appendHtml += '<div class="use-padding  use-border">';
                        appendHtml += '<div class="left-box">';
                        appendHtml += '<p><span class="square-icon">';
                        if (jsObject.commentsData[i].profileimage) {

                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + jsObject.commentsData[i].profileimage + '" />';
                        }
                        else {
                            appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                        }
                        appendHtml += '</span>';
                        appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                        appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                        appendHtml += '</div>';
                        appendHtml += '<div class="right-box">';
                        var flag1=0;
                        var flag=0;
                        var count=0;
                        if(session_user_id)
                        {
                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                            {
                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                {
                                    flag=1;
                                    break;
                                }
                                else
                                {
                                    flag=0;
                                }
                            }
                            if(flag==1)
                            {
                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                            }
                            else
                            {
                                if(session_user_id!=jsObject.commentsData[i].userid)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                                }

                            }
                        }
                        for(var j=0;j<jsObject.allLikecomment.length;j++)
                        {
                            if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                            {
                                flag1=1;
                                break;
                            }

                        }
                        for(var k=0;k<jsObject.allLikecomment.length;k++)
                        {
                            if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                            {
                                count++;
                            }
                        }
                        if(flag1==1)
                        {
                            appendHtml += ' <a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"> <i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        else
                        {
                            appendHtml += ' <a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"> <i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                        }
                        appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                        appendHtml += '</div>';
                        if (jsObject.commentsData[i].image_comments) {
                            appendHtml += '<img alt="image"   class="men-in-black" src="' + cloudfront_base_url  + jsObject.commentsData[i].image_comments + '" />';
                        }
                        appendHtml += '</div>';

                    }
                }
                if(session_user_id)
                {
                    var check_id=id.split('_')[1];
                    var check_type=id.split('_')[0];
                    if(check_type=='image')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.image_id==check_id)
                            {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='text')
                    {
                        if(jsObject.existPageData)
                        {
                            if (jsObject.existPageData.text_id == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }
                    if(check_type=='video')
                    {
                        if(jsObject.existPageData) {
                            if (jsObject.existPageData.video == check_id) {
                                $('.pinModal').attr('data-original-title', 'Pinned!');
                                $('.pinModal').attr('title', 'Pinned!');
                                $('.pinModal').css('color', 'red');
                                $('.deleteContentPage').show();

                            }
                        }
                        else {
                            $('.pinModal').attr('data-original-title', 'Pin!');
                            $('.pinModal').attr('title', 'Pin!');
                            $('.pinModal').css('color', '#fff');
                            $('.deleteContentPage').hide();
                        }
                    }

                }
                if(session_user_id)
                {
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');
                    }
                }
                var seeTaggedHtml='';
                if(jsObject.seeTagged.length>0)
                {
                    for(var p=0;p<jsObject.seeTagged.length;p++)
                    {
                        seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                    }

                }
                $('.see_tagged_name').html(seeTaggedHtml);
                $('.count_album_content').html(jsObject.count_album_content);
                $('.count_tag').html(jsObject.seeTagged.length);
                $('.count_like').html(jsObject.totalLikePerticular.total);
                $('.comment_body').html(appendHtml);
                $('.count_comment').html(jsObject.commentsData.length);
            }
        });
    });
    $("#comment_div").modal('show');
    $("#comment_div").css('width',"315px");
}

function closeComment() {
    $("#comment_div").modal('hide');
    $("#comment_div").css('width',"0");
}

function like(id)
{
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        var url=$('#base_url').val()+'Users/likeContent';
        var base_url=$('#base_url').val();
        var tagger_id=$('.tagger_user_id_modal').val();
        Pace.track(function() {
            $.ajax({
                url: url,
                type: 'post',
                data: {id: id,tagger_id:tagger_id},
                success: function (data) {

                    var jsObject = JSON.parse(data);
                    if (jsObject.existLike == true) {
                        $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                        $('.user_like_dislike_title').attr('title', 'Like!');
                        $('.user_like_dislike').css('color', '#5F5F5F');

                    }
                    else {
                        $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                        $('.user_like_dislike_title').attr('title', 'Dislike!');
                        $('.user_like_dislike').css('color', 'red');
                    }
                    $('.count_like').html(jsObject.totalLikePerticular.total);
                    $('#like_particular_' + id).html(jsObject.totalLikePerticular.total);
                }
            });
        });
    }
    else
    {
        $.notify
        ({

                message: 'You must Sign In'
            }
            , {
                type: 'danger',
                offset:
                    {
                        x: 10,
                        y: 50
                    },
                z_index: 1050,
            });
        $('.relationshipsModal').modal('hide');
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        $('#signinModal').modal('show');
    }

}




function comment_like(tagger_id,id)
{
    var session_user_id=$('#session_user_id').val();
    if(session_user_id)
    {
        var url = $('#base_url').val() + 'Users/commentLike';
        var base_url = $('#base_url').val();
        var comments_type_id = $('#comments_type_id').val();
        Pace.track(function () {
            $.ajax({
                url: url,
                type: 'post',
                data: {comment_id: id,tagger_id:tagger_id,comments_type_id:comments_type_id},
                success: function (data) {
                    var jsObject = JSON.parse(data);
                    if (jsObject.existLike == true) {
                        $('#' + id).attr('data-original-title', 'Like!');
                        $('#' + id).attr('title', 'Like!');
                        $('#' + id).find('.comment_like_dislike').css('color', '#5F5F5F');

                    }
                    else {
                        $('#' + id).attr('data-original-title', 'Dislike!');
                        $('#' + id).attr('title', 'Dislike!');
                        $('#' + id).find('.comment_like_dislike').css('color', 'red');
                    }
                    $('#count_comment_like_' + id).html(jsObject.totalCommentLike.total);

                }
            });
        });
    }
    else
    {
        $.notify
        ({

                message: 'You must Sign In'
            }
            , {
                type: 'danger',
                offset:
                    {
                        x: 10,
                        y: 50
                    },
                z_index: 1050,
            });
        $('.relationshipsModal').modal('hide');
        $('.relationshipsModal1').modal('hide');
        $("#comment_div").modal('hide');
        $('#signinModal').modal('show');
    }
}
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="dropdown"]').tooltip();
});
// Choose image file
$(document).on('click', '.image_browse_icon', function()
{
    $(".image_file").click();

});

$('document').ready(function ()
{
    /* Send Button Enable or disable for support */
    $('#comment_form textarea').on('keyup blur', function () { // fires on every keyup & blur
        var comment_text = $('#comment_text').val().trim();
        if (comment_text != '')  {                   // checks form for validity
            $('#btn_comment_relationships').prop('disabled', false);        // enables button
            $('#btn_comment_relationships').css('cursor', 'pointer');

        } else {
            $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
            $('#btn_comment_relationships').css('cursor', 'not-allowed');

        }
    });
    $('.image_file').change(function ()
    {
        if (this.files && this.files[0] &&  this.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
        {

        }
        else
        {
            $(this).val('');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    });
    $("#comment_form").validate(
        {

            submitHandler: function (form)
            {
                var session_user_id=$('#session_user_id').val();
                if(session_user_id)
                {
                    var base_url=$('#base_url').val();
                    var url = $('#base_url').val() + 'Users/commentProfilePage';
                    var comments_type_id = $('#comments_type_id').val();
                    var user_id=$('#profile_user_id').val();
                    var session_user_id=$('#session_user_id').val();
                    var comment_text = $('#comment_text').val().trim();
                    var tagger_id=$('#relationshipsModalShow_'+comments_type_id).find('.tagger_id').val();
                    if (comment_text != '')
                    {
                        var comments_form_data = new FormData();
                        comments_form_data.append('comments_type_id',comments_type_id);
                        comments_form_data.append('comment_text',comment_text);
                        comments_form_data.append('tagger_id',tagger_id);
                        if($('.image_file')[0].files[0])
                        {
                            comments_form_data.append('comment_file', $('.image_file')[0].files[0]);
                        }
                        Pace.track(function() {
                            $.ajax({
                                url: url,
                                type: 'post',
                                data: comments_form_data,
                                contentType: false,
                                processData: false,
                                cache: false,
                                success: function (data) {
                                    $('#btn_comment').prop('disabled', 'disabled');   // disables button
                                    $('#btn_comment').css('cursor', 'not-allowed');
                                    var appendHtml = '';
                                    var jsObject = JSON.parse(data);
                                    if (jsObject.commentsData.length > 0) {
                                        for (var i = 0; i < jsObject.commentsData.length; i++) {
                                            var flag=0;
                                            appendHtml += '<div class="use-padding  use-border">';
                                            appendHtml += '<div class="left-box">';
                                            appendHtml += '<p><span class="square-icon">';
                                            if (jsObject.commentsData[i].profileimage) {

                                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + jsObject.commentsData[i].profileimage + '" />';
                                            }
                                            else {
                                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url  + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                                            }
                                            appendHtml += '</span>';
                                            appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                                            appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                                            appendHtml += '</div>';
                                            appendHtml += '<div class="right-box">';
                                            for(var j=0;j<jsObject.relationshipUsers.length;j++)
                                            {
                                                if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                                {
                                                    flag=1;
                                                    break;
                                                }
                                                else
                                                {
                                                    flag=0;
                                                }
                                            }
                                            if(flag==1)
                                            {
                                                appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                if(session_user_id!=jsObject.commentsData[i].userid)
                                                {
                                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image"   src="' + cloudfront_base_url  + 'assets/img/icon1.png">&nbsp;</a>';
                                                }

                                            }
                                            var flag1=0;
                                            var count=0;
                                            for(var j=0;j<jsObject.allLikecomment.length;j++)
                                            {
                                                if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                                                {
                                                    flag1=1;
                                                    break;
                                                }

                                            }
                                            for(var k=0;k<jsObject.allLikecomment.length;k++)
                                            {
                                                if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                                                {
                                                    count++;
                                                }
                                            }
                                            if(flag1==1)
                                            {
                                                appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"> <i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                            }
                                            else
                                            {
                                                appendHtml += '<a href="javascript:void(0)" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)" > <i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                            }
                                            appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                                            appendHtml += '</div>';
                                            if (jsObject.commentsData[i].image_comments) {
                                                appendHtml += '<img alt="image"   class="men-in-black" src="' + cloudfront_base_url  + jsObject.commentsData[i].image_comments + '" />';
                                            }
                                            appendHtml += '</div>';

                                        }
                                    }
                                    $('.count_comment').html(jsObject.commentsData.length);
                                    $('.comment_body').html(appendHtml);
                                    $('#comment_text').val('');
                                    $('.image_file').val('');
                                    $('#comment_perticular_' + comments_type_id).html(jsObject.commentsData.length);
                                }
                            });
                        });
                    }
                }
                else
                {
                    $.notify
                    ({

                            message: 'You must Sign In'
                        }
                        , {
                            type: 'danger',
                            offset:
                                {
                                    x: 10,
                                    y: 50
                                },
                            z_index: 1050,
                        });
                    $('#comment_text').val('');
                    $('.image_file').val('');
                    $('.relationshipsModal').modal('hide');
                    $('.relationshipsModal1').modal('hide');
                    $("#comment_div").modal('hide');
                    $('#signinModal').modal('show');
                }
            }

        });
});
function pinModalEditDelete(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    $('.updateContentPage').attr('id',id);
    $('.deleteContentPage').attr('id',id);
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/imagePinEditDelete",
                data: {'image_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/textPinEditDelete",
                data: {'text_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/videoPinEditDelete",
                data: {'video_id':check_id},
                success: function (data)
                {
                    var obj = JSON.parse(data);
                    var appendHtml='';
                    if(obj.allPageId.length>0)
                    {
                        for(var i=0;i<obj.allPageId.length;i++)
                        {
                            if(obj.selectedPageId)
                            {
                                if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if(obj.allPageId[i].default_page==1)
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                    }
                                    else
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                    }

                                }
                                else
                                {
                                    if(obj.allPageId[i].page_title=="")
                                    {
                                        appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                    }
                                    else {
                                        appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                    }
                                }
                            }
                        }


                    }
                    $('#myModal').find('.selectPageId').html(appendHtml);
                    $('#myModal').modal('show');
                }
            });
        });
    }

}
function updateContentPage(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    var selected = $('.selectPageId option:selected');
    var page_id=selected.val();
    if(check_type=='image')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageImage",
                data: {'image_id':check_id,page_id:page_id},
                success: function (data)
                {

                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#myModal').modal('hide');
                    $('.deleteContentPage').show();

                }
            });
        });
    }
    if(check_type=='text')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageText",
                data: {'text_id':check_id,page_id:page_id},
                success: function (data)
                {
                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#myModal').modal('hide');
                    $('.deleteContentPage').show();
                }
            });
        });
    }
    if(check_type=='video')
    {
        Pace.track(function() {
            $.ajax({
                type: 'post',
                url: base_url + "Users/updateContentPageVideo",
                data: {'video_id':check_id,page_id:page_id},
                success: function (data)
                {
                    $('.pinModal').attr('data-original-title', 'Pinned!');
                    $('.pinModal').attr('title', 'Pinned!');
                    $('.pinModal').css('color', 'red');
                    $('#myModal').modal('hide');
                    $('.deleteContentPage').show();
                }
            });
        });
    }
}
function deleteContentPage(id)
{
    var base_url=$('#base_url').val();
    var check_type=id.split('_')[0];
    var check_id=id.split('_')[1];
    if(check_type=='image')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Image?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageImage",
                                    data: {'image_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#myModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='video')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Video?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageVideo",
                                    data: {'video_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#myModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }
    if(check_type=='text')
    {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to unpin Page Content Text?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function () {
                            Pace.track(function() {
                                $.ajax({
                                    type: 'post',
                                    url: base_url + "Users/deleteContentPageText",
                                    data: {'text_id':check_id},
                                    success: function (data)
                                    {
                                        $('.pinModal').attr('data-original-title', 'Pin!');
                                        $('.pinModal').attr('title', 'Pin!');
                                        $('.pinModal').css('color', '#fff');
                                        $('#myModal').modal('hide');
                                        $('.deleteContentPage').hide();
                                    }
                                });
                            });
                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });

    }

}
function requestSentFunction()
{
    $(".cancelRequestDropdown").toggle();
}

function cancelRequest(id)
{

    var base_url=$('#base_url').val();
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure want to cancel sent request?',
        buttons: {
            confirm:
                {
                    btnClass: 'btn-danger',
                    action: function ()
                    {
                        Pace.track(function() {
                            $.ajax({
                                url: base_url+"Users/cancelSentRequestOutgoing",
                                type: 'post',
                                data:{friends_id:id},
                                success: function(data)
                                {
                                    $("#allOutgoingUsers").html(data);

                                }
                            });
                        });
                    }
                },
            cancel: {
                btnClass: 'btn-info'
            }
        }
    });
}
