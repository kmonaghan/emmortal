
function readURL(input)
{

    if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif)$/))
    {
        var reader = new FileReader();

        reader.onload = function(e)
        {
            $('#image_error').css('display','none');
            $('.profile-user-img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        $('#image_error').css('display','block');
        $('#filename').val('');
    }
}
//show image in choose avatar onchange
$(document).on('change', '#filename',function() {
    readURL(this);
});
$(document).ready(function()
{

    $("#formUploadProfile").validate(
        {

        rules:
            {
                profile_image:
                    {
                        required: true
                    },
            },
        messages:
            {
                profile_image:
                    {
                        required: "Profile image file field can not be blank!"
                    },
            },
            submitHandler: function(form) {
                var url = $('#base_url').val() + 'Admin/UploadProfileImage';
                var profileImage = new FormData();
                profileImage.append('profileimage', $('#filename')[0].files[0]);
                Pace.track(function () {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: profileImage,
                        contentType: false,
                        processData: false,
                        success: function (data) {

                            if (data == 1) {
                                $.notify
                                ({

                                        message: 'Profile Image upload successfully '
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 75
                                            }
                                    });
                                location.reload();

                            }
                        }

                    });
                });
            }

        });
    //account settings
    $("#account_settings").validate(
    {
        rules:
            {
                firstname:
                    {
                        required: true
                    },
                lastname:
                    {
                        required: true
                    },
                dob:
                    {
                        required: true,
                    },
            },
        messages:
            {
                firstname:
                    {
                        required: "First name field can not be blank!"
                    },
                lastname:
                    {
                        required: "Last name field can not be blank!"
                    },
                dob:
                    {
                        required: "Date of birth field can not be blank!"
                    },

            },
        submitHandler: function(form)
        {
            var url				        = $('#base_url').val()+'Admin/accountSettings';
            var generalSettingsData=
                {
                    firstname         : $('#firstname').val(),
                    lastname          :$('#lastname').val(),
                    new_password	  : $('#lastname').val(),
                    dob               : $('#dob').val()

                };
            Pace.track(function () {
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: generalSettingsData,
                    success: function (data) {

                        if (data == 1) {
                            $.notify
                            ({

                                    message: 'Changes was successfully saved!'
                                }
                                , {
                                    type: 'success',
                                    offset:
                                        {
                                            x: 10,
                                            y: 75
                                        }
                                });
                            location.reload();

                        }
                    }

                });
            });
        }
    });

    //Change Password

    $("#change_password").validate(
        {
            rules:
                {
                    current_password:
                        {
                            required: true
                        },
                    new_password:
                        {
                            required: true,
                            minlength: 6
                        },
                    confirm_password:
                        {
                            equalTo: "#new_password"
                        },
                },
            messages:
                {
                    current_password:
                        {
                            required: "Current Password field can not be blank!"
                        },
                    new_password:
                        {
                            required: "New Password field can not be blank!",
                            minlength: "Should be at least 6 characters length"
                        },
                    confirm_password: "Enter confirm password same as New password",

                },
            submitHandler: function(form)
            {
                var url					= $('#base_url').val()+'Admin/changePassword';
                var new_password			= $('#new_password').val();
                var current_password		= $('#current_password').val();
                var changePasswordData =
                    {
                        current_password : current_password,
                        new_password	  : new_password

                    };
                Pace.track(function () {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: changePasswordData,
                        success: function (data) {
                            if (data == 1) {
                                $('#current_password_alert').css('display', 'none');
                                $('#current_password').val('')
                                $('#new_password').val('');
                                $('#confirm_password').val('');
                                $.notify
                                ({

                                        message: 'Changes was successfully saved!'
                                    }
                                    , {
                                        type: 'success',
                                        offset:
                                            {
                                                x: 10,
                                                y: 75
                                            }
                                    });
                            }
                            if (data == 0) {
                                $('#invalid_current_password').html('Invalid Current Password');
                                $('#current_password_alert').css('display', 'block');
                                $('#current_password_alert_close').click(function () {
                                    $('#current_password_alert').css('display', 'none');
                                });
                            }
                        }
                    });
                });
            }
        });
});