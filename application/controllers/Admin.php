<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
  public function __construct()
     {
         header('Access-Control-Allow-Origin: *');
         header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		 
		 
		 parent::__construct(); 
		 $this->load->model('AdminModel');
		 $this->load->model('UsersModel');
         $this->load->model('ImageUploadModel');
         $this->load->library('Aws3');
	}
	
	public function admin_session_data()
	{
		$logged_in=$this->session->userdata('admin_logged_in');
		if(!($logged_in))
		{
			redirect('/');
		}
	}
	public function dashboard()
	{
		$this->admin_session_data();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $userList=$this->AdminModel->getUserListDetails();
        $data['total_user']=count($userList);
        $select_album_style=$this->AdminModel->getAlbumBackgroundStyle();
        $data['total_album_style']=count($select_album_style);
        $select_album_style=$this->AdminModel->getPageBackgroundStyle();
        $data['total_page_style']=count($select_album_style);
        $deleted_video_details=$this->AdminModel->total_deleted_video();
        $data['total_deleted_video']=count($deleted_video_details);
        $user_given_question=$this->AdminModel->total_user_given_question(); 
        $data['total_user_given_question']=count($user_given_question);
        $data['total_foreground_style']=$this->AdminModel->total_foreground_style();
        $data['total_background_style']=$this->AdminModel->total_background_style();
        $data['total_page_foreground_style']=$this->AdminModel->total_page_foreground_style();
        $data['total_default_foreground_style']=$this->AdminModel->total_default_foreground_style();
		$this->middle = 'admin/dashboard';
        $this->load->view($this->middle, $data,TRUE);   
        $this->adminLayout();
	}
	public function admin_logout()
	{
		$this->UsersModel->updateLogoutDateTime($this->session->userdata('admin_userid'));
		$this->session->unset_userdata('admin_userid');
		$this->session->unset_userdata('admin_emailid');
		$this->session->unset_userdata('admin_password');
		$this->session->unset_userdata('admin_logged_in');
		$this->session->sess_destroy();
		redirect('/');
	}
	
	public function userLists()
	{
	    $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['userList']=$this->AdminModel->getUserListDetails();
	   	$this->middle = 'admin/usersList';
        $this->load->view($this->middle,$data,TRUE);   
        $this->adminLayout();  
	}
	public function albumBackgroundStyle()
    {
      $this->admin_session_data();
      $data=array();
      $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
      $data['getAlbumBackgroundStyle']=$this->AdminModel->getAlbumBackgroundStyle();
      $this->middle = 'admin/albumBackgroundStyle';
      $this->load->view($this->middle,$data,TRUE);   
      $this->adminLayout();
    }
    public function albumForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['getforegroundStyle']=$this->AdminModel->getforegroundStyle();
        $data['getBackgroundStyle']=$this->ImageUploadModel->select_background_images();
        $this->middle = 'admin/albumForegroundStyle';
        $this->load->view($this->middle,$data,TRUE);
        $this->adminLayout();
    }
    public function pageForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['getPageForegroundStyle']=$this->AdminModel->getPageForegroundStyle();
        $data['getPageBackgroundStyle']=$this->ImageUploadModel->select_page_background_images();
        $this->middle = 'admin/pageForegroundStyle';
        $this->load->view($this->middle,$data,TRUE);
        $this->adminLayout();
    }
    public function defaultBackgroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['getBackgroundStyle']=$this->AdminModel->getBackgroundStyle();
        $this->middle = 'admin/defaultBackgroundStyle';
        $this->load->view($this->middle,$data,TRUE);
        $this->adminLayout();
    }
    public function defaultForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['getForegroundStyle']=$this->AdminModel->getDefaultForegroundStyle();
        $this->middle = 'admin/defaultForegroundStyle';
        $this->load->view($this->middle,$data,TRUE);
        $this->adminLayout();
    }
    public function addDefaultBackgroundStyle()
    {
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['backgroundStyle']))
        {

            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["backgroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["backgroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/DefaultBackgroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('backgroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
           $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->addBackgroundStyle($data);
        if($image_insert)
        {
            redirect('Admin/defaultBackgroundStyle');
        }
    }
    public function addDefaultForegroundStyle()
    {
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['defaultForegroundStyle']))
        {

            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["defaultForegroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["defaultForegroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/DefaultForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('defaultForegroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->addDefaultForegroundStyle($data);
        if($image_insert)
        {
            redirect('Admin/defaultForegroundStyle');
        }
    }
    public function addPageForegroundStyle()
    {
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['foregroundStyle']))
        {
            $data['pageBackgroundStyleId']=$this->input->post('pageBackgroundStyleId');
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["foregroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["foregroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/PageForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('foregroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->addPageForegroundStyle($data);
        if($image_insert)
        {
            redirect('Admin/pageForegroundStyle');
        }
    }
    public function addAlbumForegroundStyle()
    {
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['albumforegroundStyle']))
        {
            $data['albumBackgroundStyleId']=$this->input->post('albumBackgroundStyleId');
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["albumforegroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["albumforegroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/AlbumForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('albumforegroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->addAlbumForegroundStyle($data);
        if($image_insert)
        {
            redirect('Admin/albumForegroundStyle');
        }
    }
    public function addAlbumBackgroundStyle()
    {
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');         
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['albumbackgroundStyle']))
        {
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["albumbackgroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["albumbackgroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }
        
        $directory='uploads/Admin/AlbumBackgroundStyle';
         if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('albumbackgroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->album_background_image($data);
        if($image_insert)
        {
          redirect('Admin/albumBackgroundStyle');
       }
    }
	
    public function pageBackgroundStyle()
    {
      $this->admin_session_data();
      $data=array();
      $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
      $data['getPageBackgroundStyle']=$this->AdminModel->getPageBackgroundStyle();
      $this->middle = 'admin/pageBackgroundStyle';
      $this->load->view($this->middle,$data,TRUE);   
      $this->adminLayout();  
    }
	
    public function addPageBackgroundStyle()
    {
      
        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');         
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        if(!empty($_FILES['pageBackgroundStyle']))
        {
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["pageBackgroundStyle"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["pageBackgroundStyle"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }
        
        $directory='uploads/Admin/PageBackgroundStyle';
         if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('pageBackgroundStyle')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $image_insert=$this->AdminModel->page_background_image($data);
        if($image_insert)
        {
          redirect('Admin/pageBackgroundStyle');
       }
      
        
    }
	
	public function editPageBackgroundStyle()
	{
	 
	    $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
		$data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
		$data['background_image_id']=$this->input->post('page_image_id');		
        if(!empty($_FILES['update_page_style']))
        {
            $getDeletedPageBackgroundStyle=$this->AdminModel->getDeletedPageBackgroundStyle($data['background_image_id']);
            if($getDeletedPageBackgroundStyle)
            {
                $full_image_name=$getDeletedPageBackgroundStyle->upload_path.''.$getDeletedPageBackgroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_page_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_page_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }
        
        $directory='uploads/Admin/PageBackgroundStyle';
         if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_page_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
		$background_image_update=$this->AdminModel->update_page_background_image($data);
        if($background_image_update)
        {
          redirect('Admin/pageBackgroundStyle');
       }

	}
    public function editAlbumForegroundStyle()
    {

        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['foregroundStyleId']=$this->input->post('editForegroundStyleId');
        $data['albumBackgroundStyleId']=$this->input->post('editAlbumBackgroundStyleId');
        if(!empty($_FILES['update_foreground_style']))
        {
            $getDeletedAlbumForegroundStyle=$this->AdminModel->getDeletedAlbumForegroundStyle($data['foregroundStyleId']);
            if($getDeletedAlbumForegroundStyle)
            {
                $full_image_name=$getDeletedAlbumForegroundStyle->upload_path.''.$getDeletedAlbumForegroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_foreground_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_foreground_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/AlbumForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_foreground_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $update_foreground_style=$this->AdminModel->update_album_foreground_style($data);
        if($update_foreground_style)
        {
            redirect('Admin/albumForegroundStyle');
        }

    }
    public function editDefaultForegroundStyle()
    {

        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['editForegroundStyleId']=$this->input->post('editForegroundStyleId');
        if(!empty($_FILES['update_foreground_style']))
        {
            $getDeletedDefaultForegroundStyle=$this->AdminModel->getDeletedDefaultForegroundStyle($data['editForegroundStyleId']);
            if($getDeletedDefaultForegroundStyle)
            {
                $full_image_name=$getDeletedDefaultForegroundStyle->upload_path.'/'.$getDeletedDefaultForegroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_foreground_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_foreground_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/DefaultForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_foreground_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $update_default_foreground_style=$this->AdminModel->update_default_foreground_style($data);
        if($update_default_foreground_style)
        {
            redirect('Admin/defaultForegroundStyle');
        }

    }
    public function editPageForegroundStyle()
    {

        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['foregroundStyleId']=$this->input->post('editForegroundStyleId');
        $data['pageBackgroundStyleId']=$this->input->post('editPageBackgroundStyleId');
        if(!empty($_FILES['update_foreground_style']))
        {
            $getDeletedPageForegroundStyle=$this->AdminModel->getDeletedPageForegroundStyle($data['foregroundStyleId']);
            if($getDeletedPageForegroundStyle)
            {
                $full_image_name=$getDeletedPageForegroundStyle->upload_path.'/'.$getDeletedPageForegroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_foreground_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_foreground_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/PageForegroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_foreground_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $update_foreground_style=$this->AdminModel->update_page_foreground_style($data);
        if($update_foreground_style)
        {
            redirect('Admin/pageForegroundStyle');
        }

    }
    public function editDefaultBackgroundStyle()
    {

        $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $data['editBackgroundStyleId']=$this->input->post('editbackgroundStyleId');
        if(!empty($_FILES['update_background_style']))
        {
            $getDeletedDefaultBackgroundStyle=$this->AdminModel->getDeletedDefaultBackgroundStyle($data['editBackgroundStyleId']);
            if($getDeletedDefaultBackgroundStyle)
            {
                $full_image_name=$getDeletedDefaultBackgroundStyle->upload_path.'/'.$getDeletedDefaultBackgroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_background_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_background_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }

        $directory='uploads/Admin/DefaultBackgroundStyle';
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_background_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
        $update_background_style=$this->AdminModel->update_background_style($data);
        if($update_background_style)
        {
            redirect('Admin/defaultBackgroundStyle');
        }

    }
	public function editAlbumBackgroundStyle()
	{
	 
	    $this->admin_session_data();
        $user_id=$this->session->userdata('admin_userid');
		$data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
		$data['background_image_id']=$this->input->post('album_image_id');		
        if(!empty($_FILES['update_album_background_style']))
        {
            $getDeletedAlbumBackgroundStyle=$this->AdminModel->getDeletedAlbumBackgroundStyle($data['background_image_id']);
            if($getDeletedAlbumBackgroundStyle)
            {
                $full_image_name=$getDeletedAlbumBackgroundStyle->upload_path.'/'.$getDeletedAlbumBackgroundStyle->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }

            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["update_album_background_style"]["name"];
            $data['image_name']=$name;
            $data['image_temp_name']=$_FILES["update_album_background_style"]["tmp_name"];
            $data['created_at']=date('Y-m-d H:i:s');
        }
        
        $directory='uploads/Admin/AlbumBackgroundStyle';
         if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
        }
        $config['upload_path']          = $directory;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $data['image_name'];
        $config['file_permissions'] 	= 0777;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('update_album_background_style')) {
            $error = array('error' => $this->upload->display_errors());
        }
        else {
            $bucket_name='bucketemmortal';
            $source_file=$data['image_temp_name'];
            $image_directory=$directory.'/'.$data['image_name'];
            $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

        }
        $data['uploadPath']=$config['upload_path'];
		$background_image_update=$this->AdminModel->update_album_background_image($data);
        if($background_image_update)
        {
          redirect('Admin/albumBackgroundStyle');
       }

	}
	
	public function deletePageBackgroundStyle()
	{
	  $this->admin_session_data();	
	  $data=array();
	  $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
	  $background_image_id=$this->input->post('pagebackgroundid');
        $getDeletedPageBackgroundStyle=$this->AdminModel->getDeletedPageBackgroundStyle($background_image_id);
        $deletedAllPageForegroundStyle=$this->AdminModel->deletedAllPageForegroundStyle($background_image_id);
        if($deletedAllPageForegroundStyle)
        {
            foreach ($deletedAllPageForegroundStyle as $value)
            {
                $full_image_name=$value->upload_path.'/'.$value->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
        }
        if($getDeletedPageBackgroundStyle)
        {
            $full_image_name=$getDeletedPageBackgroundStyle->upload_path.'/'.$getDeletedPageBackgroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
        $this->AdminModel->update_page_style_id($background_image_id);
        $delete_background_image=$this->AdminModel->delete_page_background($background_image_id);
	   if($delete_background_image)
	   {
		   redirect($this->agent->referrer());
	   }	  
       	  
		
	}
    public function deleteAlbumForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $foregroundStyleId=$this->input->post('foregroundStyleId');
        $getDeletedAlbumForegroundStyle=$this->AdminModel->getDeletedAlbumForegroundStyle($foregroundStyleId);
        if($getDeletedAlbumForegroundStyle)
        {
            $full_image_name=$getDeletedAlbumForegroundStyle->upload_path.'/'.$getDeletedAlbumForegroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
        $delete_forground_style=$this->AdminModel->delete_album_forground_style($foregroundStyleId);
        $this->AdminModel->update_album_forground_style($foregroundStyleId);
        if($delete_forground_style)
        {
            redirect($this->agent->referrer());
        }


    }
    public function deletePageForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $foregroundStyleId=$this->input->post('foregroundStyleId');
        $getDeletedPageForegroundStyle=$this->AdminModel->getDeletedPageForegroundStyle($foregroundStyleId);
        if($getDeletedPageForegroundStyle)
        {
            $full_image_name=$getDeletedPageForegroundStyle->upload_path.'/'.$getDeletedPageForegroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
        $delete_forground_style=$this->AdminModel->delete_page_foreground_style($foregroundStyleId);
        $this->AdminModel->update_page_foreground_id($foregroundStyleId);
        if($delete_forground_style)
        {
            redirect($this->agent->referrer());
        }


    }
    public function deleteDefaultBackgroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $backgroundStyleId=$this->input->post('backgroundStyleId');
        $getDeletedDefaultBackgroundStyle=$this->AdminModel->getDeletedDefaultBackgroundStyle($backgroundStyleId);
        if($getDeletedDefaultBackgroundStyle)
        {
            $full_image_name=$getDeletedDefaultBackgroundStyle->upload_path.'/'.$getDeletedDefaultBackgroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
        $delete_forground_style=$this->AdminModel->delete_background_style($backgroundStyleId);
        if($delete_forground_style)
        {
            redirect($this->agent->referrer());
        }


    }
    public function deleteDefaultForegroundStyle()
    {
        $this->admin_session_data();
        $data=array();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $foregroundStyleId=$this->input->post('foregroundStyleId');
        $getDeletedDefaultForegroundStyle=$this->AdminModel->getDeletedDefaultForegroundStyle($foregroundStyleId);
        if($getDeletedDefaultForegroundStyle)
        {
            $full_image_name=$getDeletedDefaultForegroundStyle->upload_path.'/'.$getDeletedDefaultForegroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
        $delete_default_foreground_style=$this->AdminModel->delete_default_foreground_style($foregroundStyleId);
        if($delete_default_foreground_style)
        {
            redirect($this->agent->referrer());
        }


    }
	public function deleteAlbumBackgroundStyle()
	{
	  $this->admin_session_data();	
	  $data=array();
	  $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
	  $background_image_id=$this->input->post('pagebackgroundid');
        $getDeletedAlbumBackgroundStyle=$this->AdminModel->getDeletedAlbumBackgroundStyle($background_image_id);
        $deletedAllAlbumForegroundStyle=$this->AdminModel->deletedAllAlbumForegroundStyle($background_image_id);
        if($deletedAllAlbumForegroundStyle)
        {
            foreach ($deletedAllAlbumForegroundStyle as $value)
            {
                $full_image_name=$value->upload_path.'/'.$value->image_name;
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
        }
        if($getDeletedAlbumBackgroundStyle)
        {
            $full_image_name=$getDeletedAlbumBackgroundStyle->upload_path.'/'.$getDeletedAlbumBackgroundStyle->image_name;
            unlink($full_image_name);
            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
        }
      $delete_background_image=$this->AdminModel->delete_album_background($background_image_id);
        $this->AdminModel->update_album_background_style($background_image_id);
	   if($delete_background_image)
	   {
		   redirect($this->agent->referrer());
	   }	  
       	  
		
	}
	
	
    
    public function deletedvideo()
    {
	   $this->admin_session_data();
       $data=array();
       $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
	   $data['deleted_video_details']=$this->AdminModel->total_deleted_video();
	   $this->middle = 'admin/deleteVideo';
	   $this->load->view($this->middle,$data,TRUE);   
       $this->adminLayout();
	
	} 	
    
    public function supportquestion()
    {
	   $this->admin_session_data();
	   $data=array();
	   $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
	   $data['user_given_question']=$this->AdminModel->total_user_given_question();
	   $this->middle = 'admin/support_question';
	   $this->load->view($this->middle,$data,TRUE);   
       $this->adminLayout();	
	}	
    public function UserquestionDetails()
    {
	 $this->admin_session_data();
	 $data=array();
	 $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
     $question_id=$this->input->post('qid'); 	 
	 $select_question_details=$this->AdminModel->select_question_details($question_id);
     if(!empty($select_question_details))
     {
		 foreach($select_question_details as $value)
		 {
		   $data['question']=$value['question'];	 
		 }
		 
	 }
      echo json_encode($data);	 
	}
    
    public function UserquestionDelete()
    {
	   $this->admin_session_data();
	   $data=array();
	   $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
       $question_id=$this->input->post('qid');  
	   $delete_question=$this->AdminModel->delete_question($question_id);
	   if($delete_question)
	   {
		   redirect($this->agent->referrer());
	   }		   
	}
    public function profile()
    {
        $this->admin_session_data();
        $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
        $this->middle = 'admin/profile';
        $this->load->view($this->middle, $data,TRUE);
        $this->adminLayout();
    }
    public function accountSettings()
    {
        if ($this->input->post())
        {
            $data = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'fullname' => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                'dob' => $this->input->post('dob')
            );
            $user_id = $this->session->userdata('admin_userid');
            $this->UsersModel->updateGeneralSettings($this->session->userdata('admin_userid'), $data);
            echo json_encode(TRUE);

        }
        else {
            redirect('Admin/profile');
        }
    }
    public function changePassword()
    {
        if ($this->input->post()) {
            $data = array(
                'new_password' => md5($this->input->post('new_password')),
                'current_password' => md5($this->input->post('current_password'))
            );
            $res = $this->UsersModel->userExistChangePassword($this->session->userdata('admin_userid'), $data);
            if ($res) {

                $this->UsersModel->changePasswordSettings($this->session->userdata('admin_userid'), $data);
                echo json_encode(TRUE);
            } else {
                echo json_encode(FALSE);
            }
        } else {
            redirect('Admin/profile');
        }
    }
    public function UploadProfileImage()
    {
        $user_id = $this->session->userdata('admin_userid');
        //update Profile image
        if (!empty($_FILES['profileimage']['name']))
        {
            $directory = 'uploads/' . $user_id . '/profileImage/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            $userData=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
            if($userData['profileimage']!="")
            {
                $full_image_name=$userData['profileimage'];
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $profileimg = array();
            $tmp_name = $_FILES["profileimage"]["tmp_name"];
            $name = $user_id.'_'.time().uniqid(rand()).'_'. $_FILES["profileimage"]["name"];
            $profileimg['profileimage'] = $directory . $name;
            $config['upload_path']          = $directory;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $name;
            $config['file_permissions'] 	= 0777;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('profileimage')) {
                $error = array('error' => $this->upload->display_errors());
            }
            else {
                $bucket_name='bucketemmortal';
                $source_file=$tmp_name;
                $image_directory=$directory.$name;
                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

            }
            $this->UsersModel->updateProfileImage($user_id, $profileimg);
            echo json_encode(TRUE);

        }
    }
	
	public function delete_videoUpload()
	{
	   $this->admin_session_data();
	   $data=array();
	   $data['userdata']=$this->UsersModel->fetchParticularUserData($this->session->userdata('admin_userid'));
       $vimeo_video_id=$this->input->post('vimeo_video_id');
	   $delete_video=$this->AdminModel->delete_video($vimeo_video_id);
	   if($delete_video)
	   {
		   echo json_encode(TRUE);
	   }
	   
	}
	public function activeStatusForegroundStyle()
    {
        if ($this->input->post()) {
            $foregroundStyleId=$this->input->post('id');
            $this->AdminModel->activeStatusForegroundStyle($foregroundStyleId);
            echo json_encode(TRUE);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Admin/dashboard');
        }
    }
    public function deactiveStatusForegroundStyle()
    {
        if ($this->input->post()) {
            $foregroundStyleId=$this->input->post('id');
            $this->AdminModel->deactiveStatusForegroundStyle($foregroundStyleId);
            echo json_encode(TRUE);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Admin/dashboard');
        }
    }
    public function activeStatusBackgroundStyle()
    {
        if ($this->input->post()) {
            $backgroundStyleId=$this->input->post('id');
            $this->AdminModel->activeStatusBackgroundStyle($backgroundStyleId);
            echo json_encode(TRUE);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Admin/dashboard');
        }
    }
    public function deactiveStatusBackgroundStyle()
    {
        if ($this->input->post()) {
            $backgroundStyleId=$this->input->post('id');
            $this->AdminModel->deactiveStatusBackgroundStyle($backgroundStyleId);
            echo json_encode(TRUE);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Admin/dashboard');
        }
    }
}
?>