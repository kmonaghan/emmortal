<?php
ini_set( 'memory_limit', '200M' );
ini_set('upload_max_filesize', '200M');  
ini_set('post_max_size', '200M');  
ini_set('max_input_time', 3600);  
ini_set('max_execution_time', 3600);
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class ImageUpload extends My_Controller{

    public function __construct()
     {
         parent::__construct(); 
		 $this->load->model('ImageUploadModel');
		 $this->load->model('UsersModel');
         $this->load->helper('form');
		 $this->load->library('form_validation');
		 $this->load->driver('mediaRenderer');
         $this->load->library('Aws3');
	}
	
	public function index()
    {
        $this->checkSessionData();
        $this->body = 'Users/imageUpload'; // passing body to function. change this for different views.
        $this->layout();
    }
	
	//session data set or not
	public function checkSessionData()
	{
        $logged_in = $this->session->userdata('logged_in');
        if ((!$logged_in)|| ($logged_in!==true)) {
            redirect('/');
        }
	}
	
    public function multipleImageUpload()
    {
        $data=array();
        $data['total_record']=$this->input->post('total_records');		
		$data['user_id']=$this->input->post('user_id');    
        $data['album_name']=$this->input->post('album_id');
        $data['title']='';
        $data['desc']='';
		if(!empty($data['user_id']))
		{
		    for($i=0;$i<$data['total_record'];$i++)
		    {
					if(!empty($_FILES["multiple_image_upload_".$i]))
					 {
						$uniquesavename=time().uniqid(rand());
						$name = $data['user_id'].'_'.$uniquesavename.'_'.$_FILES["multiple_image_upload_".$i]["name"];
						$data['image_name']=$name;
						$data['image_temp_name']=$_FILES["multiple_image_upload_".$i]["tmp_name"];
						$data['created_at']=date('Y-m-d H:i:s');
					 }
					if($data['album_name']=='-1')
					{
						$data['albumname']="My Chronicles";
						$data['is_default_album']='y';
						$album_details_insert=$this->ImageUploadModel->albumdetails_insert($data);
						$data['albumid']=$album_details_insert;
						$directory='uploads/'.$data['user_id'].'/album/'.$data['albumid'].'/';
					}
					else {
                        $directory = 'uploads/' . $data['user_id'] . '/album/' . $data['album_name'] . '/';
                        $data['albumid'] = $data['album_name'];
                        $data['is_default_album'] = 'n';
                        $data['is_cover_image'] = 'n';
                    }
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $returnArr=array();
                    $data['is_cover_image']='n';
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $data['image_name'];
                    $config['file_permissions'] 	= 0777;
                    $data['image_path']=$config['upload_path'];
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('multiple_image_upload_'.$i)) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$data['image_temp_name'];
                        $image_directory=$directory.$data['image_name'];
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }
                    $data['uploadPath']=$config['upload_path'];
                    $image_insert=$this->ImageUploadModel->insertalbum_image($data);

                    $inner_records=array();
                    $inner_records['albumid']=$data['albumid'];
                    $inner_records['image_id']=$image_insert;
                    $inner_records['user_id']=$data['user_id'];
		    }
            $tempArr=array();
            $get_inserted_image_details=$this->ImageUploadModel->get_inserted_image_details($data['user_id']);
            $k=0;
            if($get_inserted_image_details)
            {
                $returnArr['total_record']=count($get_inserted_image_details);
                $returnArr['album_id']=$data['albumid'];
                foreach($get_inserted_image_details as $val)
                {
                    $returnArr['image_name'][$k]=$val->image_name;
                    $returnArr['image_upload_path'][$k]=$val->uploadPath;
                    $returnArr['image_id'][$k]=$val->id;
                    $k++;
                }
            }
			echo json_encode($returnArr);			
		}
			
    }
    
    public function getSingleimage()
	{
	  $data=array();
	  $returnArr=array();
	  $k=0;
	  $image_id=$this->input->post('imgId');
	  $user_id=$this->session->userdata('userid');	
      $getImageDetails=$this->ImageUploadModel->getImageDetails($image_id);	  
	  $img_id='';
	  if($getImageDetails)
	  {
		 foreach($getImageDetails as $value)
		 {
			$data['image_name']=$value->image_name;   
            $data['image_upload_path']=$value->uploadPath;
            $data['image_id']=$value->id;
			$img_id=$value->id;
        }
	  }
	  $get_inserted_image_details=$this->ImageUploadModel->get_inserted_image_details($user_id);
	  if($get_inserted_image_details)
	  {
			foreach($get_inserted_image_details as $val)
			{
				$returnArr[$k]=$val->id;
				$k++;	  
			}
	  }
	  $total_key=count($returnArr);
	  $key = array_search($img_id, $returnArr);
	  //echo $key."<br/>".$total_key;
	  //print_r($returnArr);
	  if($key===0)
	  {
		 $data['prev_image_id']=$returnArr[$total_key-1];  
	  } 
	  else{
		 $data['prev_image_id']=$returnArr[$key-1];
	  }
	  if(($key+1)>=$total_key)
	  {
		$key=0;
		$data['next_image_id']=$returnArr[$key]; 	 
	  }
	  else
	  {
		$data['next_image_id']=$returnArr[$key+1]; 		  
	  }
	  //print_r($returnArr);
      echo json_encode($data);
	}
	
	public function deleteImageBeforefullInsert()
	{
      $tempArr=array();
	  $returnArr=array();
	  $image_id=$this->input->post('imgId');
	  $user_id=$this->input->post('user_id');
	  $getdeletedImagePath=$this->ImageUploadModel->getdeletedImagePath($image_id,$user_id);
      $full_image_name='';
	  if($getdeletedImagePath) 
      {
		  foreach($getdeletedImagePath as $val)
		  {
			 $full_image_name=$val->uploadPath.''.$val->image_name; 
		  }
		  //echo $full_image_name;
		unlink($full_image_name);
	    $this->aws3->deleteFile('bucketemmortal',$full_image_name);

	  }
	  $deleteImageBeforeInsert=$this->ImageUploadModel->deleteImageBeforeInsert($image_id,$user_id);
	  if($deleteImageBeforeInsert)
	  {
		$get_inserted_image_details=$this->ImageUploadModel->get_inserted_image_details($user_id);
        $k=0;
		if($get_inserted_image_details)
        {
			$returnArr['total_record']=count($get_inserted_image_details);
			foreach($get_inserted_image_details as $val)
			{
				$returnArr['image_name'][$k]=$val->image_name;
				$returnArr['image_upload_path'][$k]=$val->uploadPath;
				$returnArr['image_id'][$k]=$val->id;
				$k++;
			}
        }else
		{
			$returnArr['total_record']=0;
		}
          echo json_encode($returnArr);
	  }
	}

    public function imageUpload()
    {
       	/*echo "<pre>";
		print_r($_POST);
        die('aaaa');*/
		$this->checkSessionData();
        $user_id=$this->session->userdata('userid');
		$data=array();
        $returnArr=array();
		$userdata=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
		$data['user_id']=$user_id;
		$data['title']=$this->input->post('Title');
		$data['desc']=$this->input->post('desc');
		$data['album_name']=$this->input->post('album_name');
        if($data['album_name']=='-1')
        {
            $data['is_default_album']='y';
            $album_details_insert=$this->ImageUploadModel->albumdetails_insert($data);
            $data['album_name']=$album_details_insert;
        }
		$data['friendName']=$this->input->post('friend_name');
		$data['present_album_id']=$this->input->post('present_album_id');
		$data['present_image_id']=$this->input->post('present_image_id');
        $data['thumbnail_image_name']='';
        $data['thumbnail_upload_path']='';
        if($this->input->post('url')!="")
        {
            $getdeletedImagePath=$this->ImageUploadModel->getdeletedImagePath($data['present_image_id'],$user_id);
            if($getdeletedImagePath)
            {
                foreach($getdeletedImagePath as $val)
                {

                    $full_thumbnail_image_name=$val->thumbnail_upload_path.''.$val->thumbnail_image_name;
                    $thum_image_name=$val->thumbnail_image_name;
                    $original_image_name=time().$val->image_name;


                }
                if($full_thumbnail_image_name!=''||$thum_image_name!='')
                {
                    unlink($full_thumbnail_image_name);
	                $this->aws3->deleteFile('bucketemmortal',$full_thumbnail_image_name);
                }

            }

            $directoryThumbnail = 'uploads/'.$user_id.'/album_thumbnail/'.$data['present_album_id'].'/';
            if (!is_dir($directoryThumbnail)) {
                mkdir($directoryThumbnail, 0777, true);
            }
            copy($this->input->post('url'), "$directoryThumbnail/$original_image_name");
            $bucket_name='bucketemmortal';
            $source_file=$this->input->post('url');
            $image_directory=$directoryThumbnail.'/'.$original_image_name;
            $this->aws3->copyFile($bucket_name,$source_file,$image_directory);
            $data['thumbnail_image_name'] = $original_image_name;
            $data['thumbnail_upload_path'] = $directoryThumbnail;

        }
        if($data['album_name']!=$data['present_album_id'])
        {
            $data['album_name']=$data['album_name'];
        }
        else
        {
            $data['album_name']=$data['present_album_id'];
        }

        $data['modified_at']=date('Y-m-d H:i:s');
        $data['is_cover_image']='n';
        $data['albumid']=$data['album_name'];
        //$image_insert=$this->ImageUploadModel->insertalbum_image($data);
        $image_insert=$this->ImageUploadModel->update_album_image_data($data);
        $inner_records=array();
        $inner_records['albumid']=$data['album_name'];
        $inner_records['image_id']=$data['present_image_id'];
        $inner_records['user_id']=$user_id;
        $insert_albuminnerrecordsdragdrop=$this->ImageUploadModel->insert_albuminnerrecordsdragdrop($inner_records);
        $frd_tag=array();
        $frd_tag['image_id']=$data['present_image_id'];
        $frd_tag['user_id']=$user_id;
        $frd_tag['total_frd_tag']=count($data['friendName']);
        $frd_tag['frd_ids']=$data['friendName'];
        $frd_tag['created_at']=date('Y-m-d H:i:s');
        $frd_tag_insert=$this->ImageUploadModel->insertfrd_tag($frd_tag);
        if($image_insert)
        {
            $get_inserted_image_details=$this->ImageUploadModel->get_inserted_image_details($user_id);
            $k=0;
            if($get_inserted_image_details)
            {
                $returnArr['total_record']=count($get_inserted_image_details);
                foreach($get_inserted_image_details as $val)
                {
                    $returnArr['image_name'][$k]=$val->image_name;
                    $returnArr['image_upload_path'][$k]=$val->uploadPath;
                    $returnArr['image_id'][$k]=$val->id;
                    $k++;
                }
            }else
            {
                $returnArr['total_record']=0;
            }
        }
        echo json_encode($returnArr);
	}

	public function album_creation()
	{
		$this->checkSessionData();
        $user_id=$this->session->userdata('userid');
		$data=array();
		$data['background_image_id']=$this->input->post('background_image_id');
        $data['albumForegroundStyleId']=$this->input->post('albumForegroundStyleId');
		$data['user_id']=$user_id;
		$data['title']=$this->input->post('title');
		$data['desc']=$this->input->post('desc');
		$data['view_status']=$this->input->post('permission');
		$data['created_at']=date('Y-m-d H:i:s');
        $data['is_default_album']='n';
		$album_data_insert=$this->ImageUploadModel->album_insertdata($data);
        if(!empty($_FILES["imageUpload"]))
         {
		   $directory='uploads/'.$user_id.'/album/'.$album_data_insert.'/';
		   if (!is_dir($directory)) {
			mkdir($directory, 0777, true);
		   }
             $uniquesavename=time().uniqid(rand());
             $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["imageUpload"]["name"];
             $temp_name=$_FILES["imageUpload"]["tmp_name"];
             $config['upload_path']          = $directory;
             $config['allowed_types']        = 'gif|jpg|png|jpeg';
             $config['file_name']            = $name;
             $config['file_permissions'] 	= 0777;
             $this->load->library('upload', $config);
             $this->upload->initialize($config);
             if ( ! $this->upload->do_upload('imageUpload')) {
                 $error = array('error' => $this->upload->display_errors());
             }
             else {
                 $bucket_name='bucketemmortal';
                 $source_file=$temp_name;
                 $image_directory=$directory.'/'.$name;
                 $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

             }
		   $img_data['albumid']=$album_data_insert;
		   $img_data['image_name']=$name;
		   $img_data['user_id']=$user_id;
		   $img_data['image_temp_name']=$_FILES["imageUpload"]["tmp_name"];
           $img_data['uploadPath']=$config['upload_path'];
		   $img_data['created_at']=date('Y-m-d H:i:s');
		   $img_data['is_cover_image']='y';
		   $album_cover_img_insert=$this->ImageUploadModel->album_cover_image($img_data);
		   redirect('ImageUpload/albumCreation/'.$user_id.'/'.$album_data_insert.'/');
		 }

	}

    public function albumCreation()
    {
        $this->checkSessionData();
        $user_id=$this->uri->segment(3);
        $album_id=$this->uri->segment(4);
        if($user_id && $album_id && $user_id=$this->session->userdata('userid'))
        {
            $check_album_user=$this->ImageUploadModel->check_album_user($user_id,$album_id);
            if($check_album_user)
            {
                $data=array();
                $data['userdata']=$this->UsersModel->fetchPerticularUserData($user_id);
                $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                $select_album_details=$this->ImageUploadModel->select_album_details($album_id);

				foreach($select_album_details as $value)
				{
					$data['present_album_id']=$value['albumid'];
					$data['present_album_title']=$value['title'];
                    $data['present_album_description']=$value['description'];
					$data['background_image_id']=$value['album_background_id'];
                    $data['foreground_image_id']=$value['album_foreground_id'];
				}
                if(!empty($data['foreground_image_id']))
                {
                    $select_foreground_image=$this->ImageUploadModel->select_foreground_image($data['foreground_image_id']);
                    if($select_foreground_image)
                    {
                        foreach($select_foreground_image as $foreground_images)
                        {
                            $data['foreground_image_name']=$foreground_images['image_name'];
                            $data['foreground_upload_path']=$foreground_images['upload_path'];
                        }
                    }

                }
                else
                {
                    $data['foreground_image_name']='';
                    $data['foreground_upload_path']='';
                }
                if(!empty($data['background_image_id']))
                {
                    $select_background_image=$this->ImageUploadModel->select_background_image($data['background_image_id']);
                    if($select_background_image)
                    {
                        foreach($select_background_image as $back_images)
                        {
                            $data['back_image_name']=$back_images['image_name'];
                            $data['back_upload_path']=$back_images['upload_path'];
                        }
                    }
                }
                else
                {
                    $data['back_image_name']="";
                    $data['back_upload_path']="";
                }
                $select_cover_image=$this->ImageUploadModel->getAlbumdetailswithcoverImage($album_id);
                $album_name=$this->ImageUploadModel->select_album_name($user_id);
                $data['total_record_album']=count($album_name);
                for($i=0;$i<count($album_name);$i++)
                {
                    $data['album_title'][$i]=$album_name[$i]['title'];
                    $data['albumeid'][$i]=$album_name[$i]['albumid'];
                }
                $data['total_records']=count($select_cover_image);
                foreach($select_cover_image as $value)
                {
                    $data['image_name']=$value['image_name'];
                    $data['uploadPath']=$value['uploadPath'];
                    $data['present_album_id']=$album_id;
                }
                $user_id=$this->session->userdata('userid');
                $album_comment=$album_comment=$this->ImageUploadModel->get_total_comment($album_id);
                if(!empty($album_comment))
                {
                    $data['album_comment']=$album_comment->total_count;
                }
                else
                {
                    $data['album_comment']=0;
                }
                $toal_like=$this->ImageUploadModel->gettotalAlbumLike($album_id,$user_id);
                if(!empty($toal_like))
                {
                  $data['album_like']=$toal_like;
                }
                else
                {
                 $data['album_like']=0;
                }
                $isUserAlbumLike=$this->ImageUploadModel->isUserAlbumLike($album_id,$this->session->userdata('userid'));
                if(!empty($isUserAlbumLike))
                {
                    $data['user_like_album']='y';
                }
                else
                {
                    $data['user_like_album']='n';
                }
                $data['records'] = $this->ImageUploadModel->getAlbumdetailsuserwise($user_id, $album_id);
                $album_page_id=$this->ImageUploadModel->getPageId($album_id);
                $data['page_ids']=@$album_page_id->page_id;
                $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                $data['title']='Emmortal - Album Creation';
                $this->body ='Users/album_upload_inner_page';
                $this->load->view($this->body, $data,TRUE);
                $this->layout();

            }
            else
            {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                if($this->session->userdata('userid'))
                    redirect('Users/dashboard');
                else
                    redirect('/');
            }
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            if($this->session->userdata('userid'))
                redirect('Users/dashboard');
            else
                redirect('/');
        }
    }


	//public function albumlist($user_id)
	public function albumlist()
	{
		$user_id=$this->uri->segment(3);
		if($user_id)
		{
			$data=array();
            $data['title']='Emmortal - Albums List';
			$data['userdata']=$this->UsersModel->fetchPerticularUserData($user_id);
			if($data['userdata'])
            {
                $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                $album_name=$this->ImageUploadModel->select_album_name($user_id);
                if($album_name)
                {
                    $data['total_record_album']=count($album_name);
                    for($i=0;$i<count($album_name);$i++)
                    {
                        $data['album_title'][$i]=$album_name[$i]['title'];
                        $data['albumeid'][$i]=$album_name[$i]['albumid'];
                    }
                    $album_record_details=$this->ImageUploadModel->getAlbum_details($user_id);
                    $j=0;
                    if($album_record_details)
                    {
                        foreach($album_record_details as $value)
                        {
                            $data['albumid'][$j]=$value['albumid'];
                            $data['album_details'][$j]['albumid']=$value['albumid'];
                            $data['album_details'][$j]['album_title']=$value['title'];
                            $data['album_details'][$j]['is_default_album']=$value['is_default_album'];
                            $j++;
                        }
                    }
                    if(!empty($data['albumid']))
                    {
                        $data['total_album_id']=count($data['albumid']);
                    }
                    else
                    {
                        $data['total_album_id']=0;
                    }
                    for($m=0;$m < $data['total_album_id'];$m++)
                    {
                        $album_cover_image_details=$this->ImageUploadModel->getAlbumdetailswithcoverImage($data['albumid'][$m]);
                        $album_page_id=$this->ImageUploadModel->getPageId($data['albumid'][$m]);
                        $data['album_details'][$m]['page_ids']=@$album_page_id->page_id;


                        if($album_cover_image_details)
                        {
                            foreach($album_cover_image_details as $val)
                            {
                                $data['album_details'][$m]['album_cover_image_name']=$val['image_name'];
                                $data['album_details'][$m]['album_cover_image_uploadPath']=$val['uploadPath'];
                            }
                        }

                        $album_inner_image_details=$this->ImageUploadModel->getAlbumdetailswithinnerImage($data['albumid'][$m],$user_id);
                        $n=0;
                        if($album_inner_image_details)
                        {
                            foreach($album_inner_image_details as $val)
                            {
                                //echo "<pre>";
                                //print_r($val);
                                if($n < 3)
                                {
                                    if($val['type']=='image')
                                    {
                                        $data['album_details'][$m]['album_inner_record_type'][$n]='image';
                                        $data['album_details'][$m]['album_inner_image_uploadPath'][$n]=$val['image_uploadPath'];
                                        $data['album_details'][$m]['album_inner_image_name'][$n]=$val['image_name'];
                                        $data['album_details'][$m]['album_inner_thumbnail_upload_path'][$n]=$val['thumbnail_upload_path'];
                                        $data['album_details'][$m]['album_inner_thumbnail_image_name'][$n]=$val['thumbnail_image_name'];
                                    }
                                    if($val['type']=='text')
                                    {
                                        $data['album_details'][$m]['album_inner_record_type'][$n]='text';
                                        $data['album_details'][$m]['album_inner_text_title'][$n]=$val['text_title'];
                                        $data['album_details'][$m]['album_inner_text_desc'][$n]=$val['text_desc'];
                                    }
                                    if($val['type']=='video')
                                    {
                                        $data['album_details'][$m]['album_inner_record_type'][$n]='video';
                                        $data['album_details'][$m]['album_inner_video_id'][$n]=$val['vimeo_video_id'];
                                    }
                                    $data['album_details'][$m]['total_album_inner_record']=($n+1);
                                    $n++;
                                }

                            }
                            //die('aaa');
                        }

                        $total_like=$this->ImageUploadModel->gettotalAlbumLike($data['albumid'][$m],$user_id);
                        $isUserAlbumLike=$this->ImageUploadModel->isUserAlbumLike($data['albumid'][$m],$user_id);
                        if(!empty($total_like))
                        {
                            $data['album_details'][$m]['total_like']=$total_like;
                        }
                        else
                        {
                            $data['album_details'][$m]['total_like']=0;
                        }
                        $album_comment=$this->ImageUploadModel->get_total_comment($data['albumid'][$m]);
                        if(!empty($album_comment))
                        {
                            $data['album_details'][$m]['album_comment']=$album_comment->total_count;
                        }
                        else
                        {
                            $data['album_details'][$m]['album_comment']=0;
                        }
                        $isUserAlbumLike=$this->ImageUploadModel->isUserAlbumLike($data['albumid'][$m],$this->session->userdata('userid'));
                        if(!empty($isUserAlbumLike))
                        {
                            $data['album_details'][$m]['user_like_album']='y';
                        }
                        else
                        {
                            $data['album_details'][$m]['user_like_album']='n';
                        }
                    }
                }else
                {
                    $data['total_record_album']=0;
                }
                $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                $this->body ='Users/albumlist';
                $this->load->view($this->body, $data,TRUE);
                $this->layout();
            }
			else
            {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                if($this->session->userdata('userid'))
                    redirect('Users/dashboard');
                else
                    redirect('/');
            }


		}
		else
		{
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            if($this->session->userdata('userid'))
                redirect('Users/dashboard');
            else
                redirect('/');
		}
	}

	public function showComments()
	{
		//$this->checkSessionData();
		$user_id=$this->input->post('user_id');
		$album_id=$this->input->post('album_id');
		$data['commentsData']=$this->ImageUploadModel->showCommentsAlbum($album_id);
		$data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($user_id);
        $data['allLikecomment']=$this->UsersModel->allLikecomment();
		echo json_encode($data);
	}

	public function commentLike()
	{
	    //$this->checkSessionData();
		$comment_id=$this->input->post('comment_id');
		$comment_id=$this->input->post('comment_id');
		$data['existLike']=$this->UsersModel->existUserLoginLikeComment($this->session->userdata('userid'),$comment_id);
		if($data['existLike'])
		{
			$this->UsersModel->dislikeComment($this->session->userdata('userid'),$comment_id);
		}
		else
		{
			$likeData=array(
				'comment_id'	=>$comment_id,
				'user_id'	=>$this->session->userdata('userid')
			);
			$this->UsersModel->likeComment($likeData);
		}
		$data['totalCommentLike']=$this->UsersModel->totalCommentLike($comment_id);
		echo json_encode($data);

	}

	public function commentAlbum()
	{
	    $this->checkSessionData();
        if($this->input->post())
        {
			$check=explode('_',$this->input->post('comments_type_id'));

            if (is_numeric($check[0]))
            {
              $comment_text=$this->input->post('comment_text');
                if(empty($_FILES['comment_file']['name']))
                {

                    $commentsdata=array(
                        'user_id'             =>$this->session->userdata('userid'),
                        'album_id'             =>$this->input->post('comments_type_id'),
                        'description'       =>$comment_text
                    );
                }

                else
                {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata=array(
                        'user_id'             =>$this->session->userdata('userid'),
                        'album_id'             =>$this->input->post('comments_type_id'),
                        'description'       =>$this->input->post('comment_text'),
                        'image_comments'    =>$config['upload_path'].$name
                    );
                }
                $this->ImageUploadModel->CommentsAlbum($commentsdata);
                $data['commentsData']=$this->ImageUploadModel->showCommentsAlbum($this->input->post('comments_type_id'));

				$notifyCommentData=array(
				'UID' => $this->session->userdata('userid'),
				'FID'=>$this->input->post('album_user_id'),
				'notify_type'=>'albumComment',
				'notify_type_id'=>$this->input->post('comments_type_id'),
				'notify_details'=>'commented on your Album'
				);
				if($this->session->userdata('userid')!=$this->input->post('album_user_id'))
				$this->UsersModel->insertNotification($notifyCommentData);
            }
            else
            {
                    $check_type=$check[0];
                    $check_id=$check[1];
                    $user_id=$this->input->post('user_id');
                    $friends_id=$this->input->post('friends_id');
                    if($check_type=='image')
                    {
                        if(empty($_FILES['comment_file']['name']))
                        {

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'image_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text')
                            );
                        }

                        else
                        {
                            $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                            if (!is_dir($directory)) {
                                mkdir($directory, 0777, true);
                            }
                            $tmp_name = $_FILES["comment_file"]["tmp_name"];
                            $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                            $config['upload_path']          = $directory;
                            $config['allowed_types']        = 'gif|jpg|png|jpeg';
                            $config['file_name']            = $name;
                            $config['file_permissions'] 	= 0777;
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ( ! $this->upload->do_upload('comment_file')) {
                                $error = array('error' => $this->upload->display_errors());
                            }
                            else {
                                $bucket_name='bucketemmortal';
                                $source_file=$tmp_name;
                                $image_directory=$directory.$name;
                                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                            }

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'image_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text'),
                                'image_comments'    =>$config['upload_path'].$name
                            );
                        }
                        $this->ImageUploadModel->CommentsAlbum($commentsdata);
                       $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumImage($check_id);
					   $notifyCommentData=array(
						'UID' => $this->session->userdata('userid'),
						'FID'=>$this->input->post('album_user_id'),
						'notify_type'=>'imageComment',
						'notify_type_id'=>$check_id,
						'notify_details'=>'commented on your image'
						);
					if($this->session->userdata('userid')!=$this->input->post('album_user_id'))
					$this->UsersModel->insertNotification($notifyCommentData);
                    }
                    if($check_type=='text')
                    {
                        if(empty($_FILES['comment_file']['name']))
                        {

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'text_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text')
                            );
                        }

                        else
                        {
                            $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                            if (!is_dir($directory)) {
                                mkdir($directory, 0777, true);
                            }
                            $tmp_name = $_FILES["comment_file"]["tmp_name"];
                            $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                            $config['upload_path']          = $directory;
                            $config['allowed_types']        = 'gif|jpg|png|jpeg';
                            $config['file_name']            = $name;
                            $config['file_permissions'] 	= 0777;
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ( ! $this->upload->do_upload('comment_file')) {
                                $error = array('error' => $this->upload->display_errors());
                            }
                            else {
                                $bucket_name='bucketemmortal';
                                $source_file=$tmp_name;
                                $image_directory=$directory.$name;
                                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                            }

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'text_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text'),
                                'image_comments'    =>$config['upload_path'].$name
                            );
                        }
                        $this->ImageUploadModel->CommentsAlbum($commentsdata);
                       $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumText($check_id);

					   $notifyCommentData=array(
						'UID' => $this->session->userdata('userid'),
						'FID'=>$this->input->post('album_user_id'),
						'notify_type'=>'textComment',
						'notify_type_id'=>$check_id,
						'notify_details'=>'commented on your text'
						);
					if($this->session->userdata('userid')!=$this->input->post('album_user_id'))
					$this->UsersModel->insertNotification($notifyCommentData);
				 }
                    if($check_type=='video')
                    {
                        if(empty($_FILES['comment_file']['name']))
                        {

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'video_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text')
                            );
                        }

                        else
                        {
                            $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                            if (!is_dir($directory)) {
                                mkdir($directory, 0777, true);
                            }
                            $tmp_name = $_FILES["comment_file"]["tmp_name"];
                            $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                            $config['upload_path']          = $directory;
                            $config['allowed_types']        = 'gif|jpg|png|jpeg';
                            $config['file_name']            = $name;
                            $config['file_permissions'] 	= 0777;
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ( ! $this->upload->do_upload('comment_file')) {
                                $error = array('error' => $this->upload->display_errors());
                            }
                            else {
                                $bucket_name='bucketemmortal';
                                $source_file=$tmp_name;
                                $image_directory=$directory.$name;
                                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                            }

                            $commentsdata=array(
                                'user_id'             =>$this->session->userdata('userid'),
                                'video_id'             =>$check_id,
                                'description'       =>$this->input->post('comment_text'),
                                'image_comments'    =>$config['upload_path'].$name
                            );
                        }
                        $this->ImageUploadModel->CommentsAlbum($commentsdata);
                        $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumVideo($check_id);
						$notifyCommentData=array(
						'UID' => $this->session->userdata('userid'),
						'FID'=>$this->input->post('album_user_id'),
						'notify_type'=>'videoComment',
						'notify_type_id'=>$check_id,
						'notify_details'=>'commented on your video'
						);
					if($this->session->userdata('userid')!=$this->input->post('album_user_id'))
					$this->UsersModel->insertNotification($notifyCommentData);



                    }
              }
            }
            $data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
            $data['allLikecomment']=$this->UsersModel->allLikecomment();
            echo json_encode($data);
	}
	//public function albumdetails($user_id,$album_id)
	public function albumdetails()
    {
		$user_id=$this->uri->segment(3);
		$album_id=$this->uri->segment(4);
        $fetchAlbumDetails=$this->ImageUploadModel->fetchAlbumDetails($album_id);
        if($fetchAlbumDetails['viewstatus']=='friends')
        {
            $friends_data=array();
            if($this->session->userdata('userid'))
            {
                $friends_data = $this->UsersModel->friendsRelationships($user_id, $this->session->userdata('userid'));
            }
            if($friends_data || $this->session->userdata('userid') == $user_id)
            {
                if($user_id && $album_id)
                {
                    $check_album_user=$this->ImageUploadModel->check_album_user($user_id,$album_id);
                    if($check_album_user)
                    {
                        $data = array();
                        $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                        if ($user_id === $this->session->userdata('userid')) {
                            $album_name = $this->ImageUploadModel->select_album_name($user_id);
                            $data['total_record_album'] = count($album_name);
                            for ($i = 0; $i < count($album_name); $i++) {
                                $data['album_title'][$i] = $album_name[$i]['title'];
                                $data['albumeid'][$i] = $album_name[$i]['albumid'];
                            }

                        } else {
                            $data['total_record_album'] = 0;
                        }
                        $data['present_album_id'] = $album_id;
                        $data['userdata'] = $this->UsersModel->fetchPerticularUserData($user_id);
                        $data['album_id'] = $album_id;

                        $isUserAlbumLike = $this->ImageUploadModel->isUserAlbumLike($data['album_id'], $this->session->userdata('userid'));
                        if (!empty($isUserAlbumLike)) {
                            $data['album_details']['user_like_album'] = 'y';
                        } else {
                            $data['album_details']['user_like_album'] = 'n';
                        }
                        $album_records_details = $this->ImageUploadModel->getAlbumdetails($user_id, $album_id);
                        $album_page_id = $this->ImageUploadModel->getPageId($album_id);
                        $data['page_ids'] = @$album_page_id->page_id;
                        $album_cover_image_details = $this->ImageUploadModel->getAlbumdetailswithcoverImage($album_id);
                        if ($album_cover_image_details) {
                            foreach ($album_cover_image_details as $val) {
                                $data['album_cover_image_name'] = $val['image_name'];
                                $data['album_cover_image_uploadPath'] = $val['uploadPath'];
                            }
                        }
                        $album_comment = $this->ImageUploadModel->get_total_comment($album_id);
                        if (!empty($album_comment)) {
                            $data['album_comment'] = $album_comment->total_count;
                        } else {
                            $data['album_comment'] = 0;
                        }
                        $j = 0;
                        $total_like = 0;
                        if ($album_records_details) {
                            foreach ($album_records_details as $value) {
                                $data['albumid'][$j] = $value['albumid'];
                                $data['albumtitle'] = $value['title'];
                                $data['albumDescription'] = $value['description'];
                                $data['present_album_title'] = $data['albumtitle'];
                                $data['is_default_album'] = $value['is_default_album'];
                                $data['album_background_id'] = $value['album_background_id'];
                                if (isset($value['like_id'])) {
                                    $total_like = ($total_like + count($value['like_id']));
                                }
                                $data['foreground_image_id']=$value['album_foreground_id'];
                                if(!empty($data['foreground_image_id']))
                                {
                                    $select_foreground_image=$this->ImageUploadModel->select_foreground_image($data['foreground_image_id']);
                                    if($select_foreground_image)
                                    {
                                        foreach($select_foreground_image as $foreground_images)
                                        {
                                            $data['foreground_image_name']=$foreground_images['image_name'];
                                            $data['foreground_upload_path']=$foreground_images['upload_path'];
                                        }
                                    }
                                }
                                else
                                {
                                    $data['foreground_image_name']='';
                                    $data['foreground_upload_path']='';
                                }
                                $j++;
                            }
                        }
                        $data['total_like'] = $total_like;
                        if (isset($data['album_background_id'])) {
                            $select_background_image = $this->ImageUploadModel->select_background_image($data['album_background_id']);
                            if ($select_background_image) {
                                foreach ($select_background_image as $back_images) {
                                    $data['back_image_name'] = $back_images['image_name'];
                                    $data['back_upload_path'] = $back_images['upload_path'];
                                }
                            } else {
                                $data['back_image_name'] = '';
                                $data['back_upload_path'] = '';

                            }
                        } else {
                            $data['back_image_name'] = '';
                            $data['back_upload_path'] = '';
                            $data['total_like'] = 0;

                        }
                        $data['records'] = $this->ImageUploadModel->getAlbumdetailsuserwise($user_id, $album_id);
                        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                        $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                        //echo "<pre>";
                        //print_r($data);
                        //die('aaaa');
                        $data['title']='Emmortal - Albums Content';
                        $this->body = 'Users/Newly_Created_Album';  //showalbum
                        $this->load->view($this->body, $data, TRUE);
                        $this->layout();
                    }
                    else
                    {
                        $this->session->set_flashdata('invalid_page', 'Page was not found');
                        redirect('Users/dashboard');
                    }

                }
                else
                {
                    $this->session->set_flashdata('invalid_page', 'Page was not found');
                    redirect('Users/dashboard');
                }

            }
            else
            {
                $this->session->set_flashdata('invalid_page', 'In order to view the album you need to become a friend');
                redirect('ImageUpload/albumlist/'.$user_id);
            }
        }
        else
        {
            if($user_id && $album_id)
            {
                $check_album_user=$this->ImageUploadModel->check_album_user($user_id,$album_id);
                if($check_album_user)
                {
                    $data = array();
                    $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    if ($user_id === $this->session->userdata('userid')) {
                        $album_name = $this->ImageUploadModel->select_album_name($user_id);
                        $data['total_record_album'] = count($album_name);
                        for ($i = 0; $i < count($album_name); $i++) {
                            $data['album_title'][$i] = $album_name[$i]['title'];
                            $data['albumeid'][$i] = $album_name[$i]['albumid'];
                        }

                    } else {
                        $data['total_record_album'] = 0;
                    }
                    $data['present_album_id'] = $album_id;
                    $data['userdata'] = $this->UsersModel->fetchPerticularUserData($user_id);
                    $data['album_id'] = $album_id;

                    $isUserAlbumLike = $this->ImageUploadModel->isUserAlbumLike($data['album_id'], $this->session->userdata('userid'));
                    if (!empty($isUserAlbumLike)) {
                        $data['album_details']['user_like_album'] = 'y';
                    } else {
                        $data['album_details']['user_like_album'] = 'n';
                    }
                    $album_records_details = $this->ImageUploadModel->getAlbumdetails($user_id, $album_id);
                    $album_page_id = $this->ImageUploadModel->getPageId($album_id);
                    $data['page_ids'] = @$album_page_id->page_id;
                    $album_cover_image_details = $this->ImageUploadModel->getAlbumdetailswithcoverImage($album_id);
                    if ($album_cover_image_details) {
                        foreach ($album_cover_image_details as $val) {
                            $data['album_cover_image_name'] = $val['image_name'];
                            $data['album_cover_image_uploadPath'] = $val['uploadPath'];
                        }
                    }
                    $album_comment = $this->ImageUploadModel->get_total_comment($album_id);
                    if (!empty($album_comment)) {
                        $data['album_comment'] = $album_comment->total_count;
                    } else {
                        $data['album_comment'] = 0;
                    }
                    $j = 0;
                    $total_like = 0;
                    if ($album_records_details) {
                        foreach ($album_records_details as $value) {
                            $data['albumid'][$j] = $value['albumid'];
                            $data['albumtitle'] = $value['title'];
                            $data['albumDescription'] = $value['description'];
                            $data['present_album_title'] = $data['albumtitle'];
                            $data['is_default_album'] = $value['is_default_album'];
                            $data['album_background_id'] = $value['album_background_id'];
                            if (isset($value['like_id'])) {
                                $total_like = ($total_like + count($value['like_id']));
                            }
                            $data['foreground_image_id']=$value['album_foreground_id'];
                            if(!empty($data['foreground_image_id']))
                            {
                                $select_foreground_image=$this->ImageUploadModel->select_foreground_image($data['foreground_image_id']);
                                if($select_foreground_image)
                                {
                                    foreach($select_foreground_image as $foreground_images)
                                    {
                                        $data['foreground_image_name']=$foreground_images['image_name'];
                                        $data['foreground_upload_path']=$foreground_images['upload_path'];
                                    }
                                }
                            }
                            else
                            {
                                $data['foreground_image_name']='';
                                $data['foreground_upload_path']='';
                            }
                            $j++;
                        }
                    }
                    $data['total_like'] = $total_like;
                    if (isset($data['album_background_id'])) {
                        $select_background_image = $this->ImageUploadModel->select_background_image($data['album_background_id']);
                        if ($select_background_image) {
                            foreach ($select_background_image as $back_images) {
                                $data['back_image_name'] = $back_images['image_name'];
                                $data['back_upload_path'] = $back_images['upload_path'];
                            }
                        } else {
                            $data['back_image_name'] = '';
                            $data['back_upload_path'] = '';

                        }
                    } else {
                        $data['back_image_name'] = '';
                        $data['back_upload_path'] = '';
                        $data['total_like'] = 0;

                    }
                    $data['records'] = $this->ImageUploadModel->getAlbumdetailsuserwise($user_id, $album_id);
                    $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                    $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                    $data['title']='Emmortal - Albums Content';
                    $this->body = 'Users/Newly_Created_Album';  //showalbum
                    $this->load->view($this->body, $data, TRUE);
                    $this->layout();
                }
                else
                {
                    $this->session->set_flashdata('invalid_page', 'Page was not found');
                    redirect('Users/dashboard');
                }

            }
            else
            {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }
        }
    }
    public function scrollLoadAlbumContent()
    {
        if ($this->input->post())
        {
            $data = '';
            $start = $this->input->post('start');
            $user_id = $this->input->post('user_id');
            $album_id = $this->input->post('album_id');
            $albumRecords=$this->ImageUploadModel->getAllAlbumDetailsUserwise($start,$user_id,$album_id);
            $userdata=$this->UsersModel->fetchPerticularUserData($user_id);
            if(count($albumRecords)>0)
            {
                foreach ($albumRecords as $records) 
                {

                    if($records['image_id'])
                    {

                        $data = $data.'<li id="image_li_'.$records['dragdrop_id'].'" class="record reorder-photos-list ui-sortable-handle image_li">';
                        if($this->session->userdata('userid')===$userdata['user_id'])
                        {
                            $data = $data.'<div class="recicn_rel"><i class="fa fa-arrows drag_handle"></i></div>';
                        }
                        $data = $data.'<div class="record">';
                        $data = $data.'<div class="secimg">';
                        $data = $data.'<a href="#" class="image_link">';
                        if(!empty($records['thumbnail_upload_path'])||!empty($records['thumbnail_image_name']))
                        {
                            $data = $data.'<img alt="image" class="lazy album_inner_image" src="'.$this->config->item('cloudfront_base_url').''.$records['thumbnail_upload_path'].''.$records['thumbnail_image_name'].'" data-pid="'.$records['id'].'"/>';

                        }
                        else {
                                $data = $data.'<img alt="image" class="lazy album_inner_image" src="'.$this->config->item('cloudfront_base_url').''.$records['uploadPath'].''.$records['image_name'].'" data-pid="'.$records['id'].'"/>';
                            }
                        $data = $data.'<small>';
                        $data = $data.' <i class="fa fa-comment" aria-hidden="true"></i>';
                        $data = $data.'<span id="comment_perticular_image_'.$records['id'].'">';
                        if(!empty($records['total_comment']))
                        {
                            $data = $data.' '.$records["total_comment"];
                        }
                        else{
                            $data = $data.' 0';
                        }
                        $data = $data.'</span>';
                        $data = $data.' <i class="fa fa-heart" aria-hidden="true"></i>';
                        $data = $data.'<span id="like_particular_image_'.$records['id'].'">';
                        if(!empty($records['total_like'])){
                            $data = $data.' '.$records['total_like'];
                        }
                        else{
                            $data = $data. ' 0';
                        }
                        $data = $data.'</span>';
                        $data = $data.'</small>';
                        $data = $data.'</a>';
                        $data = $data.'</div>';
                        $data = $data .'</div>';
                        $data = $data . '</li>';

                }
                if($records['text_id'])
                {
                    $data = $data.'<li id="image_li_'.$records['dragdrop_id'].'" class="record reorder-photos-list ui-sortable-handle image_li">';
                    if($this->session->userdata('userid')===$userdata['user_id'])
                    {
                        $data = $data.'<div class="recicn_rel"><i class="fa fa-arrows drag_handle"></i></div>';
                    }
                    $data = $data . '<div class="record"><div class="img-responsive secimgtxt">';
                    $data = $data . '<a href="#" class="image_link album_inner_text" data-pid="'.$records['text_id'].'">';
                    $string = strip_tags($records["title"]);
                    if (strlen($string) > 140)
                    {

                        // truncate string
                        $stringCut = substr($string, 0, 140);

                        // make sure it ends in a word so assassinate doesn't become ass...
                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                    }
                    $data = $data.'<p class="text_description">'.$string.'</p>';
                    $data = $data.'<small>';
                    $data = $data.' <i class="fa fa-comment" aria-hidden="true"></i>';
                    $data = $data.'<span id="comment_perticular_text_'.$records['text_id'].'">';
                    if(!empty($records['total_comment']))
                    {
                        $data = $data.' '.$records['total_comment'];
                    }
                    else{
                        $data = $data.' 0';
                    }
                    $data = $data.'</span>';
                    $data = $data.' <i class="fa fa-heart" aria-hidden="true"></i>';
                    $data = $data.'<span id="like_particular_text_'.$records['text_id'].'">';
                    if(!empty($records['total_like'])){
                        $data = $data.' '. $records['total_like'];
                    }
                    else{
                        $data = $data.' 0';
                    }
                    $data = $data.'</span>';
                    $data = $data.'</small>';
                    $data = $data.'</a>';
                    $data = $data.'</div>';
                    $data = $data.'</div>';
                    $data = $data . '</li>';
                }
                if($records['video_id'])
                {
                    $data = $data.'<li id="image_li_'.$records['dragdrop_id'].'" class="record reorder-photos-list ui-sortable-handle image_li">';
                    if($this->session->userdata('userid')===$userdata['user_id'])
                    {
                        $data = $data.'<div class="recicn_rel"><i class="fa fa-arrows drag_handle"></i></div>';
                    }
                    $data = $data.'<div class="vimeoply">';
                    $data = $data.'<input type="hidden" name="video_id" id="video_id" value="'.$records['video_id'].'" />';
                    $data = $data.' <iframe width="420" height="247" src="https://player.vimeo.com/video/'.$records['vimeo_video_id'].'"></iframe>';
                    $data = $data.'<a href="#" class="content_linkvd show_inner_video" data-pid="'.$records['video_id'].'" >';
                    $data = $data.'<small>';
                    $data = $data.' <i class="fa fa-comment" aria-hidden="true"></i>';
                    $data = $data.'<span id="comment_perticular_video_'.$records['video_id'].'">';
                    if(!empty($records['total_comment']))
                    {
                        $data = $data.' '.$records['total_comment'];
                    }
                    else{
                        $data = $data.' 0';
                    }
                    $data = $data.'</span>';
                    $data = $data.' <i class="fa fa-heart" aria-hidden="true"></i>';
                    $data = $data.'<span id="like_particular_video_'.$records['video_id'].'">';
                    if(!empty($records['total_like'])){
                        $data = $data.' '.$records['total_like'];
                    }
                    else{
                        $data = $data.' 0';
                    }
                    $data = $data.'</span>';
                    $data = $data.'</small>';
                    $data = $data.'</a>';
                    $data = $data.'</div>';
                    $data = $data.'</div>';
                    $data = $data . '</li>';

                }

            }

        }
        else
        {
            $data = $data . '<h2 class="text-center">There\'s no content for this album yet.</h2>';
        }
        echo $data;
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');

        }
    }
	public function updateImageOrder()
	{
		$this->checkSessionData();
		$inner_record_id=$this->input->post('ids');
		//$idArray = explode(",",$inner_record_id);
      	$insert_image_order=$this->ImageUploadModel->insertImageOrder($inner_record_id);
		if($insert_image_order)
		{
			return true;
		}
	}

	public function deletePageIdInAlbumDetails()
	{
		$user_id=$this->input->post('user_id');
		$album_id=$this->input->post('album_id');
		$page_id=$this->input->post('page_id');
		$delete_pageId=$this->ImageUploadModel->delete_pageId($user_id,$album_id,$page_id);
		if($delete_pageId)
		{
		 return true;
		}
	}

	public function addAlbumlike()
    {
        if ($this->input->post())
        {
            $data['user_id']=$this->input->post('user_id');
            $data['session_user_id']=$this->session->userdata('userid');
            $data['album_id']=$this->input->post('album_id');
            $data['album_user_id']=$this->input->post('album_user_id');
            $data['created_at']=date('Y-m-d H:i:s');
            $insertAlbumlike=$this->ImageUploadModel->insertAlbumlike($data);
            if($insertAlbumlike)
            {
                $data['totalLike']=$insertAlbumlike;
                $isUserAlbumLike=$this->ImageUploadModel->isUserAlbumLike( $data['album_id'],$this->session->userdata('userid'));
                if($isUserAlbumLike)
                {
                    $data['isUserAlbumLike']='yes';
                }
                else
                {
                    $data['isUserAlbumLike']='no';
                }
                echo json_encode($data);
            }else
            {
                $data['totalLike']=0;
                $isUserAlbumLike=$this->ImageUploadModel->isUserAlbumLike( $data['album_id'],$this->session->userdata('userid'));
                if($isUserAlbumLike)
                {
                    $data['isUserAlbumLike']='yes';
                }
                else
                {
                    $data['isUserAlbumLike']='no';
                }
                echo json_encode($data);
            }

        }
		else
		{
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
		}

    }

    public function addImagelike()
    {
        if ($this->input->post())
        {
            $data['session_user_id']=$this->session->userdata('userid');
            $data['user_id']=$this->input->post('user_id');
            $data['image_id']=$this->input->post('image_id');
            $data['album_user_id']=$this->input->post('album_user_id');
            $data['created_at']=date('Y-m-d H:i:s');
            $insertImagelike=$this->ImageUploadModel->insertImagelike($data);
            if($insertImagelike)
            {
                $data['totalLike']=$insertImagelike;
                $isUserImageLike=$this->ImageUploadModel->isUserImageLike( $data['image_id'],$this->session->userdata('userid'));
                if($isUserImageLike)
                {
                    $data['isUserImageLike']='yes';
                }
                else
                {
                    $data['isUserImageLike']='no';
                }
                echo json_encode($data);
            }else
            {
                $data['totalLike']=0;
                $isUserImageLike=$this->ImageUploadModel->isUserImageLike( $data['image_id'],$this->session->userdata('userid'));
                if($isUserImageLike)
                {
                    $data['isUserImageLike']='yes';
                }
                else
                {
                    $data['isUserImageLike']='no';
                }
                echo json_encode($data);
            }
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function addTextlike()
    {
        if ($this->input->post())
        {
            $data['session_user_id']=$this->session->userdata('userid');
            $data['user_id']=$this->input->post('popup_user_id');
            $data['text_id']=$this->input->post('popup_text_id');
            $data['album_user_id']=$this->input->post('album_user_id');

		  $data['created_at']=date('Y-m-d H:i:s');
		  $insertTextlike=$this->ImageUploadModel->insertTextlike($data);
            if($insertTextlike)
            {
                $data['totalLike']=$insertTextlike;
                $isUserTextLike=$this->ImageUploadModel->isUserTextLike( $data['text_id'],$this->session->userdata('userid'));
                if($isUserTextLike)
                {
                    $data['isUserTextLike']='yes';
                }
                else
                {
                    $data['isUserTextLike']='no';
                }
                echo json_encode($data);
            }else
            {
                $data['totalLike']=0;
                $isUserTextLike=$this->ImageUploadModel->isUserTextLike( $data['text_id'],$this->session->userdata('userid'));
                if($isUserTextLike)
                {
                    $data['isUserTextLike']='yes';
                }
                else
                {
                    $data['isUserTextLike']='no';
                }
                echo json_encode($data);
            }
		}
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

	public function addvideolike()
    {
        if ($this->input->post()) {
            $data['session_user_id'] = $this->session->userdata('userid');
            $data['user_id'] = $this->input->post('user_id');
            $data['videoid'] = $this->input->post('videoid');
            $data['album_user_id'] = $this->input->post('album_user_id');
            $data['created_at'] = date('Y-m-d H:i:s');
            $insertVideolike = $this->ImageUploadModel->insertVideolike($data);
            if($insertVideolike)
            {
                $data['totalLike']=$insertVideolike;
                $isUserVideoLike=$this->ImageUploadModel->isUserVideoLike( $data['videoid'],$this->session->userdata('userid'));
                if($isUserVideoLike)
                {
                    $data['isUserVideoLike']='yes';
                }
                else
                {
                    $data['isUserVideoLike']='no';
                }
                echo json_encode($data);
            }else
            {
                $data['totalLike']=0;
                $isUserVideoLike=$this->ImageUploadModel->isUserVideoLike( $data['videoid'],$this->session->userdata('userid'));
                if($isUserVideoLike)
                {
                    $data['isUserVideoLike']='yes';
                }
                else
                {
                    $data['isUserVideoLike']='no';
                }
                echo json_encode($data);
            }
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }


    }


    public function image_details()
    {
        //$this->checkSessionData();
		$data=array();
		$image_id=$this->input->post('image_id');
        $album_id=$this->input->post('album_id');
		$userId=$this->input->post('user_id');
        if(!empty($userId))
        {
			$select_image_details=$this->ImageUploadModel->select_image_details($image_id,$album_id,$userId);
			$select_tagFrd_details=$this->ImageUploadModel->select_tagFrd_details($image_id,$album_id,$userId);
			$data['get_next_image_id']=$this->ImageUploadModel->get_next_image_id($image_id,$album_id,$userId);
            $data['get_prev_image_id']=$this->ImageUploadModel->get_prev_image_id($image_id,$album_id,$userId);
			if($select_image_details)
			{
				foreach($select_image_details as $value)
				{
					if(!empty($value['profileimage']))
					{
						$data['profileimage']=$value['profileimage'];
					}
					else
					{
						$data['profileimage']="assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg";
					}
                    if(!empty($value['thumbnail_image_name']) && !empty($value['thumbnail_upload_path']))
                    {
                        $data['image_src']=$value['thumbnail_upload_path'].$value['thumbnail_image_name'];
                    }
                    else
                    {
                        $data['image_src']=$value['uploadPath'].$value['image_name'];
                    }
					$data['image_title']=$value['image_title'];
					$data['image_desc']=$value['image_desc'];
					$data['image_id']=$value['id'];
					$data['albumid']=$value['albumid'];
					$data['page_id']=$value['page_id'];
					$data['uploadPath']=$value['uploadPath'];
					$data['image_name']=$value['image_name'];
					$data['album_title']=$value['title'];
					$data['user_name']=$value['fullname'];


				}
				//die('aaaaa');
			}
			$n=0;
            $data['total_frd_tag']=count($select_tagFrd_details);
			if($select_tagFrd_details)
			{
				foreach($select_tagFrd_details as $value)
				{
					$data['records'][$n]['url']=base_url()."Users/profile/".$value['frdTagId']."?page_id=".$value['frdTagPageId'];
					$data['records'][$n]['name']=$value['frdTagname'];
					$n++;
				}
			}

			$select_total_like_image=$this->ImageUploadModel->select_total_like_image($image_id);
			if($select_total_like_image)
			{
				foreach($select_total_like_image as $value)
				{
					$data['total_like']=$value['total_like'];
				}
			}

			$select_total_comment_image=$this->ImageUploadModel->gettotal_image_comment($image_id);
			if($select_total_comment_image)
			{
				foreach($select_total_comment_image as $value)
				{
					$data['total_comment']=$value;
				}
			}

			$select_page_id=$this->ImageUploadModel->get_page_id_for_image($image_id);
			if(!empty($select_page_id))
			{
				$data['page_ids']=$select_page_id->page_id;
			}
			else
			{
			 $data['page_ids']=0;
			}
			if($this->session->userdata('userid'))
            {
                $isUserImageLike=$this->ImageUploadModel->isUserImageLike( $data['image_id'],$this->session->userdata('userid'));
                if($isUserImageLike)
                {
                    $data['isUserImageLike']='yes';
                }
                else
                {
                    $data['isUserImageLike']='no';
                }
            }
            else
            {
                $data['isUserImageLike']='no';
            }
			//echo "<pre>";
			//print_r($data);
			echo json_encode($data);
		}


    }

	public function delete_directory($dirname) {
			 if (is_dir($dirname))
			   $dir_handle = opendir($dirname);
		 if (!$dir_handle)
			  return false;
		 while($file = readdir($dir_handle)) {
			   if ($file != "." && $file != "..") {
					if (!is_dir($dirname."/".$file))
						 unlink($dirname."/".$file);
					else
						 delete_directory($dirname.'/'.$file);
			   }
		 }
		 closedir($dir_handle);
		 rmdir($dirname);
		 return true;
	}




    public function removeAlbum($album_id)
	{
		$this->checkSessionData();
		$user_id=$this->session->userdata('userid');
        $remove_album_data=$this->ImageUploadModel->remove_album_data($album_id,$user_id);
        if($remove_album_data)
        {
            $this->aws3->deleteDirectory('bucketemmortal','uploads/'.$user_id.'/album/'.$album_id);
            $this->aws3->deleteDirectory('bucketemmortal','uploads/'.$user_id.'/album_thumbnail/'.$album_id);
			$this->delete_directory('uploads/'.$user_id.'/album/'.$album_id);
            $this->delete_directory('uploads/'.$user_id.'/album_thumbnail/'.$album_id);
			//rmdir('uploads/'.$user_id.'/album/'.$album_id);
			redirect('ImageUpload/albumlist/'.$user_id);
        }
	}
    public function removeAlbumImage($album_id,$image_id)
    {
        $this->checkSessionData();
		$user_id=$this->session->userdata('userid');

        $getdeletedImagePath=$this->ImageUploadModel->getdeletedImagePath($image_id,$user_id);
        $full_image_name='';
        $full_thumbnail_image_name='';
        if($getdeletedImagePath)
        {
            foreach($getdeletedImagePath as $val)
            {
                if($val->uploadPath!=''||$val->image_name!='')
                {
                    $full_image_name=$val->uploadPath.''.$val->image_name;
                }
                if($val->thumbnail_upload_path!=''||$val->thumbnail_image_name!='')
                {
                    $full_thumbnail_image_name=$val->thumbnail_upload_path.''.$val->thumbnail_image_name;
                }

            }
            if($full_image_name!='')
            {
                unlink($full_image_name);
	            $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            if($full_thumbnail_image_name!='')
            {
                unlink($full_thumbnail_image_name);
	            $this->aws3->deleteFile('bucketemmortal',$full_thumbnail_image_name);
            }


        }
        $remove_image_data=$this->ImageUploadModel->remove_perticular_image($album_id,$user_id,$image_id);
        if($remove_image_data)
        {
            //redirect('ImageUpload/albumdetails/'.$user_id.'/'.$album_id);
			redirect($this->agent->referrer());
        }
    }

    public function edit_album()
    {
		$this->checkSessionData();
		$data=array();
        $album_id=$this->input->post('album_id');
        $user_id=$this->input->post('user_id');
        $collect_album_data=$this->ImageUploadModel->collect_album_data($album_id,$user_id);
		$total_record=count($collect_album_data);
		foreach($collect_album_data as $value)
        {
            $data['album_background_image_id']=$value['album_background_id'];
            $data['album_foreground_id']=$value['album_foreground_id'];
        }
		$collect_album_cover_image_data=$this->ImageUploadModel->collect_album_cover_image_data($album_id);
        $data['album_id']=$album_id;
		if($collect_album_data)
        {
            foreach($collect_album_data as $value)
            {


                if(empty($value['title']))
				{
				  $data['title']='My chronicles';
				}
				else
                {
				  $data['title']=$value['title'];
				}
                $data['description']=$value['description'];
                $data['viewstatus']=$value['viewstatus'];

			}
		}
        if($collect_album_cover_image_data)
        {
            foreach($collect_album_cover_image_data as $value)
            {
    			  $data['image_name']=$value['image_name'];
	        	  $data['uploadPath']=$value['uploadPath'];
				  $data['image_id']=$value['id'];
            }
        }else
		{
		        $data['image_name']="no_cover.jpg";
	        	$data['uploadPath']="assets/img/";
		}
		$select_background_images=$this->ImageUploadModel->select_background_images();
	    $data['total_background_images']=count($select_background_images);
		if($select_background_images)
		{
			$j=0;
			foreach($select_background_images as $value)
			{
			  $data['back_ground_image_id'][$j]=$value['id'];
			  $data['back_ground_image_upload_path'][$j]=$value['upload_path'];
			  $data['back_ground_image_image_name'][$j]=$value['image_name'];
			  $j++;
			}
		}
        $select_foreground_images=$this->ImageUploadModel->select_foreground_images($data['album_background_image_id']);
        $data['total_foreground_images']=count($select_foreground_images);
        if($select_foreground_images)
        {
            $j=0;
            foreach($select_foreground_images as $value)
            {
                $data['foreground_image_id'][$j]=$value['id'];
                $data['foreground_upload_path'][$j]=$value['upload_path'];
                $data['foreground_image_name'][$j]=$value['image_name'];
                $j++;
            }
        }
		echo json_encode($data);
    }

     public function edit_inner_image()
    {
        //$this->checkSessionData();
		$data=array();
		$user_id=$this->session->userdata('userid');
        $album_id=$this->input->post('album_id');
        $image_id=$this->input->post('image_id');
        $collect_inner_image_data=$this->ImageUploadModel->collect_inner_image_data($album_id,$image_id,$user_id);
		$total_friend_list=$this->ImageUploadModel->search_friendName($user_id);
		$j=0;
		if($collect_inner_image_data)
        {
            foreach($collect_inner_image_data as $value)
            {
               /* if($value['thumbnail_image_name']&&$value['thumbnail_upload_path'])
                {
                    $data['uploadPath']=$value['thumbnail_upload_path'];
                    $data['image_name']=$value['thumbnail_image_name'];
                }
                else
                {*/
                    $data['uploadPath']=$value['uploadPath'];
                    $data['image_name']=$value['image_name'];
                    /* }*/

                $data['image_title']=$value['image_title'];
                $data['image_desc']=$value['image_desc'];
				if(!empty($value['fullname']))
                {
                 $data['tag_user_fullname'][$j]=$value['fullname'];
                }
                if(!empty($value['userid']))
                {
                 $data['tag_user_id'][$j]=$value['userid'];
                }

				$j++;
            }
			$data['image_id']=$image_id;
        }
		$k=0;
		if($total_friend_list)
		{
			foreach($total_friend_list as $value)
			{
				$data['friend_userid'][$k]=$value->userid;
				$data['friend_fullName'][$k]=$value->fullname;
				$k++;
			}
		}
		$album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
		$data['total_record_album'] = count($album_name);
		for ($i = 0; $i < count($album_name); $i++) {
		$data['album_title'][$i] = $album_name[$i]['title'];
		$data['albumeid'][$i] = $album_name[$i]['albumid'];
		}
        echo json_encode($data);
    }
    public function editImage()
    {
        $this->checkSessionData();
        $image_id=$this->input->post('image_id');
        $album_id=$this->input->post('single_image_album_id');
        $user_id=$this->session->userdata('userid');
        $data=array();
        $data['image_id']=$image_id;
        $data['user_id']=$user_id;
        $data['title']=$this->input->post('Title');
        $data['desc']=$this->input->post('desc');
        $data_tag_frd['tag_friend_name']=$this->input->post('tag_friend_name');
        $data_tag_frd['total_frd_tag']=count($data_tag_frd['tag_friend_name']);
        $data_tag_frd['user_id']=$user_id;
        $data_tag_frd['created_at']=date('Y-m-d H:i:s');
        $edit_tag_friend_list=$this->ImageUploadModel->edit_tag_friend_list($data_tag_frd,$image_id);
        if($album_id!=$this->input->post('album_name'))
        {
            $data['album_name']=$this->input->post('album_name');
        }
        else
        {
            $data['album_name']=$album_id;
        }
        $select_default_album_name=$this->ImageUploadModel->select_default_album_name($data['album_name']);

        if($select_default_album_name)
        {
            foreach($select_default_album_name as $value)
            {
                $data['is_default_album']=$value['is_default_album'];
            }
        }
        if(!empty($_FILES["editimageUpload"]["name"]))
        {
            $getdeletedImagePath=$this->ImageUploadModel->getdeletedImagePath($image_id,$user_id);
            $full_thumbnail_image_name='';
            $full_image_name='';
            if($getdeletedImagePath)
            {
                foreach($getdeletedImagePath as $val)
                {
                    if($val->uploadPath!=''||$val->image_name!='')
                    {
                        $full_image_name=$val->uploadPath.''.$val->image_name;
                    }
                    if($val->thumbnail_upload_path!=''||$val->thumbnail_image_name!='')
                    {
                        $full_thumbnail_image_name=$val->thumbnail_upload_path.''.$val->thumbnail_image_name;
                    }

                }
                if($full_image_name!='')
                {
                    unlink($full_image_name);
	                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
                }
                if($full_thumbnail_image_name!='')
                {
                    unlink($full_thumbnail_image_name);
	                $this->aws3->deleteFile('bucketemmortal',$full_thumbnail_image_name);
                }


            }

            $uniquesavename=time().uniqid(rand());
            $name = $user_id.'_'.$uniquesavename.'_'.$_FILES["editimageUpload"]["name"];
            if($data['is_default_album']=='y')
            {
                $directory='uploads/'.$user_id.'/album/default/';
                $directoryThumbnail = 'uploads/'.$user_id.'/album/default_thumbnail/'.$data['album_name'].'/';
            }
            else
            {
                $directory='uploads/'.$user_id.'/album/'.$data['album_name'].'/';
                $directoryThumbnail = 'uploads/'.$user_id.'/album_thumbnail/'.$data['album_name'].'/';
            }

            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            if (!is_dir($directoryThumbnail)) {
                mkdir($directoryThumbnail, 0777, true);
            }

            $config['upload_path']          = $directory;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $name;
            $this->load->library('upload', $config);

            $field_name = 'editimageUpload';
            if ( ! $this->upload->do_upload('editimageUpload')) {
                $error = array('error' => $this->upload->display_errors());
            }
            else {
                $uploadedImage = $this->upload->data();
                $data['image_name'] = $name;
                $data['uploadPath'] = $config['upload_path'];
                if($this->input->post('url')!="")
                {
                    copy($this->input->post('url'),"$directoryThumbnail/$name");
                    $bucket_name='bucketemmortal';
                    $source_file=$this->input->post('url');
                    $image_directory=$directoryThumbnail.'/'.$name;
                    $this->aws3->copyFile($bucket_name,$source_file,$image_directory);
                    $data['thumbnail_image_name'] = $name;
                    $data['thumbnail_upload_path'] = $directoryThumbnail;
                }
                else
                {
                    $data['thumbnail_image_name'] = "";
                    $data['thumbnail_upload_path'] = "";
                }

            }
        }
        else
        {
            if($this->input->post('url')!="")
            {
                $getdeletedImagePath=$this->ImageUploadModel->getdeletedImagePath($image_id,$user_id);
                if($getdeletedImagePath)
                {
                    foreach($getdeletedImagePath as $val)
                    {

                        $full_thumbnail_image_name=$val->thumbnail_upload_path.''.$val->thumbnail_image_name;
                        $thum_image_name=$val->thumbnail_image_name;
                        $original_image_upload_path=$val->uploadPath.''.$val->image_name;
                        $original_image_name=time().$val->image_name;


                    }
                    if($full_thumbnail_image_name!=''||$thum_image_name!='')
                    {
                        unlink($full_thumbnail_image_name);
	                    $this->aws3->deleteFile('bucketemmortal',$full_thumbnail_image_name);
                    }

                }

                if($data['is_default_album']=='y')
                {
                    $directoryThumbnail = 'uploads/'.$user_id.'/album/default_thumbnail/'.$data['album_name'].'/';
                }
                else
                {
                    $directoryThumbnail = 'uploads/'.$user_id.'/album_thumbnail/'.$data['album_name'].'/';
                }
                if (!is_dir($directoryThumbnail)) {
                    mkdir($directoryThumbnail, 0777, true);
                }
               copy($this->input->post('url'),"$directoryThumbnail/$original_image_name");
                $bucket_name='bucketemmortal';
                $source_file=$this->input->post('url');
                $image_directory=$directoryThumbnail.'/'.$original_image_name;
                $this->aws3->copyFile($bucket_name,$source_file,$image_directory);
                $data['thumbnail_image_name'] = $original_image_name;
                $data['thumbnail_upload_path'] = $directoryThumbnail;


            }
        }
        $data['modified_at']=date('Y-m-d H:i:s');
        $data['is_cover_image']='n';
        $data['albumid']=$data['album_name'];
        $image_insert=$this->ImageUploadModel->update_album_image($data);
        if($image_insert)
        {
            //redirect('ImageUpload/albumdetails/'.$user_id.'/'.$album_id);
            redirect($this->agent->referrer());
        }
    }
	
	public function editAlbumDetails()
	{
		/*echo "<pre>";
		print_r($_POST);
		
		//print_r($_FILES["imageUpload"]["name"]);
		die('aaa');*/
		$this->checkSessionData();
        $data=array();
        $user_id=$this->session->userdata('userid'); 		
        $data['album_id']=$this->input->post('album_id');
        $data['user_id']=$user_id;
		if(!empty($this->input->post('image_id')))
		{
		  $data['image_id']=$this->input->post('image_id');	
		}
		
		$data['title']=$this->input->post('title');
		$data['desc']=$this->input->post('desc');
		$data['viewstatus']=$this->input->post('permission');
		if(!empty($this->input->post('edit_background_image_id')))
		{
		 $data['edit_background_image_id']=$this->input->post('edit_background_image_id');	
		}
        if(!empty($this->input->post('editAlbumForegroundStyleId')))
        {
            $data['editAlbumForegroundStyleId']=$this->input->post('editAlbumForegroundStyleId');
        }
		if(!empty($_FILES["imageUpload"]["name"]))
         {
            $uniquesavename=time().uniqid(rand());
			$name = $user_id.'_'.$uniquesavename.'_'.$_FILES["imageUpload"]["name"];
			$data['image_name']=$name;
			$data['image_temp_name']=$_FILES["imageUpload"]["tmp_name"];
			$data['modified_at']=date('Y-m-d H:i:s');
			$data['is_cover_image']='y';
			$directory='uploads/'.$user_id.'/album/'.$data['album_id'].'/';
			if (!is_dir($directory)) {
					mkdir($directory, 0777, true);
			 }
             $config['upload_path']          = $directory;
             $config['allowed_types']        = 'gif|jpg|png|jpeg';
             $config['file_name']            = $data['image_name'];
             $config['file_permissions'] 	= 0777;
             $this->load->library('upload', $config);
             $this->upload->initialize($config);
             if ( ! $this->upload->do_upload('imageUpload')) {
                 $error = array('error' => $this->upload->display_errors());
             }
             else {
                 $bucket_name='bucketemmortal';
                 $source_file=$data['image_temp_name'];
                 $image_directory=$directory.$data['image_name'];
                 $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

             }
			$data['uploadPath']=$config['upload_path'];
		 }
		 if($this->input->post('selected_choose_album_image_name') && $this->input->post('selected_choose_album_upload_path'))
         {
             $data['image_name']=$this->input->post('selected_choose_album_image_name');
             $data['uploadPath']=$this->input->post('selected_choose_album_upload_path');

         }
		$image_insert=$this->ImageUploadModel->update_album_details($data);
		if($image_insert)
		{
			//redirect('ImageUpload/albumdetails/'.$user_id.'/'.$data['album_id']);
            redirect($this->agent->referrer());
		}
	}
    
    public function addText()
    {
        $this->checkSessionData();
        $data=array();
		$data['user_id']=$this->session->userdata('userid'); 
        $data['text_title']=$this->input->post('text_title');
        $data['text_desc']=$this->input->post('text_desc');
        $album_name=$this->input->post('album_name');
		$data['friendName']=$this->input->post('friend_name');
        $data['created_at']=date('Y-m-d H:i:s');
		$album_details=array();
		if($album_name=='-1')
		{
			$album_details['albumname']="My Chronicles";
			$album_details['album_name']=$album_name;
            $album_details['is_default_album']='y';
			$album_details['created_at']=date('Y-m-d H:i:s');
			$album_details['user_id']=$data['user_id'];
		}
        else
        { 
          	$album_details['album_name']=$album_name;
			$album_details['is_default_album']='n';
            $album_details['created_at']=date('Y-m-d H:i:s');
            $album_details['user_id']=$data['user_id'];
            
        }
		$album_details_insert=$this->ImageUploadModel->albumdetails_insert($album_details);
		if(!empty($album_details_insert))
		{
			$data['album_name']=$album_details_insert;
		}
		$text_insert=$this->ImageUploadModel->add_text_insert($data);
		$inner_records=array();
		$inner_records['albumid']=$data['album_name'];
		$inner_records['text_id']=$text_insert;
		$inner_records['user_id']=$data['user_id'];
		$albuminnerrecordsdragdrop_insert=$this->ImageUploadModel->insert_albuminnerrecordsdragdrop($inner_records);
		$frd_tag=array();
		$frd_tag['text_id']=$text_insert;
		$frd_tag['user_id']=$this->session->userdata('userid');
		$frd_tag['total_frd_tag']=count($data['friendName']);
		$frd_tag['frd_ids']=$data['friendName'];
		$frd_tag['created_at']=date('Y-m-d H:i:s');
		$frd_tag_insert=$this->ImageUploadModel->insertfrd_tag_text($frd_tag);		
		if($frd_tag_insert)
        {
            redirect('ImageUpload/albumdetails/'.$data['user_id'].'/'.$data['album_name']);
        }
                  
    }
     public function friend_list_details()
     {
		$this->checkSessionData();
        $data=array();
		$return_array=array();
		$data['user_id']=$this->session->userdata('userid');
        $return_array['album_id']=$this->input->post('album_id'); 
        $friendname_list=$this->ImageUploadModel->friend_list_userwise($data['user_id']);
		$option="";
		if($friendname_list)
		{
			foreach($friendname_list as $value)
			{
				$option=$option.'<option value="'.$value->userid.'">'.$value->fullname.'</option>';
			}
		}
		echo $option;
	 }
	 public function select_text_details()
	 {
		 //$this->checkSessionData();  
         $data=array();
         $edit_text_id=$this->input->post('edit_text_id'); 
         $user_id=$this->input->post('user_id'); 
         $album_id=$this->input->post('album_id');
		 	 
				 $select_text_details=$this->ImageUploadModel->select_text($edit_text_id,$album_id,$user_id);
                $data['get_next_text_id']=$this->ImageUploadModel->get_next_text_id($edit_text_id,$album_id,$user_id);
                 $data['get_prev_text_id']=$this->ImageUploadModel->get_prev_text_id($edit_text_id,$album_id,$user_id);
				 $select_tagFrd_details=$this->ImageUploadModel->search_text_Tag_friendName($user_id,$edit_text_id);
				 
				 if($select_text_details)
				 {
					 foreach($select_text_details as $value)
					 {
						if(!empty($value['profileimage']))
						{
						 $data['profileimage']=$value['profileimage'];	
						}
						else
						{
						 $data['profileimage']="assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg";		
						}

						
						$data['textid']=$value['textid'];
						$data['text_title']=$value['text_title'];
						$data['text_description']=$value['text_description'];
						$data['albumid']=$value['album_id'];
						$data['fullname']=$value['fullname'];
						$data['userid']=$value['user_id'];
						$data['album_title']=$value['title'];
						$data['addeddate']=$value['addeddate'];
						$data['userid']=$value['userid'];
						$data['pageid']=$value['pageid'];
						
					 }
					 //die('aaaa');
				 }
				 
				 $n=0;
				 if($select_tagFrd_details)
				 {
				   $data['total_frd_tag']=count($select_tagFrd_details);
				   foreach($select_tagFrd_details as $value)
				   {
						$data['records'][$n]['url']=base_url()."Users/profile/".$value['frdTagId']."?page_id=".$value['frdTagPageId'];
						$data['records'][$n]['name']=$value['frdTagname'];
						$n++;
				   }
				 }else
				 {
					 $data['total_frd_tag']=0;
				 }
				 $select_total_like_text=$this->ImageUploadModel->select_total_like_text($edit_text_id);
				if($select_total_like_text)
				{
					foreach($select_total_like_text as $value)
					{
						$data['total_like']=$value['total_like'];
					}
				}
				$select_total_comment_text=$this->ImageUploadModel->gettotal_text_comment($edit_text_id);
				if($select_total_comment_text)
				{
					foreach($select_total_comment_text as $value)
					{
						$data['total_comment']=$value;
					}
				}
                
                $select_page_id=$this->ImageUploadModel->get_page_id_for_text($edit_text_id);
                if(!empty($select_page_id))
                {
                    $data['page_ids']=$select_page_id->page_id;
                }
                else
                {
                 $data['page_ids']=0;    
                }
                 if($this->session->userdata('userid'))
                 {
                     $isUserTextLike=$this->ImageUploadModel->isUserTextLike( $edit_text_id,$this->session->userdata('userid'));
                     if($isUserTextLike)
                     {
                         $data['isUserTextLike']='yes';
                     }
                     else
                     {
                         $data['isUserTextLike']='no';
                     }
                 }
                 else
                 {
                     $data['isUserImageLike']='no';
                 }
                echo json_encode($data); 
		 
	 }	
	 public function select_edit_album_text()
     {
         $this->checkSessionData();  
         $data=array();
         $edit_text_id=$this->input->post('edit_text_id'); 
         $user_id=$this->session->userdata('userid'); 
         $album_id=$this->input->post('album_id');
         $select_text=$this->ImageUploadModel->select_edit_text_details($edit_text_id,$album_id,$user_id);
         $k=0;
         if($select_text)
         {
             foreach($select_text as $value)
             {
                $data['textid']=$value['textid'];
				$data['text_title']=$value['title'];
				$data['text_description']=$value['description'];
                if(!empty($value['friends_id']))
                {
                  $data['tag_user_id'][$k]=$value['friends_id'];
				  $data['friend_fullName'][$k]=$value['fullname'];
                }
				$data['album_title']=$value['title'];
                $data['addeddate']=$value['addeddate'];
				$data['userid']=$value['user_id'];
                $k++;
             }
         }
         //die('aaaa');
            $k=0;
            $total_friend_list=$this->ImageUploadModel->search_friendName($user_id);
            if($total_friend_list)
            {
                foreach($total_friend_list as $value)
                {
                    $data['friend_userid'][$k]=$value->userid;
                    $data['friend_fullName'][$k]=$value->fullname;
                    $k++;
                }
            }
			$album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
			$data['total_record_album'] = count($album_name);
			 $m=0;
			  foreach($album_name as $val)
			  {
				$data['albumTitle'][$m]=$val['title'];
			    $data['albumeid'][$m]=$val['albumid'];
				$m++;	
			  }
			echo json_encode($data);
     }
	 
	 public function editText()
	 {
		 $this->checkSessionData();
		 $data=array();
		 $user_id=$this->session->userdata('userid'); 
		 $data['edit_text_title']=$this->input->post('edit_text_title');
		 $data['text_desc']=$this->input->post('text_desc');
		 $data['album_id']=$this->input->post('album_name');
		 $data['text_id']=$this->input->post('edit_text_id');
		 $data['friendName']=$this->input->post('tag_friend_name');
		 $update_text=$this->ImageUploadModel->update_text($data);		 
		 $frd_tag=array();
		 $frd_tag['text_id']=$data['text_id'];
		 $frd_tag['user_id']=$user_id;
		 $frd_tag['total_frd_tag']=count($data['friendName']);
		 $frd_tag['frd_ids']=$data['friendName'];
		 $frd_tag['created_at']=date('Y-m-d H:i:s');
		 $frd_tag_insert=$this->ImageUploadModel->edit_tag_friend_list_text($frd_tag,$data['text_id']);
		 if($frd_tag_insert)
		 {
			//redirect ('ImageUpload/albumdetails/'.$user_id.'/'.$data['album_id']);
			redirect($this->agent->referrer());			
	    }
		 
	 }
	 public function friendsList()
	 {
		$this->checkSessionData();  
		$user_id=$this->session->userdata('userid'); 
		$search_friendName=$this->ImageUploadModel->search_friendName($user_id);
		$data='';
        foreach($search_friendName as $sf)
		{
			$data=$data.'<option value="'.$sf->userid.'">'.$sf->fullname.'</option>';
		}
        echo $data;		
       		
	 }
	 
	 public function removeAlbumText($album_id,$text_id)
	 {
		$this->checkSessionData();   
		$user_id=$this->session->userdata('userid');
        $remove_image_data=$this->ImageUploadModel->remove_perticular_text($album_id,$user_id,$text_id);
        if($remove_image_data)
        { 
            //redirect('ImageUpload/albumdetails/'.$user_id.'/'.$album_id);
			redirect($this->agent->referrer());	
        }
    }
	public function select_background_images()
	{
	  $this->checkSessionData();   
	  $data=array();
	  $select_background_images=$this->ImageUploadModel->select_background_images();
	  $data['total_background_images']=count($select_background_images);
	  if($select_background_images)
	  {
		  $j=0;
		  foreach($select_background_images as $value)
		  {
			  $data['id'][$j]=$value['id'];
			  $data['upload_path'][$j]=$value['upload_path'];
			  $data['image_name'][$j]=$value['image_name'];
			  $j++;
		  }
	  }
	  echo json_encode($data); 
	  
	}
    public function select_foreground_images()
    {
        $this->checkSessionData();
        if ($this->input->post()) {
            $albumBackgroundStyleId=$this->input->post('albumBackgroundStyleId');
            $data = array();
            $select_foreground_images = $this->ImageUploadModel->select_foreground_images($albumBackgroundStyleId);
            $data['total_foreground_images'] = count($select_foreground_images);
            if ($select_foreground_images) {
                $j = 0;
                foreach ($select_foreground_images as $value) {
                    $data['id'][$j] = $value['id'];
                    $data['upload_path'][$j] = $value['upload_path'];
                    $data['image_name'][$j] = $value['image_name'];
                    $j++;
                }
            }
            echo json_encode($data);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }

    }
    public function select_page_foreground_images()
    {
        $this->checkSessionData();
        if ($this->input->post()) {
            $pageBackgroundStyleId=$this->input->post('pageBackgroundStyleId');
            $data = array();
            $select_foreground_images = $this->ImageUploadModel->select_page_foreground_images($pageBackgroundStyleId);
            $data['total_foreground_images'] = count($select_foreground_images);
            if ($select_foreground_images) {
                $j = 0;
                foreach ($select_foreground_images as $value) {
                    $data['id'][$j] = $value['id'];
                    $data['upload_path'][$j] = $value['upload_path'];
                    $data['image_name'][$j] = $value['image_name'];
                    $j++;
                }
            }
            echo json_encode($data);
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }

    }
	public function select_page_background_images()
	{
		$this->checkSessionData();   
		$data=array();
		$data['select_page_background_images']=$this->ImageUploadModel->select_page_background_images();
        $pageBackgroundStyleId=$this->input->post('pageBackgroundStyleId');
        $data['select_page_foreground_images']=$this->ImageUploadModel->select_page_foreground_images($pageBackgroundStyleId);
		echo json_encode($data); 
	
	}  	
	
	 public function videoUpload()
	{
		$this->checkSessionData();   
	    $data=array();
		$user_id=$this->session->userdata('userid');
		$data['user_id']=$user_id;
		$album_name=$this->input->post('album_id');
		$data['video_id']=$this->input->post('video_id');
		$data['video_title']=$this->input->post('title');
		$data['video_desc']=$this->input->post('desc');
		$data['video_friends_tag']=$this->input->post('friends_id');
		$data['total_frd_tag']=count($this->input->post('friends_id'));
		$data['created_at']=date('Y-m-d H:i:s');
		$album_details=array();
		if($album_name=='-1')
		{
			
			$album_details['albumname']="My Chronicles";
			$album_details['album_name']=$album_name;
            $album_details['is_default_album']='y';
			$album_details['created_at']=date('Y-m-d H:i:s');
			$album_details['user_id']=$data['user_id'];
		}
        else
        { 
            
			$album_details['album_name']=$album_name;
			$album_details['is_default_album']='n';
			$album_details['created_at']=date('Y-m-d H:i:s');
			$album_details['user_id']=$data['user_id'];
        }
		$album_details_insert=$this->ImageUploadModel->albumdetails_insert($album_details);
		if(!empty($album_details_insert))
		{
			$data['album_id']=$album_details_insert;
		}
		$video_indatabase=$this->ImageUploadModel->insert_video_details($data);
		$inner_records=array();
		$inner_records['albumid']=$data['album_id'];
		$inner_records['video_id']=$video_indatabase;
		$inner_records['user_id']=$user_id;
		$albuminnerrecordsdragdrop_insert=$this->ImageUploadModel->insert_albuminnerrecordsdragdrop($inner_records);
		$return_array=array();		
		if($albuminnerrecordsdragdrop_insert)
		{
			$return_array['user_id']=$user_id;
			$return_array['album_id']=$data['album_id'];
			
		  echo json_encode($return_array);	
		}	
	
	}
	
	public function select_video_details()
	{
		$data['user_id']=$this->input->post('user_id');;
		$data['album_id']=$this->input->post('album_id');
		$data['video_id']=$this->input->post('video_id');
		$select_video_details_in_popup=$this->ImageUploadModel->select_video_details_in_popup($data);
        $result_array['get_next_video_id']=$this->ImageUploadModel->get_next_video_id($data['video_id'],$data['album_id'],$data['user_id']);
        $result_array['get_prev_video_id']=$this->ImageUploadModel->get_prev_video_id($data['video_id'],$data['album_id'],$data['user_id']);
		$select_video_tagFrd_details=$this->ImageUploadModel->select_video_tagFrd_details($data);
		$m=0;
		$result_array=array();
		if($select_video_details_in_popup)
		{
			foreach($select_video_details_in_popup as $value)
			{
				if(!empty($value['profileimage']))
				{
				 $result_array['profileimage']=$value['profileimage'];	
				}
				else
				{
				  $result_array['profileimage']="assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg";		
				}
				$result_array['user_id']=$value['user_id'];
				$result_array['video_id']=$value['id'];
				$result_array['albumid']=$value['albumid'];
				$result_array['video_title']=$value['video_title'];
			 	$result_array['video_desc']=$value['video_desc'];
			 	$result_array['user_fullname']=$value['fullname'];
			 	$result_array['pageid']=$value['pageid'];
			 	$result_array['album_title']=$value['title'];
			 	$result_array['vimeo_video_id']=$value['vimeo_video_id'];
				$result_array['video_url']='<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0" src="https://player.vimeo.com/video/'.$value['vimeo_video_id'].'"></iframe>';
			}
		}
		 //die('aaaa');
		if($select_video_tagFrd_details)
		{
			$n=0;
			$result_array['total_frd_tag']=count($select_video_tagFrd_details);
			foreach($select_video_tagFrd_details as $value)
			{
				/* echo "<pre>";
				print_r($value); */
				$result_array['records'][$n]['url']=base_url()."Users/profile/".$value['frdTagId']."?page_id=".$value['frdTagPageId'];
				$result_array['records'][$n]['name']=$value['frdTagname'];
				$n++;
			}
		}
		
		$select_total_like_video=$this->ImageUploadModel->gettotal_video_like($data['video_id']);
		if($select_total_like_video)
		{
			foreach($select_total_like_video as $value)
			{
				$result_array['total_like']=$value;
			}
		}
		$select_total_comment_video=$this->ImageUploadModel->gettotal_video_comment($data['video_id']);
		if($select_total_comment_video)
		{
			foreach($select_total_comment_video as $value)
			{
				$result_array['total_comment']=$value;
			}
		}
        
        $select_page_id=$this->ImageUploadModel->get_page_id_for_video($data['video_id']);
        if(!empty($select_page_id))
        {
            $result_array['page_ids']=$select_page_id->page_id;
        }
        else
        {
          $result_array['page_ids']=0;    
        }
        if($this->session->userdata('userid'))
        {
            $isUserVideoLike=$this->ImageUploadModel->isUserVideoLike($data['video_id'],$this->session->userdata('userid'));
            if($isUserVideoLike)
            {
                $result_array['isUserVideoLike']='yes';
            }
            else
            {
                $result_array['isUserVideoLike']='no';
            }
        }
        else
        {
            $result_array['isUserVideoLike']='no';
        }
		echo json_encode($result_array);
	}
	
    public function showInnerImageComments()
    {
       //$this->checkSessionData();  
       $data=array();  
        $user_id=$this->input->post('user_id');
        $image_id=$this->input->post('image_id');
        $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumImage($image_id);
        //$data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
        $data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($user_id);
        $data['allLikecomment']=$this->UsersModel->allLikecomment();
        echo json_encode($data);
    }
    
    public function showInnerTextComments()
    {
       //$this->checkSessionData();  
       $data=array();  
        $user_id=$this->input->post('user_id');
        $text_id=$this->input->post('text_id');
        $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumText($text_id);
        //$data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
        $data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($user_id);
        $data['allLikecomment']=$this->UsersModel->allLikecomment();
        echo json_encode($data);
    } 
	
    public function showInnerVideoComments()
    {
       //$this->checkSessionData();  
       $data=array();  
        $user_id=$this->input->post('user_id');
        $video_id=$this->input->post('video_id');
        $data['commentsData']=$this->ImageUploadModel->showCommentsAlbumVideo($video_id);
        //$data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
        $data['relationshipUsers']=$this->UsersModel->fetchAllRelationshipUsers($user_id);
        $data['allLikecomment']=$this->UsersModel->allLikecomment();
        echo json_encode($data);
    }
    
	
	public function select_edit_album_video()
	{
		 //$this->checkSessionData();  
         $data=array();
         $edit_video_id=$this->input->post('edit_video_id'); 
         $user_id=$this->session->userdata('userid'); 
         $album_id=$this->input->post('album_id');
         $data['album_owner_id']=$user_id;
		 $select_video=$this->ImageUploadModel->select_video($edit_video_id,$album_id,$user_id);
         if($select_video)
         {
             foreach($select_video as $value)
             {
				$data['video_id']=$value['id'];
				$data['vimeo_video_id']=$value['vimeo_video_id'];
				$data['video_url']='<iframe width="420" height="247" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" src="https://player.vimeo.com/video/'.$value['vimeo_video_id'].'"></iframe>';
             }
         }
		 $tag_friend=$this->ImageUploadModel->search_video_Tag_friendName($user_id,$edit_video_id);
		 $j=0;
		 if($tag_friend)
		 {
			foreach($tag_friend as $value)
			{
			  $data['tag_user_fullname'][$j]=$value['fullname'];
			  $data['tag_user_id'][$j]=$value['userid'];
			  $j++;
		    }
		 }
		 $total_friend_list=$this->ImageUploadModel->search_friendName($user_id);
		 $k=0;
		 if($total_friend_list)
		 {
			foreach($total_friend_list as $value)
			{
				$data['friend_userid'][$k]=$value->userid;
				$data['friend_fullName'][$k]=$value->fullname;
				$k++;
			}
		 }
        $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
        $data['total_record_album'] = count($album_name);
        for ($i = 0; $i < count($album_name); $i++) {
            $data['album_title'][$i] = $album_name[$i]['title'];
            $data['albumeid'][$i] = $album_name[$i]['albumid'];
        }
		 /*echo "<pre>";
		 print_r($data);
		 die('aaaa');*/
		 //return $data;
		 echo json_encode($data);
	}
	
	public function editvideoUpload()
    {
     $this->checkSessionData();  
     $data=array();	
     $user_id=$this->session->userdata('userid'); 
	 $data['album_id']=$this->input->post('album_name');
	 $data['video_id']=$this->input->post('edit_video_id');
	 $data['friendName']=$this->input->post('friend_name');
	 $update_video=$this->ImageUploadModel->update_video($data);
	 $frd_tag=array();
	 $frd_tag['video_id']=$data['video_id'];
	 $frd_tag['user_id']=$user_id;
	 $frd_tag['total_frd_tag']=count($data['friendName']);
	 $frd_tag['frd_ids']=$data['friendName'];
	 $frd_tag['created_at']=date('Y-m-d H:i:s');
	 $frd_tag_insert=$this->ImageUploadModel->edit_tag_friend_list_video($frd_tag);
	 if($frd_tag_insert)
	 {
		redirect ($this->agent->referrer());
	 }	 
  }	
   
   public function removeAlbumVideo($album_id,$video_id)
   {
     $this->checkSessionData();  
     $data=array();	
     $user_id=$this->session->userdata('userid');	 
	 $del_video=$this->ImageUploadModel->delete_video($album_id,$video_id,$user_id);
     if($del_video)
	 {
		 //redirect ('ImageUpload/albumdetails/'.$user_id.'/'.$album_id);
		 redirect($this->agent->referrer());
	 }		 
   }
   
   public function dashboard_image_details()
   {
	   $this->checkSessionData();  
       $data=array();	
	   $image_id=$this->input->post('image_id');
	   $image_details=$this->ImageUploadModel->getdashboardImageDetails($image_id);
	   if($image_details)
	   {
		   foreach($image_details as $value)
		   {
			 $data['image_id']=$value['id'];  
			 $data['image_title']=$value['image_title'];  
			 $data['image_desc']=$value['image_desc'];  
			 $data['uploadPath']=$value['uploadPath'];  
			 $data['image_name']=$value['image_name'];  
		   }
	   }
	   echo json_encode($data);
   }



   
	public function addPage()
	{
		$this->checkSessionData();
		if($this->input->post())
		{
			if($this->input->post('page_style_id'))
			{
				$data=array(
								'user_id'		=>$this->session->userdata('userid'),
								'page_title'	=>$this->input->post('emm_page_title'),
								'page_style_id'	=>$this->input->post('page_style_id'),
                                'page_foreground_id'	=>$this->input->post('pageForegroundStyleId')
							);
			}
			else
			{
				$data=array(
								'user_id'		=>$this->session->userdata('userid'),
								'page_title'	=>$this->input->post('emm_page_title')
							);
			}
			$this->ImageUploadModel->addPage($data);
            $data['userdata']=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
			redirect("Users/profile/".$data['userdata']['userid']."?page_id=".$data['userdata']['pageid']);
		
		}
		else
		{
			redirect('Users/dashboard');
		}
	}
	public function selectAllPageDetails()
	{
	  $this->checkSessionData();
      $user_id=$this->input->post('user_id');
	  $album_id=$this->input->post('album_id');
      $data=array();
	  $selectAllPageList=$this->ImageUploadModel->selectAllPageList($user_id);
      $k=0;
	  if($selectAllPageList)
	  {
		foreach($selectAllPageList as $value)
		{
		    if($value['default_page']==1)
            {
                if(empty($value['page_title']))
                {
                    $value['page_title']="Default Page";
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }
            }
            else
            {

                if(empty($value['page_title']))
                {
                    $value['page_title']="Page ".$k;
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }

            }
		  $data['page_name'][$k]=$value['page_title'];
		  $data['page_id'][$k]=$value['pageid'];
		  $k++;
		}	
	  }
	  $select_current_page_id=$this->ImageUploadModel->selectCurrentPage($user_id,$album_id);
      if(!empty($select_current_page_id))
	  {
		  $data['current_page_id']=$select_current_page_id->page_id;
	  }else
	  {
		$data['current_page_id']=0;  
	  }
	  
	  echo json_encode($data);
	}
	public function selectAllPageDetailsText()
	{
	  $this->checkSessionData();
      $user_id=$this->input->post('user_id');
	  $album_id=$this->input->post('album_id');
	  $text_id=$this->input->post('text_id');
      $data=array();
	  $selectAllPageList=$this->ImageUploadModel->selectAllPageList($user_id);
	  $k=0;
	  if($selectAllPageList)
	  {
		foreach($selectAllPageList as $value)
		{
            if($value['default_page']==1)
            {
                if(empty($value['page_title']))
                {
                    $value['page_title']="Default Page";
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }
            }
            else
            {

                if(empty($value['page_title']))
                {
                    $value['page_title']="Page ".$k;
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }

            }
		  $data['page_name'][$k]=$value['page_title'];
		  $data['page_id'][$k]=$value['pageid'];
		  $k++;
		}	
	  }
	  $select_current_page_id=$this->ImageUploadModel->selectCurrentPageforText($user_id,$text_id);
	  //print_r($select_current_page_id);
	  
	  if(!empty($select_current_page_id))
	  {
		  $data['current_page_id']=$select_current_page_id->page_id;
	  }
	  else
	  {
		$data['current_page_id']=0;  
	  }
	  
	  echo json_encode($data);
	}
	public function selectAllPageDetailsvideo()
	{
	  $this->checkSessionData();
      $user_id=$this->input->post('user_id');
	  $album_id=$this->input->post('album_id');
	  $video_id=$this->input->post('video_id');
      $data=array();
	  $selectAllPageList=$this->ImageUploadModel->selectAllPageList($user_id);
      $k=0;
	  if($selectAllPageList)
	  {
		foreach($selectAllPageList as $value)
		{
            if($value['default_page']==1)
            {
                if(empty($value['page_title']))
                {
                    $value['page_title']="Default Page";
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }
            }
            else
            {

                if(empty($value['page_title']))
                {
                    $value['page_title']="Page ".$k;
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }

            }
		  $data['page_name'][$k]=$value['page_title'];
		  $data['page_id'][$k]=$value['pageid'];
		  $k++;
		}	
	  }
	  $select_current_page_id=$this->ImageUploadModel->selectCurrentPageforvideo($user_id,$video_id);
      if(!empty($select_current_page_id))
	  {
		  $data['current_page_id']=$select_current_page_id->page_id;
	  }else
	  {
		$data['current_page_id']=0;  
	  }
	  
	  echo json_encode($data);
	}
	
	public function selectAllPageDetailsimage()
	{
	  $this->checkSessionData();
      $user_id=$this->input->post('user_id');
	  $album_id=$this->input->post('album_id');
	  $image_id=$this->input->post('image_id');
      $data=array();
	  $selectAllPageList=$this->ImageUploadModel->selectAllPageList($user_id);
      $k=0;
	  if($selectAllPageList)
	  {
		foreach($selectAllPageList as $value)
		{
            if($value['default_page']==1)
            {
                if(empty($value['page_title']))
                {
                    $value['page_title']="Default Page";
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }
            }
            else
            {

                if(empty($value['page_title']))
                {
                    $value['page_title']="Page ".$k;
                }
                else
                {
                    $value['page_title']=$value['page_title'];
                }

            }
		  $data['page_name'][$k]=$value['page_title'];
		  $data['page_id'][$k]=$value['pageid'];
		  $k++;
		}	
	  }
	  $select_current_page_id=$this->ImageUploadModel->selectCurrentPageforimage($user_id,$image_id);
      if(!empty($select_current_page_id))
	  {
		  $data['current_page_id']=$select_current_page_id->page_id;
	  }else
	  {
		  $data['current_page_id']=0;
	  }
	  
	  echo json_encode($data);
	}
	
	public function InsertPageIdInAlbumDetails()
	{
	  $this->checkSessionData();
      $user_id=$this->input->post('user_id');
	  $album_id=$this->input->post('album_id');
	  $page_id=$this->input->post('page_id');
	  $insert_page_id=$this->ImageUploadModel->insert_page_id($user_id,$album_id,$page_id); 
	}
	
	public function InsertPageIdInImageDetails()
    {
      $this->checkSessionData();
      $image_id=$this->input->post('image_id');
      $album_id=$this->input->post('album_id');
      $page_id=$this->input->post('page_id');
      $user_id=$this->session->userdata('userid'); 
      $insert_page_id=$this->ImageUploadModel->insert_page_id_to_image($image_id,$user_id,$page_id);     
      
    }
    public function deletePageIdInImageDetails()
    {
      //$this->checkSessionData();
	  $image_id=$this->input->post('image_id');
      $album_id=$this->input->post('album_id');
      $page_id=$this->input->post('page_id');
      $user_id=$this->session->userdata('userid'); 
      $delete_page_id=$this->ImageUploadModel->delete_page_id_to_image($image_id,$user_id,$page_id); 
      if($delete_page_id)
        {
         return true;    
        }     
      
    }
    public function deletePageIdInVideoDetails()
    {
      $this->checkSessionData();
      $video_id=$this->input->post('video_id');
      $album_id=$this->input->post('album_id');
      $page_id=$this->input->post('page_id');
      $user_id=$this->session->userdata('userid'); 
      $delete_page_id=$this->ImageUploadModel->delete_page_id_to_video($video_id,$user_id,$page_id); 
      if($delete_page_id)
        {
         return true;    
        }     
      
    }
    public function deletePageIdInTextDetails()
	{
	  $this->checkSessionData();
      $text_id=$this->input->post('text_id');
	  $album_id=$this->input->post('album_id');
	  $page_id=$this->input->post('page_id');
	  $user_id=$this->session->userdata('userid'); 
	  $delete_page_id=$this->ImageUploadModel->delete_page_id_to_text($text_id,$user_id,$page_id); 
      if($delete_page_id)
        {
         return true;    
        } 	
	  
	}
	public function InsertPageIdInVideoDetails()
	{
	  $this->checkSessionData();
      $video_id=$this->input->post('video_id');
	  $album_id=$this->input->post('album_id');
	  $page_id=$this->input->post('page_id');
	  $user_id=$this->session->userdata('userid'); 
	  $insert_page_id=$this->ImageUploadModel->insert_page_id_to_video($video_id,$user_id,$page_id); 	
	  
	}
	public function InsertPageIdInTextDetails()
	{
	  $this->checkSessionData();
      $text_id=$this->input->post('text_id');
	  $album_id=$this->input->post('album_id');
	  $page_id=$this->input->post('page_id');
	  $user_id=$this->session->userdata('userid');
	  //echo $page_id."<br/>".$text_id."<br/>".$album_id;
	  $insert_page_id=$this->ImageUploadModel->insert_page_id_to_text($text_id,$user_id,$page_id); 	
	  
	}
	
	public function videoRemove()
	{
	  $db_video_id=$this->input->post('video_id');
	  $album_id=$this->input->post('album_id');
	  $user_id=$this->input->post('user_id');
	  $total_friend_list=$this->ImageUploadModel->search_friendName($user_id);
	  $k=0;
  	  if($total_friend_list)
	  {
		foreach($total_friend_list as $value)
		{
				$data['friend_userid'][$k]=$value->userid;
				$data['friend_fullName'][$k]=$value->fullname;
				$k++;
		}
	  }
	  $data['user_id']=$user_id;
	  $data['album_id']=$album_id;
	  $delete_video=$this->ImageUploadModel->delete_video($album_id,$db_video_id,$user_id);
	  if($delete_video)
	  {
		echo json_encode($data);  
	  }	  
	}
	
	public function fetchAllImageInAlbum()
    {
        $this->checkSessionData();
        if($this->input->post())
        {
            $album_id=$this->input->post('album_id');
            $user_id=$this->session->userdata('userid');
            $data['fetchAllImageInAlbum']=$this->ImageUploadModel->fetchAllImageInAlbum($user_id,$album_id);
            echo json_encode($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
	
	
}
