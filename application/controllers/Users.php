<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UsersModel');
        $this->load->model('ImageUploadModel');
        $this->load->library('Aws3');
    }
    public function sitemap()
    {

        $data = array(
            'name'=>'home',
        );
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->body = 'Users/sitemap'; // passing body to function. change this for different views.
        $this->load->view($this->body, $data, TRUE);
        $this->layout();
    }
    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        $admin_logged_in = $this->session->userdata('admin_logged_in');
        if (($logged_in)||($logged_in==true)) {
            redirect('Users/dashboard');
        }
        else
        {
            if (($admin_logged_in)||($admin_logged_in==true)) {
                redirect('Admin/dashboard');
            }
        }
        $data['title']='Emmortal - Log In or Sign Up';
        $data['CountAllHomeContent'] = $this->UsersModel->CountAllHomeContent();
        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
        $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
        $this->body = 'Users/home'; // passing body to function. change this for different views.
        $this->load->view($this->body, $data, TRUE);
        $this->layout();
    }

    public function newsfeed()
    {
        $this->checkSessionData();
        $data['title']='Emmortal - Newsfeed';
        $data['userdata'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        $data['friendsdata'] = $this->UsersModel->friendsNewsfeed($this->session->userdata('userid'));
        $data['tagfriendcontentall'][]=array();
        foreach ($data['friendsdata'] as $friendsdata) {
            $data['tagfriendcontentall'][$friendsdata->userid] = $this->UsersModel->tagFriendsAllContent($this->session->userdata('userid'), $friendsdata->userid);

            $data['totalCommentsPerticularImage'][$friendsdata->userid] = $this->UsersModel->totalCommentsPerticularImage($this->session->userdata('userid'), $friendsdata->userid);
            $data['totalCommentsPerticularText'][$friendsdata->userid] = $this->UsersModel->totalCommentsPerticularText($this->session->userdata('userid'), $friendsdata->userid);
            $data['totalCommentsParticularVideo'][$friendsdata->userid] = $this->UsersModel->totalCommentsParticularVideo($this->session->userdata('userid'), $friendsdata->userid);

            $data['totalLikesPerticularImage'][$friendsdata->userid] = $this->UsersModel->totalLikesPerticularImage($this->session->userdata('userid'), $friendsdata->userid);
            $data['totalLikesPerticularText'][$friendsdata->userid] = $this->UsersModel->totalLikesPerticularText($this->session->userdata('userid'), $friendsdata->userid);
            $data['totalLikesParticularVideo'][$friendsdata->userid] = $this->UsersModel->totalLikesParticularVideo($this->session->userdata('userid'), $friendsdata->userid);

        }

        $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
        $data['total_record_album'] = count($album_name);
        for ($i = 0; $i < count($album_name); $i++) {
            $data['album_title'][$i] = $album_name[$i]['title'];
            $data['albumeid'][$i] = $album_name[$i]['albumid'];
        }
        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
        $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
        $this->body = 'Users/newsfeed';
        $this->load->view($this->body, $data, TRUE);
        $this->layout();
    }

    //Send Email
    public function sendEmail($data)
    {
        $this->email->from('emmortal@emmortal.com', 'Emmortal');
        $this->email->to($data['emailid']);

        $this->email->subject('Confirmation instructions');
        $this->email->message($this->load->view('emails/send_email_template', $data, TRUE));

        if (!$this->email->send()) {
            show_error($this->email->print_debugger());
        } else {
            echo json_encode(FALSE);
        }
    }

    //signup
    public function signup()
    {
        if ($this->input->post()) {

            $data = array(
                'emailid' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'fullname' => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                'dob' => $this->input->post('dob')

            );
            $res = $this->UsersModel->userExistence($data['emailid']);
            if ($res) {
                echo json_encode(TRUE);

            } else {
                $this->UsersModel->signup($data);
                $res = $this->UsersModel->userExistence($data['emailid']);
                if (!($this->UsersModel->fetchPerticularUserData($res['userid']))) {
                    $pagedata = array(
                        'user_id' => $res['userid'],
                        'default_page' => 1
                    );
                    $this->UsersModel->createDefaultPage($pagedata);
                }
                $result=$this->UsersModel->fetchPerticularUserData($res['userid']);
                $addUserDescription = array(
                    'user_id'=>$res['userid'],
                    'user_description_id'=> $res['userid'],
                    'page_id' => $result['pageid']
                );
                $addUserProfilePage = array(
                    'user_id'=>$res['userid'],
                    'user_profile_id'=> $res['userid'],
                    'page_id' => $result['pageid']
                );
                $this->UsersModel->addUserDetailsPage($addUserDescription,$addUserProfilePage);
                $this->sendEmail($data);

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }
    }

    //Email Confirmation
    public function emailConfirmation()
    {
        $emailid = $this->uri->segment('3');
        if ($emailid) {
            $email = urldecode($emailid);
            $res = $this->UsersModel->userExistence($email);
            if ($res) {
                if ($res['activation'] == 0)
                    $this->session->set_flashdata('success_email_confirm', 'Your email is successfully confirmed');
                else
                    $this->session->set_flashdata('success_email_confirm', 'Your email was already confirmed');
                $this->UsersModel->confirmEmail($email);
                if (!($this->UsersModel->fetchPerticularUserData($res['userid']))) {
                    $pagedata = array(
                        'user_id' => $res['userid'],
                        'default_page' => 1
                    );
                    $this->UsersModel->createDefaultPage($pagedata);
                }
                $checkDefalutPage = $this->UsersModel->fetchPerticularUserData($res['userid']);
                $sess_data = array(
                    'userid' => $res['userid'],
                    'emailid' => $res['emailid'],
                    'password' => $res['password'],
                    'name' => $res['firstname'] . ' ' . $res['lastname'],
                    'logged_in' => true,
                    'default_page_id' => $checkDefalutPage['pageid']
                );
                $this->session->set_userdata($sess_data);
                redirect('Users/dashboard');
            } else {
                $this->session->set_flashdata('error_email_confirm', 'Confirmation token is invalid');
                redirect('/');

            }
        } else {
            $this->session->set_flashdata('error_email_confirm', 'Confirmation token Is invalid');
            redirect('/');
        }
    }
    public function signin()
    {
        if ($this->input->post()) {
            $data = array(
                'emailid' => $this->input->post('email'),
                'password' => md5($this->input->post('password'))
            );
            $res = $this->UsersModel->checkUserLogin($data);
            if ($res) {
                if ($res['activation'] == 1 && $res['userstype'] == 'user')
                {
                    $this->UsersModel->updateSigninDateTime($res['userid']);
                    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    } else {
                        $ip = $_SERVER['REMOTE_ADDR'];
                    }
                    $this->UsersModel->createOrUpdateTrackUserDetailsAfterLogin($res['userid'],$ip);
                    if (!($this->UsersModel->fetchPerticularUserData($res['userid']))) {
                        $pagedata = array(
                            'user_id' => $res['userid'],
                            'default_page' => 1
                        );
                        $this->UsersModel->createDefaultPage($pagedata);
                    }
                    $checkDefalutPage = $this->UsersModel->fetchPerticularUserData($res['userid']);
                    $sess_data = array(
                        'userid' => $res['userid'],
                        'emailid' => $res['emailid'],
                        'password' => $res['password'],
                        'name' => $res['firstname'] . ' ' . $res['lastname'],
                        'logged_in' => true,
                        'default_page_id' => $checkDefalutPage['pageid']
                    );
                    $this->session->set_userdata($sess_data);
                    echo json_encode($res);
                }
                else {
                    if ($res['activation'] == 1 && $res['userstype'] == 'admin')
                    {
                        $this->UsersModel->updateSigninDateTime($res['userid']);
                        $sess_data = array(
                            'admin_userid' => $res['userid'],
                            'admin_emailid' => $res['emailid'],
                            'admin_password' => $res['password'],
                            'admin_name' => $res['firstname'] . ' ' . $res['lastname'],
                            'admin_logged_in' => true
                        );
                        $this->session->set_userdata($sess_data);
                        echo json_encode($res);
                    }
                    else {
                        echo json_encode($res);
                    }

                }
            }
            else {
                echo json_encode(TRUE);
            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }

    }

    public function dashboard()
    {
        $this->checkSessionData();
        $data['title']='Emmortal - Dashboard';
        $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
        $data['total_record_album'] = count($album_name);
        $data['userdata'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        for ($i = 0; $i < count($album_name); $i++) {
            $data['album_title'][$i] = $album_name[$i]['title'];
            $data['albumeid'][$i] = $album_name[$i]['albumid'];
        }
        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                    $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
        $data['CountAllHomeContent'] = $this->UsersModel->CountAllHomeContent();
        $this->body = 'Users/innerHome';
        $this->load->view($this->body, $data, TRUE);
        $this->layout();
    }

    //session data set or not
    public function checkSessionData()
    {
        $logged_in = $this->session->userdata('logged_in');
        if ((!$logged_in)|| ($logged_in!==true)) {
            redirect('/');
        }
    }

    //log out and unset the session data

    public function logout()
    {
        $this->UsersModel->updateLogoutDateTime($this->session->userdata('userid'));
        $newdata = array(
            'userid'  =>'',
            'emailid' => '',
            'password' => '',
            'logged_in' =>false,
            'default_page_id' => '',

        );

        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect('/');
    }

    //forgot password
    public function forgotPassword()
    {
        //echo json_encode($this->input->post());
        if ($this->input->post()) {
            $forgot_email = $this->input->post('forgot_email');
            $random_key = dechex(rand(0x000000, 0xFFFFFF));
            $res = $this->UsersModel->userExistence($forgot_email);
            if ($res) {
                $data = array(
                    'emailid' => $this->input->post('forgot_email'),
                    'name' => $res['firstname'] . ' ' . $res['lastname'],
                    'random_key' => $random_key
                );
                $this->UsersModel->updateForgotGetPassword($forgot_email, $random_key);
                $this->sendEmailForgotPassword($data);

            } else {
                echo json_encode(FALSE);
            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }

    }

    //Send Email for forgot password
    public function sendEmailForgotPassword($data)
    {
        $this->email->from('emmortal@emmortal.com', 'Emmortal');
        $this->email->to($data['emailid']);

        $this->email->subject('Reset password instructions');
        $this->email->message($this->load->view('emails/send_forgot_password_template', $data, TRUE));

        if (!$this->email->send()) {
            show_error($this->email->print_debugger());
        } else {
            echo json_encode(TRUE);
        }
    }

    //reset password
    public function recovery()
    {
        $emailid = $this->uri->segment('3');
        $random_key = $this->uri->segment('4');
        if ($emailid && $random_key) {
            $email = urldecode($emailid);
            //User Existence for Reset password
            $res = $this->UsersModel->userExistenceResetPassword($email, $random_key);
            if ($res) {
                $this->session->set_flashdata('success_forgot_password', $email);
                $this->UsersModel->updateForgotGetPassword($email, '');
                redirect('/');
            } else {
                $this->session->set_flashdata('error_forgot_password', 'Reset password token was expired');
                redirect('/');
            }

        } else {
            $this->session->set_flashdata('error_forgot_password', 'Reset password token is invalid');
            redirect('/');
        }
    }

    public function resetPassword()
    {
        if ($this->input->post()) {
            $data = array(
                'email' => $this->input->post('email'),
                'npassword' => md5($this->input->post('npassword'))
            );
            $res = $this->UsersModel->userExistence($data['email']);
            if ($res) {
                $userData = array(
                    'email' => $res['emailid'],
                    'name' => $res['firstname'] . ' ' . $res['lastname']
                );
                $result = $this->UsersModel->newPassword($data);
                if ($result == 1) {
                    $this->sendEmailResetPassword($userData);
                } else {
                    echo json_encode(FALSE);
                }
            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');
        }
    }

    //After reset password,send mail
    public function sendEmailResetPassword($data)
    {
        $this->email->from('emmortal@emmortal.com', 'Emmortal');
        $this->email->to($data['email']);

        $this->email->subject('Change Password instructions');
        $this->email->message($this->load->view('emails/send_reset_password_template', $data, TRUE));

        if (!$this->email->send()) {
            show_error($this->email->print_debugger());
        } else {
            echo json_encode(TRUE);
        }
    }

    public function settings()
    {
        $this->checkSessionData();
        $data['title']='Emmortal - Settings';
        $data['userdata'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
        $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
        $data['total_record_album'] = count($album_name);
        for ($i = 0; $i < count($album_name); $i++) {
            $data['album_title'][$i] = $album_name[$i]['title'];
            $data['albumeid'][$i] = $album_name[$i]['albumid'];
        }
        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
        $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
        $this->body = 'Users/settings'; // passing body to function. change this for different views.
        $this->load->view($this->body, $data, TRUE);
        $this->layout();
    }

    public function generalSettings()
    {
        if ($this->input->post())
        {
            $res = $this->UsersModel->emailExistence($this->input->post('session_email_id'),$this->input->post('emailid'));
            if ($res)
            {
                echo json_encode(FALSE);

            }
            else
            {
                $data = array(
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'fullname' => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                    'dob' => $this->input->post('dob')
                );
                $user_id = $this->session->userdata('userid');
                //update Profile image
                if (!empty($_FILES['profileimage']['name'])) {

                    $directory = 'uploads/' . $user_id . '/profileImage/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $userData=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    if($userData['profileimage']!="")
                    {
                        $full_image_name=$userData['profileimage'];
                        unlink($full_image_name);
                        $this->aws3->deleteFile('bucketemmortal',$full_image_name);
                    }
                    $profileimg = array();
                    $tmp_name = $_FILES["profileimage"]["tmp_name"];
                    $name = $user_id.'_'.time().uniqid(rand()).'_'. $_FILES["profileimage"]["name"];
                    $profileimg['profileimage'] = $directory . $name;
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('profileimage')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $this->UsersModel->updateProfileImage($user_id, $profileimg);

                }
                //update Background image

                if (!empty($_FILES['backgroundimage']['name'])) {
                    $directory = 'uploads/' . $user_id . '/backgroudImage/';
                    $userData=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    if($userData['backgroundimage']!="")
                    {
                        $full_image_name=$userData['backgroundimage'];
                        unlink($full_image_name);
                        $this->aws3->deleteFile('bucketemmortal',$full_image_name);
                    }
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $backgroundimg = array();
                    $tmp_name = $_FILES["backgroundimage"]["tmp_name"];
                    $name = $user_id.'_'.time().uniqid(rand()).'_'. $_FILES["backgroundimage"]["name"];
                    $backgroundimg['backgroundimage'] = $directory . $name;
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('backgroundimage')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $this->UsersModel->updateBackgroundImage($user_id, $backgroundimg);

                }
                if ($this->input->post('blankprofileimage') == "blank") {
                    $profileimg = array();
                    $profileimg['profileimage'] = '';
                    $this->UsersModel->updateProfileImage($user_id, $profileimg);
                }
                if ($this->input->post('blankbackgroundimage') == "blank") {
                    $backgroundimg = array();
                    $backgroundimg['backgroundimage'] = '';
                    $this->UsersModel->updateBackgroundImage($user_id, $backgroundimg);
                }
                $this->UsersModel->updateGeneralSettings($this->session->userdata('userid'), $data);
                if($this->input->post('session_email_id')!=$this->input->post('emailid'))
                {
                    $data = array(
                        'firstname' => $this->input->post('firstname'),
                        'lastname' => $this->input->post('lastname'),
                        'emailid'=>$this->input->post('emailid'),
                        'session_email_id'=>$this->input->post('session_email_id')
                    );
                    $this->UsersModel->upadteChangeEmail($this->input->post('session_email_id'));
                    $res=$this->sendChangeEmail($data);
                }
                else
                {
                    echo json_encode(TRUE);
                }

            }


        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/settings#parentVerticalTab1');
        }
    }

    public function changePassword()
    {
        if ($this->input->post()) {
            $data = array(
                'new_password' => md5($this->input->post('new_password')),
                'current_password' => md5($this->input->post('current_password'))
            );
            $res = $this->UsersModel->userExistChangePassword($this->session->userdata('userid'), $data);
            if ($res) {

                $this->UsersModel->changePasswordSettings($this->session->userdata('userid'), $data);
                echo json_encode(TRUE);
            } else {
                echo json_encode(FALSE);
            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/settings#parentVerticalTab2');
        }
    }

    //Choose for profile image and background image after email verify
    public function chooseProfileBackgroundImage()
    {

        $this->checkSessionData();

        $user_id = $this->session->userdata('userid');
        //update Profile image
        if (!empty($_FILES['profileimage']['name'])) {

            $directory = 'uploads/' . $user_id . '/profileImage/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            $userData=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
            if($userData['profileimage']!="")
            {
                $full_image_name=$userData['profileimage'];
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $profileimg = array();
            $tmp_name = $_FILES["profileimage"]["tmp_name"];
            $name = $user_id.'_'.time().uniqid(rand()).'_'. $_FILES["profileimage"]["name"];
            $profileimg['profileimage'] = $directory . $name;
            $config['upload_path']          = $directory;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $name;
            $config['file_permissions'] 	= 0777;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('profileimage')) {
                $error = array('error' => $this->upload->display_errors());
            }
            else {
                $bucket_name='bucketemmortal';
                $source_file=$tmp_name;
                $image_directory=$directory.$name;
                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

            }
            $this->UsersModel->updateProfileImage($user_id, $profileimg);

        }
        //update Background image

        if (!empty($_FILES['backgroundimage']['name'])) {

            $directory = 'uploads/' . $user_id . '/backgroundimage/';
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            $userData=$this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
            if($userData['backgroundimage']!="")
            {
                $full_image_name=$userData['backgroundimage'];
                unlink($full_image_name);
                $this->aws3->deleteFile('bucketemmortal',$full_image_name);
            }
            $backgroundimg = array();
            $tmp_name = $_FILES["backgroundimage"]["tmp_name"];
            $name = $user_id.'_'.time().uniqid(rand()).'_'. $_FILES["backgroundimage"]["name"];
            $backgroundimg['backgroundimage'] = $directory . $name;
            $config['upload_path']          = $directory;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = $name;
            $config['file_permissions'] 	= 0777;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('backgroundimage')) {
                $error = array('error' => $this->upload->display_errors());
            }
            else {
                $bucket_name='bucketemmortal';
                $source_file=$tmp_name;
                $image_directory=$directory.$name;
                $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

            }

            $this->UsersModel->updateBackgroundImage($user_id, $backgroundimg);

        }
        if ($this->input->post('blankprofileimage') == "blank") {
            $profileimg = array();
            $profileimg['profileimage'] = '';
            $this->UsersModel->updateProfileImage($user_id, $profileimg);
        }
        if ($this->input->post('blankbackgroundimage') == "blank") {
            $backgroundimg = array();
            $backgroundimg['backgroundimage'] = '';
            $this->UsersModel->updateBackgroundImage($user_id, $backgroundimg);
        }
        echo json_encode(TRUE);

    }

    public function search()
    {
        $userid = $this->uri->segment('3');
        if ($userid) {
            $res = $this->UsersModel->fetchPerticularUserData($userid);
            if ($res) {
                if ($this->session->userdata('userid')) {
                    $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
                    $data['total_record_album'] = count($album_name);
                    for ($i = 0; $i < count($album_name); $i++) {
                        $data['album_title'][$i] = $album_name[$i]['title'];
                        $data['albumeid'][$i] = $album_name[$i]['albumid'];
                    }
                }
                $data['title']='Emmortal - Find Connections';
                if ($res['userid'] == $this->session->userdata('userid')) {
                    $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    $data['userdata'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    $data['allUserdata'] = $this->UsersModel->fetchAllUserData($this->session->userdata('userid'));
                    $data['friendRequest'] = $this->UsersModel->fetchAllFriendRequest($this->session->userdata('userid'));
                    $data['acceptedfriends'] = $this->UsersModel->fetchAllAcceptedFriend($this->session->userdata('userid'));
                    $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                    $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                    $this->body = 'Users/search'; // passing body to function. change this for different views.
                    $this->load->view($this->body, $data, TRUE);
                    $this->layout();
                } else {
                    $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                    $data['userdata'] = $this->UsersModel->fetchPerticularUserData($userid);
                    $data['relationshipUsers'] = $this->UsersModel->fetchAllRelationshipUsers($userid);
                    $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                    $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                    $this->body = 'Users/user_relationship'; // passing body to function. change this for different views.
                    $this->load->view($this->body, $data, TRUE);
                    $this->layout();
                }


            } else {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }

    public function scrollLoad()
    {
        if ($this->input->post()) {
            $allUserdata = $this->UsersModel->fetchAllScrollLoad($this->session->userdata('userid'), $this->input->post('start'));
            $friendRequest = $this->UsersModel->fetchAllFriendRequest($this->session->userdata('userid'));
            $acceptedfriends = $this->UsersModel->fetchAllAcceptedFriend($this->session->userdata('userid'));
            $flag = 0;
            $flag1 = 0;
            if ($allUserdata) {
                foreach ($allUserdata as $aud) {
                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->session->userdata('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->session->userdata('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->session->userdata('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->session->userdata('userid'), $aud->userid);

                    $totalTextComments = $this->UsersModel->totalCommentsText($this->session->userdata('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->session->userdata('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->session->userdata('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    if ($friendRequest) {
                        foreach ($friendRequest as $fr) {
                            if ($fr->friendsid == $aud->userid) {
                                $flag = 1;
                                break;
                            } else {
                                $flag = 0;
                            }
                        }
                    }
                    if ($acceptedfriends) {
                        foreach ($acceptedfriends as $af) {
                            if (($af->friendsid == $aud->userid) || ($af->userid == $aud->userid)) {
                                $flag1 = 1;
                                break;
                            } else {
                                $flag1 = 0;
                            }
                        }
                    }
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                    <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>';
                    if ($flag1 == 1) {
                        $data = $data . ' <a href="' . base_url() . 'Users/relationships?user_id=' . $this->session->userdata('userid') . '&friends_id=' . $aud->userid . '"><p class="small-font">View Relationship Page</p></a>';;
                    }

                    $data = $data . '</div>
                        <div class="col-lg-3 text-center">&nbsp;</div>';

                    if ($flag == 1 && $flag1 == 0) {
                        $data = $data . '<div class="col-lg-2">
                           <ul class="right-list">
                                <button class="btn btn-success">
                                    <div class="fa fa-clock-o"></div>
                                    Request Sent
                                </button>     
                             </ul>
                        </div>';
                    } else {
                        if ($flag1 == 1) {
                            $data = $data . '<div class="col-lg-2">
                                    <ul class="right-list">   
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>';
                        } else {
                            $data = $data . '<div class="col-lg-2">
                            <ul class="right-list">
                                <button class="btn btn-info" id="' . $aud->userid . ' " onclick="addFriend(id)">
                                    <div class="fa fa-user-plus"></div>
                                    Connect
                                </button>
                            </ul>
                        </div>';
                        }
                    }
                    $data = $data . '</div>
                       </li>';
                    echo $data;
                }

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/search/');
        }
    }


    public function searchAll()
    {
        if ($this->input->post()) {
            $allUserdata = $this->UsersModel->searchUsers($this->session->userdata('userid'), $this->input->post('search_users'));
            $friendRequest = $this->UsersModel->fetchAllFriendRequest($this->session->userdata('userid'));
            $acceptedfriends = $this->UsersModel->fetchAllAcceptedFriend($this->session->userdata('userid'));
            $flag = 0;
            $flag1 = 0;
            if ($allUserdata) {
                foreach ($allUserdata as $aud) {
                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->session->userdata('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->session->userdata('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->session->userdata('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->session->userdata('userid'), $aud->userid);

                    $totalTextComments = $this->UsersModel->totalCommentsText($this->session->userdata('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->session->userdata('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->session->userdata('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    if ($friendRequest) {
                        foreach ($friendRequest as $fr) {
                            if ($fr->friendsid == $aud->userid) {
                                $flag = 1;
                                break;
                            } else {
                                $flag = 0;
                            }
                        }
                    }
                    if ($acceptedfriends) {
                        foreach ($acceptedfriends as $af) {
                            if (($af->friendsid == $aud->userid) || ($af->userid == $aud->userid)) {
                                $flag1 = 1;
                                break;
                            } else {
                                $flag1 = 0;
                            }
                        }
                    }
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                    <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->fullname . '</b></a>';
                    if ($flag1 == 1) {
                        $data = $data . ' <a href="' . base_url() . 'Users/relationships?user_id=' . $this->session->userdata('userid') . '&friends_id=' . $aud->userid . '"><p class="small-font">View Relationship Page</p></a>';;
                    }

                    $data = $data . '</div>
                        <div class="col-lg-3 text-center">&nbsp;</div>';

                    if ($flag == 1 && $flag1 == 0) {
                        $data = $data . '<div class="col-lg-2">
                            <ul class="right-list">
                                <button class="btn btn-success">
                                    <div class="fa fa-clock-o"></div>
                                    Request Sent
                                </button>     
                             </ul>
                        </div>';
                    } else {
                        if ($flag1 == 1) {
                            $data = $data . '<div class="col-lg-2">
                                     <ul class="right-list">   
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>';
                        } else {
                            $data = $data . '<div class="col-lg-2">
                            <ul class="right-list">
                                <button class="btn btn-info" id="' . $aud->userid . ' " onclick="addFriend(id)">
                                    <div class="fa fa-user-plus"></div>
                                    Connect
                                </button>
                            </ul>
                        </div>';
                        }
                    }
                    $data = $data . '</div>
                       </li>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">There are no relationships yet</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    //Add friend
    public function addFriend()
    {
        if ($this->input->post()) {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friend_id'),
                'relationshipstatus' => 'outgoing'
            );
            $res=$this->UsersModel->addFriend($data);
            $notifyData=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friend_id'),
                'notify_type'=>'friendsRequest',
                'notify_type_id'=>$res->id,
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->insertNotification($notifyData);
            $allUserdata = $this->UsersModel->fetchAllUsers($this->session->userdata('userid'));
            $friendRequest = $this->UsersModel->fetchAllFriendRequest($this->session->userdata('userid'));
            $acceptedfriends = $this->UsersModel->fetchAllAcceptedFriend($this->session->userdata('userid'));
            $flag = 0;
            $flag1 = 0;
            if ($allUserdata) {
                foreach ($allUserdata as $aud) {

                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->session->userdata('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->session->userdata('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->session->userdata('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->session->userdata('userid'), $aud->userid);

                    $totalTextComments = $this->UsersModel->totalCommentsText($this->session->userdata('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->session->userdata('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->session->userdata('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    if ($friendRequest) {
                        foreach ($friendRequest as $fr) {
                            if ($fr->friendsid == $aud->userid) {
                                $flag = 1;
                                break;
                            } else {
                                $flag = 0;
                            }
                        }
                    }
                    if ($acceptedfriends) {
                        foreach ($acceptedfriends as $af) {
                            if (($af->friendsid == $aud->userid) || ($af->userid == $aud->userid)) {
                                $flag1 = 1;
                                break;
                            } else {
                                $flag1 = 0;
                            }
                        }
                    }
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                    <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>';
                    if ($flag1 == 1) {
                        $data = $data . ' <a href="' . base_url() . 'Users/relationships?user_id=' . $this->session->userdata('userid') . '&friends_id=' . $aud->userid . '"><p class="small-font">View Relationship Page</p></a>';;
                    }

                    $data = $data . '</div>
                    <div class="col-lg-3 text-center">&nbsp;</div>';

                    if ($flag == 1 && $flag1 == 0) {
                        $data = $data . '<div class="col-lg-2">
                             <ul class="right-list">
                                <button class="btn btn-success">
                                    <div class="fa fa-clock-o"></div>
                                    Request Sent
                                </button>     
                             </ul>
                        </div>';
                    } else {
                        if ($flag1 == 1) {
                            $data = $data . '<div class="col-lg-2">
                                    <ul class="right-list">   
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>';
                        } else {
                            $data = $data . '<div class="col-lg-2">
                            <ul class="right-list">
                                <button class="btn btn-info" id="' . $aud->userid . ' " onclick="addFriend(id)">
                                    <div class="fa fa-user-plus"></div>
                                    Connect
                                </button>
                            </ul>
                        </div>';
                        }
                    }
                    $data = $data . '</div>
                       </li>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">There are no relationships yet</h2>';
                echo $data;

            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function searchOutgoingUsers()
    {
        $searchOutgoingUsers = $this->UsersModel->searchOutgoingUsers($this->session->userdata('userid'),$this->input->post('search_users'));
        if ($searchOutgoingUsers) {
            foreach ($searchOutgoingUsers as $aud) {
                $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                $data = '
                    <li>
                        <div class="relation-page">';
                if ($aud->profileimage == '') {
                    $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                } else {
                    $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                }
                $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                 </div>
                                 <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-2">
                                    <ul class="right-list">
                                        <button onclick="requestSentFunction()" class="btn btn-success dropdown-btn">
                                            <div class="fa fa-clock-o"></div>
                                            Request Sent &nbsp;<span class="caret"></span>
                                        </button>
                                        <div class="dropdown-cancel-content cancelRequestDropdown">
                                            <button onclick="cancelRequest('.$aud->userid.')" class="btn btn-warning"><i class="fa fa-times"></i> Cancel Request</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </li>';
                echo $data;
            }

        } else {
            $data = '<h2 style="text-align: center">There are no outgoing relationships requests</h2>';
            echo $data;

        }
    }
    public function allOutgoingUsers()
    {
        $allOutgoingUsers = $this->UsersModel->fetchAllOutgoingUsers($this->session->userdata('userid'));
        if ($allOutgoingUsers) {
            foreach ($allOutgoingUsers as $aud) {
                $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                $data = '
                    <li>
                        <div class="relation-page">';
                if ($aud->profileimage == '') {
                    $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                } else {
                    $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                }
                $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                 </div>
                                 <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-2">
                                     <ul class="right-list">
                                        <button onclick="requestSentFunction()" class="btn btn-success dropdown-btn">
                                            <div class="fa fa-clock-o"></div>
                                            Request Sent &nbsp;<span class="caret"></span>
                                        </button>
                                        <div class="cancelRequestDropdown dropdown-cancel-content">
                                            <button onclick="cancelRequest('.$aud->userid.')" class="btn btn-warning"><i class="fa fa-times"></i> Cancel Request</button>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </li>';
                echo $data;
            }

        } else {
            $data = '<h2 style="text-align: center">There are no outgoing relationships requests</h2>';
            echo $data;

        }
    }

    public function allIncomingUsers()
    {
        $allIncomingUsers = $this->UsersModel->fetchAllIncomingUsers($this->session->userdata('userid'));
        if ($allIncomingUsers) {
            foreach ($allIncomingUsers as $aud) {
                $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                $data = '
                    <li>
                        <div class="relation-page">';
                if ($aud->profileimage == '') {
                    $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                } else {
                    $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                }
                $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                 </div>
                                 <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-3">
                                    <ul class="right-list">
                                        <button id="' . $aud->userid . '" class="btn btn-primary" onclick="acceptFriend(id)"><div class="fa fa-user-plus"></div> Accept</button>
                                         <button id="' . $aud->userid . '" class="btn btn-warning" onclick="declineFriend(id)"><div class="fa fa-user-plus"></div> Decline</button>
                                    </ul>
                                </div>
                            </div>
                        </li>';
                echo $data;
            }

        } else {
            $data = '<h2 style="text-align: center">There are no incoming relationships requests</h2>';
            echo $data;

        }
    }

    public function allRelationshipUsers()
    {
        $relationshipUsers = $this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
        if ($relationshipUsers) {
            foreach ($relationshipUsers as $aud) {
                $totalLikesImage = $this->UsersModel->totalLikesImage($this->session->userdata('userid'), $aud->userid);
                $totalLikesText = $this->UsersModel->totalLikesText($this->session->userdata('userid'), $aud->userid);
                $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->session->userdata('userid'), $aud->userid);

                $total_tag_content = $this->UsersModel->totalTagContent($this->session->userdata('userid'), $aud->userid);

                $totalTextComments = $this->UsersModel->totalCommentsText($this->session->userdata('userid'), $aud->userid);
                $totalImageComments = $this->UsersModel->totalCommentsImage($this->session->userdata('userid'), $aud->userid);
                $totalVideoComments = $this->UsersModel->totalVideoComments($this->session->userdata('userid'), $aud->userid);

                $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                $data = '
                    <li>
                        <div class="relation-page">';
                if ($aud->profileimage == '') {
                    $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                } else {
                    $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                }
                $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                     <a href="' . base_url() . 'Users/relationships?user_id=' . $this->session->userdata('userid') . '&friends_id=' . $aud->userid . '"><p class="small-font">View Relationship Page</p></a>
                                 </div>
                                 <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-2">
								 	<ul class="right-list">   
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>
                            </div>
                        </li>';
                echo $data;
            }

        } else {
            $data = '<h2 style="text-align: center">You have no relationships yet</h2>';
            echo $data;

        }
    }

    public function acceptFriend()
    {
        if ($this->input->post()) {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friend_id'),
                'friendshipdate' => date("Y-m-d H:i:s"),
                'requestaccept' => '1',
                'relationshipstatus' => 'accepted'
            );
            $this->UsersModel->acceptFriendRequest($data);
            $notifyData1=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friend_id'),
                'notify_type'=>'friendsAccept',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->updateNotificationAcceptFriendRequest($notifyData1);
            $notifyData2=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friend_id'),
                'notify_type'=>'friendsRequestAccept',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'<b>Accepted </b>your relationship request'
            );
            $this->UsersModel->insertNotification($notifyData2);
            $allIncomingUsers = $this->UsersModel->fetchAllIncomingUsers($this->session->userdata('userid'));
            if ($allIncomingUsers) {
                foreach ($allIncomingUsers as $aud) {
                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                 </div>
                                  <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-3">
                                    <ul class="right-list">
                                        <button id="' . $aud->userid . '" class="btn btn-primary" onclick="acceptFriend(id)"><div class="fa fa-user-plus"></div> Accept</button>
                                        <button id="' . $aud->userid . '" class="btn btn-warning" onclick="declineFriend(id)"><div class="fa fa-user-plus"></div> Decline</button>
                                    </ul>
                                </div>
                            </div>
                        </li>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">There are no incoming relationships requests</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }

    public function searchRelationship()
    {
        if ($this->input->post()) {
            $allSearchRelationshipUsers = $this->UsersModel->searchRelationships($this->session->userdata('userid'), $this->input->post('search_users'));
            if ($allSearchRelationshipUsers) {
                foreach ($allSearchRelationshipUsers as $aud) {
                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->session->userdata('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->session->userdata('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->session->userdata('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->session->userdata('userid'), $aud->userid);

                    $totalTextComments = $this->UsersModel->totalCommentsText($this->session->userdata('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->session->userdata('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->session->userdata('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class=col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                     <a href="' . base_url() . 'Users/relationships?user_id=' . $this->session->userdata('userid') . '&friends_id=' . $aud->userid . '"><p class="small-font">View Relationship Page</p></a>
                                 </div>
                                 <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-2">
                                    <ul class="right-list">   
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>
                            </div>
                        </li>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">You have no relationships yet</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    //Decline Friend
    public function declineFriends()
    {
        if ($this->input->post()) {
            $data = array(
                'userid' => $this->input->post('friends_id'),
                'friendsid' => $this->session->userdata('userid'),
                'requestaccept' => '0',
                'relationshipstatus' => 'outgoing'
            );
            $this->UsersModel->declineFriendsRequest($data);
            $notifyData1=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsDecline',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->updateNotificationAcceptFriendRequest($notifyData1);
            $notifyData2=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsRequestDecline',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'<b>Declined </b>your relationship request'
            );
            $this->UsersModel->insertNotification($notifyData2);
            $allIncomingUsers = $this->UsersModel->fetchAllIncomingUsers($this->session->userdata('userid'));
            if ($allIncomingUsers) {
                foreach ($allIncomingUsers as $aud) {
                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '
                    <li>
                        <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                </div>';
                    } else {
                        $data = $data . '
                                <div class="col-lg-3 text-center">
                                    <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                     <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                 </div>
                                  <div class="col-lg-3 text-center">&nbsp;</div>
                                 <div class="col-lg-3">
                                    <ul class="right-list">
                                        <button id="' . $aud->userid . '" class="btn btn-primary" onclick="acceptFriend(id)"><div class="fa fa-user-plus"></div> Accept</button>
                                        <button id="' . $aud->userid . '" class="btn btn-warning" onclick="declineFriend(id)"><div class="fa fa-user-plus"></div> Decline</button>
                                    </ul>
                                </div>
                            </div>
                        </li>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">There are no incoming relationships requests</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }

    public function relationships()
    {
        $user_id = $this->input->get('user_id');
        $friends_id = $this->input->get('friends_id');
        if ($user_id != '' && $friends_id != '') {
            $data['friends_data'] = $this->UsersModel->friendsRelationships($user_id, $friends_id);
            if ($data['friends_data']) {

                $data['title']='Emmortal - Relationships';
                $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
                $data['total_record_album'] = count($album_name);
                for ($i = 0; $i < count($album_name); $i++) {
                    $data['album_title'][$i] = $album_name[$i]['title'];
                    $data['albumeid'][$i] = $album_name[$i]['albumid'];
                }

                $data['user_data'] = $this->UsersModel->fetchPerticularUserData($user_id);
                $data['friend_data'] = $this->UsersModel->fetchPerticularUserData($friends_id);
                $data['userdata'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                $data['tagfriendcontentall'] = $this->UsersModel->tagFriendsAllContent($user_id, $friends_id);
                $data['totalTextComments'] = $this->UsersModel->totalCommentsText($user_id, $friends_id);
                $data['totalImageComments'] = $this->UsersModel->totalCommentsImage($user_id, $friends_id);
                $data['totalCommentsPerticularImage'] = $this->UsersModel->totalCommentsPerticularImage($user_id, $friends_id);
                $data['totalCommentsPerticularText'] = $this->UsersModel->totalCommentsPerticularText($user_id, $friends_id);
                $data['totalLikesImage'] = $this->UsersModel->totalLikesImage($user_id, $friends_id);
                $data['totalLikesText'] = $this->UsersModel->totalLikesText($user_id, $friends_id);
                $data['totalLikesPerticularImage'] = $this->UsersModel->totalLikesPerticularImage($user_id, $friends_id);
                $data['totalLikesPerticularText'] = $this->UsersModel->totalLikesPerticularText($user_id, $friends_id);

                $data['totalVideoComments'] = $this->UsersModel->totalVideoComments($user_id, $friends_id);
                $data['totalLikesVideo'] = $this->UsersModel->totalLikesVideo($user_id, $friends_id);
                $data['totalLikesParticularVideo'] = $this->UsersModel->totalLikesParticularVideo($user_id, $friends_id);
                $data['totalCommentsParticularVideo'] = $this->UsersModel->totalCommentsParticularVideo($user_id, $friends_id);
                $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                $this->body = 'Users/relationships';
                $this->load->view($this->body, $data, TRUE);
                $this->layout();
            } else {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function showCommentRelationships()
    {
        if ($this->input->post()) {
            $check = explode('_', $this->input->post('comments_type_id'));
            $check_type = $check[0];
            $check_id = $check[1];
            if ($check_type == 'image')
            {
                $data['commentsData'] = $this->UsersModel->showCommentsImage($check_id);
                if ($this->session->userdata('userid')) {
                    $data['existLike'] = $this->UsersModel->existUserLoginLikeImage($this->session->userdata('userid'), $check_id);
                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularImage($check_id);
                $data['seeTagged'] = $this->UsersModel->seeTaggedImage($check_id);
                if($this->session->userdata('userid'))
                {
                    $Pagedata=array(
                        'user_id'=>$this->session->userdata('userid'),
                        'image_id'=>$check_id
                    );
                    $data['existPageData']=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                }
            }
            if ($check_type == 'text')
            {
                $data['commentsData'] = $this->UsersModel->showCommentsText($check_id);
                if ($this->session->userdata('userid')) {
                    $data['existLike'] = $this->UsersModel->existUserLoginLikeText($this->session->userdata('userid'), $check_id);
                }
                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularText($check_id);
                $data['seeTagged'] = $this->UsersModel->seeTaggedText($check_id);

                if($this->session->userdata('userid'))
                {
                    $Pagedata=array(
                        'user_id'=>$this->session->userdata('userid'),
                        'text_id'=>$check_id
                    );
                    $data['existPageData']=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                }
            }
            if ($check_type == 'video')
            {
                $data['commentsData'] = $this->UsersModel->showCommentsVideo($check_id);
                if ($this->session->userdata('userid')) {
                    $data['existLike'] = $this->UsersModel->existUserLoginLikeVideo($this->session->userdata('userid'), $check_id);
                }
                $data['totalLikePerticular'] = $this->UsersModel->totalLikeParticularVideo($check_id);
                $data['seeTagged'] = $this->UsersModel->seeTaggedVideo($check_id);

                if($this->session->userdata('userid'))
                {
                    $Pagedata=array(
                        'user_id'=>$this->session->userdata('userid'),
                        'video_id'=>$check_id
                    );
                    $data['existPageData']=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                }
            }
            $data['count_album_content'] = $this->UsersModel->countAlbumContent($this->input->post('album_id'), $this->input->post('tagger_id'));
            if ($this->session->userdata('userid')) {
                $data['relationshipUsers'] = $this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
            }

            $data['allLikecomment'] = $this->UsersModel->allLikecomment();
            echo json_encode($data);

        }

    }

    public function commentRelationships()
    {
        if ($this->input->post()) {
            $check = explode('_', $this->input->post('comments_type_id'));
            $check_type = $check[0];
            $check_id = $check[1];
            $user_id = $this->input->post('user_id');
            $friends_id = $this->input->post('friends_id');
            if ($check_type == 'image') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'image_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {

                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }
                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'image_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );
                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'imageComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your image.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsImage($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsImage($check_id);
            }
            if ($check_type == 'text') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'text_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }
                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'text_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );
                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'textComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your text.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsText($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsText($check_id);


            }
            if ($check_type == 'video') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'video_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'video_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );
                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'videoComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your video.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsVideo($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsVideo($check_id);


            }
            $data['totalTextComments'] = $this->UsersModel->totalCommentsText($user_id, $friends_id);
            $data['totalImageComments'] = $this->UsersModel->totalCommentsImage($user_id, $friends_id);
            $data['totalVideoComments'] = $this->UsersModel->totalVideoComments($user_id, $friends_id);
            $data['relationshipUsers'] = $this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
            $data['allLikecomment'] = $this->UsersModel->allLikecomment();
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }

    public function support()
    {
        $this->checkSessionData();
        if ($this->input->post()) {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'question' => $this->input->post('send_message')
            );
            $this->UsersModel->sendQuestion($data);
            echo json_encode(TRUE);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/settings#parentVerticalTab5');
        }
    }
    public function notification()
    {
        $data['allIncomingUsers'] = $this->UsersModel->fetchAllIncomingUsers($this->session->userdata('userid'));
        $data['allAcceptedUsers'] = $this->UsersModel->fetchAllAcceptedUsers($this->session->userdata('userid'));
        $data['allAcceptedUsersFriendsRequest'] = $this->UsersModel->fetchAllAcceptedRelationshipsRequest($this->session->userdata('userid'));
        $data['fetchAllUserData'] = $this->UsersModel->fetchAllUser();
        echo json_encode($data);
    }
    public function getNotification()
    {
        //echo "<pre>";
       // print_r($this->input->post('userid'));
        /*if ($this->input->post()) {*/
            $data['allNotification'] = $this->UsersModel->getNotification($this->input->post('userid'));
            $data['countNotication'] = $this->UsersModel->countNotication($this->input->post('userid'));
            //print_r($data['allNotification']);
           echo json_encode($data);
        /*}
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }*/
    }
    public function markAllRead()
    {
        if ($this->input->post())
        {
            $this->UsersModel->markAllRead($this->input->post('userid'));
        }
        else {
               $this->session->set_flashdata('invalid_page', 'Page was not found');
               redirect('Users/dashboard');
           }
    }
    //Accept friend request for notification
    public function accept()
    {
        if ($this->input->post()) {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friends_id'),
                'friendshipdate' => date("Y-m-d H:i:s"),
                'requestaccept' => '1',
                'relationshipstatus' => 'accepted'
            );
            $this->UsersModel->acceptFriendRequest($data);
            $notifyData1=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsAccept',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->updateNotificationAcceptFriendRequest($notifyData1);
            $notifyData2=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsRequestAccept',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'<b>Accepted </b>your relationship request'
            );
            $this->UsersModel->insertNotification($notifyData2);


        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    //decline friend request for notification
    public function decline()
    {
        if ($this->input->post()) {

            $data = array(
                'userid' => $this->input->post('friends_id'),
                'friendsid' => $this->session->userdata('userid'),
                'requestaccept' => '0',
                'relationshipstatus' => 'outgoing'
            );
            $this->UsersModel->declineFriendsRequest($data);
            $notifyData1=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsDecline',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->updateNotificationAcceptFriendRequest($notifyData1);
            $notifyData2=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsRequestDecline',
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'<b>Declined </b>your relationship request'
            );
            $this->UsersModel->insertNotification($notifyData2);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }
    public function likeRelationships()
    {
        if ($this->input->post()) {
            $check = explode('_', $this->input->post('id'));
            $check_type = $check[0];
            $check_id = $check[1];
            $user_id = $this->input->post('user_id');
            $friends_id = $this->input->post('friends_id');
            if ($check_type == 'image') {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeImage($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'image_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'imageLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post image'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeImage($this->session->userdata('userid'), $check_id);

                }
                else
                {

                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeImage($likeData);

                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularImage($check_id);

            }
            if ($check_type == 'text') {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeText($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'text_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'textLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post text'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeText($this->session->userdata('userid'), $check_id);

                }
                else
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeText($likeData);
                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularText($check_id);
            }
            if ($check_type == 'video')
            {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeVideo($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'video_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'videoLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post video'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeVideo($this->session->userdata('userid'), $check_id);

                }
                else
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeVideo($likeData);
                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikeParticularVideo($check_id);
            }
            $data['totalLikesImage'] = $this->UsersModel->totalLikesImage($user_id, $friends_id);
            $data['totalLikesText'] = $this->UsersModel->totalLikesText($user_id, $friends_id);
            $data['totalLikesVideo'] = $this->UsersModel->totalLikesVideo($user_id, $friends_id);
            echo json_encode($data);
        }
    }
    public function likeContent()
    {
        if ($this->input->post()) {
            $check = explode('_', $this->input->post('id'));
            $check_type = $check[0];
            $check_id = $check[1];
            if ($check_type == 'image') {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeImage($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'image_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'imageLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post image'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeImage($this->session->userdata('userid'), $check_id);

                }
                else
                {

                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeImage($likeData);

                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularImage($check_id);

            }
            if ($check_type == 'text') {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeText($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'text_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'textLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post text'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeText($this->session->userdata('userid'), $check_id);

                }
                else
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeText($likeData);
                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikePerticularText($check_id);
            }
            if ($check_type == 'video')
            {
                $data['existLike'] = $this->UsersModel->existUserLoginLikeVideo($this->session->userdata('userid'), $check_id);
                $likeData = array(
                    'video_id' => $check_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $notifyLikeData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'videoLike',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'likes your post video'
                );
                if ($data['existLike'])
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    $this->UsersModel->dislikeVideo($this->session->userdata('userid'), $check_id);

                }
                else
                {
                    if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                        $this->UsersModel->insertNotification($notifyLikeData);
                    $this->UsersModel->likeVideo($likeData);
                }

                $data['totalLikePerticular'] = $this->UsersModel->totalLikeParticularVideo($check_id);
            }
            echo json_encode($data);
        }
    }
    public function searchRelationshipUser()
    {
        if ($this->input->post()) {
            $allSearchRelationshipUsers = $this->UsersModel->searchRelationships($this->input->post('userid'), $this->input->post('search_users'));
            $userData = $this->UsersModel->fetchPerticularUserData($this->input->post('userid'));
            if ($allSearchRelationshipUsers) {
                foreach ($allSearchRelationshipUsers as $aud) {

                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->input->post('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->input->post('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->input->post('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->input->post('userid'), $aud->userid);


                    $totalTextComments = $this->UsersModel->totalCommentsText($this->input->post('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->input->post('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->input->post('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;


                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '<div class="row contnarea">
                                <div class="col-md-8">
                                    <div class="albmprevwconctn">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>';
                    } else {
                        $data = $data . '
                                <img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/>';
                    }
                    $data = $data . ' </div>
                                    <div class="albmtilconctn">
                                        <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <h6>' . $aud->fullname . '</h6></a>
                                        <a href="' . base_url() . 'Users/relationships?user_id=' . $this->input->post('userid') . '&friends_id=' . $aud->userid . '">View Relationship Page</a>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <ul class="right-listcont">  
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>
                            </div>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">' . $userData['fullname'] . ' has no relationships yet</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function RelationshipUser()
    {
        if ($this->input->post()) {
            $relationshipUsers = $this->UsersModel->fetchAllRelationshipUsers($this->input->post('userid'));
            $userData = $this->UsersModel->fetchPerticularUserData($this->input->post('userid'));
            if ($relationshipUsers) {
                foreach ($relationshipUsers as $aud) {
                    $totalLikesImage = $this->UsersModel->totalLikesImage($this->input->post('userid'), $aud->userid);
                    $totalLikesText = $this->UsersModel->totalLikesText($this->input->post('userid'), $aud->userid);
                    $totalLikesVideo = $this->UsersModel->totalLikesVideo($this->input->post('userid'), $aud->userid);

                    $total_tag_content = $this->UsersModel->totalTagContent($this->input->post('userid'), $aud->userid);

                    $totalTextComments = $this->UsersModel->totalCommentsText($this->input->post('userid'), $aud->userid);
                    $totalImageComments = $this->UsersModel->totalCommentsImage($this->input->post('userid'), $aud->userid);
                    $totalVideoComments = $this->UsersModel->totalVideoComments($this->input->post('userid'), $aud->userid);

                    $total_likes = $totalLikesImage->total_like + $totalLikesText->total_like + $totalLikesVideo->total_like;
                    $total_comments = $totalImageComments->total_comments + $totalTextComments->total_comments + $totalVideoComments->total_comments;

                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '<div class="row contnarea">
                                <div class="col-md-8">
                                    <div class="albmprevwconctn">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>';
                    } else {
                        $data = $data . '
                                <img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/>';
                    }
                    $data = $data . ' </div>
                                    <div class="albmtilconctn">
                                        <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <h6>' . $aud->fullname . '</h6></a>
                                        <a href="' . base_url() . 'Users/relationships?user_id=' . $this->input->post('userid') . '&friends_id=' . $aud->userid . '">View Relationship Page</a>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <ul class="right-listcont">  
                                        <li>' . $total_tag_content . '<span><img alt="image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/icon3.png" /></span></li>
                                        <li>' . $total_comments . '<span><i class="fa fa-comment" aria-hidden="true"></i></span></li>
                                        <li>' . $total_likes . '<span><i class="fa fa-heart" aria-hidden="true"></i></span></li>     
                                    </ul>
                                </div>
                            </div>';
                    echo $data;
                }

            } else {
                $data = '<h2 style="text-align: center">' . $userData['fullname'] . ' has no relationships yet</h2>';
                echo $data;

            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function commentLike()
    {
        if ($this->input->post()) {
            $comment_id = $this->input->post('comment_id');
            $data['existLike'] = $this->UsersModel->existUserLoginLikeComment($this->session->userdata('userid'), $comment_id);
            if ($data['existLike']) {

                $this->UsersModel->dislikeComment($this->session->userdata('userid'), $comment_id);
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                {
                    $check = explode('_', $this->input->post('comments_type_id'));
                    $check_type = $check[0];
                    $check_id = $check[1];
                    if ($check_type == 'tribute')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'tributeCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    }
                    if ($check_type == 'image')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'imageCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    }
                    if ($check_type == 'text')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'textCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    }
                    if ($check_type == 'video')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'videoCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->deleteNotification($notifyLikeData);
                    }


                }


            }
            else
            {
                $likeData = array(
                    'comment_id' => $comment_id,
                    'user_id' => $this->session->userdata('userid')
                );
                $this->UsersModel->likeComment($likeData);
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                {
                    $check = explode('_', $this->input->post('comments_type_id'));
                    $check_type = $check[0];
                    $check_id = $check[1];
                    if ($check_type == 'tribute')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'tributeCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->insertNotification($notifyLikeData);
                    }
                    if ($check_type == 'image')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'imageCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->insertNotification($notifyLikeData);
                    }
                    if ($check_type == 'text')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'textCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->insertNotification($notifyLikeData);
                    }
                    if ($check_type == 'video')
                    {
                        $notifyLikeData=array(
                            'UID' => $this->session->userdata('userid'),
                            'FID'=>$this->input->post('tagger_id'),
                            'notify_type'=>'videoCommentLike',
                            'notify_type_id'=>$check_id,
                            'notify_details'=>'likes your comment'
                        );
                        $this->UsersModel->insertNotification($notifyLikeData);
                    }


                }
            }
            $data['totalCommentLike'] = $this->UsersModel->totalCommentLike($comment_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function commentProfilePage()
    {
        if ($this->input->post()) {
            $check = explode('_', $this->input->post('comments_type_id'));
            $check_type = $check[0];
            $check_id = $check[1];
            if ($check_type == 'tribute')
            {
                $tribute_id = $check_id;
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'tribute_id' => $tribute_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'tribute_id' => $tribute_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );
                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'tributeComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your tribute.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsTribute($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsTribute($tribute_id);
            }

            if ($check_type == 'image') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'image_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'image_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );

                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'imageComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your image.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsImage($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsImage($check_id);
            }
            if ($check_type == 'text') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'text_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'text_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );
                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'textComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your text.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsText($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsText($check_id);


            }
            if ($check_type == 'video') {
                if (empty($_FILES['comment_file']['name'])) {

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'video_id' => $check_id,
                        'description' => $this->input->post('comment_text')
                    );
                } else {
                    $directory = 'uploads/' . $this->session->userdata('userid') . '/comment_file/';
                    if (!is_dir($directory)) {
                        mkdir($directory, 0777, true);
                    }
                    $tmp_name = $_FILES["comment_file"]["tmp_name"];
                    $name = $this->session->userdata('userid').'_'.time().uniqid(rand()).'_'. $_FILES["comment_file"]["name"];
                    $config['upload_path']          = $directory;
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['file_name']            = $name;
                    $config['file_permissions'] 	= 0777;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('comment_file')) {
                        $error = array('error' => $this->upload->display_errors());
                    }
                    else {
                        $bucket_name='bucketemmortal';
                        $source_file=$tmp_name;
                        $image_directory=$directory.$name;
                        $this->aws3->sendFile($bucket_name,$source_file,$image_directory);

                    }

                    $commentsdata = array(
                        'user_id' => $this->session->userdata('userid'),
                        'video_id' => $check_id,
                        'description' => $this->input->post('comment_text'),
                        'image_comments' => $config['upload_path'] . $name
                    );

                }
                $notifyCommentsData=array(
                    'UID' => $this->session->userdata('userid'),
                    'FID'=>$this->input->post('tagger_id'),
                    'notify_type'=>'videoComment',
                    'notify_type_id'=>$check_id,
                    'notify_details'=>'commented on your video.'
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyCommentsData);
                $this->UsersModel->CommentsVideo($commentsdata);
                $data['commentsData'] = $this->UsersModel->showCommentsVideo($check_id);


            }
            $data['relationshipUsers'] = $this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
            $data['allLikecomment'] = $this->UsersModel->allLikecomment();
            echo json_encode($data);
        }

    }
    public function updateRelationshipsContentOrder()
    {
        if ($this->input->post()) {
            $alltagid = $this->input->post('alltagid');
            $res = $this->UsersModel->updateRelationshipsContentOrder($alltagid);
            echo $res;
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }
    public function addTribute()
    {
        if ($this->input->post()) {
            $tribute_friends_id = $this->input->post('tribute_friends_id');
            for ($i = 0; $i < count($tribute_friends_id); $i++) {
                $tributeData = array(
                    'user_id' => $this->session->userdata('userid'),
                    'friends_id' => $tribute_friends_id[$i],
                    'tribute_description' => $this->input->post('tributeDescription')
                );
                $res=$this->UsersModel->addTribute($tributeData);
                $notifyData=array(
                    'UID'   => $this->session->userdata('userid'),
                    'FID'  => $tribute_friends_id[$i],
                    'notify_type'=>'tagTribute',
                    'notify_type_id'=>$res->tribute_id,
                    'notificationdate'=>date("Y-m-d H:i:s"),
                    'notify_details'=>'Tagged you on a tribute'
                );
                $this->UsersModel->insertNotification($notifyData);
            }
            echo json_encode(TRUE);

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function tributes()
    {
        $userid = $this->uri->segment('3');
        if ($userid) {
            $data['title']='Emmortal - Tributes';
            $data['userdata'] = $this->UsersModel->fetchPerticularUserData($userid);
            if ($data['userdata']) {
                if ($this->session->userdata('userid')) {
                    $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
                    $data['total_record_album'] = count($album_name);
                    for ($i = 0; $i < count($album_name); $i++) {
                        $data['album_title'][$i] = $album_name[$i]['title'];
                        $data['albumeid'][$i] = $album_name[$i]['albumid'];
                    }
                }
                $data['existPinned']=$this->UsersModel->existPinnedPage($userid);
                $data['tributeCommentsCount'] = $this->UsersModel->tributeCommentsCount($userid);
                $data['tributesData'] = $this->UsersModel->fetchAllTributes($userid);
                $data['totalTributeLike'] = $this->UsersModel->allTributeLikes($userid);
                $data['existLike'] = $this->UsersModel->existUserLoginLikeTributes($this->session->userdata('userid'));
                $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                $this->body = 'Users/tributes';
                $this->load->view($this->body, $data, TRUE);
                $this->layout();
            } else {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function tributeDetails()
    {
        $userid = $this->uri->segment('3');
        $tribute_id = $this->input->get('tribute_id');
        if ($userid && $tribute_id) {
            $data['title']='Emmortal - Tribute Details';
            $data['userdata'] = $this->UsersModel->fetchPerticularUserData($userid);
            $data['tributesData'] = $this->UsersModel->fetchTributesDetailsByTributeId($userid,$tribute_id);
            if ($data['userdata'] && $data['tributesData']) {
                $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
                $data['total_record_album'] = count($album_name);
                for ($i = 0; $i < count($album_name); $i++) {
                    $data['album_title'][$i] = $album_name[$i]['title'];
                    $data['albumeid'][$i] = $album_name[$i]['albumid'];
                }
                $data['existPinned']=$this->UsersModel->existPinnedPage($userid);
                $data['totalTributeLike'] = $this->UsersModel->totalTributeLike($tribute_id);
                $data['existLike'] = $this->UsersModel->existUserLoginLikeTribute($this->session->userdata('userid'), $tribute_id);
                $data['tributeCommentsCount'] = $this->UsersModel->tributeCommentsCountByTributeId($tribute_id);
                $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                $this->body = 'Users/tribute_details';
                $this->load->view($this->body, $data, TRUE);
                $this->layout();
            } else {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function deleteRelationshipsTagContent()
    {
        if ($this->input->post()) {
            $row = $this->UsersModel->fetchRelationshipsTagContent($this->input->post('tagId'));
            if ($row->image_id) {
                $this->UsersModel->deleteImageLike($row->image_id);
                $this->UsersModel->deleteImageTribute($row->image_id);

            }
            if ($row->video_id) {
                $this->UsersModel->deleteImageLike($row->video_id);
                $this->UsersModel->deleteVideoTribute($row->video_id);
            }
            if ($row->text_id) {
                $this->UsersModel->deleteTextLike($row->text_id);
                $this->UsersModel->deleteTextTribute($row->text_id);
            }
            $res = $this->UsersModel->deleteRelationshipsTagContent($this->input->post('tagId'));
            echo json_encode($res);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function editTribute()
    {
        if ($this->input->post()) {
            $this->UsersModel->editTribute($this->input->post('tribute_id'), $this->input->post('tributeDescription'));
            echo json_encode(TRUE);

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function confirmDeleteTributeRequest()
    {
        if ($this->input->post())
        {
            $this->UsersModel->deleteTribute($this->input->post('tribute_id'));

            $notifyData1 = array(
                'UID' => $this->session->userdata('userid'),
                'FID' => $this->input->post('friends_id'),
                'notify_type' => 'deleteTributeRequestConfirm',
                'notify_type_id' => $this->input->post('tribute_id'),
                'notificationdate' => date("Y-m-d H:i:s"),
                'notify_details' => 'Sent you delete tribute request'
            );
            $this->UsersModel->updateNotificationDeleteTributeRequest($notifyData1);
            $notifyData2 = array(
                'UID' => $this->session->userdata('userid'),
                'FID' => $this->input->post('friends_id'),
                'notify_type' => 'confirmDeleteTributeRequest',
                'notify_type_id' => $this->input->post('tribute_id'),
                'notificationdate' => date("Y-m-d H:i:s"),
                'notify_details' => '<b>Confirm</b>Sent you delete tribute request'
            );
            $this->UsersModel->insertNotification($notifyData2);
            $notifyData3=
                array(
                    'UID' => $this->session->userdata('userid'),
                    'FID' => $this->input->post('friends_id'),
                    'notify_type' => 'tagTribute',
                    'notify_type_id' => $this->input->post('tribute_id')
                );
            $this->UsersModel->deleteNotificationTagTribute($notifyData3);

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function declineDeleteTributeRequest()
    {
        if ($this->input->post())
        {
            $notifyData1 = array(
                'UID' => $this->session->userdata('userid'),
                'FID' => $this->input->post('friends_id'),
                'notify_type' => 'deleteTributeRequestDecline',
                'notify_type_id' => $this->input->post('tribute_id'),
                'notificationdate' => date("Y-m-d H:i:s"),
                'notify_details' => 'Sent you delete tribute request'
            );
            $this->UsersModel->updateNotificationDeleteTributeRequest($notifyData1);
            $notifyData2 = array(
                'UID' => $this->session->userdata('userid'),
                'FID' => $this->input->post('friends_id'),
                'notify_type' => 'declineDeleteTributeRequest',
                'notify_type_id' => $this->input->post('tribute_id'),
                'notificationdate' => date("Y-m-d H:i:s"),
                'notify_details' => '<b>Declined</b>your delete tribute request'
            );
            $this->UsersModel->insertNotification($notifyData2);

        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function likeTribute()
    {
        if ($this->input->post()) {
            $tribute_id = $this->input->post('tribute_id');
            $data['existLike'] = $this->UsersModel->existUserLoginLikeTribute($this->session->userdata('userid'), $tribute_id);

            $notifyLikeData=array(
                'UID' => $this->session->userdata('userid'),
                'FID'=>$this->input->post('tagger_id'),
                'notify_type'=>'tributeLike',
                'notify_type_id'=>$tribute_id,
                'notify_details'=>'likes your post tribute'
            );
            if ($data['existLike']) {
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->deleteNotification($notifyLikeData);
                $this->UsersModel->dislikeTribute($this->session->userdata('userid'), $tribute_id);

            } else {
                $likeData = array(
                    'tribute_id' => $tribute_id,
                    'user_id' => $this->session->userdata('userid')
                );
                if($this->session->userdata('userid')!=$this->input->post('tagger_id'))
                    $this->UsersModel->insertNotification($notifyLikeData);
                $this->UsersModel->likeTribute($likeData);
            }
            $data['totalTributeLike'] = $this->UsersModel->totalTributeLike($tribute_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function showCommentsTribute()
    {
        if ($this->input->post()) {
            $tribute_id = $this->input->post('tribute_id');
            $data['commentsData'] = $this->UsersModel->showCommentsTribute($tribute_id);
            if ($this->session->userdata('userid')) {
                $data['relationshipUsers'] = $this->UsersModel->fetchAllRelationshipUsers($this->session->userdata('userid'));
            }
            $data['allLikecomment'] = $this->UsersModel->allLikecomment();
            echo json_encode($data);

        }
    }



    public function friendsList()
    {
        if ($this->input->post()) {
            $user_id = $this->session->userdata('userid');
            $userid = $this->input->post('userid');
            $search_friendName = $this->ImageUploadModel->search_friendName($user_id);
            $data = '';
            foreach ($search_friendName as $sf) {
                if ($sf->userid == $userid) {
                    $data = $data . '<option value="' . $sf->userid . '" selected="selected">' . $sf->fullname . '</option>';
                } else {
                    $data = $data . '<option value="' . $sf->userid . '">' . $sf->fullname . '</option>';
                }
            }
            echo $data;
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }

    public function scrollLoadHomeContent()
    {
        if ($this->input->post()) {

            $start = $this->input->post('start');
            $getAllHomeContent = $this->UsersModel->getAllHomeContent($start);
            $data = '';
            if ($getAllHomeContent) {
                foreach ($getAllHomeContent as $tfc) {
                    $data = $data . '<li>';
                    if ($tfc['image_id']) {
                        $totalLikeParticular=$this->UsersModel->totalLikePerticularImage($tfc['image_id']);
                        $totalCommentParticular=$this->UsersModel->totCommentParticularImage($tfc['image_id']);
                        $image_id = 'image_' . $tfc['image_id'];
                        if(!empty($tfc["thumbnail_upload_path"])||!empty($tfc["thumbnail_image_name"]))
                        {
                            $image = $tfc['thumbnail_upload_path'] . $tfc['thumbnail_image_name'];
                        }
                        else
                        {
                            $image = $tfc['uploadPath'] . $tfc['image_name'];
                        }
                        $title=$tfc['image_title'];
                        $desc=$tfc['image_desc'];
                        $fullname=$tfc['fullname'];
                        $album_name=$tfc['title'];
                        if (strlen($title) > 40)
                        {


                            $title = substr($title, 0, 40).'...';

                        }
                        if (strlen($desc) > 40)
                        {

                            // truncate string
                            $stringCut = substr($desc, 0, 40);

                            // make sure it ends in a word so assassinate doesn't become ass...
                            $desc = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                        }
                        if (strlen($fullname) > 13)
                        {
                            $fullname = substr($fullname, 0, 13).'...';

                        }
                        if (strlen($album_name) > 13)
                        {

                            $album_name = substr($album_name, 0, 13).'...';
                        }
                        $data = $data . '<a class="content_link" href="javascript:void(0)" id="relationshipsModalShow_' . $image_id . '" onClick="showModal(\''.$image_id .'\')">';
                        $data = $data . '<img alt="image" class="lazy" src="' . $this->config->item("cloudfront_base_url") . $image . '"/>';
                        $data = $data . '<input type="hidden" value="' . $title . '" class="content_title">';
                        $data = $data . '<input type="hidden" value="' . $desc . '" class="content_description">';
                        $data = $data . '<input type="hidden" value="' . $fullname . '" class="tagger_name">';
                        $data = $data . '<input type="hidden" value="' . $tfc['userid'] . '" class="tagger_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['pageid'] . '" class="tagger_page_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['albumid'] . '" class="album_id">';
                        $data = $data . '<input type="hidden" value="' . $album_name . '" class="album_name">';
                        if ($tfc['profileimage']) {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . $tfc['profileimage'] . '" style="display: none" />';
                        } else {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                        }
                        $data = $data .'<small>';
                        if($totalCommentParticular) {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_' . $image_id . '">
                                                   ' . $totalCommentParticular->total . '
                                                </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_'.$image_id.'">
                                                0
                                                </span>';
                        }
                        if($totalLikeParticular)
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $image_id . '">
                                 ' . $totalLikeParticular->total . '
                            </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $image_id . '">
                                 0
                            </span>';
                        }
                        $data = $data .'</small>';
                        $data = $data . '</a>';
                        $data = $data . '</li>';

                    }
                    if ($tfc['text_id']) {
                        $text_id='text_'.$tfc['text_id'];
                        $totalLikeParticular=$this->UsersModel->totalLikePerticularText($tfc['text_id']);
                        $totalCommentParticular=$this->UsersModel->totCommentParticularText($tfc['text_id']);
                        $string = strip_tags($tfc['text_title']);
                        if (strlen($string) > 140)
                        {
                            $string = substr($string, 0, 140).'...';
                        }
                        $data = $data . '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' . $text_id . '" onClick="showModal(\''.$text_id .'\')">';
                        $data = $data . '<p class="text_description">'.$string.'</p>';
                        $data = $data . '<input type="hidden" value="' . $tfc['text_title'] . '" class="content_title">';
                        $data = $data . '<input type="hidden" value="' . $tfc['text_description'] . '" class="content_description">';
                        $data = $data . '<input type="hidden" value="' . $tfc['fullname'] . '" class="tagger_name">';
                        $data = $data . '<input type="hidden" value="' . $tfc['userid'] . '" class="tagger_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['pageid'] . '" class="tagger_page_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['albumid'] . '" class="album_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['title'] . '" class="album_name">';
                        if ($tfc['profileimage']) {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . $tfc['profileimage'] . '" style="display: none" />';
                        } else {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                        }
                        $data = $data .'<small>';
                        if($totalCommentParticular) {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_' . $text_id . '">
                                                   ' . $totalCommentParticular->total . '
                                                </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_'.$text_id.'">
                                                0
                                                </span>';
                        }
                        if($totalLikeParticular)
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $text_id . '">
                                 ' . $totalLikeParticular->total . '
                            </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $text_id . '">
                                 0
                            </span>';
                        }
                        $data = $data .'</small>';
                        $data = $data . '</a>';
                        $data = $data . '</li>';

                    }
                    if ($tfc['video_id']) {
                        $video_id='video_'.$tfc['video_id'];
                        $video='https://player.vimeo.com/video/'.$tfc['vimeo_video_id'];
                        $totalLikeParticular=$this->UsersModel->totalLikeParticularVideo($tfc['video_id']);
                        $totalCommentParticular=$this->UsersModel->totCommentParticularVideo($tfc['video_id']);

                        $title=$tfc['video_title'];
                        $desc=$tfc['video_desc'];
                        $fullname=$tfc['fullname'];
                        $album_name=$tfc['title'];
                        if (strlen($title) > 40)
                        {


                            $title = substr($title, 0, 40).'...';

                        }
                        if (strlen($desc) > 40)
                        {

                            // truncate string
                            $stringCut = substr($desc, 0, 40);

                            // make sure it ends in a word so assassinate doesn't become ass...
                            $desc = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                        }
                        if (strlen($fullname) > 13)
                        {
                            $fullname = substr($fullname, 0, 13).'...';

                        }
                        if (strlen($album_name) > 13)
                        {

                            $album_name = substr($album_name, 0, 13).'...';
                        }
                        $data = $data .'<iframe src="'.$video.'" width="100%" height="100px" frameborder="0"></iframe>';
                        $data = $data . '<a class="content_link content_linkvd" href="javascript:void(0)" id="relationshipsModalShow_' . $video_id . '" onClick="showModal(\''.$video_id.' \')">';
                        $data = $data . ' <iframe src="'.$video.'" width="100%" height="100px" frameborder="0" style="display:none;"></iframe>';
                        $data = $data . '<input type="hidden" value="' . $title . '" class="content_title">';
                        $data = $data . '<input type="hidden" value="' . $desc . '" class="content_description">';
                        $data = $data . '<input type="hidden" value="' . $fullname . '" class="tagger_name">';
                        $data = $data . '<input type="hidden" value="' . $tfc['userid'] . '" class="tagger_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['pageid'] . '" class="tagger_page_id">';
                        $data = $data . '<input type="hidden" value="' . $tfc['albumid'] . '" class="album_id">';
                        $data = $data . '<input type="hidden" value="' . $album_name . '" class="album_name">';
                        if ($tfc['profileimage']) {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . $tfc['profileimage'] . '" style="display: none" />';
                        } else {
                            $data = $data . '<img alt="image" class="lazy tagger_image" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" style="display: none" />';
                        }
                        $data = $data .'<small>';
                        if($totalCommentParticular) {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_' . $video_id . '">
                                                   ' . $totalCommentParticular->total . '
                                                </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-comment" aria-hidden="true"></i> 
                                                <span id="comment_perticular_'.$video_id.'">
                                                0
                                                </span>';
                        }
                        if($totalLikeParticular)
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $video_id . '">
                                 ' . $totalLikeParticular->total . '
                            </span>';
                        }
                        else
                        {
                            $data = $data . '<i class="fa fa-heart color-red" aria-hidden="true"></i> 
                            <span id="like_particular_' . $video_id . '">
                                 0
                            </span>';
                        }
                        $data = $data .'</small>';
                        $data = $data . '</a>';
                        $data = $data . '</li>';

                    }
                }

            }
            echo $data;
        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('/');

        }

    }
    public function profile()
    {
        $userid = $this->uri->segment('3');
        if ($userid) {
            $data['title']='Emmortal - Profile Page';
            $data['profileData'] = $this->UsersModel->userExistOrNot($userid);
            if ($data['profileData']) {
                $page_id = $this->input->get('page_id');
                if ($page_id) {
                    $data['pageData'] = $this->UsersModel->pageExistOrNot($userid, $page_id);
                    if ($data['pageData'])
                    {
                        $seeMyProfileStatus=$this->UsersModel->seeMyProfileStatus($userid);
                        if(($seeMyProfileStatus['seeme']=='friends') && !($this->session->userdata('userid')))
                        {
                            $this->session->set_flashdata('invalid_page', 'Profile view permission denied!!!');
                            redirect('/');
                        }

                        if (($seeMyProfileStatus['seeme']=='friends') && $this->session->userdata('userid') && ($this->session->userdata('userid')!=$userid ))
                        {
                            $checkFriendOrNot=$this->UsersModel->checkFriendOrNot($userid,$this->session->userdata('userid'));
                            if(!$checkFriendOrNot)
                            {
                                $this->session->set_flashdata('invalid_page', 'Profile view permission denied!!!');
                                redirect('Users/dashboard');
                            }
                        }
                        if ($this->session->userdata('userid')) {
                            $album_name = $this->ImageUploadModel->select_album_name($this->session->userdata('userid'));
                            $data['total_record_album'] = count($album_name);
                            for ($i = 0; $i < count($album_name); $i++) {
                                $data['album_title'][$i] = $album_name[$i]['title'];
                                $data['albumeid'][$i] = $album_name[$i]['albumid'];
                            }
                            $data['isfriends']=$this->UsersModel->isFriendsOrNot($userid,$this->session->userdata('userid'));
                            $data['isfriendsRequest']=$this->UsersModel->isFriendsRequestOrNot($userid,$this->session->userdata('userid'));
                            $data['user_data'] = $this->UsersModel->fetchPerticularUserData($this->session->userdata('userid'));
                        }

                        $data['pageAllContentCount']=$this->UsersModel->pageAllContentCount($page_id);
                        $data['page_content'] = $this->UsersModel->pageContent($page_id);
                        $data['pages'] = $this->UsersModel->pageDetails($userid);
                        $data['userdata'] = $this->UsersModel->fetchPerticularUserData($userid);
                        $data['fetchForegroundStyle'] =$this->UsersModel->fetchForegroundStyle();
                        if(!empty($data['page_content']->page_foreground_id))
                        {
                            $select_page_foreground_image=$this->UsersModel->select_page_foreground_image($data['page_content']->page_foreground_id);
                            if($select_page_foreground_image)
                            {
                                foreach($select_page_foreground_image as $foreground_images)
                                {
                                    $data['foreground_image_name']=$foreground_images['image_name'];
                                    $data['foreground_upload_path']=$foreground_images['upload_path'];
                                }
                            }
                        }
                        else
                        {
                            $data['foreground_image_name']='';
                            $data['foreground_upload_path']='';
                        }
                        $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
                        $this->body = 'Users/profile_page'; // passing body to function. change this for different views.
                        $this->load->view($this->body, $data, TRUE);
                        $this->layout();
                    } else {
                        $this->session->set_flashdata('invalid_page', 'Page was not found');
                        redirect('Users/dashboard');
                    }

                } else {
                    $this->session->set_flashdata('invalid_page', 'Page was not found');
                    redirect('Users/dashboard');
                }

            } else {
                $this->session->set_flashdata('invalid_page', 'Page was not found');
                redirect('Users/dashboard');
            }
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function pageContent()
    {
        if ($this->input->post())
        {
            $data['fetchBackgroundStyle'] =$this->UsersModel->fetchBackgroundStyle();
            $data['page_content'] = $this->UsersModel->pageContent($this->input->post('page_id'));
            $data['pageAllContent'] = $this->UsersModel->pageAllContent($this->input->post('page_id'));
            $data['totalCommentParticularImage'] = $this->UsersModel->totalCommentParticularImage($this->input->post('page_id'));
            $data['totalLikeParticularImage'] = $this->UsersModel->totalLikeParticularImage($this->input->post('page_id'));
            $data['totalLikeParticularText'] = $this->UsersModel->totalLikeParticularText($this->input->post('page_id'));
            $data['totalCommentParticularText'] = $this->UsersModel->totalCommentParticularText($this->input->post('page_id'));
            $data['totalLikeParticularVideo'] = $this->UsersModel->totalLikePerticularVideo($this->input->post('page_id'));
            $data['totalCommentParticularVideo'] = $this->UsersModel->totalCommentParticularVideo($this->input->post('page_id'));
            $data['totalContentParticularAlbum'] = $this->UsersModel->totalContentParticularAlbum($this->input->post('page_id'));
            $data['totalLikeParticularAlbum'] = $this->UsersModel->totalLikeParticularAlbum($this->input->post('page_id'));
            $data['totalCommentParticularAlbum'] = $this->UsersModel->totalCommentParticularAlbum($this->input->post('page_id'));
            $data['totalLikeParticularTribute'] = $this->UsersModel->totalLikeParticularTribute($this->input->post('page_id'));
            $data['totalCommentParticularTribute'] = $this->UsersModel->totalCommentParticularTribute($this->input->post('page_id'));
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function imagePinEditDelete()
    {
        if ($this->input->post())
        {
            $image_id=$this->input->post('image_id');
            $userid=$this->session->userdata('userid');
            $data['allPageId']=$this->UsersModel->allPageId($userid);
            $data['selectedPageId']=$this->UsersModel->selectedPageIdImage($userid,$image_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function textPinEditDelete()
    {
        if ($this->input->post())
        {
            $text_id=$this->input->post('text_id');
            $userid=$this->session->userdata('userid');
            $data['allPageId']=$this->UsersModel->allPageId($userid);
            $data['selectedPageId']=$this->UsersModel->selectedPageIdText($userid,$text_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function videoPinEditDelete()
    {
        if ($this->input->post())
        {
            $video_id=$this->input->post('video_id');
            $userid=$this->session->userdata('userid');
            $data['allPageId']=$this->UsersModel->allPageId($userid);
            $data['selectedPageId']=$this->UsersModel->selectedPageIdVideo($userid,$video_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function tributePinEditDelete()
    {
        if ($this->input->post())
        {
            $tribute_id=$this->input->post('tribute_id');
            $userid=$this->session->userdata('userid');
            $data['allPageId']=$this->UsersModel->allPageId($userid);
            $data['selectedPageId']=$this->UsersModel->selectedPageIdTribute($userid,$tribute_id);
            echo json_encode($data);
        } else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updateContentPageImage()
    {
        if ($this->input->post())
        {
            $image_id=$this->input->post('image_id');
            $page_id=$this->input->post('page_id');
            $userid=$this->session->userdata('userid');
            $Pagedata=array(
                'page_id'=>$page_id,
                'user_id'=>$userid,
                'image_id'=>$image_id
            );
            $data=array(
                'user_id'=>$userid,
                'image_id'=>$image_id
            );
            $existData=$this->UsersModel->selectedPageIdImage($userid,$image_id);
            if($existData)
            {

                $this->UsersModel->updatePagerecordsdragdropImage($Pagedata);
                echo json_encode(TRUE);
            }
            else
            {
                $selectedPageId=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                if($selectedPageId)
                {
                    $this->UsersModel->updatePagerecordsdragdropImage($Pagedata);
                    echo json_encode($selectedPageId);
                }
                else
                {
                    $this->UsersModel->insertPagerecordsdragdrop($Pagedata);
                    echo json_encode(FALSE);
                }

            }

        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }

    public function updateContentPageVideo()
    {
        if ($this->input->post())
        {
            $video_id=$this->input->post('video_id');
            $page_id=$this->input->post('page_id');
            $userid=$this->session->userdata('userid');
            $Pagedata=array(
                'page_id'=>$page_id,
                'user_id'=>$userid,
                'video_id'=>$video_id
            );
            $data=array(
                'user_id'=>$userid,
                'video_id'=>$video_id
            );
            $existData=$this->UsersModel->selectedPageIdVideo($userid,$video_id);
            if($existData)
            {

                $this->UsersModel->updatePagerecordsdragdropVideo($Pagedata);
                echo json_encode(TRUE);
            }
            else
            {
                $selectedPageId=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                if($selectedPageId)
                {
                    $this->UsersModel->updatePagerecordsdragdropVideo($Pagedata);
                    echo json_encode(TRUE);
                }
                else {
                    $this->UsersModel->insertPagerecordsdragdrop($Pagedata);
                    echo json_encode(FALSE);
                }
            }

        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updateContentPageText()
    {
        if ($this->input->post())
        {
            $text_id=$this->input->post('text_id');
            $page_id=$this->input->post('page_id');
            $userid=$this->session->userdata('userid');
            $Pagedata=array(
                'page_id'=>$page_id,
                'user_id'=>$userid,
                'text_id'=>$text_id
            );
            $data=array(
                'user_id'=>$userid,
                'text_id'=>$text_id
            );
            $existData=$this->UsersModel->selectedPageIdText($userid,$text_id);
            if($existData)
            {

                $this->UsersModel->updatePagerecordsdragdropText($Pagedata);
                echo json_encode(TRUE);
            }
            else
            {
                $selectedPageId=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                if($selectedPageId)
                {
                    $this->UsersModel->updatePagerecordsdragdropText($Pagedata);
                    echo json_encode(TRUE);
                }
                else {
                    $this->UsersModel->insertPagerecordsdragdrop($Pagedata);
                    echo json_encode(FALSE);
                }
            }

        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updateContentPageTribute()
    {
        if ($this->input->post())
        {
            $tribute_id=$this->input->post('tribute_id');
            $page_id=$this->input->post('page_id');
            $userid=$this->session->userdata('userid');
            $Pagedata=array(
                'page_id'=>$page_id,
                'user_id'=>$userid,
                'tribute_id'=>$tribute_id
            );
            $data=array(
                'user_id'=>$userid,
                'tribute_id'=>$tribute_id
            );
            $existData=$this->UsersModel->selectedPageIdTribute($userid,$tribute_id);
            if($existData)
            {

                $this->UsersModel->updatePagerecordsdragdropTribute($Pagedata);
                echo json_encode(TRUE);
            }
            else
            {
                $selectedPageId=$this->UsersModel->existPagerecordsdragdrop($Pagedata);
                if($selectedPageId)
                {
                    $this->UsersModel->updatePagerecordsdragdropTribute($Pagedata);
                    echo json_encode($selectedPageId);
                }
                else
                {
                    $this->UsersModel->insertPagerecordsdragdrop($Pagedata);
                    echo json_encode(FALSE);
                }

            }

        }
        else {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageImage()
    {
        if ($this->input->post())
        {
            $image_id=$this->input->post('image_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'image_id'=>$image_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
            {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageText()
    {
        if ($this->input->post())
        {
            $text_id=$this->input->post('text_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'text_id'=>$text_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageVideo()
    {
        if ($this->input->post())
        {
            $video_id=$this->input->post('video_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'video_id'=>$video_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageProfileImage()
    {
        if ($this->input->post())
        {
            $user_profile_id=$this->input->post('user_profile_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'user_profile_id'=>$user_profile_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageUserDescription()
    {
        if ($this->input->post())
        {
            $user_description_id=$this->input->post('user_description_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'user_description_id'=>$user_description_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deleteContentPageTribute()
    {
        if ($this->input->post())
        {
            $tribute_id=$this->input->post('tribute_id');
            $userid=$this->session->userdata('userid');
            $data=array(
                'user_id'=>$userid,
                'tribute_id'=>$tribute_id
            );
            $this->UsersModel->deleteContentPage($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updatePageContentResizableDraggable()
    {
        if ($this->input->post())
        {
            $id=$this->input->post('id');
            $sizex=$this->input->post('sizex');
            $sizey=$this->input->post('sizey');
            $col=$this->input->post('col');
            $row=$this->input->post('row');
            $count=1;
            for($i=0;$i<count($id);$i++)
            {
                $data=array(
                            'records_order'=>$count,
                            'dragdrop_id'=>$id[$i],
                            'sizex'=>$sizex[$i],
                            'sizey'=>$sizey[$i],
                            'col'=>$col[$i],
                            'row'=>$row[$i],
                        );
                $this->UsersModel->updatePageContentResizableDraggable($data);
                $count++;
            }

        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function privacySettings()
    {
        if ($this->input->post())
        {
            $data = array(
                'userid'=>  $this->session->userdata('userid'),
                'findme' => $this->input->post('findme'),
                'seeme' =>  $this->input->post('seeme')
            );
            $this->UsersModel->updatePrivacySettings($data);
            echo json_encode(TRUE);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function sendRequestDeleteTribute()
    {
        if ($this->input->post())
        {
            $notifyData = array(
                'UID' => $this->session->userdata('userid'),
                'FID' => $this->input->post('friends_id'),
                'notify_type' => 'deleteTributeRequest',
                'notify_type_id' => $this->input->post('tribute_id'),
                'notificationdate' => date("Y-m-d H:i:s"),
                'notify_details' => 'Sent you delete tribute request'
            );
            $this->UsersModel->insertNotification($notifyData);
            echo json_encode(TRUE);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updateNotifySeen()
    {
        if ($this->input->post())
        {
            $this->UsersModel->updateNotifySeen($this->input->post('notifyId'));
            //echo json_encode($this->input->post('notifyId'));
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function updatePage()
    {
        if ($this->input->post())
        {
            $data= array
                (
                    'pageid'=>$this->input->post('edit_page_id'),
                    'page_style_id'=>$this->input->post('edit_page_style_id'),
                    'page_title'=>$this->input->post('edit_page_title'),
                    'page_foreground_id'=>$this->input->post('editPageForegroundStyleId'),
                );
            $this->UsersModel->updatePage($data);
            redirect($this->agent->referrer());
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function deletePage()
    {
        if ($this->input->post())
        {
            $page_id=$this->input->post('page_id');
            $user_id=$this->session->userdata('userid');
            $this->UsersModel->deletePage($page_id,$user_id);
            echo json_encode(TRUE);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }

    }
    public function checkSessionLogin()
    {
        if ($this->input->post())
        {
            $user_id=$this->input->post('user_id');
            $data['session_data']=$this->UsersModel->fetchSessionData($user_id);
            echo json_encode($data);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function keepMeSignnedIn()
    {
        $sess_data = array(
            'password' => $this->input->post('password'),
            'emailid'=>$this->input->post('email'),
        );
        $this->session->set_userdata($sess_data);
    }
    public function sendChangeEmail($data)
    {
        $this->email->from('emmortal@emmortal.com', 'Emmortal');
        $this->email->to($data['emailid']);

        $this->email->subject('change Email instructions');
        $this->email->message($this->load->view('emails/change_email_template', $data, TRUE));

        if (!$this->email->send()) {
            show_error($this->email->print_debugger());
        } else {
            echo json_encode('success');
        }
    }
    public function changeEmail()
    {
        $old_email_id = $this->uri->segment('3');
        $emailid = $this->uri->segment('4');
        if ($emailid && $old_email_id)
        {
            $oldemailid = urldecode($old_email_id);
            $email = urldecode($emailid);
            $res = $this->UsersModel->checkChangeEmail($oldemailid);
            if ($res)
            {
                $this->UsersModel->changeEmail($oldemailid,$email);
                $this->session->set_flashdata('success_email_change', 'Your email id is change successfully');
                redirect('Users/settings#parentVerticalTab1');
            }
            else
            {
                $this->session->set_flashdata('error_email_change', 'Confirmation token was expired');
                redirect('Users/settings#parentVerticalTab1');

            }
        }
        else {
            $this->session->set_flashdata('error_email_change', 'Confirmation token Is invalid');
            redirect('/');
        }
    }
    public function addFriends()
    {
        if ($this->input->post())
        {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friends_id'),
                'relationshipstatus' => 'outgoing'
            );
            $res=$this->UsersModel->addFriend($data);
            $notifyData=array(
                'UID'   => $this->session->userdata('userid'),
                'FID'  => $this->input->post('friends_id'),
                'notify_type'=>'friendsRequest',
                'notify_type_id'=>$res->id,
                'notificationdate'=>date("Y-m-d H:i:s"),
                'notify_details'=>'Sent you relationship request'
            );
            $this->UsersModel->insertNotification($notifyData);
            echo json_encode($res);

        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function unfriend()
    {
        if ($this->input->post())
        {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friends_id'),
                'relationshipstatus' => 'accepted',
            );
            $this->UsersModel->unfriend($data);
            echo json_encode(TRUE);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function cancelSentRequest()
    {
        if ($this->input->post())
        {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friends_id'),
                'relationshipstatus' => 'outgoing',
            );
            $this->UsersModel->cancelSentRequest($data);
            echo json_encode(TRUE);
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }
    public function cancelSentRequestOutgoing()
    {
        if ($this->input->post())
        {
            $data = array(
                'userid' => $this->session->userdata('userid'),
                'friendsid' => $this->input->post('friends_id'),
                'relationshipstatus' => 'outgoing',
            );
            $this->UsersModel->cancelSentRequest($data);
            $allOutgoingUsers = $this->UsersModel->fetchAllOutgoingUsers($this->session->userdata('userid'));
            if ($allOutgoingUsers) {
                foreach ($allOutgoingUsers as $aud) {
                    $userdata = $this->UsersModel->fetchPerticularUserData($aud->userid);
                    $data = '
                        <li>
                            <div class="relation-page">';
                    if ($aud->profileimage == '') {
                        $data = $data . '<div class="col-lg-3 text-center">
                                        <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/></span>
                                    </div>';
                    } else {
                        $data = $data . '
                                    <div class="col-lg-3 text-center">
                                        <span><img alt="image" class="lazy relation-profile-img use-border-solid" src="' . $this->config->item("cloudfront_base_url") . $aud->profileimage . '"/></span>
                                    </div>';
                    }
                    $data = $data . ' <div class="col-lg-3 text-center">
                                         <a href="' . base_url() . 'Users/profile/' . $aud->userid . '?page_id=' . $userdata["pageid"] . '"> <b>' . $aud->firstname . ' ' . $aud->lastname . '</b></a>
                                     </div>
                                     <div class="col-lg-3 text-center">&nbsp;</div>
                                     <div class="col-lg-2">
                                         <ul class="right-list">
                                            <button onclick="requestSentFunction()" class="btn btn-success dropdown-btn">
                                                <div class="fa fa-clock-o"></div>
                                                Request Sent &nbsp;<span class="caret"></span>
                                            </button>
                                            <div class="dropdown-cancel-content cancelRequestDropdown">
                                                <button onclick="cancelRequest('.$aud->userid.')" class="btn btn-warning"><i class="fa fa-times"></i> Cancel Request</button>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </li>';
                    echo $data;
                }

            }
            else {
                $data = '<h2 style="text-align: center">There are no outgoing relationships requests</h2>';
                echo $data;

            }
        }
        else
        {
            $this->session->set_flashdata('invalid_page', 'Page was not found');
            redirect('Users/dashboard');
        }
    }


}



