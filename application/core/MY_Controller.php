<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
 { 
   //set the class variable.
   var $template  = array();
   var $data      = array();
   //Load layout    
   public function adminLayout() {
     // making temlate and send data to view.
     $this->template['header']   = $this->load->view('adminLayout/header', $this->data, true);
     $this->template['left']   = $this->load->view('adminLayout/left', $this->data, true);
     $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
     $this->template['footer'] = $this->load->view('adminLayout/footer', $this->data, true);
     $this->load->view('adminLayout/index', $this->template);
   }
   public function layout() {
     // making temlate and send data to view.
     $this->template['header']   = $this->load->view('layout/header', $this->data, true);
     $this->template['body'] = $this->load->view($this->body, $this->data, true);
     $this->template['footer'] = $this->load->view('layout/footer', $this->data, true);
     $this->load->view('layout/index', $this->template);
   }
}
?>