<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <META http-equiv="Content-type" content="text/html; charset=UTF-8">
    <meta name="description" content="Your family photo album for the interactive age, Emmortal.com is your opportunity to decide how you want your lifetime of experiences, passions, and celebrations to be remembered for future generations.">
    <meta name="keywords" content="emmortal,social networking service,social network,social media apps,social media marketing,social media sites,social media service,social networking apps,social networking sites,social media manager,what is social media,social media,social marketing,social media management,social media platforms,social media strategy,social sites,social media jobs,all social media,social media analytics,social network analysis,social media news,social media campaign,social media agency,social media marketing strategy,social tools,social add world,social media sign up,social network news">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title><link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/favicon.png" />
    <!-- css Link -->
    <?php $this->load->view('Users/css_link') ?>
    <!-- js Script -->
    <?php $this->load->view('Users/js_script') ?>
</head>
