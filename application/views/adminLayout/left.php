  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <?php  if(!empty($userdata['profileimage'])){ ?>
                <img src="<?php echo $this->config->item("cloudfront_base_url").''.$userdata['profileimage'];?>" class="img-circle" alt="User Image" style="width:26px; height:18px;">
            <?php }else{?>
                <img src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" class="img-circle" alt="User Image" style="width:26px; height:18px;">
            <?php }?>
        </div>
        <div class="pull-left info">
          <p><?php echo $userdata['fullname'];?></p>
          <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php if($this->uri->segment(2)=='dashboard'){?>
        <li class="active">
          <a href="<?php echo base_url()?>Admin/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>          
        </li>
        <?php }else{?>
         <li class="">
          <a href="<?php echo base_url()?>Admin/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>          
        </li>
        <?php }?>
        <?php if($this->uri->segment(2)=='userLists'){?>
        <li class="active">
          <a href="<?php echo base_url()?>Admin/userLists"><i class="fa fa-user" aria-hidden="true"></i><span>Users</span></a>
        </li>
        <?php }else{?>
        <li class="">
          <a href="<?php echo base_url()?>Admin/userLists"><i class="fa fa-user" aria-hidden="true"></i><span>Users</span></a>
        </li>
        <?php }?>
          <?php if($this->uri->segment(2)=='defaultBackgroundStyle'){?>
          <li class="active">
              <a href="<?php echo base_url()?>Admin/defaultBackgroundStyle"><i class="fa fa-image"></i><span>Default Background Style</span></a>
          </li>
          <?php }else{?>
          <li class="">
              <a href="<?php echo base_url()?>Admin/defaultBackgroundStyle"><i class="fa fa-image"></i><span>Default Background Style</span></a>
          </li>
          <?php }?>
          <?php if($this->uri->segment(2)=='defaultForegroundStyle'){?>
              <li class="active">
                  <a href="<?php echo base_url()?>Admin/defaultForegroundStyle"><i class="fa fa-file-image-o"></i><span>Default Foreground Style</span></a>
              </li>
          <?php }else{?>
              <li class="">
                  <a href="<?php echo base_url()?>Admin/defaultForegroundStyle"><i class="fa fa-file-image-o"></i><span>Default Foreground Style</span></a>
              </li>
          <?php }?>
          <li class="treeview">
              <a href="javascript:void(0)">
                  <i class="fa fa-folder-open"></i>
                  <span>Album Style</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <?php if($this->uri->segment(2)=='albumBackgroundStyle'){?>
                      <li class="active">
                          <a href="<?php echo base_url()?>Admin/albumBackgroundStyle"><i class="fa fa-image" aria-hidden="true"></i> <span>Album Background Style</span></a>
                      </li>
                  <?php }else{?>
                      <li class="">
                          <a href="<?php echo base_url()?>Admin/albumBackgroundStyle"><i class="fa fa-image" aria-hidden="true"></i> <span>Album Background Style</span></a>
                      </li>
                  <?php }?>
                  <?php if($this->uri->segment(2)=='albumForegroundStyle'){?>
                      <li class="active">
                          <a href="<?php echo base_url()?>Admin/albumForegroundStyle"><i class="fa fa-file-image-o"></i><span>Album Foreground Style</span></a>
                      </li>
                  <?php }else{?>
                      <li class="">
                          <a href="<?php echo base_url()?>Admin/albumForegroundStyle"><i class="fa fa-file-image-o"></i><span>Album Foreground Style</span></a>
                      </li>
                  <?php }?>
              </ul>
          </li>
          <li class="treeview">
              <a href="javascript:void(0)">
                  <i class="fa fa-folder-open"></i>
                  <span>Page Style</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <?php if($this->uri->segment(2)=='pageBackgroundStyle'){?>
                      <li class="active">
                          <a href="<?php echo base_url()?>Admin/pageBackgroundStyle"><i class="fa fa-image" aria-hidden="true"></i> <span>Page Background Style</span></a>
                      </li>
                  <?php }else{?>
                      <li class="">
                          <a href="<?php echo base_url()?>Admin/pageBackgroundStyle"><i class="fa fa-image" aria-hidden="true"></i> <span>Page Background Style</span></a>
                      </li>
                  <?php }?>
                  <?php if($this->uri->segment(2)=='pageForegroundStyle'){?>
                      <li class="active">
                          <a href="<?php echo base_url()?>Admin/pageForegroundStyle"><i class="fa fa-file-image-o"></i><span>Page Foreground Style</span></a>
                      </li>
                  <?php }else{?>
                      <li class="">
                          <a href="<?php echo base_url()?>Admin/pageForegroundStyle"><i class="fa fa-file-image-o"></i><span>Page Foreground Style</span></a>
                      </li>
                  <?php }?>
              </ul>
          </li>

        <?php if($this->uri->segment(2)=='deletedvideo'){?>    
		<li class="active">
          <a href="<?php echo base_url()?>Admin/deletedvideo"><i class="fa fa-video-camera" aria-hidden="true"></i> <span>Deleted Video</span></a>
        </li>
        <?php }else{?>   
         <li class="">
          <a href="<?php echo base_url()?>Admin/deletedvideo"><i class="fa fa-video-camera" aria-hidden="true"></i> <span>Deleted Video</span></a>          
        </li>
        <?php }?>
         <?php if($this->uri->segment(2)=='supportquestion'){?>  
		<li class="active">
          <a href="<?php echo base_url()?>Admin/supportquestion"><i class="fa fa-question" aria-hidden="true"></i> <span>Support Question</span></a>          
        </li>
        <?php }else{?>
         <li class="">
          <a href="<?php echo base_url()?>Admin/supportquestion"><i class="fa fa-question" aria-hidden="true"></i> <span>Support Question</span></a>          
        </li>
         <?php }?>   	
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>