<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="<?php echo base_url() ?>Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $total_user;?></h3>

              <p>Total Users</p>
            </div>
            <div class="icon">
              <span class="glyphicon glyphicon-user"></span>
            </div>
            <a href="<?php echo base_url()?>Admin/userLists" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                  <div class="inner">
                      <h3><?php echo $total_background_style;?></sup></h3>

                      <p>Default Background Style</p>
                  </div>
                  <div class="icon">
                      <i class="fa fa-file-image-o" aria-hidden="true"></i>
                  </div>
                  <a href="<?php echo base_url()?>Admin/defaultBackgroundStyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-fuchsia">
                  <div class="inner">
                      <h3><?php echo $total_default_foreground_style;?></sup></h3>

                      <p>Default Foreground Style</p>
                  </div>
                  <div class="icon">
                      <i class="fa fa-image" aria-hidden="true"></i>
                  </div>
                  <a href="<?php echo base_url()?>Admin/defaultForegroundStyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                  <div class="inner">
                      <h3><?php echo $total_album_style;?></sup></h3>

                      <p>Album Background Style</p>
                  </div>
                  <div class="icon">
                      <i class="fa fa-image" aria-hidden="true"></i>
                  </div>
                  <a href="<?php echo base_url()?>Admin/albumBackgroundStyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-maroon">
                  <div class="inner">
                      <h3><?php echo $total_foreground_style;?></sup></h3>

                      <p>Album Foreground Style</p>
                  </div>
                  <div class="icon">
                      <i class="fa fa-file-image-o" aria-hidden="true"></i>
                  </div>
                  <a href="<?php echo base_url()?>Admin/albumForegroundStyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $total_page_style;?></h3>

              <p>Page Background Style</p>
            </div>
            <div class="icon">
             <i class="fa fa-image" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url()?>Admin/pageBackgroundstyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-navy">
                  <div class="inner">
                      <h3><?php echo $total_page_foreground_style;?></sup></h3>

                      <p>Page Foreground Style</p>
                  </div>
                  <div class="icon">
                      <i class="fa fa-file-image-o" aria-hidden="true"></i>
                  </div>
                  <a href="<?php echo base_url()?>Admin/pageForegroundStyle" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
          </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $total_deleted_video;?></h3>

              <p>Deleted Video</p>
            </div>
            <div class="icon">
              <i class="fa fa-video-camera" aria-hidden="true"></i>
            </div>
            <a href="<?php echo base_url()?>Admin/deletedvideo" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-fuchsia">
            <div class="inner">
              <h3><?php echo $total_user_given_question;?></h3>

              <p>User Given Question</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
            <a href="<?php echo base_url()?>Admin/supportquestion" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->