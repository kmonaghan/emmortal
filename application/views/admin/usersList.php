<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>User Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>
      <!-- Main content -->
      <section class="content">
          <div class="row">
              <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Registered users list</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <table id="example1" class="display table table-bordered table-hover">
                              <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email Id</th>
                                        <th>DOB</th>
                                        <th>User Type</th>
                                    </tr>
                            </thead>
                            <!--Table head-->
                            <tbody>
                                <?php
                                if($userList) {
                                    foreach ($userList as $user) { ?>
                                        <tr>
                                            <td><?php echo $user['fullname']; ?></td>
                                            <td><?php echo $user['emailid']; ?></td>
                                            <td><?php echo date('d-M-Y', strtotime($user['dob'])); ?></td>
                                            <td><?php echo $user['userstype']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </div>
