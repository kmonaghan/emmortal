<?php
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin", "*");
header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <section class="content-header">
      <h1>
        Deleted Video
        <small>User Deleted Video List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Deleted Video</li>
      </ol>
    </section>
    <!-- Main content -->
      <section class="content">
          <div class="row">
              <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Deleted Video List</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          <table id="example1" class="display table table-bordered table-hover">
                              <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Video Id in Vimeo</th>
                                    <th>Vimeo URL</th>
                                    <th>Deleted Time</th>
                                    <th>Action</th>
                                </tr>
                              </thead>
                            <tbody>
                            <?php if($deleted_video_details) {
                                foreach ($deleted_video_details as $deleted_video)
                                {
                                    ?>
                                <tr>
                                    <td><?php echo $deleted_video['fullname']; ?></td>
                                    <td><?php echo $deleted_video['vimeo_video_id']; ?></td>
                                    <td>
                                        <a href="<?php echo 'https://vimeo.com/'.$deleted_video['vimeo_video_id'];?>" target="_blank">
                                            <?php echo 'https://vimeo.com/'.$deleted_video['vimeo_video_id'];?>
                                        </a>
                                    </td>
                                    <td><?php echo date('d-M-Y h:i:s A',strtotime($deleted_video['modified_at'])); ?></td>
                                    <td>
                                        <input type="hidden" name="vimeo_file" id="vimeo_file" value="<?php echo $deleted_video['vimeo_video_id']; ?>"/>
                                        <input type="hidden" id="accessToken" class="form-control" value="c33fba66113deeb226ce50f1112ce702"></input>
                                        <a href="javascript:void(0);" class="delete_vimeo btn btn-danger" data-pid="<?php echo $deleted_video['vimeo_video_id']; ?>">
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                <?php }
                             } ?>
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </div>
<script>
$('document').ready(function(){
$( ".delete_vimeo" ).on( "click", function(e){
        e.preventDefault();
		var vimeo_video_id=$(this).attr('data-pid');
        var oauthToken='c33fba66113deeb226ce50f1112ce702';
        var vimeo_url="https://api.vimeo.com/videos/"+vimeo_video_id;
        $.ajax({
                 url: vimeo_url,
                 method: 'DELETE',
				 type: 'application/x-www-form-urlencoded',
                 crossDomain: true,
                 beforeSend: function (xhr) {
                     xhr.setRequestHeader ("Authorization", "Basic " + btoa('kmonaghan2013@hotmail.com'+':'+'emmortal1'));
                     xhr.setRequestHeader ('Authorization', 'Bearer '+ oauthToken );
                    },
                    success: function(data,txtStatus,xhr) {
                        $.post("<?php echo base_url()?>Admin/delete_videoUpload",{'vimeo_video_id':vimeo_video_id},function(res){
								var obj = JSON.parse(res);
								location.reload();
						    });                        
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                     console.log(XMLHttpRequest);
					 console.log(textStatus);
                     console.log(errorThrown);
             },
        }); 
	});
});
</script>