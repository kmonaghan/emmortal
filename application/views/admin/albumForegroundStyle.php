<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <section class="content-header">
      <h1>
          Album Foreground Style
        <small>Album Foreground Style</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><i class="fa fa-folder-open"></i> Album Style</li>
        <li class="active">Album Foreground Style</li>
      </ol>
    </section>
      <section class="content">
          <div class="row">
              <div class="col-xs-12">
                  <div class="box">
                      <div class="box-header">
                          <h3 class="box-title">Upload Album Foreground Style</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <h5>Choose Album Foreground Style</h5>
                         <div class="file-upload">
                             <form name="albumStyle" action="<?php echo base_url()?>Admin/addAlbumForegroundStyle" method="post" enctype="multipart/form-data">
                                  <div class="file-select">
                                     <div class="file-select-button" id="fileName">Choose File</div>
                                      <input type="file" name="albumforegroundStyle" id="chooseFile">
                                  </div>
                                 <div class="file-select-name" id="noFile" style="display: none;"></div>
                                 <h5>Select Album Background Style</h5>
								  <div class="foreground-thurumb">
								    <span id="page_style">
                                        <?php if($getBackgroundStyle)
                                            {
                                                foreach($getBackgroundStyle as $backgroundStyle)
                                                { ?>
                                                    <div class="page-style-item" id="albumBackgroundStyleId_<?php echo $backgroundStyle['id']; ?>" onclick="selectAlbumBackgroundStyle(id)">
									                    <img class="album_back_image" src="<?php echo $this->config->item("cloudfront_base_url").$backgroundStyle['upload_path'].'/'.$backgroundStyle['image_name']; ?>">
									                </div>
                                                <?php
                                                }
                                            } ?>
									</span>
								  </div>
                                 <input type="hidden" id="albumBackgroundStyleId" name="albumBackgroundStyleId">
                                 <p class="text-red album_background_error" style="display: none">Please Select Album Background Style</p>
                                 <button type="submit" name="submit_button" id="subbtn">Submit</button>
                          </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Album Foreground List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="30%">Foreground Style</th>
                                    <th width="30%">Main Background Style</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($getforegroundStyle) {
                                    foreach ($getforegroundStyle as $foregroundStyle) {
                                        ?>
                                        <tr>
                                            <td>
                                                <img class="img-responsive" src="<?php echo $this->config->item("cloudfront_base_url") .$foregroundStyle['upload_path'] . '/' . $foregroundStyle['image_name']; ?>" alt="Not Found"/>
                                            </td>
                                            <td>
                                                <img class="img-responsive" src="<?php echo $this->config->item("cloudfront_base_url") .$foregroundStyle['album_upload_path'] . '/' . $foregroundStyle['album_image_name']; ?>" alt="Not Found"/>
                                            </td>
                                            
                                            <td>
                                                <a class="editAlbumForegroundStyle btn btn-primary" href="javascript:void(0);" data-album-style-id="<?php echo $foregroundStyle['album_back_id']; ?>" data-pid="<?php echo $foregroundStyle['id']; ?>">
                                                    <i class="fa fa-edit"></i> Edit
                                                </a>
                                                <a class="deleteAlbumForegroundStyle btn btn-danger" href="javascript:void(0);" data-pid="<?php echo $foregroundStyle['id']; ?>">
                                                    <i class="fa fa-trash"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
  

<!---------------------for question view popup----------------------->
<div class="modal fade" id="basicModaledit" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content modal-outer foreground_modal">
      <div class="modal-header">
        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Edit Album Foreground Style</h4>
      </div>
      <div class="modal-body">
	       <section class="content-header">
		    <div class="file-upload">
                  <form id="show_page_background_image" name="editForegroundStyle" action="<?php echo base_url()?>Admin/editAlbumForegroundStyle" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="editForegroundStyleId" id="editForegroundStyleId" value=""/>
                      <input type="hidden" name="editAlbumBackgroundStyleId" id="editAlbumBackgroundStyleId" value=""/>
                      <h5>Choose Foreground Style</h5>
                      <div class="file-select">
                          <div class="file-select-button" id="fileName">Choose File</div>
                          <input type="file" name="update_foreground_style" id="update_chooseFile">
                      </div>
                      <div class="file-select-name" id="update_noFile" style="display:none;"></div>
                      <h5>Select Background Style</h5>
                      <div class="foreground-thurumb">
                        <span id="page_style">
                            <?php if($getBackgroundStyle)
                            {
                                foreach($getBackgroundStyle as $backgroundStyle)
                                { ?>
                                    <div class="page-style-item" id="albumBackgroundStyle_<?php echo $backgroundStyle['id']; ?>" onclick="editAlbumBackgroundStyle(id)">
                                            <img class="album_back_image" src="<?php echo $this->config->item("cloudfront_base_url").$backgroundStyle['upload_path'].'/'.$backgroundStyle['image_name']; ?>">
                                        </div>
                                    <?php
                                }
                            } ?>
                        </span>
                      </div>
                        <input type="submit" name="submit_button"value="Submit" class="update_subbtn" id="subbtn">
                  </form>
            </div>
      </div>
    </div>
</div>  
</div>
    <!---------------------for question view popup End----------------------->
<!---confirmation popup start-->
	<div class="cd-popup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="cd-popup-container">
		<p>Are you sure you want to delete this album foreground Style?</p>
		<input type="hidden" id="foregroundStyleId" value=""/>
		<ul class="cd-buttons">
			<li><a href="javascript:void(0);" class="confirm_yes">Yes</a></li>
			<li><a href="javascript:void(0);" class="confirm_no">No</a></li>
		</ul>
		<a href="#" class="cd-popup-close img-replace">&nbsp;</a>
	</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->
	<!--confirmation popup end--->	
	
<script>
    function readURL(input)
    {

        if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
        {
            $('#noFile').hide();
        }
        else
        {
            $('#chooseFile').val('');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    }
    //show image in choose avatar onchange
    $(document).on('change', '#chooseFile',function() {
        readURL(this);
    });
    function readURL1(input)
    {

        if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
        {
            $('#update_noFile').hide();
        }
        else
        {
            $('#update_chooseFile').val('');
            $.alert({
                title: 'Alert!',
                content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
            });
        }
    }
    //show image in choose avatar onchange
    $(document).on('change', '#update_chooseFile',function() {
        readURL1(this);
    });
    //update file chooseFile
    $('.update_subbtn').click(function(){
        if($('#update_chooseFile').get(0).files.length === 0)
        {
            $('#update_chooseFile').css('border', 'red');
            $('#update_noFile').show();
            $('#update_noFile').text('Please Choose Album Foreground Style').css('color','red');
            $("#update_chooseFile").focus();
            return false;
        }
        else if($('#update_chooseFile').get(0).files.length>0)
        {
            return true;
        }

    });
	
	
	
    $('#subbtn').click(function(){
           if ($('#chooseFile').get(0).files.length === 0) {
             $('#chooseFile').css('border', 'red');
             $('#noFile').show();
             $('#noFile').text('Please Choose Album Foreground Style').css('color','red');
             $("#chooseFile").focus();
            return false;  
          }
          else if($('#chooseFile').get(0).files.length>0)
          {
            return true;  
          }
    });

    $('#subbtn').click(function(){
        if($('#albumBackgroundStyleId').val())
        {
            $('.album_background_error').css('display','none');
            return true;
        }
        else
        {
            $('.album_background_error').css('display','block');
            return false;
        }
    });
     
	$('.editAlbumForegroundStyle').click(function(e){
		e.preventDefault();
		var foregroundStyleId=$(this).attr('data-pid');
        var albumBackgroundStyleId=$(this).attr('data-album-style-id');
		$('#editForegroundStyleId').val(foregroundStyleId);
        $('#editAlbumBackgroundStyleId').val(albumBackgroundStyleId);
        $('.hli').toggleClass('hli');
        $('#albumBackgroundStyle_'+albumBackgroundStyleId).toggleClass('hli');
		$('#basicModaledit').modal('show');
	});
	
	
	
	//confirmation popup 
	
	//open popup
	$('.deleteAlbumForegroundStyle').on('click', function(event){
		event.preventDefault();
		var foregroundStyleId=$(this).attr('data-pid');
		//alert(page_background_id);
        $('#foregroundStyleId').val(foregroundStyleId);
		$('.cd-popup').addClass('is-visible');
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	
	$('.confirm_no').on('click',function(event){
		event.preventDefault();
		//alert('aaaaa');
		$('.cd-popup').removeClass('is-visible');		
		//$('.delpage_style').modal('hide');
	});
	
	$('.confirm_yes').on('click',function(event){
		event.preventDefault();
		var foregroundStyleId=$('#foregroundStyleId').val();
		var base_url='<?php echo base_url();?>';
        var pagebackground_delete_url=base_url+'Admin/deleteAlbumForegroundStyle';
	    var data='foregroundStyleId='+foregroundStyleId;
		$.ajax({
                url: pagebackground_delete_url,
                type: 'post',
                data:data,
                cache : false,
				processData: false,
				success: function (data){
					$('.cd-popup').removeClass('is-visible');
					location.reload();
				}
		});
	});
    function selectAlbumBackgroundStyle(id)
    {
        $('.album_background_error').css('display','none');
        var albumBackgroundStyleId=id.split('_')[1];
        $('#albumBackgroundStyleId').val(albumBackgroundStyleId);
        $('.hli').toggleClass('hli');
        $('#'+id).toggleClass('hli');
    }
    function editAlbumBackgroundStyle(id)
    {
        var albumBackgroundStyleId=id.split('_')[1];
        $('#editAlbumBackgroundStyleId').val(albumBackgroundStyleId);
        $('.hli').toggleClass('hli');
        $('#'+id).toggleClass('hli');
    }
</script>