<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
          <h1>
            Support Question
            <small>User Given Support Question</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="<?php echo base_url() ?>Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Support Question</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Support Question List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Users Name</th>
                                    <th>Users Email Id</th>
                                    <th>Users Question</th>
                                    <th>Question Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <!--Table head-->
                            <tbody>
                                <?php if($user_given_question)
                                {
                                    foreach ($user_given_question as $userQuestion)
                                    {
                                        ?>
                                        <tr>
                                              <td><?php echo $userQuestion['fullname']; ?></td>
                                              <td><?php echo $userQuestion['emailid']; ?></td>
                                              <td><?php echo (substr( $userQuestion['question'], 0, 25));?></td>
                                              <td><?php echo  $userQuestion['sendingdate']; ?></td>
                                              <td>
                                                  <a class="viewquestion_details btn btn-info" href="javascript:void(0);" data-pid="<?php echo $userQuestion['id']; ?>">
                                                      <i class="fa fa-info-circle"></i> View
                                                  </a>
                                                  <span> </span>
                                                  <a class="delquestion_details btn btn-danger" href="javascript:void(0);" data-pid="<?php echo $userQuestion['id'] ;?>">
                                                      <i class="fa fa-trash"></i> Delete
                                                  </a>

                                              </td>
                                        </tr>
                                 <?php }
                                 }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>





 <!---------------------for question view popup----------------------->
<div class="modal fade" id="basicModalquestion" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content modal-outer">
      <div class="modal-header">
        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!--<h4 class="modal-title">Modal Header</h4>-->
      </div>
      <div class="modal-body">
	       <p class="text-justify" id="question_details"></p>
      </div>
      <!--<div class="modal-footer">-->
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      <!--</div>-->
    </div>
</div>  
</div>
    <!---------------------for question view popup End----------------------->
	
	
	<!---confirmation popup start-->
	<div class="cd-popup" role="alert" aria-hidden="true">
	<div class="cd-popup-container">
		<p>Are you sure you want to delete this element?</p>
		<input type="hidden" id="quiestion_id" value=""/>
		<ul class="cd-buttons">
			<li><a href="javascript:void(0);" class="confirm_yes">Yes</a></li>
			<li><a href="javascript:void(0);" class="confirm_no">No</a></li>
		</ul>
		<a href="javascript:void(0);" class="cd-popup-close img-replace">&nbsp;</a>
	</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->
	<!--confirmation popup end--->
<script>
	$('.viewquestion_details').click(function(e){
		e.preventDefault();
		var question_id=$(this).attr('data-pid'); 
		var base_url='<?php echo base_url();?>';
		var question_details_url=base_url+'Admin/UserquestionDetails';
		var data='qid='+question_id;
		$.ajax({
			url: question_details_url,
			type: 'post',
			data: data,
			success: function (data)
			{
			  //console.log(data);
			  var obj2= JSON.parse(data);
			  $('#question_details').html(obj2.question);
			 }
			});
	   
	   
		 $('#basicModalquestion').modal('show');
	});
	
	
	
	//confirmation popup 
	
	//open popup
	$('.delquestion_details').on('click', function(event){
		event.preventDefault();
		var ques_id=$(this).attr('data-pid');
        $('#quiestion_id').val(ques_id);
		$('.cd-popup').addClass('is-visible');
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	
	$('.confirm_no').on('click',function(event){
		event.preventDefault();
        $('.cd-popup').removeClass('is-visible');
	});
	
	$('.confirm_yes').on('click',function(event){
		event.preventDefault();
		var q_id=$('#quiestion_id').val();
		//alert(q_id);
		var base_url='<?php echo base_url();?>';
        var question_delete_url=base_url+'Admin/UserquestionDelete';
	    var data='qid='+q_id;
		$.ajax({
                url: question_delete_url,
                type: 'post',
                data:data,
                cache : false,
				processData: false,
				success: function (data){
                    $('.cd-popup').removeClass('is-visible');
                    location.reload();
				}
		});
	});

</script>