<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'Admin/dashboard' ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                <form id="formUploadProfile" method="post"enctype="multipart/form-data">
                <?php if(!empty($userdata['profileimage'])){ ?>
                    <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['profileimage'] ?>" />

                <?php } ?>
                <?php if(empty($userdata['profileimage']))
                { ?>
                    <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->config->item("cloudfront_base_url") ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />
                <?php } ?>
                    <h3 class="profile-username text-center"><?php echo $userdata['fullname']; ?></h3>
                    <p class="text-muted text-center">Admin</p>
                <div class="form-group has-feedback">
                    <input type="file" name="profile_image" id="filename" class="form-control"/>
                </div>
                <div class="col-sm-offset-9 col-sm-3">
               <button type="submit" class="btn btn-primary">Upload</button>
                 </div>
                    <p class="error" id="image_error" style="display: none">Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!</p>
                 <br>
              <br>
			 </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
		  <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="glyphicon glyphicon-user margin-r-5"></i>Name:</strong>
                <p class="text-muted"><?php echo $userdata['fullname']; ?></p>
                <hr>
              <strong><i class="fa fa-calendar margin-r-5"></i> DOB:</strong>
              <p class="text-muted"><?php echo date('d-m-Y', strtotime($userdata['dob'])); ?></p>
              <hr>
              <strong><i class="glyphicon glyphicon-envelope margin-r-5"></i>E-mail:</strong>
              <p class="text-muted"><?php echo $userdata['emailid']; ?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
		 <div class="col-md-8">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
			   <li class="active"><a href="#changePassword" data-toggle="tab">Change Password</a></li>
			    <li><a href="#settings" data-toggle="tab">Account Settings</a></li>
            </ul>
            <div class="tab-content">
           <div class="tab-pane" id="settings">
               <form id="account_settings" method="post" class="form-horizontal">
                  <div class="form-group">
                    <label for="firstname" class="col-sm-3 control-label">First Name</label>

                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name" value="<?php echo $userdata['firstname']; ?>" />
                    </div>
                  </div>
                   <div class="form-group">
                       <label for="lastname" class="col-sm-3 control-label">Last Name</label>

                       <div class="col-sm-9">
                           <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last Name" value="<?php echo $userdata['lastname']; ?>" />
                       </div>
                   </div>
                   <div class="form-group">
                       <label for="email" class="col-sm-3 control-label">Email Id</label>

                       <div class="col-sm-9">
                           <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?php echo $userdata['emailid']; ?>" disabled="disabled" style="cursor: not-allowed"/>
                       </div>
                   </div>

                  <div class="form-group">
                    <label for="dob" class="col-sm-3 control-label">Date Of Birth</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control datepicker" name="dob" id="dob" value="<?php echo $userdata['dob']; ?>" placeholder="Date of Birth">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
			   <div class="active tab-pane" id="changePassword">
                   <form id="change_password" method="post"  class="form-horizontal">
                  <div class="form-group">
                    <label for="current_password" class="col-sm-3 control-label">Current Password</label>

                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Current password">
                        <div class="alert alert-danger alert-dismissable" id="current_password_alert" style="display:none">
                            <a href="javascript:void(0)" id="current_password_alert_close" class="close">×</a>
                            <p id="invalid_current_password"></p>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new_password" class="col-sm-3 control-label">New Password</label>

                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password" class="col-sm-3 control-label">Retype Password</label>

                    <div class="col-sm-9">
                      <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                    </div>
                  </div>
                
                  
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-primary">Reset Password</button>
                    </div>
                  </div>
				          
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
