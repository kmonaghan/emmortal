<!-- line modal 2-->
<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-box">
        <div class="modal-content modal-outer">
            <div class="modal-header modal-headernew">
                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Forgot Password</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form id="forgotPassword" method="post">
                    <div class="form-group  full-width">
                        <input type="text" class="form-control" id="forgot_email" placeholder="Email" name="forgot_email" autocomplete="off">
                    </div>

                    <div class="alert alert-danger alert-dismissable" id="forgot_email_alert" style="display:none">
                        <a href="javascript:void(0)" id="forgot_email_alert_close" class="close">×</a>
                        <p id="invalid_forgot_email_alert"></p>
                    </div>
                    <div class="modal-footersec">
                        <div class="button-div">
                            <button type="button" class="btn modal-button btn-primary pull-left" id="btn_back_forgotPassword">Back</button>
                            <button type="submit" class="btn modal-button pull-right btn-success" id="btn_forgotPassword">Recovery <i class="fa fa-spinner icon_recovery_spinner" style="display: none"></i></button>
                        </div>
                        <!--<p>Don't have an account? Please, <a href="#" class="link-color">Sign Up</a></p>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
