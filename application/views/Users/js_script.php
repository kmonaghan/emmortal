     <!-- jQuery 3 -->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery/dist/jquery.min.js"></script>
     <!--- drag-drop--->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.drag-drop.plugin.js"></script>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <!-- Jquery Validation Plugin -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
   <!-- bootstrap datepicker -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/css/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- notice bootstrap notify  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-notify/bootstrap-notify.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-notify/bootstrap-notify-init.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/easyResponsiveTabs.js"></script>
	<!--- These two added for select tag a friend--->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2.full.min.js"></script>
    <!-- progress bar js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>
    <!-- date moment js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.js"></script>
    <!--  alert confirm js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.gridster.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lightbox/js/lightbox.min.js"></script>
    
     <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
     <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <!-- Aviary Crop !-->
     <script type="text/javascript" src="https://dme0ih8comzn4.cloudfront.net/imaging/v3/editor.js"></script>
     <!-- Jquery Lazy loader !-->
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery.lazy-master/jquery.lazy.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery.lazy-master/jquery.lazy.plugins.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery.lazy-master/jquery.lazy.plugins.min.js"></script>