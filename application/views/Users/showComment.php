<div class="modal right fade" id="comment_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="closeComment()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="comment_body">
                </div>
                <div class="use-padding">
                    <form method="post" enctype="multipart/form-data" id="comment_form">
                        <input type="hidden" value="" id="comments_type_id">
                        <span class="square-icon">
                        <?php if($user_data['profileimage']) { ?>
                            <img alt="image" class="img-circle" style="height: 30px;width: 30px" src="<?php echo $this->config->item("cloudfront_base_url").$user_data['profileimage']; ?>" />
                        <?php }
                        else { ?>
                            <img alt="image" class="img-circle" style="height: 30px;width: 30px" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />
                        <?php }
                        ?>
                            <br>
                        <input type="file" style="display:none" class="image_file" name="image_file">
                        <i data-toggle="tooltip" data-placement="right" title="Attach a photo" class="fa fa-paperclip fa-lg image_browse_icon" aria-hidden="true" style="cursor:pointer"></i>
                    </span>
                        <textarea name="comment_text"  id="comment_text" placeholder="Write text here..."></textarea>
                        <button type="submit" id="btn_comment_relationships" disabled style="cursor: not-allowed">Submit</button>
                        <br>
                    </form>
                </div>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->