 <!-- line modal 2-->
    <div class="modal fade" id="signinModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-box">
            <div class="modal-content modal-outer">
                <div class="modal-header modal-headernew">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title">Sign In</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form id="signin" method="post">
                        <div class="form-group  full-width">
                            <input type="text" class="form-control" id="emailid" placeholder="Email" name="emailid" autocomplete="off">
                        </div>
                        <div class="form-group full-width">
                            <input type="password" class="form-control" id="pass" placeholder="Password" name="pass" autocomplete="off">
                        </div>
                         <div class="alert alert-danger alert-dismissable" id="user_alert" style="display:none">
  						  <a href="#" id="user_alert_close" class="close">×</a>
    						<span id="invalid_user_alert"></span>
  						</div>
                        <a href="#" id="forgotPasswordLink" class="link-color use-margin">Forgot Password?</a>
                
                         <div class="modal-footersec">
                            <div class="button-div">
                                <button type="submit" class="btn modal-button btn-success" id="btn_signin">Sign In <i class="fa fa-spinner icon_signin_spinner" style="display: none"></i></button>
                            </div>
                            <p>Don't have an account? Please, <a href="#" id="sign-up-link" class="link-color " >Sign Up</a></p>
                   		</div>
                    </form>
                </div>
            </div>
        </div>
    </div>