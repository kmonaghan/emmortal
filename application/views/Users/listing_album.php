<body>

<div id="main-wrapper">
    <?php $this->load->view('Users/innerHeader') ?>
  <div class="container">     
<div class="pagearea">
       <div class="col-md-12 text-right">
          <button class="createpg" type="button" data-toggle="modal" data-target="#emm-modal-next">Create Album</button>
    <div id="emm-modal-next" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content emm-modal-content2">
                <div class="modal-header emm-modal-header" style="border: none">
                    <div class="emm-cover-photo">
                        <img src="<?php echo base_url()?>assets/img/album.jpg" />
                    </div>
					<div class="emm-cover-text">
                        <p><a href="#">Choose Cover Photo</a></p>
                        <p><a href="#">Edit Cover Photo</a></p>
                    </div>
                </div>
                <form id="upload_file_image" name="image_upload" method="post" action="<?php echo base_url();?>ImageUpload/addtitledesc/" enctype="multipart/form-data" />
				<div class="modal-body emm-modal-body">
                    <div class="emm-page-title">
                        <label for="emm_page_title" class="page-tile-label">
                            Album Title:
                            <input type="text" id="emm_album_title" name="emm_album_title">
                        </label>
                        <label for="emm_album_text" class="page-tile-label">
                            Text:
                            <textarea class="border-make" rows="4" id="emm_album_text" name="emm_album_desc" cols="77" placeholder="Click to Add Text"></textarea>
                        </label>
                    </div>
                    <div class="emm-page-style">
                        <div class="page-style-label">
                            Page Style:
                        </div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/bg-30f1579a38f9a4f9ee2786790691f8df.jpg" /></div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/banner3.jpg" /></div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/bg2.jpg" /></div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/bg-30f1579a38f9a4f9ee2786790691f8df.jpg" /></div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/banner3.jpg" /></div>
                        <div class="page-style-item"><img src="<?php echo base_url()?>assets/img/bg2.jpg" /></div>
                        <div class="page-style-item"></div>
                        <div class="page-style-item"></div>
                    </div>
                </div>
				<button type="button" class="modal-submit-btn">
                        <a href="#;" id="submit_album">Submit</a>
                    </button>

				</form>
			</div>
        </div>

    </div>
       </div>
	   <?php
	   if($total_record!=0)
	   {
	   ?>
       <div class="col-md-12">
         <table class="dataTable">
    <thead>
        <tr>
            <th>Serial Numbers</th>
            <th>Album Title</th>
            <th>Album Description</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
	<?php
	for($i=0;$i<$total_record;$i++)
	{
	?>
        <tr>
            <td><?php echo $i+1;?></td>
			<td>
			  <a href="<?php echo base_url();?>ImageUpload/album/<?php echo $albumeid[$i];?>"><?php echo $title[$i];?></a>
			</td>
            <td><?php echo $description[$i];?></td>           
			<td><a href="<?php echo base_url();?>ImageUpload/showalbum/<?php echo $albumeid[$i];?>">Add More Images...</a></td>
        </tr>
	<?php
	}
	?>	
    </tbody>
</table>
       </div>
	<?php
	   }
	   else
	   {
		?>
		 <div style="color:white"><?php echo "Sorry!You did not create any album.";?></div>
	 <?php	  
	  }
	?>   
     </div>
     </div>
    </div>
</body>

<!----------table-------->

<script>
 var smallBreak = 800; // Your small screen breakpoint in pixels
var columns = $('.dataTable tr').length;
var rows = $('.dataTable th').length;

$(document).ready(shapeTable());
$(window).resize(function() {
    shapeTable();
});

function shapeTable() {
    if ($(window).width() < smallBreak) {
        for (i=0;i < rows; i++) {
            var maxHeight = $('.dataTable th:nth-child(' + i + ')').outerHeight();
            for (j=0; j < columns; j++) {
                if ($('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').outerHeight() > maxHeight) {
                    maxHeight = $('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').outerHeight();
                }
              	if ($('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').prop('scrollHeight') > $('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').outerHeight()) {
                    maxHeight = $('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').prop('scrollHeight');
                }
            }
            for (j=0; j < columns; j++) {
                $('.dataTable tr:nth-child(' + j + ') td:nth-child(' + i + ')').css('height',maxHeight);
                $('.dataTable th:nth-child(' + i + ')').css('height',maxHeight);
            }
        }
    } else {
        $('.dataTable td, .dataTable th').removeAttr('style');
    }
}

$('document').ready(function(){
	$('#submit_album').click(function(){
		//upload_file_image
		$( "form" ).submit();
	});
	
});
</script>
