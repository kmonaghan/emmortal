<script type="text/javascript">
    $(document).ready(function()
    {

        var foreground_image_name="<?php echo $foreground_image_name; ?>";
        var foreground_upload_path="<?php echo $foreground_upload_path; ?>";
        var cloudfront_base_url=$('#cloudfront_base_url').val();

        if(foreground_image_name && foreground_upload_path )
        {
            $('.foreground_dynamic').css("background-image", "url("+cloudfront_base_url+foreground_upload_path+"/"+escape(foreground_image_name)+")");

        }
        else
        {
            var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
            if(image_name&&upload_path)
            {
                $('.foreground_dynamic').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
            }
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var album_background_image="<?php echo $back_image_name;  ?>";
        var  album_background_upload_path="<?php echo $back_upload_path;  ?>";
        if(album_background_image && album_background_upload_path)
        {
            $('body').css('background-image', 'url(' +cloudfront_base_url+album_background_upload_path+'/'+escape(album_background_image) + ')');
        }
        else
        {
            var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
            if(default_background_image)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
            }
            else
            {
                var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
                var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
                if(image_name && upload_path)
                {
                    $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
                }
                else
                {
                    $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
                }
            }

        }
    });
</script>
<?php $this->load->view('Users/innerHeader'); ?>
<script src="<?php echo base_url(); ?>assets/js/Newly_Created_Album.js"></script>
   <div class="container">
            <div class="profilearea">
              <div class="profilearea_in foreground_dynamic" id="profilearea_in">
               <div class="col-md-12">
                  <div class="row">
                  <input type="hidden" id="Album_user_id" value="<?php echo $this->uri->segment('3');?>"/>
                  <input type="hidden" id="present_album_id" value="<?php echo $this->uri->segment('4');?>"/>
                      <input type="hidden" class="album_background_image_id" value="<?php echo $album_background_id; ?>"/>
                  <?php if((!empty($back_upload_path))&&(!empty($back_image_name))){?>
                  <input type="hidden" id="background_img_src" value="<?php echo $this->config->item("cloudfront_base_url").''.$back_upload_path.'/'.$back_image_name ?>"/>
                    <?php
                  }
                  ?>
                    <div class="col-md-9 text-left">
                      <span class="countuser"><?php echo count($records)?></span><span class="countusertitle">
                      <input type="hidden" name="total_records" id="total_records" value="<?php echo count($records)?>"/>
                      <?php if(empty($albumtitle))
                              {
                                if(!empty($is_default_album) && ($is_default_album==='y'))
                                {
                                    echo 'My chronicles';
                                }                
                              }
                              else
                              {
                                echo $albumtitle;  
                              }
                      ?>
                      </span>
                       
                    </div>

                    <div class="col-md-3 text-right">
					<?php
					if(!empty($total_like)){
						if($album_details['user_like_album']==='y')
						{
					?>
                     <a class="countuser countlike albumCountLike"  href="#" data-toggle="tooltip" title="Dislike!"><i class="fa fa-heart" aria-hidden="true" style="color:red"></i> <span class="total_album_like"><?php echo $total_like;?></span></a>
					<?php
						}
						if($album_details['user_like_album']==='n')
						{
						?>
                        <a class="countuser countlike albumCountLike"  href="#" data-toggle="tooltip" title="Like!"><i class="fa fa-heart" aria-hidden="true"></i> <span class="total_album_like"><?php echo $total_like;?></span></a>
					  <?php	
						}
					}else{
					?>
					<a class="countuser countlike albumCountLike"  href="#" data-toggle="tooltip" title="Like!"><i class="fa fa-heart" aria-hidden="true"></i> <span class="total_album_like"><?php echo $total_like;?></span></a>
					<?php }?>
                     <a class="countuser tribute_comment" href="#" data-toggle="tooltip" title="Tribute!" data-pid="<?php echo $album_id;?>"><i class="fa fa-comment" aria-hidden="true"></i> <span class="album_tribute_comment"> <?php echo $album_comment;?></span></a>
                     <?php
							if($this->session->userdata('userid'))
							{
								if($this->session->userdata('userid')===$userdata['user_id']){?>
								<a href="#" class="countuser dropdown-toggle" title="Settings" data-toggle="dropdown">
										<i class="fa fa-cog" aria-hidden="true"></i>
								</a> 
								<ul class="dropdown-menu">
									<?php if(!empty($page_ids)){?>
									<li id="pining"><a href="#;" class="pinging" data-pid="<?php echo $album_id;?>" style="color:red"> <i class="glyphicon glyphicon-pushpin"></i> Pinned</a></li>
									<?php }else{?>
									<li id="pining"><a href="#;" class="pinging" data-pid="<?php echo $album_id;?>"><i class="glyphicon glyphicon-pushpin"></i> Pin</a></li>
									<?php }?>
									<li><a href="#;"id="EditAlbum" data-pid="<?php echo $album_id;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
									<li><a href="#;" id="RemoveAlbum"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a></li>
								  </ul>
          
                     <?php } 
							}
					 ?>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-md-12">
				   
                      <div class="col-md-6"><div class="row"><div class="canvas-placeholdernw">					   
                        <?php
                         //if(isset($records['is_cover_image'])=='y')
                        if(!empty($album_cover_image_name))
                        {
                        ?>                     
                         <img alt="image" class="lazy img-responsive" src="<?php echo $this->config->item("cloudfront_base_url").''.$album_cover_image_uploadPath.''.$album_cover_image_name;?>" alt="album image">
                        <?php
                        }else
                        {
                       ?>
                       <img alt="image" class="lazy img-responsive" src="<?php echo $this->config->item("cloudfront_base_url").'assets/img/no_cover.jpg';?>" alt="album image">
                        <?php    
                        }
                        ?>
                        </div></div></div> 
						<div class="col-md-6"><span class="album_description"><?php echo $albumDescription; ?></span></div>
                   </div>
                  </div>
                  <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" value="0" id="row_count">
                                <input type="hidden" value="<?php echo count($records) ?>" id="total_row">
                               <div class="thumbalbum">          
                                  <ul class="album-list reorder_ul reorder-photos-list" id="albumContent">
                                  </ul>
                                
                               </div>
                            </div>
                        </div> 
                                            
                                            
              </div>
            </div>
        </div>
    </div>


    <!---------------------for image popup----------------------->
     <div class="modal fade" id="basicModalPicture" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    
        <div class="modal-dialog">
            <div class="modal-content modal-outer imghldr">
                        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <?php if($this->session->userdata('userid'))
							{
								if($this->session->userdata('userid')===$userdata['user_id']){
						?>
						<div class="modrel">
                 <li><a href="#" data-toggle="tooltip" title="Pin!"><i class="fa fa-thumb-tack image_pining" aria-hidden="true"></i></a></li>
                 <li><a href="#" data-toggle="tooltip" title="Edit!"><span class="glyphicon glyphicon-pencil EditImage"></span></a></li>
                 <li><a href="#" data-toggle="tooltip" title="Delete!"><i class="fa fa-trash-o deleteImage" aria-hidden="true"></i></a></li>
                            </div>
							<?php 
								}
							}?>
                <div class="modal-body">             
                    <div class="col-lg-8 no-padding">
					<a href="" class="full_image_view example-image-link" data-lightbox="example-1">		
						<img alt="image" class="left-image example-image" src="#" id="popup_image" />
						</a>
                        <input type="hidden" name="popup_image_id" class="popup_image_id" id="popup_image_id" value=""/>
                        <input type="hidden" name="popup_album_id" class="popup_album_id" id="popup_album_id" value=""/>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text" id="album_title"></p>
                            <p class="modal-text" id="album_desciption"></p>

                        </div>
                        <div class="modal-footer text-footer margintop">                        
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="#" class="tagger_profile_link">
                                        <span class="tagger_name_modal" id="photo_upload_user_name"></span>
                                    </a>
                                </h3>
                                <h5><a href="#" class="tagger_album_link"><i class="album_name_modal" id="photo_upload_album_name"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu tag_frd_name" id="tagger_name"></ul>
                                    </li>
                                    <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li>
                                        <a href="#" class="modal_comment_id user_like_dislike_image" data-toggle="tooltip" title="Like!">
                                            <span>
                                                <i class="fa fa-heart total_like_dislike_image" aria-hidden="true"></i>
                                            </span>
                                             <span class="count_like"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="modal_comment_id user_comment_image" data-toggle="tooltip" title="Tribute!">
                                              <span>
                                                <i class="fa fa-comment total_comment_image" aria-hidden="true"></i>
                                            </span>
                                             <span class="count_comment"></span>
									    </a>
                                    </li>
                                </ul>
                            </div>
                    </div>
                </div>
                <div class="prev_file_show_modal">
                    <button class="previous_content album_inner_image">
                        <span class="glyphicon glyphicon-triangle-left"></span>
                    </button>
                </div>
                <div class="next_file_show_modal">
                    <button class="next_content album_inner_image">
                        <span class="glyphicon glyphicon-triangle-right"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!---------------------for image popup End----------------------->
	<!---------------------for video popup Start--------------------->
	<div class="modal fade in" id="basicModalvideo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">
                        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<?php if($this->session->userdata('userid'))
							{
								if($this->session->userdata('userid')===$userdata['user_id']){
						?>
						<div class="modrel">
				 <li><a href="#" data-toggle="tooltip" title="Pin!"><i class="fa fa-thumb-tack video_pining" aria-hidden="true"></i></a></li>
				 <li><a href="#" data-toggle="tooltip" title="Edit!"><span class="glyphicon glyphicon-pencil EditVideo"></span></a></li>
                 <li><a href="#" data-toggle="tooltip" title="Delete!"><i class="fa fa-trash-o deletevideo" aria-hidden="true"></i></a></li>
							</div>
							<?php
							}
							}
							?>
                <div class="modal-body">			 
                    <div class="col-lg-8 no-padding">
					  <div class="left-image show_video_popup"></div>
                        <!--<a href="#" class="" data-toggle="modal" data-target="#basicModalbigimg">
						   <img alt="image" class="left-image popup_image" src="#" id="popup_image" />-->
						   <!--<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" id="popup_image" class="left-image" frameborder="0" style="display: none"></iframe>-->
						</a>
                        <input type="hidden" name="popup_video_id" class="popup_video_id" id="popup_video_id" value=""/>
                        <input type="hidden" name="popup_album_id" class="popup_album_id" id="popup_album_id" value=""/>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text" id="video_title"></p>
                            <p class="modal-text" id="video_desciption"></p>

						</div>
                        <div class="modal-footer text-footer margintop">						
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle video_upload_user_img" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="#" class="tagger_profile_link">
                                        <span class="tagger_name_modal" id="video_upload_user_name"></span>
                                    </a>
                                </h3>
                                <h5><a href="#" class="tagger_album_link"><i class="album_name_modal" id="video_upload_album_name"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag total_tag_frd">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu video_tag_frd_name" id="tagger_name"></ul>
                                    </li>
                                    <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content total_video_like">0</span></a></li>
                                    <li>
                                        <a href="#" class="modal_comment_id user_like_dislike_video" data-toggle="tooltip" title="Like!">
                                            <span>
                                                <i class="fa fa-heart total_like_dislike_video" aria-hidden="true"></i>
                                            </span>
                                            <span class="count_like"></span>
									    </a>
                                    </li>
                                    <li>
                                        <a href="#" class="modal_comment_id user_comment_video"  data-toggle="tooltip" title="Tribute!">
                                            <span>
                                                <i class="fa fa-comment total_comment_video" aria-hidden="true"></i>
                                            </span>
                                            <span class="count_comment"></span>
									    </a>
                                    </li>
                                </ul>
                            </div>
                    </div>
                </div>
                <div class="prev_file_show_modal">
                    <button class="previous_content_video show_inner_video">
                        <span class="glyphicon glyphicon-triangle-left"></span>
                    </button>
                </div>
                <div class="next_file_show_modal">
                    <button class="next_content_video show_inner_video">
                        <span class="glyphicon glyphicon-triangle-right"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>

	<!---------------------for video popup end------------------------>
<!---------------------for text popup start------------------------->
	<div class="modal fade" id="showtextcontent" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content modal-outer">
                        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                 <?php if($this->session->userdata('userid'))
							{
								if($this->session->userdata('userid')===$userdata['user_id']){
						?>
				 <div class="modrel">
                 <li><a href="#" data-toggle="tooltip" title="Pin!"><i class="fa fa-thumb-tack text_pining" aria-hidden="true"></i></a></li>
                 <li><a href="#" data-toggle="tooltip" title="Edit!"><span class="glyphicon glyphicon-pencil EditText"></span></a></li>
                 <li><a href="#" data-toggle="tooltip" title="Delete!"><i class="fa fa-trash-o deleteText" aria-hidden="true"></i></a></li>
                            </div>
                <?php
							}
							}
				?>
                <div class="modal-body">
                    <div class="col-lg-12 no-padding">
						<p class="modal-text" id="show_text_title"></p>
						<p class="left-image content_text_modal" id="txtEditornew"></p>
                        
                        <input type="hidden" name="popup_text_id" class="popup_text_id" id="popup_text_id" value=""/>
                        <input type="hidden" name="popup_album_id" class="popup_album_id" id="popup_album_id" value=""/>
                    </div>
                    <div class="col-lg-12 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <!--<p class="modal-text" id="show_text_title"></p>-->
                            <p class="modal-text" id="show_text_desciption"></p>

                        </div>
                        <div class="modal-footer text-footer txttopmrgn">                        
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle text_author_image" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="#" class="tagger_profile_link text_user_link">
                                        <span class="tagger_name_modal" id="text_upload_user_name"></span>
                                    </a>
									<input type="hidden" name="popup_text_id" id="popup_text_id"/>
									<input type="hidden" name="popup_text_album_id" id="popup_text_album_id"/>
                                </h3>
                                <h5><a href="#" class="tagger_album_link text_album_link"><i class="album_name_modal" id="text_upload_album_name"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag_text"></span><b class="caret"></b></a>
                                        <ul class="dropdown-menu tag_frd_name text_tagger_name" id="tagger_name"></ul>
                                    </li>
                                    <li><a href="#" class="tagger_album_link text_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content total_text_like"></span></a></li>
                                    
                                    <li>
                                        <a href="#" class="modal_comment_id user_like_dislike_text" data-toggle="tooltip" title="Like!">
                                            <span>
                                                <i class="fa fa-heart total_like_dislike_text" aria-hidden="true"></i>
                                            </span>
                                            <span class="count_like"></span></a></li>
                                    
                                    
                                    <li>
                                        <a href="#" class="modal_comment_id user_comment_text" data-toggle="tooltip" title="Tribute!">
                                            <span>
                                                <i class="fa fa-comment total_comment_text" aria-hidden="true"></i>
                                            </span>
                                            <span class="count_comment"></span>
									
									</a></li>
                                </ul>
                            </div>
                    </div>
                </div>
                <div class="prev_file_show_modal">
                    <button class="previous_content_text album_inner_text">
                        <span class="glyphicon glyphicon-triangle-left"></span>
                    </button>
                </div>
                <div class="next_file_show_modal">
                    <button class="next_content_text album_inner_text">
                        <span class="glyphicon glyphicon-triangle-right"></span>
                    </button>
                </div>
            </div>
        </div>
</div>

	<!---------------------for text popup end---------------------------->    
	<!--for Pinging Album Start-->		
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Pin to which page?</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
		  <div class="col-md-6">
		  <div class="pinbtnarealft">
		    <p>Pin to page: 
			<select name="PiningToPage" id="PiningToPage">
		    </select></p>
			<input type="hidden" name="PingalbumId" class="PingalbumId" value=""/>
			<input type="hidden" name="PinguserId" class="PinguserId" value=""/>
		  </div>
		  </div>
		  <div class="col-md-6 text-center">
		  <div class="pinbtnarea">
		  <div class="pinbtn"><button type="submit" class="modal-submit-btn Pin_submit">Pin</button></div>
		  <div class="pinbtn"><button type="submit" class="modal-submit-btn unpin_submit">Unpin</button></div>
		  </div>
		  </div>
		</div>
      </div>
     
    </div>

  </div>
</div>
 <!--Pinging End-->
<!--for Pining Inner records-->
<div id="myModalInnerText" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center">Pin to which page?</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
		  <div class="col-md-6">
		  <div class="pinbtnarealft">
		    <p>Pin to page: 
			<select name="PiningToPageforText" id="PiningToPageforText">
		    </select></p>
			<input type="hidden" name="PingalbumId" class="PingalbumId" value=""/>
			<input type="hidden" name="PinguserId" class="PinguserId" value=""/>
			<input type='hidden' class='ping_text_id' value=''/>
		  </div>
		  </div>
		  <div class="col-md-6 text-center">
		  <div class="pinbtnarea">
		  <div class="pinbtn"><button type="submit" class="modal-submit-btn Pin_submit_Text">Pin</button></div>
		  <div class="pinbtn"><button type="submit" class="modal-submit-btn unPin_submit_Text">Unpin</button></div>
		  </div>
		  </div>
		</div>
      </div>
     
    </div>

  </div>
</div>
<!--Pinging End-->
<?php $this->load->view('Users/EditvideoUpload'); ?>
<?php $this->load->view('Users/editalbum'); ?>
<?php $this->load->view('Users/editSingleImageUpload'); ?>
<?php $this->load->view('Users/editText'); ?>
<?php $this->load->view('Users/addvideoUpload'); ?>
<?php $this->load->view('Users/showComment_album'); ?>
<?php $this->load->view('Users/innerImagePining'); ?>
<?php $this->load->view('Users/innerVideoPining'); ?>

<div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-outer">

            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <div class="modrel">
                <?php if($this->session->userdata('userid'))
                { ?>
                    <li>
                        <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                    </li>
                    <li>
                        <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                } ?>
            </div>
            <div class="modal-body">

                <div class="col-lg-8 no-padding">
                    <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
					<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
					<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
					</a>
                </div>
                <div class="col-lg-4 no-padding padding-left">
                    <div class="modal-body no-padding">
                        <p class="modal-text content_title_modal"></p>
                        <p class="modal-text content_description_modal"></p>

                    </div>

                    <div class="modal-footer text-footer add-margintop">
                        <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                            <a href="#" class="tagger_profile_link">
                                <span class="tagger_name_modal"></span>
                            </a>
                        </h3>
                        <input type="hidden" class="tagger_user_id_modal">
                        <h5><a href="#" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                        <ul class="footer-icon">
                            <li>
                                <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                <ul class="dropdown-menu see_tagged_name">

                                </ul>
                            </li>
                            <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                            <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                            <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-outer">

            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <div class="modrel">
                <?php if($this->session->userdata('userid'))
                { ?>
                    <li>
                        <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                    </li>
                    <li>
                        <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                } ?>
            </div>
            <div class="modal-body">

                <div class="col-lg-12 no-padding">
                        <p class="modal-text content_title_modal"></p>
                        <p class="left-image content_text_modal" style="display: none"></p>
                </div>
                <div class="col-lg-12 no-padding padding-left">
                    <div class="modal-body no-padding">
                        <p class="modal-text content_description_modal"></p>

                    </div>

                    <div class="modal-footer text-footer txttopmrgn">
                        <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                            <a href="#" class="tagger_profile_link">
                                <span class="tagger_name_modal"></span>
                            </a>
                        </h3>
                        <input type="hidden" class="tagger_user_id_modal">
                        <h5><a href="#" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                        <ul class="footer-icon">
                            <li>
                                <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                <ul class="dropdown-menu see_tagged_name">

                                </ul>
                            </li>
                            <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                            <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                            <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="pinModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close close-new" data-dismiss="modal">
            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Pin to which page?</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="pinbtnarealft">
                            <p>Pin to page:
                                <select class="selectPageId" name="selectPageId">

                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="pinbtnarea">
                            <div class="pinbtn">
                                <button type="button" class="modal-submit-btn updateContentPage" onclick="updateContentPage(id)">Pin</button>
                            </div>
                            <div class="pinbtn">
                                <button type="button" class="modal-submit-btn deleteContentPage" onclick="deleteContentPage(id)">Unpin</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
