  <!-- About Us Modal-->
    <div class="modal fade" id="aboutUsModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-box">
            <div class="modal-content modal-outer">
                <div class="modal-header modal-headernew">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">About Us</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <p class="moodal-text">Your family photo album for the interactive age, Emmortal.com is your opportunity to decide how you want your lifetime of experiences, passions, and celebrations to be remembered for future generations.</p>
                </div>
                <div class="modal-footer close-area">
                    <div class="btn-group btn-group-justified button-style" role="group" aria-label="group button">
                        <div class="btn-group cancel-button" role="group">
                            <button type="button" class="btn btn-default main-button" data-dismiss="modal" role="button">Close</button>
                        </div>
                        <div class="btn-group btn-delete hidden" role="group">
                            <button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal" role="button">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>