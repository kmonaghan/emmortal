    <!-- jquery ui-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/easy-responsive-tabs.css " />

    <link href="<?php echo base_url()?>assets/css/select2.min.css" type="text/css" rel="stylesheet"/>
    <!-- Pace for progressbar style -->
    <link href="<?php echo base_url()?>assets/css/pace-theme-flash.css" type="text/css" rel="stylesheet"/>
    <!-- alert confirm css -->
    <link href="<?php echo base_url()?>assets/css/jquery-confirm.min.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/jquery.gridster.min.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/lightbox/css/lightbox.min.css" type="text/css" rel="stylesheet"/>
    <!--crop !-->
    <link href="<?php echo base_url()?>assets/croppr/croppr.css" type="text/css" rel="stylesheet"/>
