<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {

        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
        if(default_background_image)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
        }
        else
        {
            var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
            if(image_name && upload_path)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
            }
        }
    });
</script>
<!--  Tribute js -->
<script src="<?php echo base_url(); ?>assets/js/tributes.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tribute_details.js"></script>
<body>
<div id="main-wrapper">
    <?php $this->load->view('Users/innerHeader') ?>
    <input type="hidden" id="tribute_user_id" value="<?php echo $userdata['userid']; ?>">
    <div class="container">
        <div class="profilearea">
            <div class="profilearea_in foreground_dynamic">
                <div class="col-md-12 tributesec">
                    <div class="col-md-8 text-left">
                        <div class="row">
                            <span class="countusertri"><?php echo count($tributesData); ?></span>
                            <span class="countusertitle"><?php echo $userdata['fullname']; ?> Tributes</span>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="row">
                            <?php if($this->session->userdata('userid')) {
                                if ($userdata['userid'] != $this->session->userdata('userid')) { ?>
                                    <button id="btnAddTribute" type="button" name="btn_publish" class="bckbtntri btn btn-info"><i class="fa fa-plus-circle fa-xs"></i> Add
                                        Tribute
                                    </button>
                                    <?php
                                }
                            }?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <?php
                        if($tributesData) {
                            foreach ($tributesData as $td)
                            {
                                $flag_particular_tribute_like=0;
                                $particular_tribute_like=0;
                                $flag_login_user_like=0;
                                $like_title="Like!";
                                $flag_particular_tribute_comment=0;
                                $particular_tribute_comment=0;
                                $flag_pin=0;
                                if($existPinned)
                                {
                                    foreach ($existPinned as $ep)
                                    {
                                        if($ep->tribute_id==$td->tribute_id)
                                        {

                                            $flag_pin=1;
                                            break;
                                        }
                                        else {
                                            $flag_pin=0;
                                        }
                                    }
                                }
                                if($totalTributeLike)
                                {
                                    foreach ($totalTributeLike as $tfl)
                                    {
                                        if($tfl->tribute_id==$td->tribute_id)
                                        {
                                            $flag_particular_tribute_like=1;
                                            $particular_tribute_like=$tfl->total;
                                            break;
                                        }
                                        else {
                                            $flag_particular_tribute_like=0;
                                        }
                                    }
                                }
                                if($existLike)
                                {
                                    foreach ($existLike as $el)
                                    {
                                        if(($el->tribute_id==$td->tribute_id) && ($el->user_id==$this->session->userdata('userid')))
                                        {
                                            $flag_login_user_like=1;
                                            $like_title="Dislike!";
                                            break;
                                        }
                                        else {
                                            $like_title="Like!";
                                            $flag_login_user_like=0;
                                        }
                                    }
                                }
                                if($tributeCommentsCount)
                                {
                                    foreach ($tributeCommentsCount as $tcc)
                                    {
                                        if($tcc->tribute_id==$td->tribute_id)
                                        {
                                            $flag_particular_tribute_comment=1;
                                            $particular_tribute_comment=$tcc->total;
                                            break;
                                        }
                                        else {
                                            $flag_particular_tribute_comment=0;
                                        }
                                    }
                                }
                                ?>
                                <div class="col-md-6">
                                    <div class="triproin">
                                        <div class="tripro">
                                            <?php
                                            if($td->profileimage=='')
                                            { ?>
                                                <img class="img-responsive" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>
                                                <?php
                                            }
                                            else
                                            {?>
                                                <img class="img-responsive" src="<?php echo $this->config->item("cloudfront_base_url").$td->profileimage ?>"/>
                                                <?php
                                            }?>
                                        </div>
                                        <a href="<?php echo base_url().'Users/tributeDetails/'.$userdata['userid'].'?tribute_id='.$td->tribute_id; ?>"><h3>Tribute from <?php echo $td->fullname; ?></h3></a>
                                        <?php
                                        $string = strip_tags($td->tribute_description);
                                        if (strlen($string) > 140)
                                        {
                                            $string = substr($string, 0, 140).'...';;

                                        }
                                        ?>
                                        <p><?php echo $string ?></p>
                                        <div class="trilisticn">
                                            <a class="countuser user_like_dislike_title" href="#" data-toggle="tooltip" title="<?php echo $like_title; ?>" id="tribute_<?php echo $td->tribute_id; ?>" onClick="likeDislike(id,<?php echo $userdata['userid']; ?>)">
                                                <?php if($flag_login_user_like==1)
                                                {?>
                                                    <i class="fa fa-heart user_like_dislike" style="color: red;" aria-hidden="true"></i>
                                                    <?php
                                                }
                                                else
                                                {?>

                                                    <i class="fa fa-heart user_like_dislike" aria-hidden="true"></i>
                                                    <?php
                                                }
                                                if($flag_particular_tribute_like==1)
                                                {?>
                                                    <span class="count_like"><?php echo $particular_tribute_like; ?></span>
                                                    <?php
                                                }
                                                else
                                                {?>
                                                    <span class="count_like"><?php echo $particular_tribute_like; ?></span>
                                                    <?php
                                                }
                                                ?>
                                            </a>
                                            <a class="countuser" id="tribute_<?php echo $td->tribute_id; ?>" href="#" data-toggle="tooltip" title="Tribute!" onClick="comment_tribute(id)">
                                                <i class="fa fa-comment" aria-hidden="true"></i>
                                                <?php if($flag_particular_tribute_comment==1)
                                                {?>
                                                    <span class="count_comment"><?php echo $particular_tribute_comment; ?></span>
                                                    <?php
                                                }
                                                else{
                                                    ?>
                                                    <span class="count_comment">0</span>
                                                <?php } ?>

                                            </a>
                                            <?php if(($td->user_id==$this->session->userdata('userid')) || ($td->friends_id==$this->session->userdata('userid')) || ($userdata['user_id']==$this->session->userdata('userid')))
                                            { ?>
                                                <a href="#" class="countuser dropdown-toggle" data-toggle="dropdown" title="Settings!">
                                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                                </a>
                                            <?php } ?>
                                            <ul class="dropdown-menu">
                                                <?php if($userdata['user_id']==$this->session->userdata('userid'))
                                                { ?>
                                                    <li>
                                                        <a href="#" id="tributes_<?php echo $td->tribute_id; ?>" onClick="pinModalEditDelete(id);" >
                                                            <input type="hidden" class="flag_pin" value="<?php echo $flag_pin; ?>">
                                                          <?php if($flag_pin==1)
                                                          {?>
                                                              <i class="fa fa-thumb-tack pinIcon" aria-hidden="true" style="color:red;"></i>
                                                              <span class="pinUnpinText"> Pinned</span>
                                                          <?php
                                                          }
                                                          else
                                                          {?>
                                                              <i class="fa fa-thumb-tack pinIcon" aria-hidden="true"></i>
                                                              <span class="pinUnpinText"> Pin</span>
                                                          <?php
                                                          }?>

                                                        </a>
                                                    </li>
                                                <?php  } ?>
                                                <?php if($td->user_id==$this->session->userdata('userid'))
                                                { ?>
                                                    <li>
                                                        <a href="#;" id="edit_<?php echo $td->tribute_id; ?>" onClick="editTribute(id);">
                                                            <input type="hidden" value="<?php echo $td->tribute_description ?>" class="tribute_description">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </li>
                                                <?php  } ?>
                                                <?php if($td->friends_id==$this->session->userdata('userid'))
                                                { ?>
                                                    <li>
                                                        <a href="#;" id="delete_<?php echo $td->tribute_id; ?>" onClick="sendRequestDeleteTribute(id,<?php echo $td->user_id; ?>);">
                                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                            Delete
                                                        </a>
                                                    </li>
                                                <?php  } ?>
                                            </ul>
                                        </div>
                                        <div class="viewprotri">
                                            <a href="<?php echo base_url().'Users/profile/'.$td->userid.'?page_id='.$td->pageid ?>">
                                                View profile page
                                            </a>
                                            <?php echo date('d/m/Y',strtotime($td->created_date)); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        else
                        {?>
                            <h2 style="text-align: center"><?php echo $userdata['fullname']; ?> doesn't have tributes </h2>

                        <?php }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-8 no-padding">
                     <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
					<img class="example-image left-image content_image_modal" src="" style="display: none"/>
					<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
					</a>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer add-margintop">
                            <h3>
                                    <span class="square-icon">
                                    <img src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-12 no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="left-image content_text_modal" style="display: none"></p>
                    </div>
                    <div class="col-lg-12 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer txttopmrgn">
                            <h3>
                                    <span class="square-icon">
                                    <img src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('Users/editImageModal'); ?>
    <?php $this->load->view('Users/editTextModal'); ?>
    <?php $this->load->view('Users/EditvideoUpload'); ?>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-new" data-dismiss="modal">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Pin to which page?</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="pinbtnarealft">
                                <p>Pin to page:
                                    <select class="selectPageId" name="selectPageId">

                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="pinbtnarea">
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Pin</button>
                                </div>
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <?php if($this->session->userdata('userid'))
    { ?>
        <?php $this->load->view('Users/editTributeModal'); ?>
        <?php $this->load->view('Users/addTributeModal'); ?>
        <?php
    } ?>
    <?php $this->load->view('Users/showComment'); ?>

</div>
</body>
