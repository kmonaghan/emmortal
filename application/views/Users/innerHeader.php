
<input type="hidden" id="user_page_id" value="">
<section class="top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <a href="<?php echo base_url('Users/dashboard') ?>" class="logo-style">
                    <img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/Emmortal-Logo.png" width="72" height="35" />
                </a><b>Powered by <a href="http://delgence.com/" target="_blank">Delgence</a></b>
               
            </div>
            <div class="col-lg-6">                
                <nav class="navbar navbar-default">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span></button>
					</div>
					 <div class="collapse navbar-collapse" id="navbar-collapse-2">
						  <ul class="nav navbar-nav">
							  <?php if(!empty($userdata['profileimage'])){ ?>
							  <li>
								 <img alt="image" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['profileimage'] ?>" /> <a class="hdusr" href="<?php echo base_url(); ?>Users/profile/<?php echo $userdata['userid']; ?>?page_id=<?php echo $userdata['pageid']; ?>"><?php echo $userdata['fullname']; ?></a>
							  </li>
							  <?php } ?>
							  <?php if(empty($userdata['profileimage'])) { ?>
								 <li>
									<img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" /> <a class="hdusr" href="<?php echo base_url(); ?>Users/profile/<?php echo $userdata['userid']; ?>?page_id=<?php echo $userdata['pageid']; ?>"><?php echo $userdata['fullname']; ?></a>
								   
								 </li>   
							  <?php } ?>
							  
								<li class=""><a href="<?php echo base_url() ?>ImageUpload/albumlist/<?php echo $userdata['userid'];?>">Albums</a></li>
								<li class=""><a href="<?php echo base_url(); ?>Users/search/<?php echo $userdata['userid']; ?>" >Relationships</a></li>
								<li class=""><a href="<?php echo base_url(); ?>Users/tributes/<?php echo $userdata['userid']; ?>">Tributes</a></li>
						   
						  </ul>
						  
						</div>
      
    				</nav>
            </div>
            <div class="col-lg-2">
                <ul class="nav navbar-nav hamburger-menu">
                    
                    <?php if($this->session->userdata('userid'))
                        {?>
                            <li class="dropdown hover-effect">
                                <a href="<?php echo base_url(); ?>Users/search/<?php echo $this->session->userdata('userid'); ?>">
                                    <i class="fa fa-search search-icon" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li class="dropdown notifications-menu">
                                <a id="notification" href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell"><span class="notification-count"></span></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <div class="col-md-12 notifications_area">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5>Notifications (<span class="notification-count">0</span>)</h5>
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <a href="javascript:void(0)" class="mark" onclick="markAllRead()">Mark all as read</a>
                                                    </div>
                                                </div>
                                                <div id="notification_body">

                                                </div>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown hover-effect">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#add" class="dropdown-toggle drop-list-icon2" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-user menu-icon">
						                <i class="fa fa-plus" aria-hidden="true" data-toggle="modal" data-target="#basicModalEmmUpdated2"></i>
                                    </span>
                                </a>
                            </li>
                        <?php
                        } ?>
                    <li class="dropdown hover-effect">
                        <a href="javascript:void(0)" class="dropdown-toggle drop-list-icon" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-user menu-icon">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu topbar-dropdown drop-border drop-bg">
                           <?php if($this->session->userdata('userid'))
                            {?>
                                <li>
                                    <a href="<?php echo base_url(); ?>Users/profile/<?php echo $this->session->userdata('userid'); ?>?page_id=<?php echo $this->session->userdata('default_page_id'); ?>" class="btn btn-primary center-block sign-up">My Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('Users/newsfeed'); ?>" class="btn btn-primary center-block sign-up">Newsfeed</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('Users/settings'); ?>" class="btn btn-primary center-block sign-up">Settings</a>
                                </li>
                                <?php
                            } ?>
                             <?php if(!$this->session->userdata('userid'))
                            {?>
                                <li>
                                    <a data-toggle="modal" data-target="#signupModal" class="btn btn-primary center-block sign-up">Sign Up</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" data-target="#signinModal" class="btn btn-primary center-block sign-up">Sign In</a>
                                </li>
                            <?php } ?>
                            <li>
                                <a data-toggle="modal" data-target="#aboutUsModal" class="btn btn-primary center-block sign-up">About Us</a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#terms_ConditionsModal" class="btn btn-primary center-block sign-up">Terms & Conditions</a>
                            </li>
                            <?php if($this->session->userdata('userid'))
                            {?>
                                <li>
                                    <a href="<?php echo base_url('Users/logout')?>" class="btn btn-primary center-block sign-up">Log Out</a>
                                </li>
                            <?php
                            } ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
    <input type="hidden" id="cloudfront_base_url" value="<?php echo $this->config->item("cloudfront_base_url"); ?>">
    <input type="hidden" id="session_user_id" value="<?php echo $this->session->userdata('userid'); ?>">
    <input type="hidden" id="is_logged_in" value="<?php echo $this->session->userdata('logged_in'); ?>">
    <input type="hidden" id="session_email_id" value="<?php echo $this->session->userdata('emailid'); ?>">
    <input type="hidden" id="session_pass" value="<?php echo $this->session->userdata('password'); ?>">
    <input type="hidden" id="new_pass" value="">
    <input type="hidden" id="new_email" value="">
</section>
<div class="modal fade" id="logoutTimer" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <i class="glyphicon glyphicon-alert"></i>
                <span class="modal-title">Your session is about to expire!</span>
            </div>
            <div class="modal-body">
                <h4 id="logoutMessage"></h4>
                <h4>Do You want to stay signed in?</h4>
            </div>
            <div class="modal-footer pull-right popupLogoutTimer">
                <button type="button" onclick="keepMeSignnedIn();" class="btn btn-primary">Yes, keep me signned in</button>
                <button type="button" onclick="signMeOut();" class="btn btn-default">No, sign me out</button>
            </div>
        </div>
    </div>
</div>
<?php if(!$this->session->userdata('userid'))
{?>
    <!-- Signup Modal -->
    <?php $this->load->view('Users/signup') ?>

    <!-- signin Modal -->
    <?php $this->load->view('Users/signin') ?>

    <!-- Forgot Password Modal -->
    <?php $this->load->view('Users/forgotPassword') ?>

    <!-- Reset Password Modal -->
    <?php $this->load->view('Users/resetPassword') ?>
<?php } ?>
<!-- About Modal -->
<?php $this->load->view('Users/aboutUs') ?>
<!-- Term & Conditions Modal -->
<?php $this->load->view('Users/term_conditions') ?>
<?php
if($this->session->userdata('userid'))
{ ?>
    <!-- Page Modal -->
    <?php $this->load->view('Users/pageModal') ?>
    <!-- Choose Profile & Background Image Modal -->
    <?php $this->load->view('Users/chooseProfileImageModal'); ?>
    <?php $this->load->view('Users/file_upload');?>
    <?php $this->load->view('Users/addSingleImageUpload'); ?>
    <?php $this->load->view('Users/createalbum'); ?>
    <?php $this->load->view('Users/createText'); ?>
    <?php $this->load->view('Users/addvideoUpload'); ?>
    <!-- Tribute Modal for create -->
    <?php $this->load->view('Users/tributeModal'); ?>
    <script src="<?php echo base_url(); ?>assets/js/notification.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sessionLoginCheck.js"></script>

<?php }
?>

<?php
if($this->session->flashdata('invalid_page')){ ?>
    <script>
        $(document).ready(function()
        {
            $.notify
            ({

                    message: '<?php echo $this->session->flashdata('invalid_page'); ?>'
                }
                ,{
                    type: 'danger',
                    offset:
                        {
                            x:10,
                            y:50
                        }
                });
        });



    </script>
<?php }
?>
<?php
if($this->session->flashdata('success_email_change')){ ?>
    <script>
        $(document).ready(function()
        {
            $.notify
            ({

                    message: '<?php echo $this->session->flashdata('success_email_change'); ?>'
                }
                ,{
                    type: 'success',
                    offset:
                        {
                            x:10,
                            y:50
                        }
                });
        });
    </script>
<?php }
?>
<?php
if($this->session->flashdata('error_email_change')){ ?>
    <script>
        $(document).ready(function()
        {
            $.notify
            ({

                    message: '<?php echo $this->session->flashdata('error_email_change'); ?>'
                }
                ,{
                    type: 'danger',
                    offset:
                        {
                            x:10,
                            y:50
                        }
                });
        });



    </script>
<?php }
?>
