<!-- line modal 2-->
<div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-box">
        <div class="modal-content modal-outer">
            <div class="modal-header modal-headernew">
                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Reset Password</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form id="resetPassword" method="post">
                    <input type="hidden" id="email_reset">
                    <div class="form-group  full-width">
                        <input type="password" class="form-control" id="npass" placeholder="New Password" name="npass" autocomplete="off">
                    </div>
                    <div class="form-group  full-width">
                        <input type="password" class="form-control" id="cpass" placeholder="Confirm Password" name="cpass" autocomplete="off">
                    </div>
                    <div class="alert alert-danger alert-dismissable" id="forgot_email_alert" style="display:none">
                        <a href="#" id="forgot_email_alert_close" class="close">×</a>
                        <p id="invalid_forgot_email_alert"></p>
                    </div>
                    <div class="modal-footersec">
                        <div class="button-div">
                            <button type="submit" class="btn modal-button pull-right btn-success" id="btn_resetPassword">Reset Password <i class="fa fa-spinner icon_reset_spinner" style="display: none"></i> </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
