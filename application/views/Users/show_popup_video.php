    <div class="modal fade in" id="basicModalvideo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">
                        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<?php if(!empty($this->session->userdata['userid']))
							{
						?>
						<div class="modrel">
				 <li><a href=""><i class="fa fa-thumb-tack video_pining" aria-hidden="true"></i></a></li>
				 <li><a href=""><span class="glyphicon glyphicon-pencil EditVideo"></span></a></li>
                 <li><a href=""><i class="fa fa-trash-o deletevideo" aria-hidden="true"></i></a></li>                            
							</div>
							<?php
							}
							?>
                <div class="modal-body">			 
                    <div class="col-lg-8 no-padding">
					  <div class="left-image show_video_popup"></div>
                        <!--<a href="#" class="" data-toggle="modal" data-target="#basicModalbigimg">
						   <img class="left-image popup_image" src="#" id="popup_image" />-->
						   <!--<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" id="popup_image" class="left-image" frameborder="0" style="display: none"></iframe>-->
						</a>
                        <input type="hidden" name="popup_video_id" class="popup_video_id" id="popup_video_id" value=""/>
                        <input type="hidden" name="popup_album_id" class="popup_album_id" id="popup_album_id" value=""/>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text" id="video_title"></p>
                            <p class="modal-text" id="video_desciption"></p>

						</div>
                        <div class="modal-footer text-footer margintop">						
                                <h3>
                                    <span class="square-icon">
                                    <!--<img src="" class="tagger_image_modal img-circle video_upload_user_img" style="height: 30px;width: 30px"/>-->
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal" id="video_upload_user_name"></span>
                                    </a>
                                </h3>
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal" id="video_upload_album_name"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag total_tag_frd">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu video_tag_frd_name" id="tagger_name"></ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img src="<?php echo base_url(); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content total_video_like">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Like!"><span><i class="fa fa-heart user_like_dislike_video" aria-hidden="true"></i></span>
									<!--<span class="count_like"></span>-->
									</a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!"><span><i class="fa fa-comment" aria-hidden="true"></i></span>
									<span class="count_comment"></span>
									</a></li>
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
