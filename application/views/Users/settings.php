<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {

        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
        if(default_background_image)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
        }
        else
        {
            var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
            if(image_name && upload_path)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
            }
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/settings.js"></script>
<body>
<div id="main-wrapper">
    <?php $this->load->view('Users/innerHeader') ?>
    <div class="container">
        <div class="profilearea">
            <div class="profilearea_in_set">
                <h4>Settings</h4>
                <div class="col-md-12">
                    <div class="row">
                        <div id="parentVerticalTab">
                            <ul class="resp-tabs-list hor_1">
                                <li>General</li>
                                <li>Security</li>
                                <li>Purchasing</li>
                                <li>Privacy</li>
                                <li>Support</li>
                            </ul>
                            <div class="resp-tabs-container hor_1">
                                <div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <h5>Personal Information</h5>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <form id="general_settings" method="post" enctype="multipart/form-data">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-6"><div class="row"><input type="text" name="firstname" id="firstname" class="form" placeholder="First Name" value="<?php echo $userdata['firstname']; ?>" /></div></div>
                                                            <div class="col-md-6"><div class="row"><input type="text" name="lastname" id="lastname" class="form" placeholder="Last Name" value="<?php echo $userdata['lastname']; ?>" /></div></div>
                                                            <div class="col-md-12"><div class="row"><input type="text" name="email" id="email" class="form" placeholder="Email" value="<?php echo $userdata['emailid']; ?>"/></div></div>
                                                            <div class="col-md-12"><div class="row">
                                                                    <div class="input-group date datepicker">
                                                                        <input type="text" class="form-control" name="dob" id="dob" value="<?php echo $userdata['dob']; ?>">
                                                                        <span class="input-group-addon" id="dt"><i class="fa fa-birthday-cake"></i></span>
                                                                    </div>
                                                                </div></div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                    	<?php if($userdata['profileimage']){?>
															<div class="canvas-placeholder" style="height: 134px;">
																	<i class="fa fa-picture-o" id="blank_avatar" style="display:none"></i>
																	<img alt="image" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['profileimage']?>" id="img_choose_avatar" style="height: 134px; width: 100%;">
																</div>
																	<span class="btn btn-danger choose_avatar" id="remove_avatar"><i class="glyphicon glyphicon-remove-circle"></i>  Remove avatar</span>
																	<input id="choose_avatar_file" type="file" name="choose_avatar" class="avatar">
                                                                    <div class="error" id="avatar_image_error" style="display:none">Invalid Image</div>
															<?php
															}
															else
															{?>
																<div class="canvas-placeholder" style="height: 134px;">
																	<i class="fa fa-picture-o" id="blank_avatar"></i>
																	<img alt="image" src="" id="img_choose_avatar" style="height: 134px; width: 100%; display: none">
																</div>
																	<span class="btn btn-success choose_avatar" id="choose_avatar"><i class="glyphicon glyphicon-upload"></i>  Choose avatar</span>
																	<input id="choose_avatar_file" type="file" name="choose_avatar" class="avatar">
                                                                     <div class="error" id="avatar_image_error" style="display:none">Invalid Image</div>
														<?php } ?>
                                                    </div>
                                                   <div class="col-md-3">
                                                   <?php if($userdata['backgroundimage']){?>
                                                   		<div class="canvas-placeholder" style="height: 134px;">
                                                            <i class="fa fa-picture-o" id="blank_background" style="display:none"></i>
                                                            <img alt="image" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['backgroundimage']?>" id="img_choose_background" style="height: 134px; width: 100%">
                                                        </div>
                                                            <span class="btn btn-danger choose_background" id="remove_background"><i class="glyphicon glyphicon-remove-circle"></i>  Remove background</span>
                                                            <input id="choose_background_file" type="file" name="choose_background" class="avatar">
                                                             <div class="error" id="background_image_error" style="display:none">Invalid Image</div>
                                                  		 <?php
															}
														else
														{?>
                                                        <div class="canvas-placeholder" style="height: 134px;">
                                                            <i class="fa fa-picture-o" id="blank_background"></i>
                                                            <img alt="image" src="" id="img_choose_background" style="height: 134px; width: 100%; display: none">
                                                        </div>
                                                            <span class="btn btn-success choose_background" id="choose_background"><i class="glyphicon glyphicon-upload"></i>  Choose background</span>
                                                            <input id="choose_background_file" type="file" name="choose_background" class="avatar">
                                                             <div class="error" id="background_image_error" style="display:none">Invalid Image</div>
 													<?php } ?>
                                                    </div>
                                                   
                                                    <div class="col-md-12">
                                                        <button type="submit" class="modal-submit-btn btn btn-success" id="btn_save_general_setting">Save</button>
                                                     </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <div>
                                	
                                   <form id="change_password" method="post">
                                	<h5>Change password</h5>
                                   <div class="row">
                                   		<div class="col-lg-12">
                                      		<div class="form-group">
                                       			<input type="password" class="form-control" name="current_password" id="current_password" placeholder="Current Password">
                                               	 <div class="alert alert-danger alert-dismissable" id="current_password_alert" style="display:none">
  						 							 <a href="#" id="current_password_alert_close" class="close">×</a>
    													<p id="invalid_current_password"></p>
  												</div>
                                      		</div>
                                    	</div>
                                    </div>
                                     <div class="row">
                                   		<div class="col-lg-12">
                                      		<div class="form-group">
                                       			<input type="password" class="form-control" name="new_password" id="new_password"  placeholder="New Password">
                                      		</div>
                                    	</div>
                                    </div>
                                     <div class="row">
                                   		<div class="col-lg-12">
                                      		<div class="form-group">
                                       			<input type="password" class="form-control" name="confirm_password" id="confirm_password"  placeholder="Confirm Password">
                                      		</div>
                                    	</div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                    
                                    <br>
                                    <br>
                                </div>
                                <div>
                                    <form method="post">
                                	<h5>Purchasing</h5>
                                   <div class="row">
                                   		<div class="col-lg-12">
                                      		<div class="form-group">
                                                <label>Debit/Credit card Number</label>
                                                <input type="number" class="form-control" name="num" placeholder="Debit/Credit Card Number" maxlength="16">
                                      		</div>
                                    	</div>
                                    </div>
									<div class="row">
									<div class="col-md-12">
                                   		<div class="col-md-8">
										<div class="row">
										<div class="col-md-4">
										<div class="row">
                                      		<div class="form-group">
                                                <label>Valid Upto</label>
                                                <select name="" class="form-control">
                                                    <option selected disabled>MM</option>
                                                    <option>Jan</option>
                                                    <option>Feb</option>
                                                    <option>Mar</option>
                                                    <option>Apr</option>
                                                    <option>May</option>
                                                    <option>jun</option>
                                                    <option>Jul</option>
                                                    <option>Aug</option>
                                                    <option>Sep</option>
                                                    <option>Oct</option>
                                                    <option>Nov</option>
                                                    <option>Dec</option>
                                                </select>
                                               </div>	
											   </div>
											   </div>  
											   <div class="col-md-4">
											   <div class="form-group">
                                                <label>&nbsp;</label>
                                                <select name="" class="form-control">
                                                    <option>18</option>
                                                    <option>19</option>
                                                    <option>20</option>
                                                    <option>21</option>
                                                    <option>22</option>
                                                    <option>23</option>
                                                    <option>24</option>
                                                    <option>25</option>
                                                    <option>26</option>
                                                    <option>27</option>
                                                    <option>28</option>
                                                    <option>29</option>
                                                    <option>30</option>
                                                    <option>31</option>
                                                </select>
                                               </div>
											   </div>
                                      		</div>
                                    	</div>
										<div class="col-md-4">
										<div class="row">
                                      		<div class="form-group">
                                                <label>CVV Code</label>
                                                <input type="number" class="form-control" name="num" placeholder="CVV Code" maxlength="3" minlength="3">
                                               	 </div>
                                      		</div>
                                    	</div>
                                    </div>  
									</div>                                   
                                    <button type="submit" class="btn btn-primary">Pay Order</button>
                                    </form>
                                    <br>
                                    <br>
                                </div>
                                <div>
                                    <h5>Privacy settings</h5>
                                    <div class="col-md-12">
                                        <form id="privacy_settings" method="post">
                                            <div class="row">
                                                <div class="col-md-6">
                                                   <label>
                                                       Who can see my Emmortal profile?
                                                   </label>
                                                    <div>
                                                        <select class="form-control" name="seeme" id="seeme">
                                                            <option value="all" <?php if($userdata['seeme']=="all") { echo "selected"; } ?>>For all</option>
                                                            <option value="friends" <?php if($userdata['seeme']=="friends") { echo "selected"; } ?>>For friends</option>
                                                        </select>
                                                    </div>
                                                    <label>
                                                        Who can find me?
                                                    </label>
                                                    <div>
                                                        <select class="form-control" name="findme" id="findme">
                                                            <option value="all" <?php if($userdata['findme']=="all") { echo "selected"; } ?>>For all</option>
                                                            <option value="friends" <?php if($userdata['findme']=="friends"){ echo "selected"; } ?>>For friends</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-6">
                                                    Content
                                                    <div>
                                                        <input type="checkbox"><span> Place all Tributes on my page automatically</span>
                                                    </div>
                                                    <div>
                                                        <input type="checkbox"><span> Place all new content on my page automatically</span>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success">Save</button>

                                        </form>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                <div>
                                    <form id="support" method="post">
                                        <h5>Send us your question</h5>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea id="send_message" name="send_message" class="form-control" placeholder="Write a message text" rows="5" style="resize:vertical; "></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" id="btn_send" class="btn btn-primary" disabled="disabled">Send</button>
                                    </form>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-8 no-padding">
                        <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
						<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
						<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                display: none"></iframe>
						</a>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer add-margintop">
                            <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-12 no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="left-image content_text_modal" style="display: none"></p>
                    </div>
                    <div class="col-lg-12 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer txttopmrgn">
                            <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('Users/showComment'); ?>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-new" data-dismiss="modal">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Pin to which page?</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="pinbtnarealft">
                                <p>Pin to page:
                                    <select class="selectPageId" name="selectPageId">

                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="pinbtnarea">
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Pin</button>
                                </div>
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <?php $this->load->view('Users/editImageModal'); ?>
    <?php $this->load->view('Users/editTextModal'); ?>
    <?php $this->load->view('Users/EditvideoUpload'); ?>
</div>

<!-------------vertical_tab-------->
<script type="text/javascript">
    $(document).ready(function() {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#fff', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
        
    });
</script>
</body>