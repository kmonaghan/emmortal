 <div id="editPageModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content emm-modal-content modal-outer">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            
              <form id="form_page" method="post" action="<?php echo base_url('Users/updatePage'); ?>">
                <div class="modal-head" style="border: none">
                    <button type="submit" class="modal-submit-btn">Update</button>
                </div>
                <div class="modal-body emm-modal-body">
                    <div class="emm-page-title">
                        <div class="page-title-label">
                            Page Title:
                            <input class="emm_page_title" type="text" name="edit_page_title" id="edit_page_title" placeholder="Page Title">
                            <input type="hidden" id="edit_page_style_id" name="edit_page_style_id"/>
                            <input type="hidden" id="edit_page_id" name="edit_page_id"/>
                            <input type="hidden" class="editPageForegroundStyleId" name="editPageForegroundStyleId" value=""/>
                        </div>
                    </div>
                    <div class="emm-page-style">
                        <div class="page-style-label">
                            Page Background Style:
                        </div>
                       <span id="edit_page_style"></span>
                    </div>
                    <br>
                    <div class="emm-page-style">
                        <div class="page-style-label edit_page_foreground_label">

                        </div>
                        <span class="edit_page_foreground_style"></span>
                    </div>
                </div>
                </form>
            </div>
        </div>
 </div>
