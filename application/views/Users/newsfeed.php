<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {

        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
        if(default_background_image)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
        }
        else
        {
            var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
            if(image_name && upload_path)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
            }
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/newsfeed.js"></script>
<body>
	<div id="main-wrapper">
        <?php $this->load->view('Users/innerHeader') ?>
        <div class="container">
            <div class="profilearea">
                <div class="profilearea_in foreground_dynamic">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="pophd">
                                <h3>Newsfeed</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="album-list">
                                    <?php
                                        if($friendsdata) {
                                            if ($tagfriendcontentall) {

                                                foreach ($tagfriendcontentall as $key_tag => $value) {
                                                    $flag_value = 0;
                                                    $fullname = '';
                                                    $profileimage = '';
                                                    $pageid = 0;
                                                    foreach ($friendsdata as $fd) {
                                                        if ($fd->userid == $key_tag) {
                                                            $flag_value = 1;
                                                            $fullname = $fd->fullname;
                                                            $profileimage = $fd->profileimage;
                                                            $pageid = $fd->pageid;
                                                            break;
                                                        }
                                                    }
                                                    if ($flag_value == 1) {
                                                        ?>
                                                        <li class="cmnt">
                                                            <div class="col-md-12">
                                                                <div class="col-md-7">
                                                                    <div class="row">
                                                                <span>
                                                                    <?php if ($profileimage) { ?>
                                                                        <img alt="image" class="relation-profile-img"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url") . $profileimage; ?>"/>
                                                                    <?php } else { ?>
                                                                        <img alt="image" class="relation-profile-img"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>
                                                                    <?php }
                                                                    ?>
                                                                </span>
                                                                        <span>
                                                                    <?php if ($userdata['profileimage']) { ?>
                                                                        <img alt="image" class="relation-profile-img"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url") . $userdata['profileimage']; ?>"/>
                                                                    <?php } else { ?>
                                                                        <img alt="image" class="relation-profile-img"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"/>
                                                                    <?php }
                                                                    ?>
                                                                </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                <span>
                                                                    <a href="<?php echo base_url() . 'Users/profile/' . $key_tag . '?page_id=' . $pageid ?>"><strong><?php echo $fullname; ?></strong></a> and
                                                                </span>
                                                                    <span>
                                                                    <a href="<?php echo base_url() . 'Users/profile/' . $userdata['userid'] . '?page_id=' . $userdata['pageid'] ?>"><strong><?php echo $userdata['fullname']; ?></strong></a> are now friends
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                    foreach ($value as $tfc) {
                                                        if ($tfc['image_id']) {
                                                            $flag_image_cmt = 0;
                                                            $flag_image_like = 0;
                                                            $total_like_perticular_image = 0;
                                                            $total_comment_perticular_image = 0;
                                                            $image_id = 'image_' . $tfc['image_id'];
                                                            if(!empty($tfc["thumbnail_upload_path"])||!empty($tfc["thumbnail_image_name"]))
                                                            {
                                                                $image = $tfc['thumbnail_upload_path'] . $tfc['thumbnail_image_name'];
                                                            }
                                                            else
                                                            {
                                                                $image = $tfc['uploadPath'] . $tfc['image_name'];
                                                            }
                                                            $title=$tfc['image_title'];
                                                            $desc=$tfc['image_desc'];
                                                            $fullname=$tfc['fullname'];
                                                            $album_name=$tfc['title'];
                                                            if (strlen($title) > 40)
                                                            {


                                                                $title = substr($title, 0, 40).'...';

                                                            }
                                                            if (strlen($desc) > 40)
                                                            {

                                                                // truncate string
                                                                $stringCut = substr($desc, 0, 40);

                                                                // make sure it ends in a word so assassinate doesn't become ass...
                                                                $desc = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                            }
                                                            if (strlen($fullname) > 13)
                                                            {
                                                                $fullname = substr($fullname, 0, 13).'...';

                                                            }
                                                            if (strlen($album_name) > 13)
                                                            {

                                                                $album_name = substr($album_name, 0, 13).'...';
                                                            }
                                                            ?>
                                                            <li>
                                                                <a class="content_link" href="#"
                                                                   id="relationshipsModalShow_<?php echo $image_id; ?>"
                                                                   onClick="showModal('<?php echo $image_id; ?>',<?php echo $key_tag; ?>)">
                                                                    <img alt="image" class="lazy" src="<?php echo $this->config->item("cloudfront_base_url") . $image ?>"/>
                                                                    <input type="hidden"
                                                                           value="<?php echo $title; ?>"
                                                                           class="content_title">
                                                                    <input type="hidden"
                                                                           value="<?php echo $desc; ?>"
                                                                           class="content_description">
                                                                    <input type="hidden"
                                                                           value="<?php echo $fullname; ?>"
                                                                           class="tagger_name">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['userid']; ?>"
                                                                           class="tagger_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['pageid']; ?>"
                                                                           class="tagger_page_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['albumid']; ?>"
                                                                           class="album_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $album_name; ?>"
                                                                           class="album_name">
                                                                    <?php if ($tfc['profileimage']) { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url") . $tfc['profileimage']; ?>"
                                                                             style="display: none"/>
                                                                    <?php } else { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"
                                                                             style="display: none"/>
                                                                    <?php }
                                                                    ?>
                                                                    <?php if ($totalCommentsPerticularImage) {
                                                                        foreach ($totalCommentsPerticularImage as $key => $tcpi) {

                                                                            foreach ($tcpi as $cpi) {

                                                                                if ($tfc['image_id'] == $cpi->image_id) {

                                                                                    $flag_image_cmt = 1;
                                                                                    $total_comment_perticular_image = $cpi->total;
                                                                                    break;
                                                                                }

                                                                            }


                                                                        }

                                                                    }

                                                                    if ($totalLikesPerticularImage) {
                                                                        foreach ($totalLikesPerticularImage as $key => $tlpi) {
                                                                            foreach ($tlpi as $lpi) {
                                                                                if ($tfc['image_id'] == $lpi->image_id) {
                                                                                    $flag_image_like = 1;
                                                                                    $total_like_perticular_image = $lpi->total;
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <small>
                                                                        <?php if ($flag_image_cmt == 1) { ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $image_id; ?>"><?php echo $total_comment_perticular_image; ?></span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $image_id; ?>">0</span>
                                                                            <?php
                                                                        }
                                                                        if ($flag_image_like == 1) {
                                                                            ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $image_id; ?>"><?php echo $total_like_perticular_image; ?></span>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $image_id; ?>">0</span>
                                                                        <?php }
                                                                        ?>
                                                                    </small>
                                                                </a>
                                                            </li>
                                                        <?php }
                                                        if ($tfc['text_id']) {
                                                            $text_id = 'text_' . $tfc['text_id'];
                                                            $flag_text_cmt = 0;
                                                            $total_comment_perticular_text = 0;
                                                            $flag_text_like = 0;
                                                            $total_like_perticular_text = 0;

                                                            ?>
                                                            <li>
                                                                <a class="content_link content_linkvd"
                                                                   href="#"
                                                                   id="relationshipsModalShow_<?php echo $text_id; ?>"
                                                                   onClick="showModal('<?php echo $text_id; ?>',<?php echo $key_tag; ?>)">

                                                                    <?php
                                                                    $string = strip_tags($tfc['text_title']);
                                                                    if (strlen($string) > 75) {


                                                                        $string = substr($string, 0, 140).'...';

                                                                    }
                                                                    ?>
                                                                    <p class="text_description"><?php echo $string; ?></p>
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['text_title']; ?>"
                                                                           class="content_title">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['text_description']; ?>"
                                                                           class="content_description">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['fullname']; ?>"
                                                                           class="tagger_name">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['userid']; ?>"
                                                                           class="tagger_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['pageid']; ?>"
                                                                           class="tagger_page_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['albumid']; ?>"
                                                                           class="album_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['title']; ?>"
                                                                           class="album_name">
                                                                    <?php if ($tfc['profileimage']) { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url") . $tfc['profileimage']; ?>"
                                                                             style="display: none"/>
                                                                    <?php } else { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"
                                                                             style="display: none"/>
                                                                    <?php }
                                                                    ?>
                                                                    <?php if ($totalCommentsPerticularText) {
                                                                        foreach ($totalCommentsPerticularText as $key => $tcpt) {
                                                                            foreach ($tcpt as $cpt) {
                                                                                if ($tfc['text_id'] == $cpt->text_id) {
                                                                                    $flag_text_cmt = 1;
                                                                                    $total_comment_perticular_text = $cpt->total;
                                                                                    break;
                                                                                }

                                                                            }

                                                                        }
                                                                    }
                                                                    if ($totalLikesPerticularText) {
                                                                        foreach ($totalLikesPerticularText as $key => $tlpt) {
                                                                            foreach ($tlpt as $lpt) {
                                                                                if ($lpt->text_id == $tfc['text_id']) {
                                                                                    $flag_text_like = 1;
                                                                                    $total_like_perticular_text = $lpt->total;
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <small>
                                                                        <?php if ($flag_text_cmt == 1) { ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $text_id; ?>"><?php echo $total_comment_perticular_text; ?></span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $text_id; ?>">0</span>
                                                                            <?php
                                                                        }
                                                                        if ($flag_text_like == 1) {
                                                                            ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $text_id; ?>"><?php echo $total_like_perticular_text; ?></span>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $text_id; ?>">0</span>
                                                                        <?php }
                                                                        ?>
                                                                    </small>
                                                                </a>
                                                            </li>
                                                        <?php }
                                                        if ($tfc['video_id']) {
                                                            $video_id = 'video_' . $tfc['video_id'];
                                                            $video = 'https://player.vimeo.com/video/' . $tfc['vimeo_video_id'];
                                                            $flag_video_cmt = 0;
                                                            $total_comment_particular_video = 0;
                                                            $flag_video_like = 0;
                                                            $total_like_particular_video = 0;

                                                            $title=$tfc['video_title'];
                                                            $desc=$tfc['video_desc'];
                                                            $fullname=$tfc['fullname'];
                                                            $album_name=$tfc['title'];
                                                            if (strlen($title) > 40)
                                                            {


                                                                $title = substr($title, 0, 40).'...';

                                                            }
                                                            if (strlen($desc) > 40)
                                                            {

                                                                // truncate string
                                                                $stringCut = substr($desc, 0, 40);

                                                                // make sure it ends in a word so assassinate doesn't become ass...
                                                                $desc = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                                            }
                                                            if (strlen($fullname) > 13)
                                                            {
                                                                $fullname = substr($fullname, 0, 13).'...';

                                                            }
                                                            if (strlen($album_name) > 13)
                                                            {

                                                                $album_name = substr($album_name, 0, 13).'...';
                                                            }
                                                            ?>
                                                            <li>
                                                                <iframe src="<?php echo $video; ?>" width="100%"
                                                                        height="100px" frameborder="0"></iframe>
                                                                <a class="content_link content_linkvd"
                                                                   href="#"
                                                                   id="relationshipsModalShow_<?php echo $video_id; ?>"
                                                                   onClick="showModal('<?php echo $video_id; ?>',<?php echo $key_tag; ?>)">
                                                                    <iframe src="<?php echo $video; ?>" width="100%"
                                                                            height="100px" frameborder="0"
                                                                            style="display:none;"></iframe>
                                                                    <input type="hidden"
                                                                           value="<?php echo $title; ?>"
                                                                           class="content_title">
                                                                    <input type="hidden"
                                                                           value="<?php echo $desc; ?>"
                                                                           class="content_description">
                                                                    <input type="hidden"
                                                                           value="<?php echo $fullname; ?>"
                                                                           class="tagger_name">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['userid']; ?>"
                                                                           class="tagger_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['pageid']; ?>"
                                                                           class="tagger_page_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $tfc['albumid']; ?>"
                                                                           class="album_id">
                                                                    <input type="hidden"
                                                                           value="<?php echo $album_name; ?>"
                                                                           class="album_name">
                                                                    <?php if ($tfc['profileimage']) { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url") . $tfc['profileimage']; ?>"
                                                                             style="display: none"/>
                                                                    <?php } else { ?>
                                                                        <img alt="image" class="tagger_image"
                                                                             src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg"
                                                                             style="display: none"/>
                                                                    <?php }
                                                                    ?>
                                                                    <?php if ($totalCommentsParticularVideo) {
                                                                        foreach ($totalCommentsParticularVideo as $key => $tcpv) {
                                                                            foreach ($tcpv as $cpv) {
                                                                                if ($tfc['video_id'] == $cpv->video_id) {
                                                                                    $flag_video_cmt = 1;
                                                                                    $total_comment_particular_video = $cpv->total;
                                                                                    break;
                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                    if ($totalLikesParticularVideo) {
                                                                        foreach ($totalLikesParticularVideo as $key => $tlpv) {
                                                                            foreach ($tlpv as $lpv) {
                                                                                if ($tfc['video_id'] == $lpv->video_id) {
                                                                                    $flag_video_like = 1;
                                                                                    $total_like_particular_video = $lpv->total;
                                                                                    break;
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <small>
                                                                        <?php if ($flag_video_cmt == 1) { ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $video_id; ?>"><?php echo $total_comment_particular_video; ?></span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <i class="fa fa-comment"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="comment_perticular_<?php echo $video_id; ?>">0</span>
                                                                            <?php
                                                                        }
                                                                        if ($flag_video_like == 1) {
                                                                            ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $video_id; ?>"><?php echo $total_like_particular_video; ?></span>
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-heart color-red"
                                                                               aria-hidden="true"></i> <span
                                                                                    id="like_particular_<?php echo $video_id; ?>">0</span>
                                                                        <?php }
                                                                        ?>
                                                                    </small>
                                                                </a>
                                                            </li>
                                                            <?php
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        else
                                        {?>
                                            <h2 style="text-align: center">You have no Newsfeed yet</h2>
                                        <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-8 no-padding">
                            <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
							<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
							<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
							</a>
                        </div>
                        <div class="col-lg-4 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer add-margintop">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-12 no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="left-image content_text_modal" style="display: none"></p>
                        </div>
                        <div class="col-lg-12 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer txttopmrgn">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('Users/showComment'); ?>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <button type="button" class="close close-new" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center">Pin to which page?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="pinbtnarealft">
                                    <p>Pin to page:
                                        <select class="selectPageId" name="selectPageId">

                                        </select>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="pinbtnarea">
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Pin</button>
                                    </div>
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <?php $this->load->view('Users/editImageModal'); ?>
        <?php $this->load->view('Users/editTextModal'); ?>
        <?php $this->load->view('Users/EditvideoUpload'); ?>
    </div>
</body>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

</script>
<script>
    $('document').ready(function()
    {
        $('.pinModal').click(function()
        {
            $('#myModal').modal('show');
        });
    });
</script>