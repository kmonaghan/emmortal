 <!-- line modal 4-->
    <div class="modal fade" id="terms_ConditionsModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-box">
            <div class="modal-content modal-outer">
                <div class="modal-header modal-headernew">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">EMMORTAL TERMS AND CONDITIONS</h3>
                </div>
                <div class="modal-body terms-body">

                    <!-- content goes here -->
                    <div class="moodal-text terms-c">
                        <h4>OVERVIEW</h4>
                        <p>This website, technology, and the services provided through this website and associated technology are owned and operated by Emmortal, LLC (hereinafter "Emmortal"). Emmortal offers its services, including all information, tools, features, technology and services available from this website to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.</p>
                        <p>By visiting the Emmortal website and/ or purchasing any services or products and/or using any information, features, tools, and technology from Emmortal (individually and collectively the "Services"), you agree to be bound by the following terms and conditions ("Terms and Conditions" or "Terms"), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms and Conditions apply to all users of this website and the Services, including, without limitation, users who are browsers, vendors, customers, merchants, and/ or contributors of content.</p>
                        <p>Please read these Terms and Conditions carefully before accessing or using the Emmortal website or the Services. By accessing or using any part of the Emmortal website or the Services, you agree to be bound by these Terms and Conditions. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any Services. If these Terms and Conditions are considered an offer, acceptance is expressly limited to these Terms and Conditions.</p>
                        <p>Any new features, tools, and/or services and/or substantive change to existing features, tools, and/or services which are added and/or made to the Emmortal website shall also be subject to these Terms and Conditions. You can review the most current version of the Terms and Conditions at any time on the Emmortal website. Emmortal reserves the right to update, change or replace any part of these Terms and Conditions by posting updates and/or changes to the Emmortal website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the Emmortal website and the Services following the posting of any changes constitutes acceptance of those changes.</p>
                        <p>The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.</p>
                        <h4>SECTION 1 – USER'S USE OF SERVICES TERMS</h4>
                        <p>By agreeing to these Terms and Conditions, you represent that you are at least the age of majority in your state, province, or country of residence, or that you are the age of majority in your state, province, or country of residence and you have given Emmortal your consent to allow any of your minor dependents to use the Emmortal website and access the Services.</p>
                        <p>You may not use the Services for any illegal or unauthorized purpose nor may you, in the use of the Services, violate any laws in your jurisdiction (including but not limited to copyright laws).</p>
                        <p>You are not permitted to transmit any worms or viruses or any code of a destructive nature.</p>
                        <p>A breach or violation of any of the Terms may result in the immediate termination of the Services.</p>
                        <p>You are responsible for the security of your own account with Emmortal including your password and account credentials. You are required to notify Emmortal if your account is in anyway compromised including, but not limited to having lost, stolen, or otherwise provided unauthorized access to third parties. You are solely responsible for any activity that occurs under your account and are responsible for any loss or misuse of your content or breach of ownership or intellectual property rights by any third party if you fail to maintain the security of your password and credentials.</p>
                        <h4>SECTION 1 – USER'S USE OF SERVICES TERMS</h4>
                        <h4>SECTION 2 - GENERAL CONDITIONS</h4>
                        <p>Emmortal reserves the right to refuse service to anyone for any reason at any time.</p>
                        <p>You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices.</p>
                        <p>You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Services without the prior, express written permission by Emmortal.</p>
                    </div>
                </div>
                <div class="modal-footer close-area">
                    <div class="btn-group btn-group-justified button-style" role="group" aria-label="group button">
                        <div class="btn-group cancel-button" role="group">
                            <button type="button" class="btn btn-default main-button" data-dismiss="modal" role="button">Close</button>
                        </div>
                        <div class="btn-group btn-delete hidden" role="group">
                            <button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal" role="button">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>