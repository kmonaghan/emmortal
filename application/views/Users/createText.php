<div class="modal fade" id="addtextcontent" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog txtcls" role="document">
            <div class="modal-content modal-outer">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                
				
            <div class="profilearea_txt">
              <div class="profilearea_in_txt">
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3>Create new text entry</h3>
					</div>
				  </div>
                  <form name="text_add" method="post" action="<?php echo base_url()?>ImageUpload/addText/" enctype="multipart/form-data">
				  <div class="row canvarea2">
				    <div class="col-md-12">
					 <input name="text_title" id="edit_text_title" type="text" class="form-control" placeholder="Title" required maxlength="140">
					<textarea id="txtEditor" class="ecit_txtEditor" name="text_desc" placeholder="Description" required></textarea>
					 <div class="col-md-9 text-left"><div class="row"><select name="album_name" class="txtfl">
                      <?php 
					 if($total_record_album>0)
					 {
						 for($i=0;$i<$total_record_album;$i++)
						 {
							 if(!empty($present_album_id))
							 {
					?>
						<option value="<?php echo $albumeid[$i]?>" <?php if($present_album_id==$albumeid[$i]){?>selected=selected<?php }?>><?php echo $album_title[$i];?></option>
					<?php
							 }
							 else
							 {
					?>
								<option value="<?php echo $albumeid[$i]?>"><?php echo $album_title[$i];?></option>
					<?php 
							 }
						 }
					 }
					 else
					 {
						?>
						<option value="-1">My Chronicles</option>	 
						<?php
					}
					?>
                    </select></div></div>
					<div class="col-md-3 text-right"><div class="row"><!--<a href="#"  class="addalbm2_txt create_album"><i class="fa fa-plus" aria-hidden="true"></i> New Album</a>-->
					 </div></div>
                      <div class="col-md-12">
                      <div class="row">
                      <div class="outer_select">
					       <b>Type Your Friend's Name</b>
                            <select name="friend_name[]" class="select2 friend_list_add_text" style="width:500px;" multiple="multiple"></select>
                        </div>
                        </div></div>
					<div class="row text-right">
					  <button type="submit" name="btn_publish" class="bckbtn">Publish</button>
                        <button type="button" id="btn_back_text" class="bckbtn">Back</button>
					</div>
					</div>
                    </form>
				  </div>
				</div>
              </div>
            </div>
        </div></div></div>
<script>  
 $('document').ready(function(){ 
    $('.create_album').click(function(){
        $('#addalbumUpload').modal('show');
        $('#addtextcontent').modal('hide');    
    });

    $('#btn_back_text').click(function(){
        $('#addtextcontent').modal('hide');
		$('#basicModalEmmUpdated2').modal('show');
    }); 
 });
</script>