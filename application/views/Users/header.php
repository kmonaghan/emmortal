<script>
    window.history.pushState('forward', null, '');
    window.addEventListener('popstate', function (event)
    {
        window.history.pushState('forward', null, '');
    });
</script>
<section class="top-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10">
                    <a href="<?php echo base_url() ?>" class="logo-style"><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/Emmortal-Logo.png" width="72" height="35" /></a>
					<b>Powered by <a href="http://delgence.com/" target="_blank">Delgence</a></b>
                </div>
                <div class="col-lg-2">
                    <ul class="nav navbar-nav hamburger-menu">      
                        <li class="dropdown hover-effect">
                            <a href="#" class="dropdown-toggle drop-list-icon" data-toggle="dropdown"><span class="glyphicon glyphicon-user menu-icon pull-right"><i class="fa fa-bars" aria-hidden="true"></i></span></a>
                            <ul class="dropdown-menu topbar-dropdown drop-border drop-bg">
                                <li>
                                    <a data-toggle="modal" data-target="#signupModal" class="btn btn-primary center-block sign-up">Sign Up</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" data-target="#signinModal" class="btn btn-primary center-block sign-up">Sign In</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" data-target="#aboutUsModal" class="btn btn-primary center-block sign-up">About Us</a>
                                </li>
                                <li>
                                    <a data-toggle="modal" data-target="#terms_ConditionsModal" class="btn btn-primary center-block sign-up">Terms & Conditions</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<input type="hidden" id="session_user_id" value="<?php echo $this->session->userdata('userid'); ?>">
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
<input type="hidden" id="cloudfront_base_url" value="<?php echo $this->config->item("cloudfront_base_url"); ?>">
<!-- Signup Modal -->
<?php $this->load->view('Users/signup') ?>

<!-- signin Modal -->
<?php $this->load->view('Users/signin') ?>
<!-- About Modal -->
<?php $this->load->view('Users/aboutUs') ?>

<!-- Term & Conditions Modal -->
<?php $this->load->view('Users/term_conditions') ?>

<!-- Forgot Password Modal -->
<?php $this->load->view('Users/forgotPassword') ?>
<!-- Reset Password Modal -->
<?php $this->load->view('Users/resetPassword') ?>
