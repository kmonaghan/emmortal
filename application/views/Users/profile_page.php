<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var foreground_image_name="<?php echo $foreground_image_name; ?>";
        var foreground_upload_path="<?php echo $foreground_upload_path; ?>";
        if(foreground_image_name && foreground_upload_path )
        {
            $('.foreground_dynamic').css("background-image", "url("+cloudfront_base_url+foreground_upload_path+"/"+escape(foreground_image_name)+")");

        }
        else
        {
            var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
            if(image_name&&upload_path)
            {
                $('.foreground_dynamic').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('.foreground_dynamic').css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
            }
        }
    });
</script>
<?php $this->load->view('Users/innerHeader') ?>
<script src="<?php echo base_url(); ?>assets/js/profile_page.js"></script>
<body>
<div id="main-wrapper">
    <?php
    if($this->session->userdata('userid'))
    {
        if($page_content->user_id!=$this->session->userdata('userid') && $isfriends)
        {?>
            <div class="dropdown connected-icon">
                <button onClick="ConnectedFunction()" class="btn btn-primary dropdown-btn">
                    <i class="fa fa-check"></i> Connected &nbsp;<span class="caret"></span>
                </button>
                <div id="unfriendDropdown" class="dropdown-content">
                    <button onClick="unfriend(<?php echo $page_content->user_id; ?>)" class="btn btn-danger"><i class="fa fa-user-times"></i> Unfriend</button>
                </div>
            </div>
        <?php
        }
        if($page_content->user_id!=$this->session->userdata('userid') && !$isfriends )
        {
            if($isfriendsRequest)
            {?>
                <div class="dropdown connected-icon">
                    <button onClick="requestSentFunction()" class="btn btn-success dropdown-btn">
                        <div class="fa fa-clock-o"></div>
                        Request Sent &nbsp;<span class="caret"></span>
                    </button>
                    <div id="cancelRequestDropdown" class="dropdown-content">
                        <button onClick="cancelRequest(<?php echo $page_content->user_id; ?>)" class="btn btn-warning"><i class="fa fa-times"></i> Cancel Request</button>
                    </div>
                </div>

            <?php
            }
            else
            {

            ?>
                <div class="dropdown connected-icon" id="addFriends">
                    <button onClick="addFriends(<?php echo $page_content->user_id; ?>)" class="btn btn-info dropdown-btn">
                        <i class="fa fa-user-plus"></i> Connect</span>
                    </button>
                </div>
        <?php
            }
        }
        ?>
        <?php
        if($page_content->user_id==$this->session->userdata('userid'))
        {
            ?>
            <input type="hidden" value="<?php echo $page_content->page_title; ?>" id="page_title">
            <input type="hidden" value="<?php echo $this->session->userdata('default_page_id'); ?>" id="default_page_id">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <span class="settings-icon" data-toggle="tooltip" title="Settings" data-placement="left">
                    <i class="fa fa-cog" aria-hidden="true" ></i>
                </span>
            </a>
            <ul class="dropdown-menu page_settings">
                <?php
                if($page_content->default_page==1)
                {?>
                    <li><a href="javascript:void(0);" onClick="editPage('<?php echo $page_content->pageid;?>','<?php echo $page_content->page_style_id;?>','<?php echo $page_content->page_foreground_id;?>')"><i class="fa fa-image" aria-hidden="true"></i> Change Background</a></li>
                    <?php
                }
                if($page_content->default_page==0)
                {?>
                    <li><a href="javascript:void(0);" onClick="editPage('<?php echo $page_content->pageid;?>','<?php echo $page_content->page_style_id;?>','<?php echo $page_content->page_foreground_id;?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Page</a></li>
                    <li><a href="javascript:void(0);" onClick="deletePage('<?php echo $page_content->pageid;?>')"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Page</a></li>
                    <?php
                }
                ?>
            </ul>
            <?php $this->load->view('Users/editPageModal'); ?>
        <?php
        }
    } ?>
    <div class="container">
        <div class="profile_page_head">
        </div>
        <input type="hidden" value="0" id="row_count">
        <input type="hidden" value="<?php echo $pageAllContentCount ?>" id="total_row">
        <input type="hidden" value="<?php echo $page_content->pageid; ?>" id="page_id">
        <input type="hidden" value="<?php echo $page_content->user_id; ?>" id="page_user_id">
        <div class="body-area">
            <div id="imgsc" class="foreground_dynamic">
                <div class="gridster">
                    <ul id="page_content">

                    </ul>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="javascript:void(0)" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id);">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="javascript:void(0)" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="javascript:void(0)" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-8 no-padding">
                            <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
							<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
							<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
							</a>
                        </div>
                        <div class="col-lg-4 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer add-margintop">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="javascript:void(0)" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="javascript:void(0)" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="javascript:void(0)" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="javascript:void(0)" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id);">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="javascript:void(0)" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="javascript:void(0)" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="left-image content_text_modal" style="display: none"></p>
                        </div>
                        <div class="col-lg-12 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_description_modal"></p>
                            </div>
                            <div class="modal-footer text-footer txttopmrgn">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="javascript:void(0)" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="javascript:void(0)" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="javascript:void(0)" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <button type="button" class="close close-new" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center">Move/Unpin</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="pinbtnarealft">
                                    <p>Move to page:
                                        <select class="selectPageId" name="selectPageId">

                                        </select>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="pinbtnarea">
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Move</button>
                                    </div>
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <?php $this->load->view('Users/showComment'); ?>
        <?php $this->load->view('Users/editImageModal'); ?>
        <?php $this->load->view('Users/editTextModal'); ?>
        <?php $this->load->view('Users/EditvideoUpload'); ?>
        <div id="radioWrap">
            <ul id="radio">
                <?php if($pages)
                {	foreach($pages as $p)
                {
                    if($pageData['pageid']==$p->pageid)
                    {
                        ?>
                        <a id="<?php echo $p->pageid; ?>" href="<?php echo base_url(); ?>Users/profile/<?php echo $p->user_id; ?>?page_id=<?php echo $p->pageid; ?>">
                            <li class="actscrl"></li>
                        </a>
                        <?php
                    }
                    else
                    {?>
                        <a id="<?php echo $p->pageid; ?>" href="<?php echo base_url(); ?>Users/profile/<?php echo $p->user_id; ?>?page_id=<?php echo $p->pageid; ?>">
                            <li></li>
                        </a>
                        <?php
                    }
                }
                }?>

            </ul>
        </div>
    </div>
</div>
<?php if($page_content->page_style_id)
    {?>
        <script>
            $(document).ready(function()
            {
                var cloudfront_base_url=$('#cloudfront_base_url').val();
                var upload_path="<?php echo $page_content->upload_path; ?>";
                var image_name="<?php echo $page_content->image_name; ?>";
                var image = cloudfront_base_url + upload_path + "/" + escape(image_name);
                $('body').css("background-image", "url(" + image + ")");

            });
        </script>
    <?php } ?>
    <?php if($page_content->user_id==$this->session->userdata('userid'))
    {
        $pageid=$page_content->pageid;

        ?>
        <script>
            $(document).ready(function()
            {
                $('#user_page_id').val(<?php echo $pageid; ?>);

            });
        </script>
    <?php } ?>
	
</body>

