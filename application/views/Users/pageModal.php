 <div id="pageModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content emm-modal-content modal-outer">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            
              <form id="form_page" method="post" action="<?php echo base_url('ImageUpload/addPage'); ?>">
                <div class="modal-head" style="border: none">
                    <button type="submit" class="modal-submit-btn">Submit</button>
                </div>
                <div class="modal-body emm-modal-body">
                    <div class="emm-page-title">
                        <div class="page-title-label">
                            Page Title:
                            <input  class="emm_page_title"  type="text" name="emm_page_title" id="emm_page_title" placeholder="Page Title">
                            <input type="hidden" id="page_style_id" name="page_style_id"/>
                            <input type="hidden" class="pageForegroundStyleId" name="pageForegroundStyleId" value="0"/>
                        </div>
                    </div>
                    <div class="emm-page-style">
                        <div class="page-style-label">
                            Page Background Style:
                        </div>
                       <span id="page_style"></span>   
                    </div>
                    <br>
                    <div class="emm-page-style">
                        <div class="page-style-label page_foreground_label">

                        </div>
                        <span class="page_foreground_style"></span>
                    </div>
                </div>
                </form>
            </div>
        </div></div>

<script>
$(document).ready(function()
{
	$('#addPage').click(function(){
	  $('#pageModal').modal('show');
	  $('#basicModalEmmUpdated2').modal('hide');
    });
	var base_url=$('#base_url').val();
	$('#addPage').click(function(){
		
		$.ajax({
			url: base_url+"ImageUpload/select_page_background_images",
            type: 'post',
            success: function(data)
            {
			  var obj = JSON.parse(data);
			  if(obj.select_page_background_images.length>0)
			  {
				   var option='';
				   for(var i=0;i<obj.select_page_background_images.length;i++)
				   {
					 option +='<div class="page-style-item" id="pageInnerDiv_'+obj.select_page_background_images[i].id+'" onclick="select_page_style(id)">';
					 option +='<img alt="image" class="album_back_image" src="'+base_url+obj.select_page_background_images[i].upload_path+'/'+obj.select_page_background_images[i].image_name+'" /></div>';
				   }
				   $('#page_style').html(option);
			  }
            }
		});		
	});  
});
function select_page_style(id)
{
	var page_style_id=id.split('_')[1];
	$('#page_style_id').val(page_style_id);
	$('.hli').toggleClass('hli');    
 	$('#'+id).toggleClass('hli');
    $.ajax({
        url: "<?php echo base_url('ImageUpload/select_page_foreground_images') ?>",
        type: 'post',
        data:{pageBackgroundStyleId:page_style_id},
        success: function(data)
        {
            var base_url='<?php echo base_url();?>';
            var obj2 = JSON.parse(data);
            var total_foreground_images=obj2.total_foreground_images;
            var option=new Array();
            if(total_foreground_images>0)
            {
                $('.page_foreground_label').html('Page Foreground Style');
            }
            else
            {
                $('.page_foreground_label').html('');
            }
            for(var i=0;i<total_foreground_images;i++)
            {
                option[i]='<div class="page-style-item" id="page_foreground_image_inner_div_'+obj2.id[i]+'" onclick="selectPageForegroundImage('+obj2.id[i]+')";><img alt="image" alt="image" class="page_foreground_image" data-pid="'+obj2.id[i]+'" src="'+base_url+obj2.upload_path[i]+'/'+obj2.image_name[i]+'" /></div>';
            }
            $('.page_foreground_style').html(option);
        }
    });
}
function selectPageForegroundImage(id) {
    $('.pageForegroundStyleId').val(id);
    $('.toggle').toggleClass('toggle');
    $('#page_foreground_image_inner_div_'+id).toggleClass('toggle');
}
</script>
