<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {

        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
        if(default_background_image)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
        }
        else
        {
            var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
            if(image_name && upload_path)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
            }
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/search.js"></script>
<body>
    <div id="main-wrapper">
        <?php $this->load->view('Users/innerHeader') ?>
        <div class="container">
            <div class="find-connection foreground_dynamic">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="use-padding">
                            <h1>Find Connections</h1>
                                <input type="text" name="search" placeholder="Type here to search...." id="search">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <ul class="tabs">
                        <li class="active" rel="tab1">All</li>
                        <li rel="tab2">Relationships</li>
                        <li rel="tab3">Outgoing</li>
                        <li rel="tab4">Incoming</li>
					</ul>
					<div class="tab_container foreground_dynamic">
  						<h3 class="d_active tab_drawer_heading" rel="tab1">All</h3>
  							<div id="tab1" class="tab_content">
      							<p>
                                    <input type="hidden" value="0" id="row_count">
                                    <input type="hidden" value="<?php echo $allUserdata->total_user ?>" id="total_row">
                                  <ul class="search-list" id="allUserList">
                                    </ul>
      							</p>
 							 </div>
  							<!-- #tab1 -->
  							<h3 class="tab_drawer_heading" rel="tab2">Relationships</h3>
  							<div id="tab2" class="tab_content">
    							<p>
                                	<ul class="search-list" id="allRelationshipUsers">

                                    </ul>
                              	</p>
 							</div>
  							<!-- #tab2 -->
  							<h3 class="tab_drawer_heading" rel="tab3">Outgoing</h3>
  							<div id="tab3" class="tab_content">
                                <p>
                                    <ul class="search-list" id="allOutgoingUsers">

                                    </ul>
                                </p>
 							</div>
  							<!-- #tab3 -->
  							<h3 class="tab_drawer_heading" rel="tab4">Incoming</h3>
  							<div id="tab4" class="tab_content">
  								<p>
                                    <ul class="search-list" id="allIncomingUsers">

                                    </ul>
      							</p>
  							</div>
  							<!-- #tab4 --> 
						</div>
						<!-- .tab_container -->
					</div>
                </div>
            </div>
        <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-8 no-padding">
                            <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
							<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
							<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
							</a>
                        </div>
                        <div class="col-lg-4 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer add-margintop">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-12 no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="left-image content_text_modal" style="display: none"></p>
                        </div>
                        <div class="col-lg-12 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer txttopmrgn">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('Users/showComment'); ?>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <button type="button" class="close close-new" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-center">Pin to which page?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="pinbtnarealft">
                                    <p>Pin to page:
                                        <select class="selectPageId" name="selectPageId">

                                        </select>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="pinbtnarea">
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Pin</button>
                                    </div>
                                    <div class="pinbtn">
                                        <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <?php $this->load->view('Users/editImageModal'); ?>
        <?php $this->load->view('Users/editTextModal'); ?>
        <?php $this->load->view('Users/EditvideoUpload'); ?>
    </div>
</body>
<!----responsive_tab----------->
<script>
      // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {

      $(".tab_content").hide();
      var activeTab = $(this).attr("rel");
      $("#"+activeTab).fadeIn();

      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {

      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel");
      $("#"+d_activeTab).fadeIn();

	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");

	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });


	/* Extra class "tab_last"
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
	$('document').ready(function ()
    {
       if($("ul.tabs li[rel='tab1']").attr('class','active'))
       {
           $("ul.tabs li[rel='tab1']").click();
       }

    });

 </script>


