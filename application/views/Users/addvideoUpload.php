<div id="modaloutr">
<input type="hidden" id="user_id" value="<?php echo $this->session->userdata('userid');?>"/>
    <div class="modal fade" id="addvideoUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-body text-center add-paddingnw">
			<!--<form id="upload_file_image" name="image_upload" method="post" action="<?php //echo base_url()?>ImageUpload/videoUpload/" enctype="multipart/form-data"/>-->
             <div class="profilearear">
              <div class="profilearea_innewr">
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3 id="headerCaption">Upload new Video</h3>
					</div>
				  </div>
				  <div class="row canvarea">
				  <input type="hidden" name="album_id" id="album_id" value=""/>
				  <input type="hidden" name="video_user_id" id="video_user_id" value=""/>
				  <input type="hidden" name="previous_page" id="previous_page" value=""/>
				  <div id="upldprogrss">
                  <div class="vidtitle" id="videoTitle" style="display: none">Video Upload Progress</div>
				  <div id="progress-container" class="progress" style="display: none">
                  
					<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%</div>
				 </div>
				</div>
				  <div class="col-md-12">
					<div id="results"></div>
				   </div>
				    <div class="col-md-6">
					 <!--<input type="file" name="file_data" id="file_data">-->
					  <div class="col-md-12">
					         <input type="hidden" id="video_album_id" name="video_album_id" value="<?php echo $this->uri->segment(3);?>" />
							 <input type="hidden" id="accessToken" class="form-control" value="c33fba66113deeb226ce50f1112ce702"></input>
							 <!--<input type="hidden" id="accessToken" class="form-control" value="ce937a2d90e98f2572cd3459b4a01608"></input>-->
							<div class="form-group">
								<input type="text" name="name" id="videoName" class="form-control" placeholder="Video name" value=""></input>
							</div>
							<div class="form-group">
								<input type="text" name="description" id="videoDescription" class="form-control" placeholder="Video description" value=""></input>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" id="upgrade_to_1080" checked="checked" name="upgrade_to_1080" readonly="readonly" disabled> Make Public
								</label>
							</div>
							<!--<div class="checkbox">
								<label>
									<input type="checkbox" id="make_private" name="make_private"> Make Private </input>
								</label>
							</div>-->
					  </div>
					  <div class="col-md-6">
						<h6>Type Your Friend's Name</h6>
						<div class="outer_selectvd">
							<select name="friend_name[]" class="select2 friend_list_add_video" style="width:464px;" multiple="multiple"></select>
						</div>
					</div>
					<div class="col-md-12">
						<select name="album_name" id="album_name" class="txtflvd">
					<?php 
					 if($total_record_album>0)
					 {
						 for($i=0;$i<$total_record_album;$i++)
						 {
							 if(!empty($present_album_id))
							 {
					?>
						<option value="<?php echo $albumeid[$i]?>" <?php if($present_album_id==$albumeid[$i]){?>selected=selected<?php }?>><?php echo $album_title[$i];?></option>
					<?php
							 }
							 else
							 {
					?>
								<option value="<?php echo $albumeid[$i]?>"><?php echo $album_title[$i];?></option>
					<?php 
							 }
						 }
					 }
					 else
					 {
						?>
						<option value="-1">My Chronicles</option>	 
						<?php
					}
					?>
					</select>
					</div>
				</div>
				<div class="col-md-6">
						<div id="drop_zone">Drop Files Here</div>
							<br/>
                            <button class="btn btn-block btn-info" id="btnBrowse">Browse</button>
                            <input id="browse" type="file" style="display: none;">
							<!--<label class="btn btn-block btn-info">Browse&hellip; <input id="browse" type="file" style="display: none;"></label>-->
				</div>
				<div class="row text-right">
                        <button type="submit" id="btn_publish_video" name="btn_publish_video" class="bckbtn">Publish</button>
                        <button type="button" id="btn_back_video" class="bckbtn">Back</button>
					</div>

				</div>
				</div>
				</div>
				</div>
              </div>
            </div>
		<!--</form>-->
                </div>
              
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vimeo-upload.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bragit.js"></script>

<script type="text/javascript">
	/**
         * Called when files are dropped on to the drop target or selected by the browse button.
         * For each file, uploads the content to Drive & displays the results when complete.
         */
        function handleFileSelect(evt) {
            evt.stopPropagation()
            evt.preventDefault()

            var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files
            var results = document.getElementById('results')

            /* Clear the results div */
            while (results.hasChildNodes()) results.removeChild(results.firstChild)

            /* Rest the progress bar and show it */
            updateProgress(0)
            document.getElementById('videoTitle').style.display = 'block';
            document.getElementById('progress-container').style.display = 'block'

            /* Instantiate Vimeo Uploader */
            ;(new VimeoUpload({
                name: document.getElementById('videoName').value,
                description: document.getElementById('videoDescription').value,
                //private: document.getElementById('make_private').checked,
                file: files[0],
                token: document.getElementById('accessToken').value,
                upgrade_to_1080: document.getElementById('upgrade_to_1080'),
				onError: function(data) {
                    showMessage('<strong>Error</strong>: ' + JSON.parse(data).error, 'danger')
                },
                onProgress: function(data) {
                    updateProgress(data.loaded / data.total)
                },
				onComplete: function(videoId, index){
					var album_id=$('#album_name').val();
					var video_id=videoId;
					 // Here ajax will call to save the record in database	
					var selected_friend_list=$('.friend_list_add_video').val();
					var user_id=$('.user_id').val();
					var name=document.getElementById('videoName').value;
					var desc=document.getElementById('videoDescription').value;
					var url;
					if (index > -1) {
							/* The metadata contains all of the uploaded video(s) details see: https://developer.vimeo.com/api/endpoints/videos#/{video_id} */
							//url = this.metadata[index].link //
							
							/* add stringify the json object for displaying in a text area */
							var pretty = JSON.stringify(this.metadata[index], null, 2);
        					$.post("<?php echo base_url()?>ImageUpload/videoUpload",{'album_id':album_id,'video_id':video_id,'friends_id':selected_friend_list,'title':name,'desc':desc},function(res){
								var obj = JSON.parse(res);
								console.log(obj);
								url = "<?php echo base_url()?>ImageUpload/albumdetails/" + obj.user_id + '/' + obj.album_id;
								$('#album_id').val(obj.album_id);
								$('#video_user_id').val(obj.user_id);
								$('#upldprogrss').hide();	
								showMessage('<strong>Upload Successful</strong>: check uploaded video @ <a href="' + url + '">' + url + '</a>. Open the Console for the response details.');
						    });
							//console.log(pretty) /* echo server data */
						}
						
						
                }
            })).upload()

            /* local function: show a user message */
            function showMessage(html, type) {
                /* hide progress bar */
                document.getElementById('progress-container').style.display = 'none'
                /* display alert message */
                var element = document.createElement('div')
                element.setAttribute('class', 'alert alert-' + (type || 'success'))
                element.innerHTML = html
                results.appendChild(element)
            }
        }

        /**
         * Dragover handler to set the drop effect.
         */
        function handleDragOver(evt) {
            evt.stopPropagation()
            evt.preventDefault()
            evt.dataTransfer.dropEffect = 'copy'
        }

        /**
         * Updat progress bar.
         */
        function updateProgress(progress){
            progress = Math.floor(progress * 100)
			/*var submit_button=document.getElementById('btn_publish')
			if(progress>0 && progress<100)
			{
				submit_button.innerHTML='Submitting.....'
			}
			if(progress==100)
			{
				submit_button.innerHTML='Update Successfully'
			}*/
            var element = document.getElementById('progress')
            element.setAttribute('style', 'width:' + progress + '%')
            element.innerHTML = '&nbsp;' + progress + '%'
        }
        /**
         * Wire up drag & drop listeners once page loads
         */
        document.addEventListener('DOMContentLoaded', function() {
            var dropZone = document.getElementById('drop_zone')
            var browse = document.getElementById('browse')
            dropZone.addEventListener('dragover', handleDragOver, false)
            dropZone.addEventListener('drop', handleFileSelect, false)
            browse.addEventListener('change', handleFileSelect, false)
        });
	
$('#btn_back_video').click(function(event){
		event.preventDefault();
        $('#addvideoUpload').modal('hide');
		var prevpage=$('#previous_page').val();
		if(prevpage=='frmEditpage')
		{
		  $('#basicModalvideo').modal('show');	
		}else
		{
		  $('#basicModalEmmUpdated2').modal('show');	
		}
});	

//	
$('#btn_publish_video').click(function(){
    //alert('aaa');
	  //var album_id=$('#video_album_id').val();
	  var album_id=$('#album_id').val();
	  var user_id=$('#video_user_id').val();
      //alert(album_id);
      //alert(user_id); 
      if(album_id!=''& user_id!='')
      {
        window.location.href ="<?php echo base_url()?>ImageUpload/albumdetails/" + user_id + '/' + album_id;   
      }
		
});

$(document).on('click', '#btnBrowse', function()
    {
        $("#browse").click();

    });
</script>
