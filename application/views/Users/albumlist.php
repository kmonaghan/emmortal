<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {

        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
        if(default_background_image)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
        }
        else
        {
            var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
            var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
            if(image_name && upload_path)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
            }
            else
            {
                $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
            }
        }
    });
</script>
<!-- Header Link -->
<?php $this->load->view('Users/innerHeader') ?>
<div class="container">
    <div class="profilearea">
        <div class="profilearea_in foreground_dynamic">
            <div class="col-md-12">
                <div class="row">
                    <h4>Life Events &nbsp;<?php echo $userdata['fullname'];?></h4>
                </div>
                <div class="row">
                    <div class="col-md-12" id="allUserList">
                        <?php
                        if($total_record_album>0)
                        {
                        ?>
                        <?php
                        for($i=0;$i<$total_album_id;$i++)
                        {
                        ?>
                        <div class="row albumtot">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="albmprevw">
                                        <?php
                                        if(!empty($album_details[$i]['album_cover_image_name']))
                                        {
                                        ?>
                                        <img alt="image" class="lazy img-responsive" src="<?php echo $this->config->item("cloudfront_base_url").''.$album_details[$i]['album_cover_image_uploadPath'].''.$album_details[$i]['album_cover_image_name'];?>"> </div>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <img alt="image" class="img-responsive lazy" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_cover.jpg"> </div>
                                <?php
                                }
                                ?>
                                <div class="albmtil">
                                    <h6>
                                        <a href="<?php echo base_url()?>ImageUpload/albumdetails/<?php echo $userdata['userid']; ?>/<?php echo $album_details[$i]['albumid']; ?>">
                                            <?php
                                            if(empty($album_details[$i]['album_title']))
                                            {
                                                if($album_details[$i]['is_default_album']==='y')
                                                {
                                                    echo 'My chronicles';
                                                }
                                            }
                                            else
                                            {
                                                echo $album_details[$i]['album_title'];
                                            }
                                            ?>
                                        </a>
                                        <input type="hidden" name="album_user_id" id="album_user_id" value="<?php echo $this->uri->segment(3);?>"/>
                                    </h6>
                                    <?php
                                    if($album_details[$i]['total_like']>0)
                                    {
                                        if($album_details[$i]['user_like_album']==='y')
                                        {
                                            ?>
                                            <a class="countuser albumCountLike" href="#" data-toggle="tooltip" title="Dislike!" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-heart" aria-hidden="true" style="color:red"></i> <span class="total_like_album"><?php echo $album_details[$i]['total_like'];?></span></a>
                                            <?php
                                        }
                                        if($album_details[$i]['user_like_album']==='n')
                                        {
                                            ?>
                                            <a class="countuser albumCountLike" href="#" data-toggle="tooltip" title="Like!" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-heart" aria-hidden="true"></i> <span class="total_like_album"><?php echo $album_details[$i]['total_like'];?></span></a>
                                            <?php

                                        }
                                    }
                                    else
                                    {
                                        ?>
                                        <a class="countuser albumCountLike" href="#" data-toggle="tooltip" title="Like!" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-heart" aria-hidden="true"></i> <span class="total_like_album"><?php echo $album_details[$i]['total_like'];?></span></a>
                                    <?php }
                                    ?>
                                    <a class="countuser count_comment" href="#" data-toggle="tooltip" title="Tribute!" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-comment" aria-hidden="true"></i> <span id="album_comment_<?php echo $album_details[$i]['albumid'];?>"><?php echo $album_details[$i]['album_comment'];?></span></a>
                                    <?php if($this->session->userdata('userid')){
                                        if($this->session->userdata('userid')===$userdata['userid']){
                                            ?>
                                            <a href="#" class="countuser dropdown-toggle" title="Settings" data-toggle="dropdown">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <?php if(!empty($album_details[$i]['page_ids'])){?>
                                                    <li><a href="#" class="pinging" data-pid="<?php echo $album_details[$i]['albumid'];?>" id="album_<?php echo $album_details[$i]['albumid'];?>" style="color:red"><!--<i class="fa fa-thumb-tack red_color" aria-hidden="true" style="color:red"></i>--><i class="glyphicon glyphicon-pushpin"></i> Pinned</a></li>
                                                <?php }else{?>
                                                    <li><a href="#" class="pinging" data-pid="<?php echo $album_details[$i]['albumid'];?>" id="album_<?php echo $album_details[$i]['albumid'];?>"><i class="glyphicon glyphicon-pushpin"></i> Pin</a></li>
                                                <?php }?>
                                                <li><a href="#" class="EditAlbum" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></li>
                                                <li><a href="#" class="delete_album" data-pid="<?php echo $album_details[$i]['albumid'];?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a></li>
                                            </ul>


                                            <?php
                                        }
                                    }?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php
                            if(!empty($album_details[$i]['total_album_inner_record']))
                            {
                                ?>
                                <div class="row">

                                    <?php
                                    for($n=0; $n < ($album_details[$i]['total_album_inner_record']);$n++)
                                    {
                                        if($album_details[$i]['album_inner_record_type'][$n]==='image')
                                        {
                                            if(!empty($album_details[$i]['album_inner_thumbnail_upload_path'][$n]) && !empty($album_details[$i]['album_inner_thumbnail_image_name'][$n]))
                                            { ?>
                                                <div class="albmprevwnew"><img alt="image" class="img-responsive lazy" src="<?php echo $this->config->item("cloudfront_base_url").''.$album_details[$i]['album_inner_thumbnail_upload_path'][$n].''.$album_details[$i]['album_inner_thumbnail_image_name'][$n];?>" alt=""></div>
                                            <?php } else { ?>
                                                <div class="albmprevwnew"><img alt="image" class="img-responsive lazy" src="<?php echo $this->config->item("cloudfront_base_url").''.$album_details[$i]['album_inner_image_uploadPath'][$n].''.$album_details[$i]['album_inner_image_name'][$n];?>" alt=""></div>
                                                <?php
                                            }
                                        }
                                        if($album_details[$i]['album_inner_record_type'][$n]==='text')
                                        {

                                            ?>
                                            <div class="albmprevwnew"><?php echo substr($album_details[$i]["album_inner_text_title"][$n],0,25);?></div>
                                            <?php
                                        }
                                        if($album_details[$i]['album_inner_record_type'][$n]==='video')
                                        {
                                            ?>
                                            <div class="albmprevwnew"><iframe src="https://player.vimeo.com/video/<?php echo $album_details[$i]["album_inner_video_id"][$n]; ?>" width="100%" height="100px" frameborder="0"></iframe><div class="ifrmopacity"></div></div>
                                            <?php
                                        }
                                    }
                                    ?>


                                </div>
                                <?php
                            }
                            /* else
                            {
                            ?>
                            <div class="row">
                              <div class="albmprevwnew"><img alt="image" class="img-responsive lazy" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/not-found.png"> </div>
                              <div class="albmprevwnew"><img alt="image" class="img-responsive lazy" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/not-found.png"> </div>
                            </div>
                            <?php
                             }*/
                            ?>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                    <?php
                    }else
                    {?>
                        <h2 class="text-center"><?php echo $userdata['fullname'];?> doesn't have albums</h2>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--for Pinging Start-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Pin to which page?</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="pinbtnarealft">
                            <p>Pin to page:
                                <select name="PiningToPage" id="PiningToPage">
                                </select></p>
                            <input type="hidden" name="PingalbumId" class="PingalbumId" value=""/>
                            <input type="hidden" name="PinguserId" class="PinguserId" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="pinbtnarea">
                            <div class="pinbtn"><button type="submit" class="modal-submit-btn Pin_submit">Pin</button></div>
                            <div class="pinbtn"><button type="submit" class="modal-submit-btn unpin_submit">Unpin</button></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<?php $this->load->view('Users/editalbum'); ?>
<?php $this->load->view('Users/showComment_album'); ?>

<div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-outer">

            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <div class="modrel">
                <?php if($this->session->userdata('userid'))
                { ?>
                    <li>
                        <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                            <i class="glyphicon glyphicon-pencil EditText"></i>
                        </a>
                    </li>
                    <li>
                        <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                } ?>
            </div>
            <div class="modal-body">

                <div class="col-lg-8 no-padding">
                    <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
                        <img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
                        <iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
                    </a>
                </div>
                <div class="col-lg-4 no-padding padding-left">
                    <div class="modal-body no-padding">
                        <p class="modal-text content_title_modal"></p>
                        <p class="modal-text content_description_modal"></p>

                    </div>

                    <div class="modal-footer text-footer add-margintop">
                        <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                            <a href="" class="tagger_profile_link">
                                <span class="tagger_name_modal"></span>
                            </a>
                        </h3>
                        <input type="hidden" class="tagger_user_id_modal">
                        <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                        <ul class="footer-icon">
                            <li>
                                <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                <ul class="dropdown-menu see_tagged_name">

                                </ul>
                            </li>
                            <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                            <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                            <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-outer">

            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <div class="modrel">
                <?php if($this->session->userdata('userid'))
                { ?>
                    <li>
                        <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                            <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                            <i class="glyphicon glyphicon-pencil EditText"></i>
                        </a>
                    </li>
                    <li>
                        <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                } ?>
            </div>
            <div class="modal-body">

                <div class="col-lg-12 no-padding">
                    <p class="modal-text content_title_modal"></p>
                    <p class="left-image content_text_modal" style="display: none"></p>
                </div>
                <div class="col-lg-12 no-padding padding-left">
                    <div class="modal-body no-padding">
                        <p class="modal-text content_description_modal"></p>

                    </div>

                    <div class="modal-footer text-footer txttopmrgn">
                        <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                            <a href="" class="tagger_profile_link">
                                <span class="tagger_name_modal"></span>
                            </a>
                        </h3>
                        <input type="hidden" class="tagger_user_id_modal">
                        <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                        <ul class="footer-icon">
                            <li>
                                <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                <ul class="dropdown-menu see_tagged_name">

                                </ul>
                            </li>
                            <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                            <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                            <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="pinModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close close-new" data-dismiss="modal">
            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Pin to which page?</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="pinbtnarealft">
                            <p>Pin to page:
                                <select class="selectPageId" name="selectPageId">

                                </select>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="pinbtnarea">
                            <div class="pinbtn">
                                <button type="button" class="modal-submit-btn updateContentPage" onclick="updateContentPage(id)">Pin</button>
                            </div>
                            <div class="pinbtn">
                                <button type="button" class="modal-submit-btn deleteContentPage" onclick="deleteContentPage(id)">Unpin</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php $this->load->view('Users/editImageModal'); ?>
<?php $this->load->view('Users/editTextModal'); ?>
<?php $this->load->view('Users/EditvideoUpload'); ?>
<!--<div id="loader-icon"><img alt="image" src="<?php //echo base_url()?>assets/img/LoaderIcon.gif" /><div>-->
<script>
    var cloudfront_base_url=$('#cloudfront_base_url').val();
    //for Pining section

    $('.pinging').click(function(e){
        e.preventDefault();
        var userId='<?php echo $this->uri->segment('3');?>';
        var albumId = $(this).attr('data-pid');
        var data="user_id="+userId+"&album_id="+albumId;
        Pace.track(function() {
            $.ajax({
                url: '<?php echo base_url();?>ImageUpload/selectAllPageDetails',
                type: 'post',
                data: data,
                success: function (data) {
                    var obj2 = JSON.parse(data);
                    $('.PingalbumId').val(albumId);
                    $('.PinguserId').val(userId);
                    //console.log(obj2);
                    var option = new Array();
                    if (obj2.page_name) {
                        //$.each(obj2.page_name, function(index, value) {
                        for (i = 0; i < obj2.page_name.length; i++) {
                            option[i] = $('<option></option>').attr("value", obj2.page_id[i]).text(obj2.page_name[i]);
                        }
                        $("#PiningToPage").html(option);
                        //});

                        for (i = 0; i < obj2.page_id.length; i++) {
                            $("#PiningToPage option[value='" + obj2.current_page_id + "']").attr('selected', 'selected');
                        }
                    }
                    if (obj2.current_page_id == 0) {
                        $('.unpin_submit').css('display', 'none');
                    }
                    else {
                        $('.unpin_submit').css('display', 'block');
                    }

                }
            });
        });
        $('#myModal').modal('show');
    });

    $('.Pin_submit').click(function(e){
        e.preventDefault();
        var albumid=$('.PingalbumId').val();
        var page_id=$('#PiningToPage').val();
        var userId=$('.PinguserId').val();
        var data="user_id="+userId+"&album_id="+albumid+"&page_id="+page_id;
        Pace.track(function() {
            $.ajax({
                url: '<?php echo base_url();?>ImageUpload/InsertPageIdInAlbumDetails',
                type: 'post',
                data: data,
                success: function (data) {
                    //$('.pinging').attr("id",'album_'+albumid);
                    $('#myModal').modal('hide');
                    $('#album_' + albumid).html('<i class="glyphicon glyphicon-pushpin red_color" aria-hidden="true"></i> Pinned').css('color', 'red');
                }
            });
        });

    });

    //unpin_submit
    $('.unpin_submit').click(function(e){
        e.preventDefault();
        var albumid=$('.PingalbumId').val();
        var page_id=$('#PiningToPage').val();
        var userId=$('.PinguserId').val();
        var base_url=$('#base_url').val();
        var data="user_id="+userId+"&album_id="+albumid+"&page_id="+page_id;
        var un_pin_submit_url=base_url+'ImageUpload/deletePageIdInAlbumDetails';
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure want to Unpin your Album?',
            buttons: {
                confirm:
                    {
                        btnClass: 'btn-danger',
                        action: function()
                        {
                            Pace.track(function() {
                                $.ajax({
                                    url: un_pin_submit_url,
                                    type: 'post',
                                    data: data,
                                    success: function (data) {
                                        $('#myModal').modal('hide');
                                        $('.pinging').html('<i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Pin').css('color', '#428bca');
                                    }
                                });
                            });

                        }
                    },
                cancel: {
                    btnClass: 'btn-info'
                }
            }
        });
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[dropdown-toggle="tooltip"]').tooltip();
        //albumCountLike
        $(function(){
            $(".albumCountLike").on("click", function(){
                var this_elem = $(this);
                var user_id=$('#session_user_id').val();
                var base_url=$('#base_url').val();
                var album_user_id=$('#album_user_id').val();
                var album_id = $(this).attr("data-pid");
                if(user_id!='')
                {
                    Pace.track(function () {
                        $.ajax({
                            type: 'post',
                            url: base_url + 'ImageUpload/addAlbumlike',
                            data: {'user_id': user_id, 'album_id': album_id, 'album_user_id': album_user_id},
                            success: function (res) {
                                var obj = JSON.parse(res);
                                if(obj.isUserAlbumLike=='yes')
                                {
                                    this_elem.children('.fa-heart').css('color','red');
                                    this_elem.children('.total_like_album').text(obj.totalLike);
                                    this_elem.attr('data-original-title', 'Dislike!');
                                }
                                else
                                {
                                    this_elem.children('.fa-heart').css('color','#fff');
                                    this_elem.children('.total_like_album').text(obj.totalLike);
                                    this_elem.attr('data-original-title', 'Like!');
                                }

                            }
                        });
                    });
                }
                else
                {
                    $.notify
                    ({
                            message: 'You must Sign In'
                        }
                        , {
                            type: 'danger',
                            offset:
                                {
                                    x: 10,
                                    y: 50
                                },
                            z_index: 1050,
                        });
                    $('#signinModal').modal('show');
                }
            });
        });
        //This is use for edit the album in a popup
        $('.EditAlbum').click(function(e){
            e.preventDefault();
            var albumId = $(this).attr('data-pid');
            //alert(albumId);
            var base_url='<?php echo base_url();?>';
            var user_id='<?php echo $this->uri->segment('3');?>';
            Pace.track(function () {
                $.ajax({
                    type: 'post',
                    url: base_url + 'ImageUpload/edit_album',
                    data: {'album_id': albumId, 'user_id': user_id},
                    success: function (res) {
                        var obj2 = JSON.parse(res);
                        var img_src = obj2.uploadPath + obj2.image_name;
                        if (img_src != "") {
                            $('#edit_album_imagePreview1').attr('src', cloudfront_base_url + img_src);
                            $('#edit_album_imagePreview1').show();
                            $('#edit_Remove_uploadFile1').show();
                            $('#edit_choosephoto1').hide();
                        }
                        if (img_src == false) {
                            $('#edit_album_imagePreview1').attr('src', "");
                            $('#edit_album_imagePreview1').hide();
                            $('#edit_Remove_uploadFile1').hide();
                            $('#edit_choosephoto1').show();
                        }
                        $('.edit_album_id').val(obj2.album_id);
                        $('#image_id').val(obj2.image_id);
                        $('#edit_album_title').val(obj2.title);
                        $('.edit_album_txtEditor1').val(obj2.description);

                        if (obj2.total_background_images != null && obj2.back_ground_image_id != null && obj2.back_ground_image_upload_path != null) {
                            var total_background_images = obj2.total_background_images;
                            var option = new Array();
                            for (i = 0; i < total_background_images; i++) {
                                if (obj2.back_ground_image_id[i] == obj2.album_background_image_id) {
                                    option[i] = '<div class="page-style-item hli" id="edit_album_back_image_inner_div_' + obj2.back_ground_image_id[i] + '" onclick="edit_readme(' + obj2.back_ground_image_id[i] + ')";><img alt="image" class="album_back_image" data-pid="' + obj2.back_ground_image_id[i] + '" src="' + cloudfront_base_url + obj2.back_ground_image_upload_path[i] + '/' + obj2.back_ground_image_image_name[i] + '"/></div>';
                                    $('#edit_background_image_id').val(obj2.album_background_image_id);
                                }
                                else {
                                    option[i] = '<div class="page-style-item" id="edit_album_back_image_inner_div_' + obj2.back_ground_image_id[i] + '" onclick="edit_readme(' + obj2.back_ground_image_id[i] + ')";><img alt="image" class="album_back_image" data-pid="' + obj2.back_ground_image_id[i] + '" src="' + cloudfront_base_url + obj2.back_ground_image_upload_path[i] + '/' + obj2.back_ground_image_image_name[i] + '"/></div>';

                                }
                            }
                        }
                        if (obj2.foreground_image_id != null && obj2.foreground_image_name != null && obj2.foreground_upload_path != null) {
                            var total_foreground_images = obj2.total_foreground_images;
                            var option1 = new Array();
                            for (i = 0; i < total_foreground_images; i++) {
                                if (obj2.foreground_image_id[i] == obj2.album_foreground_id) {
                                    option1[i] = '<div class="page-style-item toggle" id="edit_album_foreground_image_inner_div_' + obj2.foreground_image_id[i] + '" onclick="editAlbumForegroundImage(' + obj2.foreground_image_id[i] + ')";><img alt="image" class="album_foreground_image" data-pid="' + obj2.foreground_image_id[i] + '" src="' + cloudfront_base_url + obj2.foreground_upload_path[i] + '/' + obj2.foreground_image_name[i] + '" /></div>';
                                    $('.editAlbumForegroundStyleId').val(obj2.album_foreground_id);
                                }
                                else {
                                    option1[i] = '<div class="page-style-item" id="edit_album_foreground_image_inner_div_' + obj2.foreground_image_id[i] + '" onclick="editAlbumForegroundImage(' + obj2.foreground_image_id[i] + ')";><img alt="image" class="album_foreground_image" data-pid="' + obj2.foreground_image_id[i] + '" src="' + cloudfront_base_url + obj2.foreground_upload_path[i] + '/' + obj2.foreground_image_name[i] + '" /></div>';

                                }
                            }
                        }
                        if(total_foreground_images>0)
                        {
                            $('.album_foreground_header').html('Album Foreground Style');
                        }
                        else
                        {
                            $('.album_foreground_header').html('');
                        }
                        $('.album_view_permission option[value='+obj2.viewstatus+']').attr('selected','selected');
                        $('.edit_album_back_image_outr').html(option);
                        $('.edit_album_foreground_image_outr').html(option1);
                        $('#editalbumUpload').modal('show');
                    }
                });
            });
        });

        //for delete Album

        $('.delete_album').click(function(e){
            //e.preventDefault();
            var this_elem_album = $(this);
            var base_url='<?php echo base_url();?>';
            var album_id=$(this).attr("data-pid");
            var url=base_url+"ImageUpload/removeAlbum/"+album_id;

            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete your Album?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function()
                            {
                                window.location.replace(url);
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });
        });
    });
    //open the comment pop-up and show the records
    $( ".count_comment" ).on( "click", function(e) {
        e.preventDefault();
        var base_url='<?php echo base_url()?>';
        var userId='<?php echo $this->uri->segment(3)?>';
        var session_user_id='<?php echo $this->session->userdata('userid');?>';
        var albumId = $(this).attr('data-pid');
        var data="user_id="+userId+"&album_id="+albumId;
        Pace.track(function() {
            $.ajax({
                url: '<?php echo base_url()?>ImageUpload/showComments',
                type: 'post',
                data: data,
                cache: false,
                processData: false,
                success: function (data) {
                    var appendHtml = '';
                    var jsObject = JSON.parse(data);
                    //console.log(jsObject);
                    if (jsObject.commentsData.length > 0) {
                        for (var i = 0; i < jsObject.commentsData.length; i++) {
                            appendHtml += '<div class="use-padding  use-border">';
                            appendHtml += '<div class="left-box">';
                            appendHtml += '<p><span class="square-icon">';
                            if (jsObject.commentsData[i].profileimage) {

                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                            }
                            else {
                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                            }
                            appendHtml += '</span>';
                            appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                            appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                            appendHtml += '</div>';
                            appendHtml += '<div class="right-box">';
                            if (session_user_id) {
                                for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                                    if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                        flag = 1;
                                        break;
                                    }
                                    else {
                                        flag = 0;
                                    }
                                }
                                if (flag == 1) {
                                    appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                }
                                else {
                                    if (session_user_id != jsObject.commentsData[i].userid) {
                                        appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                    }

                                }
                            }

                            var flag1 = 0;
                            var count = 0;
                            for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                                if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                    flag1 = 1;
                                    break;
                                }

                            }
                            for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                                if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                    count++;
                                }
                            }
                            if (flag1 == 1) {
                                appendHtml += '<a href="#" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            else {
                                appendHtml += '<a href="#" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                            appendHtml += '</div>';
                            if (jsObject.commentsData[i].image_comments) {
                                appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                            }
                            appendHtml += '</div>';

                        }

                    }
                    $('#comments_type_id').val(albumId);
                    $('.comment_body').html(appendHtml);
                    $('#album_comment_' + albumId).html(jsObject.commentsData.length);

                }
            });
        });
        $("#comment_div").modal('show');
        $("#comment_div").css('width',"315px");

    });
    //closeComment
    function closeComment() {
        $("#comment_div").modal('hide');
        $("#comment_div").css('width',"0");
    }
    // Choose image file
    $(document).on('click', '.image_browse_icon', function()
    {
        $(".image_file").click();

    });
    //comment like
    function comment_like(id)
    {
        var session_user_id=$('#session_user_id').val();
        if(session_user_id)
        {
            var url=$('#base_url').val()+'ImageUpload/commentLike';
            var base_url = $('#base_url').val();
            Pace.track(function () {
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {comment_id: id},
                    success: function (data) {
                        var jsObject = JSON.parse(data);
                        if (jsObject.existLike == true) {
                            $('#' + id).attr('data-original-title', 'Like!');
                            $('#' + id).attr('title', 'Like!');
                            $('#' + id).find('.comment_like_dislike').css('color', '#5F5F5F');

                        }
                        else {
                            $('#' + id).attr('data-original-title', 'Dislike!');
                            $('#' + id).attr('title', 'Dislike!');
                            $('#' + id).find('.comment_like_dislike').css('color', 'red');
                        }
                        $('#count_comment_like_' + id).html(jsObject.totalCommentLike.total);

                    }
                });
            });
        }
        else
        {
            $.notify
            ({

                    message: 'You must Sign In'
                }
                , {
                    type: 'danger',
                    offset:
                        {
                            x: 10,
                            y: 50
                        },
                    z_index: 1050,
                });
            $('.relationshipsModal').modal('hide');
            $('.relationshipsModal1').modal('hide');
            $("#comment_div").modal('hide');
            $('#signinModal').modal('show');
        }
    }
    //comment submit
    $('document').ready(function ()
    {
        /* Send Button Enable or disable for support */
        $('#album_comment_form textarea').on('keyup blur', function () { // fires on every keyup & blur
            var comment_text = $('#comment_text').val().trim();
            if (comment_text != '')  {                   // checks form for validity
                $('#btn_comment_relationships').prop('disabled', false);        // enables button
                $('#btn_comment_relationships').css('cursor', 'pointer');

            } else {
                $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
                $('#btn_comment_relationships').css('cursor', 'not-allowed');

            }
        });
        $('.image_file').change(function ()
        {
            if (this.files && this.files[0] &&  this.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
            {

            }
            else
            {
                $(this).val('');
                $.alert({
                    title: 'Alert!',
                    content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
                });
            }
        });
        $("#album_comment_form").validate(
            {

                submitHandler: function(form)
                {
                    var session_user_id=$('#session_user_id').val();
                    if(session_user_id) {
                        var base_url = $('#base_url').val();
                        var url = $('#base_url').val() + 'ImageUpload/commentAlbum';
                        var comments_type_id = $('#comments_type_id').val();
                        //var user_id=$('#profile_user_id').val();
                        var user_id = '<?php echo $this->uri->segment(3);?>';
                        var session_user_id = $('#session_user_id').val();
                        var comment_text = $('#comment_text').val().trim();
                        if (comment_text != '') {
                            var comments_form_data = new FormData();
                            comments_form_data.append('comments_type_id', comments_type_id);
                            comments_form_data.append('comment_text', comment_text);
                            comments_form_data.append('album_user_id', user_id);
                            if ($('.image_file')[0].files[0]) {
                                comments_form_data.append('comment_file', $('.image_file')[0].files[0]);
                            }
                            Pace.track(function () {
                                $.ajax({
                                    url: url,
                                    type: 'post',
                                    data: comments_form_data,
                                    contentType: false,
                                    processData: false,
                                    cache: false,
                                    success: function (data) {
                                        $('#btn_comment_relationships').prop('disabled', 'disabled');   // disables button
                                        $('#btn_comment_relationships').css('cursor', 'not-allowed');
                                        var appendHtml = '';
                                        var jsObject = JSON.parse(data);
                                        if (jsObject.commentsData.length > 0) {
                                            for (var i = 0; i < jsObject.commentsData.length; i++) {
                                                var flag = 0;
                                                appendHtml += '<div class="use-padding  use-border">';
                                                appendHtml += '<div class="left-box">';
                                                appendHtml += '<p><span class="square-icon">';
                                                if (jsObject.commentsData[i].profileimage) {

                                                    appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                                                }
                                                else {
                                                    appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                                                }
                                                appendHtml += '</span>';
                                                appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id=' + jsObject.commentsData[i].pageid + '"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                                                appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                                                appendHtml += '</div>';
                                                appendHtml += '<div class="right-box">';
                                                for (var j = 0; j < jsObject.relationshipUsers.length; j++) {
                                                    if (jsObject.relationshipUsers[j].userid == jsObject.commentsData[i].userid) {
                                                        flag = 1;
                                                        break;
                                                    }
                                                    else {
                                                        flag = 0;
                                                    }
                                                }
                                                if (flag == 1) {
                                                    appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + session_user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                                }
                                                else {
                                                    if (session_user_id != jsObject.commentsData[i].userid) {
                                                        appendHtml += '<a href="' + base_url + 'Users/relationships?user_id=' + user_id + '&friends_id=' + jsObject.commentsData[i].userid + '"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png">&nbsp;</a>';
                                                    }

                                                }
                                                var flag1 = 0;
                                                var count = 0;
                                                for (var j = 0; j < jsObject.allLikecomment.length; j++) {
                                                    if (session_user_id == jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid == jsObject.allLikecomment[j].comment_id) {
                                                        flag1 = 1;
                                                        break;
                                                    }

                                                }
                                                for (var k = 0; k < jsObject.allLikecomment.length; k++) {
                                                    if (jsObject.commentsData[i].tributesid == jsObject.allLikecomment[k].comment_id) {
                                                        count++;
                                                    }
                                                }
                                                if (flag1 == 1) {
                                                    appendHtml += '<a href="#" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"><i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                                }
                                                else {
                                                    appendHtml += '<a href="#" id="' + jsObject.commentsData[i].tributesid + '" data-toggle="tooltip" title="Like!" onclick="comment_like(id)" ><i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                                                }
                                                appendHtml += ' <span id="count_comment_like_' + jsObject.commentsData[i].tributesid + '">' + count + '</span>';
                                                appendHtml += '</div>';
                                                if (jsObject.commentsData[i].image_comments) {
                                                    appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                                                }
                                                appendHtml += '</div>';

                                            }
                                        }
                                        //$('.count_comment').html(jsObject.commentsData.length);
                                        $('#album_comment_' + comments_type_id).html(jsObject.commentsData.length);
                                        $('.comment_body').html(appendHtml);
                                        $('#comment_text').val('');
                                        $('.image_file').val('');
                                        $('#comment_particular_' + comments_type_id).html(jsObject.commentsData.length);
                                    }
                                });
                            });
                        }
                    }
                    else
                    {
                        $.notify
                        ({

                                message: 'You must Sign In'
                            }
                            , {
                                type: 'danger',
                                offset:
                                    {
                                        x: 10,
                                        y: 50
                                    },
                                z_index: 1050,
                            });
                        $('#comment_text').val('');
                        $('.image_file').val('');
                        $('.relationshipsModal').modal('hide');
                        $('.relationshipsModal1').modal('hide');
                        $("#comment_div").modal('hide');
                        $('#signinModal').modal('show');
                    }
                }
            });
    });
</script>

<script>
    function deleteContentPost(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];
        var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
        if(check_type == 'image')
        {
            var image_url = base_url + "ImageUpload/removeAlbumImage/" + album_id + '/' + check_id;
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete your Image?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function () {
                                window.location.replace(image_url);
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });
        }
        if(check_type == 'text')
        {
            var text_url=base_url+"ImageUpload/removeAlbumText/"+album_id+'/'+check_id;
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete your Text Content?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function()
                            {
                                window.location.replace(text_url);
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });
        }
        if(check_type == 'video')
        {
            var video_url=base_url+"ImageUpload/removeAlbumVideo/"+album_id+'/'+check_id;
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to delete your Video?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function()
                            {
                                window.location.replace(video_url);
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });
        }
    }
    function editContentPost(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];
        var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
        if(check_type == 'image')
        {


            $('.relationshipsModal').modal('hide');
            $("#comment_div").modal('hide');

            Pace.track(function()
            {
                $.ajax({
                    type: 'post',
                    url: base_url + "ImageUpload/edit_inner_image",
                    data: {'album_id':album_id,'image_id':check_id },
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        var appendHtml= '';
                        if(obj.albumeid)
                        {
                            for(var i=0;i<obj.albumeid.length;i++)
                            {
                                if(album_id==obj.albumeid[i])
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                                }

                                else
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                                }

                            }
                        }
                        else
                        {
                            appendHtml += '<option value="-1">My Chronicles</option>'
                        }
                        $('.album_list_id').html(appendHtml);
                        $('#single_image_album_id').val(album_id);
                        $('#editSingleImageUpload').modal('show');
                        var img_src=cloudfront_base_url+obj.uploadPath+obj.image_name;
                        $('#edit_imagePreview2').attr('src',img_src);
                        $('.editImageIconForEdit').css('display','block');
                        if(img_src!='')
                        {
                            $('#edit_Remove_uploadFile2').show();
                            $('#edit_imagePreview2').show();
                            $('#edit_choosephoto2').hide();
                        }

                        $('#edit_title').val(obj.image_title);
                        $('.edit_txtEditor').val(obj.image_desc);
                        $('#edit_inner_image_id').val(obj.image_id);
                        if(obj.image_id)
                        {
                            $(".friend_list_edit_single").empty();
                            if(obj.friend_fullName!=null)
                            {
                                $.each(obj.friend_fullName, function(index, value) {
                                    option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                    $(".friend_list_edit_single").append(option);
                                });
                                if(obj.tag_user_id)
                                {
                                    for (var i=0;i<obj.tag_user_id.length;i++){
                                        $(".friend_list_edit_single option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                                    }
                                }

                            }

                        }

                    }
                });
            });

        }
        if(check_type == 'text')
        {
            $('.relationshipsModal1').modal('hide');
            $("#comment_div").modal('hide');
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "ImageUpload/select_edit_album_text",
                    data: {'album_id': album_id, 'edit_text_id': check_id},
                    success: function (data) {
                        var obj = JSON.parse(data);
                        var appendHtml= '';
                        if(obj.albumeid.length>0)
                        {
                            for(var i=0;i<obj.albumeid.length;i++)
                            {
                                if(album_id==obj.albumeid[i])
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.albumTitle[i]+'</option>';
                                }

                                else
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.albumTitle[i]+'</option>';
                                }

                            }
                        }
                        else
                        {
                            appendHtml += '<option value="-1">My Chronicles</option>'
                        }
                        $('.album_list_id').html(appendHtml);
                        $('#show_edit_text_title').val(obj.text_title);
                        $('.edit_txtEditor').html(obj.text_description);
                        $('#edit_text_id').val(obj.textid);
                        $('#edittextcontent').modal('show');
                        if (obj.textid) {
                            $(".friend_list_edit_text").empty();
                            if (obj.friend_fullName != null) {
                                $.each(obj.friend_fullName, function (index, value) {
                                    option = $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                    $(".friend_list_edit_text").append(option);
                                });
                                for (i = 0; i < obj.tag_user_id.length; i++) {
                                    $(".friend_list_edit_text option[value='" + obj.tag_user_id[i] + "']").attr('selected', 'selected');
                                }
                            }
                        }
                    }
                });
            });
        }
        if(check_type == 'video')
        {
            $('.relationshipsModal').modal('hide');
            $("#comment_div").modal('hide');
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "ImageUpload/select_edit_album_video",
                    data: {'album_id': album_id, 'edit_video_id': check_id},
                    success: function (data) {
                        var obj = JSON.parse(data);
                        var appendHtml= '';
                        if(obj.albumeid.length>0)
                        {
                            for(var i=0;i<obj.albumeid.length;i++)
                            {
                                if(album_id==obj.albumeid[i])
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'" selected>'+obj.album_title[i]+'</option>';
                                }

                                else
                                {
                                    appendHtml += '<option value="'+obj.albumeid[i]+'">'+obj.album_title[i]+'</option>';
                                }

                            }
                        }
                        else
                        {
                            appendHtml += '<option value="-1">My Chronicles</option>'
                        }
                        $('.album_list_id').html(appendHtml);
                        $('#editvideoUpload').modal('show');
                        $('#edit_video_id').val(obj.video_id);
                        $('.show_edit_video').html(obj.video_url);
                        if(obj.video_id)
                        {
                            $(".friend_list_edit_video").empty();
                            if(obj.friend_fullName!=null)
                            {
                                $.each(obj.friend_fullName, function(index, value) {
                                    option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
                                    $(".friend_list_edit_video").append(option);
                                });

                                for (i=0;i<obj.tag_user_id.length;i++){
                                    $(".friend_list_edit_video option[value='"+obj.tag_user_id[i]+"']").attr('selected', 'selected');
                                }
                            }

                        }
                    }
                });
            });
        }
    }
    function showModal(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];

        var content_title=$('#relationshipsModalShow_'+id).find('.content_title').val();
        var content_description=$('#relationshipsModalShow_'+id).find('.content_description').val();
        var tagger_name=$('#relationshipsModalShow_'+id).find('.tagger_name').val();
        var tagger_image=$('#relationshipsModalShow_'+id).find('.tagger_image').attr('src');
        var album_name=$('#relationshipsModalShow_'+id).find('.album_name').val();
        var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
        var tagger_page_id=$('#relationshipsModalShow_'+id).find('.tagger_page_id').val();
        var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
        var tagger_profile=base_url+'Users/profile/'+tagger_id+'?page_id='+tagger_page_id;
        var tagger_album=base_url+'ImageUpload/albumdetails/'+tagger_id+'/'+album_id;

        $('#album_comment_form').find('#comments_type_id').val(id);
        $('.relationshipsModal').find('.comment_icon').attr('id',id);
        $('.tagger_user_id_modal').val(tagger_id);
        if(check_type == 'image')
        {
            var img=$('#relationshipsModalShow_'+id).find('img').attr('src');
            $('.relationshipsModal').find('.content_image_modal').attr('src',img);
            $('.relationshipsModal').find('.full_image_view ').attr('href',img);

            $('.relationshipsModal').find('.tagger_profile_link').attr('href',tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href',tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src',tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id',id);

            $('.relationshipsModal').find('.content_text_modal').css('display','none');
            $('.relationshipsModal').find('.content_video_modal').css('display','none');
            $('.relationshipsModal').find('.content_image_modal').css('display','block');

            $('.relationshipsModal').modal('show');


        }
        if(check_type == 'video')
        {
            var video=$('#relationshipsModalShow_'+id).find('iframe').attr('src');
            $('.relationshipsModal').find('.content_video_modal').attr('src',video);

            $('.relationshipsModal').find('.tagger_profile_link').attr('href',tagger_profile);
            $('.relationshipsModal').find('.tagger_album_link').attr('href',tagger_album);
            $('.relationshipsModal').find('.content_title_modal').html(content_title);
            $('.relationshipsModal').find('.content_description_modal').html(content_description);
            $('.relationshipsModal').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal').find('.album_name_modal').html(album_name);
            $('.relationshipsModal').find('.tagger_image_modal').attr('src',tagger_image);
            $('.relationshipsModal').find('.modal_comment_id').attr('id',id);

            $('.relationshipsModal').find('.content_text_modal').css('display','none');
            $('.relationshipsModal').find('.content_video_modal').css('display','block');
            $('.relationshipsModal').find('.content_image_modal').css('display','none');

            $('.relationshipsModal').modal('show');

        }
        if(check_type=='text')
        {
            var text=$('#relationshipsModalShow_'+id).find('.content_description').val();
            $('.relationshipsModal1').find('.content_description_modal').html('');

            $('.relationshipsModal1').find('.tagger_profile_link').attr('href',tagger_profile);
            $('.relationshipsModal1').find('.tagger_album_link').attr('href',tagger_album);
            /*$('.relationshipsModal1').find('.content_title_modal').html(content_title);*/
            $('.relationshipsModal1').find('.tagger_name_modal').html(tagger_name);
            $('.relationshipsModal1').find('.album_name_modal').html(album_name);
            $('.relationshipsModal1').find('.tagger_image_modal').attr('src',tagger_image);
            $('.relationshipsModal1').find('.modal_comment_id').attr('id',id);

            $('.relationshipsModal1').find('.content_text_modal').html(text);
            $('.relationshipsModal1').find('.content_image_modal').css('display','none');
            $('.relationshipsModal1').find('.content_video_modal').css('display','none');
            $('.relationshipsModal1').find('.content_text_modal').css('display','block');

            $('.relationshipsModal1').modal('show');
        }

        var session_user_id=$('#session_user_id').val();
        $('.editContentId').attr('id',id);
        $('.deleteContentId').attr('id',id);
        $('.pinModal').attr('id',id);
        if(tagger_id==session_user_id)
        {
            $('.editContentId').show();
            $('.deleteContentId').show();
            $('.pinModal').show();
        }
        else
        {
            $('.editContentId').hide();
            $('.deleteContentId').hide();
            $('.pinModal').hide();
        }
        openComment1(id);
    }
    function openComment1(id)
    {
        var url=$('#base_url').val()+'Users/showCommentRelationships';
        var base_url=$('#base_url').val();
        var user_id=$('#session_user_id').val();
        var session_user_id=$('#session_user_id').val();
        var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
        var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
        Pace.track(function() {
            $.ajax({
                url: url,
                type: 'post',
                data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
                success: function (data) {
                    var appendHtml = '';
                    var jsObject = JSON.parse(data);
                    if (jsObject.commentsData.length > 0) {
                        for (var i = 0; i < jsObject.commentsData.length; i++) {
                            appendHtml += '<div class="use-padding  use-border">';
                            appendHtml += '<div class="left-box">';
                            appendHtml += '<p><span class="square-icon">';
                            if (jsObject.commentsData[i].profileimage) {

                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                            }
                            else {
                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                            }
                            appendHtml += '</span>';
                            appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                            appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                            appendHtml += '</div>';
                            appendHtml += '<div class="right-box">';
                            var flag1=0;
                            var flag=0;
                            var count=0;
                            if(session_user_id)
                            {
                                for(var j=0;j<jsObject.relationshipUsers.length;j++)
                                {
                                    if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                    {
                                        flag=1;
                                        break;
                                    }
                                    else
                                    {
                                        flag=0;
                                    }
                                }
                                if(flag==1)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png"></a>';
                                }
                                else
                                {
                                    if(session_user_id!=jsObject.commentsData[i].userid)
                                    {
                                        appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png"></a>';
                                    }

                                }
                            }
                            for(var j=0;j<jsObject.allLikecomment.length;j++)
                            {
                                if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                                {
                                    flag1=1;
                                    break;
                                }

                            }
                            for(var k=0;k<jsObject.allLikecomment.length;k++)
                            {
                                if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                                {
                                    count++;
                                }
                            }
                            if(flag1==1)
                            {
                                appendHtml += '<a href="#" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"> <i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            else
                            {
                                appendHtml += '<a href="#" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"> <i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                            appendHtml += '</div>';
                            if (jsObject.commentsData[i].image_comments) {
                                appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                            }
                            appendHtml += '</div>';

                        }
                    }
                    if(session_user_id)
                    {
                        var check_id=id.split('_')[1];
                        var check_type=id.split('_')[0];
                        if(check_type=='image')
                        {
                            if(jsObject.existPageData)
                            {
                                if (jsObject.existPageData.image_id==check_id)
                                {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }
                        if(check_type=='text')
                        {
                            if(jsObject.existPageData)
                            {
                                if (jsObject.existPageData.text_id == check_id) {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }
                        if(check_type=='video')
                        {
                            if(jsObject.existPageData) {
                                if (jsObject.existPageData.video == check_id) {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }

                    }
                    if(session_user_id)
                    {
                        if (jsObject.existLike == true) {
                            $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                            $('.user_like_dislike_title').attr('title', 'Dislike!');
                            $('.user_like_dislike').css('color', 'red');

                        }
                        else {
                            $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                            $('.user_like_dislike_title').attr('title', 'Like!');
                            $('.user_like_dislike').css('color', '#5F5F5F');
                        }
                    }
                    var seeTaggedHtml='';
                    if(jsObject.seeTagged.length>0)
                    {
                        for(var p=0;p<jsObject.seeTagged.length;p++)
                        {
                            seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                        }

                    }
                    $('.see_tagged_name').html(seeTaggedHtml);
                    $('.count_album_content').html(jsObject.count_album_content);
                    $('.count_tag').html(jsObject.seeTagged.length);
                    $('.count_like').html(jsObject.totalLikePerticular.total);
                    $('.comment_body').html(appendHtml);
                    $('.modal_comment_id#'+id).find('.count_comment').html(jsObject.commentsData.length);
                }
            });
        });
    }
    function openComment(id)
    {

        var url=$('#base_url').val()+'Users/showCommentRelationships';
        var base_url=$('#base_url').val();
        var user_id=$('#session_user_id').val();
        var session_user_id=$('#session_user_id').val();
        var tagger_id=$('#relationshipsModalShow_'+id).find('.tagger_id').val();
        var album_id=$('#relationshipsModalShow_'+id).find('.album_id').val();
        $('#album_comment_form').find('#comments_type_id').val(id);
        Pace.track(function() {
            $.ajax({
                url: url,
                type: 'post',
                data: {comments_type_id: id,user_id:user_id,album_id:album_id,tagger_id:tagger_id},
                success: function (data) {
                    var appendHtml = '';
                    var jsObject = JSON.parse(data);
                    if (jsObject.commentsData.length > 0) {
                        for (var i = 0; i < jsObject.commentsData.length; i++) {
                            appendHtml += '<div class="use-padding  use-border">';
                            appendHtml += '<div class="left-box">';
                            appendHtml += '<p><span class="square-icon">';
                            if (jsObject.commentsData[i].profileimage) {

                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + jsObject.commentsData[i].profileimage + '" />';
                            }
                            else {
                                appendHtml += '<img alt="image" class="img-circle" style="height: 30px;width:30px" src="' + cloudfront_base_url + 'assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />';
                            }
                            appendHtml += '</span>';
                            appendHtml += '<a class="comment_user" href="' + base_url + 'Users/profile/' + jsObject.commentsData[i].userid + '?page_id='+ jsObject.commentsData[i].pageid +'"> <b>' + jsObject.commentsData[i].fullname + '</b></a></p>';
                            appendHtml += '<p>' + jsObject.commentsData[i].description + '</p>';
                            appendHtml += '</div>';
                            appendHtml += '<div class="right-box">';
                            var flag1=0;
                            var flag=0;
                            var count=0;
                            if(session_user_id)
                            {
                                for(var j=0;j<jsObject.relationshipUsers.length;j++)
                                {
                                    if(jsObject.relationshipUsers[j].userid==jsObject.commentsData[i].userid)
                                    {
                                        flag=1;
                                        break;
                                    }
                                    else
                                    {
                                        flag=0;
                                    }
                                }
                                if(flag==1)
                                {
                                    appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+session_user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png"></a>';
                                }
                                else
                                {
                                    if(session_user_id!=jsObject.commentsData[i].userid)
                                    {
                                        appendHtml += '<a href="'+base_url+'Users/relationships?user_id='+user_id+'&friends_id='+jsObject.commentsData[i].userid+'"><img alt="image" src="' + cloudfront_base_url + 'assets/img/icon1.png"></a>';
                                    }

                                }
                            }
                            for(var j=0;j<jsObject.allLikecomment.length;j++)
                            {
                                if(session_user_id==jsObject.allLikecomment[j].user_id && jsObject.commentsData[i].tributesid==jsObject.allLikecomment[j].comment_id)
                                {
                                    flag1=1;
                                    break;
                                }

                            }
                            for(var k=0;k<jsObject.allLikecomment.length;k++)
                            {
                                if(jsObject.commentsData[i].tributesid==jsObject.allLikecomment[k].comment_id)
                                {
                                    count++;
                                }
                            }
                            if(flag1==1)
                            {
                                appendHtml += ' <a href="#" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Dislike!" onclick="comment_like(id)"> <i style="color:red;" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            else
                            {
                                appendHtml += ' <a href="#" id="'+jsObject.commentsData[i].tributesid+'" data-toggle="tooltip" title="Like!" onclick="comment_like(id)"> <i style="color: rgb(95, 95, 95);" class="fa fa-heart comment_like_dislike" aria-hidden="true"></i></a>';
                            }
                            appendHtml +=' <span id="count_comment_like_'+jsObject.commentsData[i].tributesid+'">'+count+'</span>';
                            appendHtml += '</div>';
                            if (jsObject.commentsData[i].image_comments) {
                                appendHtml += '<img alt="image" class="men-in-black" src="' + cloudfront_base_url + jsObject.commentsData[i].image_comments + '" />';
                            }
                            appendHtml += '</div>';

                        }
                    }
                    if(session_user_id)
                    {
                        var check_id=id.split('_')[1];
                        var check_type=id.split('_')[0];
                        if(check_type=='image')
                        {
                            if(jsObject.existPageData)
                            {
                                if (jsObject.existPageData.image_id==check_id)
                                {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }
                        if(check_type=='text')
                        {
                            if(jsObject.existPageData)
                            {
                                if (jsObject.existPageData.text_id == check_id) {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }
                        if(check_type=='video')
                        {
                            if(jsObject.existPageData) {
                                if (jsObject.existPageData.video == check_id) {
                                    $('.pinModal').attr('data-original-title', 'Pinned!');
                                    $('.pinModal').attr('title', 'Pinned!');
                                    $('.pinModal').css('color', 'red');
                                    $('.deleteContentPage').show();

                                }
                            }
                            else {
                                $('.pinModal').attr('data-original-title', 'Pin!');
                                $('.pinModal').attr('title', 'Pin!');
                                $('.pinModal').css('color', '#fff');
                                $('.deleteContentPage').hide();
                            }
                        }

                    }
                    if(session_user_id)
                    {
                        if (jsObject.existLike == true) {
                            $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                            $('.user_like_dislike_title').attr('title', 'Dislike!');
                            $('.user_like_dislike').css('color', 'red');

                        }
                        else {
                            $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                            $('.user_like_dislike_title').attr('title', 'Like!');
                            $('.user_like_dislike').css('color', '#5F5F5F');
                        }
                    }
                    var seeTaggedHtml='';
                    if(jsObject.seeTagged.length>0)
                    {
                        for(var p=0;p<jsObject.seeTagged.length;p++)
                        {
                            seeTaggedHtml +='<li><a href="'+base_url+'Users/profile/'+jsObject.seeTagged[p].userid+'?page_id='+jsObject.seeTagged[p].pageid+'">'+jsObject.seeTagged[p].fullname+'</a></li>';
                        }

                    }
                    $('.see_tagged_name').html(seeTaggedHtml);
                    $('.count_album_content').html(jsObject.count_album_content);
                    $('.count_tag').html(jsObject.seeTagged.length);
                    $('.count_like').html(jsObject.totalLikePerticular.total);
                    $('.comment_body').html(appendHtml);
                    $('.modal_comment_id#'+id).find('.count_comment').html(jsObject.commentsData.length);
                }
            });
        });
        $("#comment_div").modal('show');
        $("#comment_div").css('width',"315px");
    }

    function like(id)
    {
        var session_user_id=$('#session_user_id').val();
        if(session_user_id)
        {
            var url=$('#base_url').val()+'Users/likeContent';
            var base_url=$('#base_url').val();
            var tagger_id=$('.tagger_user_id_modal').val();
            Pace.track(function() {
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {id: id,tagger_id:tagger_id},
                    success: function (data) {

                        var jsObject = JSON.parse(data);
                        if (jsObject.existLike == true) {
                            $('.user_like_dislike_title').attr('data-original-title', 'Like!');
                            $('.user_like_dislike_title').attr('title', 'Like!');
                            $('.user_like_dislike').css('color', '#5F5F5F');

                        }
                        else {
                            $('.user_like_dislike_title').attr('data-original-title', 'Dislike!');
                            $('.user_like_dislike_title').attr('title', 'Dislike!');
                            $('.user_like_dislike').css('color', 'red');
                        }
                        $('.count_like').html(jsObject.totalLikePerticular.total);
                        $('#like_particular_' + id).html(jsObject.totalLikePerticular.total);
                    }
                });
            });
        }
        else
        {
            $.notify
            ({

                    message: 'You must Sign In'
                }
                , {
                    type: 'danger',
                    offset:
                        {
                            x: 10,
                            y: 50
                        },
                    z_index: 1050,
                });
            $('.relationshipsModal').modal('hide');
            $('.relationshipsModal1').modal('hide');
            $("#comment_div").modal('hide');
            $('#signinModal').modal('show');
        }

    }

    function pinModalEditDelete(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];
        $('.updateContentPage').attr('id',id);
        $('.deleteContentPage').attr('id',id);
        if(check_type=='image')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/imagePinEditDelete",
                    data: {'image_id':check_id},
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        var appendHtml='';
                        if(obj.allPageId.length>0)
                        {
                            for(var i=0;i<obj.allPageId.length;i++)
                            {
                                if(obj.selectedPageId)
                                {
                                    if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }


                        }
                        $('#myModal').find('.selectPageId').html(appendHtml);
                        $('#myModal').modal('show');
                    }
                });
            });
        }
        if(check_type=='text')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/textPinEditDelete",
                    data: {'text_id':check_id},
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        var appendHtml='';
                        if(obj.allPageId.length>0)
                        {
                            for(var i=0;i<obj.allPageId.length;i++)
                            {
                                if(obj.selectedPageId)
                                {
                                    if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }


                        }
                        $('#myModal').find('.selectPageId').html(appendHtml);
                        $('#myModal').modal('show');
                    }
                });
            });
        }
        if(check_type=='video')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/videoPinEditDelete",
                    data: {'video_id':check_id},
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        var appendHtml='';
                        if(obj.allPageId.length>0)
                        {
                            for(var i=0;i<obj.allPageId.length;i++)
                            {
                                if(obj.selectedPageId)
                                {
                                    if(obj.selectedPageId.page_id==obj.allPageId[i].pageid)
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'" selected>Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '" selected>' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].default_page==1)
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                            }
                                            else
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                            }

                                        }
                                        else
                                        {
                                            if(obj.allPageId[i].page_title=="")
                                            {
                                                appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                            }
                                            else {
                                                appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    if(obj.allPageId[i].default_page==1)
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Default Page</option>';
                                        }
                                        else
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">'+obj.allPageId[i].page_title+'</option>';
                                        }

                                    }
                                    else
                                    {
                                        if(obj.allPageId[i].page_title=="")
                                        {
                                            appendHtml +='<option value="'+obj.allPageId[i].pageid+'">Page '+i+'</option>';
                                        }
                                        else {
                                            appendHtml += '<option value="' + obj.allPageId[i].pageid + '">' + obj.allPageId[i].page_title + '</option>';
                                        }
                                    }
                                }
                            }


                        }
                        $('#myModal').find('.selectPageId').html(appendHtml);
                        $('#myModal').modal('show');
                    }
                });
            });
        }

    }
    function updateContentPage(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];
        var selected = $('.selectPageId option:selected');
        var page_id=selected.val();
        if(check_type=='image')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/updateContentPageImage",
                    data: {'image_id':check_id,page_id:page_id},
                    success: function (data)
                    {

                        $('.pinModal').attr('data-original-title', 'Pinned!');
                        $('.pinModal').attr('title', 'Pinned!');
                        $('.pinModal').css('color', 'red');
                        $('#pinModal').modal('hide');
                        $('.deleteContentPage').show();

                    }
                });
            });
        }
        if(check_type=='text')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/updateContentPageText",
                    data: {'text_id':check_id,page_id:page_id},
                    success: function (data)
                    {
                        $('.pinModal').attr('data-original-title', 'Pinned!');
                        $('.pinModal').attr('title', 'Pinned!');
                        $('.pinModal').css('color', 'red');
                        $('#pinModal').modal('hide');
                        $('.deleteContentPage').show();
                    }
                });
            });
        }
        if(check_type=='video')
        {
            Pace.track(function() {
                $.ajax({
                    type: 'post',
                    url: base_url + "Users/updateContentPageVideo",
                    data: {'video_id':check_id,page_id:page_id},
                    success: function (data)
                    {
                        $('.pinModal').attr('data-original-title', 'Pinned!');
                        $('.pinModal').attr('title', 'Pinned!');
                        $('.pinModal').css('color', 'red');
                        $('#pinModal').modal('hide');
                        $('.deleteContentPage').show();
                    }
                });
            });
        }
    }
    function deleteContentPage(id)
    {
        var base_url=$('#base_url').val();
        var check_type=id.split('_')[0];
        var check_id=id.split('_')[1];
        if(check_type=='image')
        {

            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to unpin Page Content Image?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function () {
                                Pace.track(function() {
                                    $.ajax({
                                        type: 'post',
                                        url: base_url + "Users/deleteContentPageImage",
                                        data: {'image_id':check_id},
                                        success: function (data)
                                        {
                                            $('.pinModal').attr('data-original-title', 'Pin!');
                                            $('.pinModal').attr('title', 'Pin!');
                                            $('.pinModal').css('color', '#fff');
                                            $('#pinModal').modal('hide');
                                            $('.deleteContentPage').hide();
                                        }
                                    });
                                });
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });

        }
        if(check_type=='video')
        {

            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to unpin Page Content Video?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function () {
                                Pace.track(function() {
                                    $.ajax({
                                        type: 'post',
                                        url: base_url + "Users/deleteContentPageVideo",
                                        data: {'video_id':check_id},
                                        success: function (data)
                                        {
                                            $('.pinModal').attr('data-original-title', 'Pin!');
                                            $('.pinModal').attr('title', 'Pin!');
                                            $('.pinModal').css('color', '#fff');
                                            $('#pinModal').modal('hide');
                                            $('.deleteContentPage').hide();
                                        }
                                    });
                                });
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });

        }
        if(check_type=='text')
        {

            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure want to unpin Page Content Text?',
                buttons: {
                    confirm:
                        {
                            btnClass: 'btn-danger',
                            action: function () {
                                Pace.track(function() {
                                    $.ajax({
                                        type: 'post',
                                        url: base_url + "Users/deleteContentPageText",
                                        data: {'text_id':check_id},
                                        success: function (data)
                                        {
                                            $('.pinModal').attr('data-original-title', 'Pin!');
                                            $('.pinModal').attr('title', 'Pin!');
                                            $('.pinModal').css('color', '#fff');
                                            $('#pinModal').modal('hide');
                                            $('.deleteContentPage').hide();
                                        }
                                    });
                                });
                            }
                        },
                    cancel: {
                        btnClass: 'btn-info'
                    }
                }
            });

        }

    }
</script>