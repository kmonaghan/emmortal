<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
        if(image_name && upload_path)
        {
            $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/home.js"></script>
<body class="body_back">
    <div id="main-wrapper">
        <!-- Header Link -->
        <?php $this->load->view('Users/header') ?>
            <div class="container">
                <div class="body-area">
                    <div class="foreground_dynamic" id="imgsc-dash">
                        <input type="hidden" value="0" id="row_count">
                        <input type="hidden" value="<?php echo $CountAllHomeContent ?>" id="total_row">
                        <ul class="album-list" id="homeContent">
                        </ul>
                    </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-8 no-padding">
                            <a href="#" class="full_image_view example-image-link" data-lightbox="example-set">
							<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
							<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
							</a>
                        </div>
                        <div class="col-lg-4 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer add-margintop">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="#" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="#" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-outer">

                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <div class="modrel">
                        <?php if($this->session->userdata('userid'))
                        { ?>
                            <li>
                                <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip">
                                    <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                    <i class="glyphicon glyphicon-pencil EditText"></i>
                                </a>
                            </li>
                            <li>
                                <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php
                        } ?>
                    </div>
                    <div class="modal-body">

                        <div class="col-lg-12 no-padding">
                                <p class="modal-text content_title_modal"></p>
                                <p class="left-image txtmodara content_text_modal" style="display: none"></p>
                        </div>
                        <div class="col-lg-12 no-padding padding-left">
                            <div class="modal-body no-padding">
                                <p class="modal-text content_description_modal"></p>

                            </div>

                            <div class="modal-footer text-footer txttopmrgn">
                                <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                    <a href="#" class="tagger_profile_link">
                                        <span class="tagger_name_modal"></span>
                                    </a>
                                </h3>
                                <input type="hidden" class="tagger_user_id_modal">
                                <h5><a href="#" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                                <ul class="footer-icon">
                                    <li>
                                        <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                        <ul class="dropdown-menu see_tagged_name">

                                        </ul>
                                    </li>
                                    <li><a href="#" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                    <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                    <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal right fade" id="comment_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onClick="closeComment()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="comment_body">
                        </div>
                        <div class="use-padding">
                            <form method="post" enctype="multipart/form-data" id="comment_form">
                                <input type="hidden" value="" id="comments_type_id">
                                <span class="square-icon">
                                    <img alt="image" class="img-circle" style="height: 30px;width: 30px" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/no_avatar-4a24e6328b8108841fcf2f92ebc22261.jpg" />
                                    <br>
                                    <input type="file" style="display:none" class="image_file" name="image_file">
                                    <i data-toggle="tooltip" data-placement="right" title="Attach a photo" class="fa fa-paperclip fa-lg image_browse_icon" aria-hidden="true" style="cursor:pointer"></i>
                                </span>
                                <textarea name="comment_text"  id="comment_text" placeholder="Write text here..."></textarea>
                                <button type="submit" id="btn_comment_relationships" disabled style="cursor: not-allowed">Submit</button>
                                <br>
                            </form>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    <?php
    if($this->session->flashdata('error_forgot_password')){ ?>
        <script>
            $(document).ready(function()
            {
                $.notify
                ({

                        message: '<?php echo $this->session->flashdata('error_forgot_password'); ?>'
                    }
                    ,{
                        type: 'danger',
                        offset:
                            {
                                x:10,
                                y:50
                            }
                    });
            });



        </script>
    <?php }
    ?>
    <?php
    if($this->session->flashdata('success_forgot_password')){
        ?>
        <script>
            $(document).ready(function()
            {
                $('#email_reset').val('<?php echo $this->session->flashdata('success_forgot_password'); ?>');
                $('#resetPasswordModal').modal('show');
            });



        </script>
    <?php }
    ?>
    <?php
    if($this->session->flashdata('error_email_confirm')){ ?>
        <script>
            $(document).ready(function()
            {
                $.notify
                ({

                        message: '<?php echo $this->session->flashdata('error_email_confirm'); ?>'
                    }
                    ,{
                        type: 'danger',
                        offset:
                            {
                                x:10,
                                y:50
                            }
                    });
            });



        </script>
    <?php }
    ?>
    <?php
    if($this->session->flashdata('invalid_page')){ ?>
        <script>
            $(document).ready(function()
            {
                $.notify
                ({

                        message: '<?php echo $this->session->flashdata('invalid_page'); ?>'
                    }
                    ,{
                        type: 'danger',
                        offset:
                            {
                                x:10,
                                y:50
                            }
                    });
            });



        </script>
    <?php }
    ?>
</body>

</html>
