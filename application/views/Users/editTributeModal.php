<div class="modal fade" id="editTributeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
 <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			
    <div class="profilearea_txt">
        <div class="profilearea_in_txt">
            <div class="col-md-12">
                <div class="row">
                    <div class="pophd">
                        <h3>Edit tribute for <?php echo $userdata['fullname']; ?></h3>
                    </div>
                </div>
                <form id="editFormTribute" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                    <input type="hidden" id="tribute_id" value="">
                    <div class="row canvarea2">
                        <div class="col-md-12">
                            <textarea id="editTributeDescription" name="editTributeDescription" class="form-control" placeholder="Tribute Description"></textarea>
                            <div class="row text-right">
                                <button class="bckbtn" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div></div></div>