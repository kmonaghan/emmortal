<div class="modal fade" id="addTributeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog txtcls">
        <div class="modal-content modal-outer">
            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <div class="modal-body text-center add-paddingnw">
                <div class="profilearea_txt">
                    <div class="profilearea_in_txt">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="pophd">
                                    <h3>Create new tribute</h3>
                                </div>
                            </div>
                            <form id="formAddTribute" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="row canvarea2">
                                    <div class="col-md-12">
                                        <div class="outer_select_tribute">
                                            <select id="add_tribute_friends_id" name="add_tribute_friends_id[]" class="select2 form-control" data-placeholder="Type in a persons name to send a tribute..." data-allow-clear="true" multiple>
                                            </select>
                                        </div>
                                        <textarea id="addtributeDescription" name="addtributeDescription" class="form-control" placeholder="Tribute Description"></textarea>
                                        <div class="row text-right">
                                            <button class="bckbtn" type="submit">Publish</button>
                                            <button type="button" class="bckbtn" id="backAddTribute">Back</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  Tribute js -->
<script src="<?php echo base_url(); ?>assets/js/tributes.js"></script>
