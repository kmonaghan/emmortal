<div class="modal fade" id="basicModalEmmUpdated2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			
                <div class="modal-body text-center">
                    <div class="modal-header">
                        <h3>What Would You Like to Add?</h3>
                    </div>
                    <div class="col-lg-6">
					      <div class="left-block">
	
							<span><i class="fa fa-file"></i><a href="#"><p id="addNewtext">New Text</p></a></span>
							<span><i class="fa fa-camera"></i><a href="#"><p id="addNewvideo">New Video</p></a></span>
							<span><i class="fa fa-square" aria-hidden="true"></i><a href="#"><p id="addNewSinglePhoto">New Photos</p></a></span>
                        </div>
                    </div>
                    <div class="col-lg-6 no-padding new-add-margin">
                        <div class="roght-block">
                            <div class="right-top-block">
                                <ul class="modal-list">
                                    <li><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" /><a href="#"><p id="addAlbum">Album</p></a></span></li>
                                    <li><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon2.png" /><a href="#"><p id="addPage">Page</p></a></span></li>
                                </ul>
                            </div>
                            <div class="right-bottom-block">
                                <ul class="modal-list">
                                    <li><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon1.png" /><a href="<?php echo base_url(); ?>Users/search/<?php echo $this->session->userdata('userid'); ?> "><p>Relationship</p></a></span></li>
                                    <li><span><i class="fa fa-heart" aria-hidden="true"></i><a href="#"><p id="addTribute">Tribute</p></a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
<script>
    var cloudfront_base_url=$('#cloudfront_base_url').val();
$(document).ready(function(){
	$('#addNewSinglePhoto').click(function()
    {
        $.ajax({
            url: "<?php echo base_url('ImageUpload/friendsList') ?>",
            type: 'post',
            success: function(data)
            { 
			 $('.friend_list_add_single').html(data);
            }
        });
		var pg_id=$('#user_page_id').val();
	    if(pg_id!='')
		{
			$('#current_pg_id').val(pg_id);
		}else
		{
			$('#current_pg_id').val('');
		}
        $('#addSingleImageUpload').modal('show');
		 //$('#headerCaption').text('Create new photo');
		$('#basicModalEmmUpdated2').modal('hide');
		  	
	});
    $('#addAlbum').click(function(){
		
		$.ajax({
			url: "<?php echo base_url('ImageUpload/select_background_images') ?>",
            type: 'post',
            success: function(data)
            { 
			  var base_url='<?php echo base_url();?>';
			  var obj2 = JSON.parse(data);
			   var total_background_images=obj2.total_background_images;
			   var option=new Array();
			   
			   for(i=0;i<total_background_images;i++)
			   {
				 option[i]='<div class="page-style-item" id="album_back_image_inner_div_'+obj2.id[i]+'" onclick="readme('+obj2.id[i]+')";><img alt="image" class="album_back_image" data-pid="'+obj2.id[i]+'" src="'+cloudfront_base_url+obj2.upload_path[i]+'/'+obj2.image_name[i]+'" /></div>';
			   }
			   $('.album_back_image_outr').html(option);
            }
		});	
	  $('#addalbumUpload').modal('show');
	  $('#basicModalEmmUpdated2').modal('hide');	
	});
    $('#addNewvideo').click(function(){
		$.ajax({
            url: "<?php echo base_url('ImageUpload/friendsList') ?>",
            type: 'post',
            success: function(data)
            { 
			 $('.friend_list_add_video').html(data);
            }
        });
		$('#addvideoUpload').modal('show');
	  $('#basicModalEmmUpdated2').modal('hide');
	});	
	

    $('#addNewtext').click(function(){
		$.ajax({
            url: "<?php echo base_url('ImageUpload/friendsList') ?>",
            type: 'post',
            success: function(data)
            { 
			 $('.friend_list_add_text').html(data);
            }
        });
	  $('#addtextcontent').modal('show');
	  $('#basicModalEmmUpdated2').modal('hide');
		
	});		
});
</script>
