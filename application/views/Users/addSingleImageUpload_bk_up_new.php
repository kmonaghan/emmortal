<div id="modaloutr">
    <div class="modal fade" class="addSingleImageUpload" id="addSingleImageUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        
		<div class="modal-dialog txtcls">
		<div class="prev_file_show"><a href="" id="previous_image_show"><span class="glyphicon glyphicon-triangle-left"></span></a></div>
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-body text-center">
			  <div class="profilearear">
              <div class="profilearea_innewr">
                  <div class="lds-ring" style="display: none"><div></div><div></div><div></div><div></div></div>
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3 id="headerCaption">Create new photo</h3>
					 <div id="countphoto"></div>
					 </div>
				  </div>
				  <div class="row canvarea">
				    <form id="upload_only_image" name="only_image" method="post" action="" enctype="multipart/form-data">
                    <div class="col-md-6">
					<div id="image_container2" class="canvas-placeholdernewr">
                        <div class="rotation editImageIconForAdd" id="editImagePreview" style="display: none;"><i class="fa fa-edit"></i></div>
						<img id="imagePreview2" name="upload_image" src="#" style="height:360px; width:459px">
					</div>
                        <button id="choose_photo" class="modal-submit-btn" type="button">Choose Photo</button>
					    <input id="upload_File2" name="multiple_image_upload[]" multiple="multiple" class="modal-submit-btn" type="file" style="display: none"/>
                        <input type="hidden" id="user_id" name="user_id" value="<?php echo $this->session->userdata('userid');?>"/>
                        <button id="Remove_uploadFile2" class="remvphoto" type="button">Remove Photo</button>
                        <div id="image_error" class="error_image"></div>
					</div>
                    </form>
                    <form id="upload_file_image_content" name="image_upload" method="post" action="" enctype="multipart/form-data">
					<div class="col-md-6">
						<input type="hidden" id="total_upload_image" name="total_upload_image" value="">
                        <input type="hidden" id="present_image_id" name="present_image_id" value=""> 
                        <input type="hidden" id="single_image_upload_album_id" name="present_album_id" value=""> 
                        <input type="hidden" id="next_image_id" name="next_image_id" value="">
                        <input type="hidden" id="previous_image_id" name="previous_image_id" value="">
                        <input type="hidden" id="xImage" name="xImage" />
                        <input type="hidden" id="yImage" name="yImage" />
                        <input type="hidden" id="widthImage" name="widthImage" />
                        <input type="hidden" id="heightImage" name="heightImage" />
                        <input type="hidden" id="rotateImage" name="rotateImage" value="0" />
						<input name="Title" id="title" type="text" class="form-control" placeholder="Title">
						<textarea id="txtEditor"  name="desc" placeholder="Description"></textarea>
                        <h6>Type Your Friend's Name</h6>
                        <div class="outer_select">
                            <select name="friend_name[]" class="select2 friend_list_add_single" style="width:464px;" multiple="multiple"></select>
                        </div>
                        <!--<div class="emm-page-style_txt album_back_image_outr">
                        </div>
						<input type="hidden" class="background_image_id" name="background_image_id" value=""/>-->
                    </div>
					
					<!--<div class="col-md-6">
					Background Image
					</div>-->	
					<input type="hidden" id="current_pg_id" name="current_pg_id" value="">
					
						<!--<textarea id="txtEditor" required="required"></textarea>-->
					<select name="album_name" id="album_name" class="txtflsec">
					<?php 
					 if($total_record_album>0)
					 {
						 for($i=0;$i<$total_record_album;$i++)
						 {
							 if(!empty($present_album_id))
							 {
					?>
						<option value="<?php echo $albumeid[$i]?>" <?php if($present_album_id==$albumeid[$i]){?>selected=selected<?php }?>><?php echo $album_title[$i];?></option>
					<?php
							 }
							 else
							 {
					?>
								<option value="<?php echo $albumeid[$i]?>"><?php echo $album_title[$i];?></option>
					<?php 
							 }
						 }
					 }
					 else
					 {
						?>
						<option value="-1">My Chronicles</option>	 
						<?php
					}
					?>
					</select>
                      
					<div class="row text-right">
                        <button type="submit" name="btn_publish" id="add_single_img_btn_publish" class="bckbtn">Publish</button>

					  <a href="#" id="btn_back_image" class="bckbtn">Back</a>
					</div>
					</div>
				  </div>
				</div>
              </div>
            </div>
		</form>
                </div>
              <div class="next_file_show"><a href="" id="next_image_show"><span class="glyphicon glyphicon-triangle-right"></span></a></div>
            </div>
			
        </div></div>
		
<!--<div id='loadingmessage' style='display:none'>
  <img src='<?php /*echo base_url();*/?>assets/img/animated-loading-gif-2.gif'/>
</div>-->



<script>
var names = [];
var this_elem;
slider_detais=new Array();
//show image in choose Image onchange
$(document).on('change', '#upload_File2',function() { 
	 this_elem=$(this)[0];
	var files =this_elem.files;
	if(files.length >= 14){
        $.alert({
			title: 'Alert!',
			content: 'You can choose  upto 14 images!',
		});
    }else{
		var form_data = new FormData();
		var base_url='<?php echo base_url();?>';
		var user_id=$('#user_id').val();
		var albumId = $('#album_name').val();
		upload_file_url=new Array();
		form_data.append("total_records",files.length);
		for(var i=0; i<files.length; i++)
        {
		   var ext = files[i].name.split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                $.alert({
                    title: 'Alert!',
                    content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
                });
			}else{
				form_data.append("multiple_image_upload_"+i,files[i]);
                form_data.append("user_id", user_id);
				form_data.append("album_id", albumId);
                $(".lds-ring").show();
			}
		}
		$.ajax({
					url: '<?php echo base_url()?>ImageUpload/multipleImageUpload',
					type: 'POST',
					data: form_data,
					cache: false,
					contentType: false,
					processData: false,
                    complete: function() {
                        $(".lds-ring").hide();
                    },
					success: function (data){
						var obj2= JSON.parse(data);
						//console.log(obj2);
						if(obj2.total_record>1)
						{
							for(i=0;i<obj2.total_record;i++)
							{
							  upload_file_url[i]=base_url+obj2.image_upload_path[i]+obj2.image_name[i];
							  slider_detais['image_'+i]=obj2.image_id[i];
							  slider_detais['image_url_'+i]=upload_file_url[i];

							  $('#imagePreview2').attr("src",upload_file_url[i]).css('display','block');

                                $('.editImageIconForAdd').css('display','block');

							  $('#present_image_id').val(obj2.image_id[i]);
							  $('#total_upload_image').val(obj2.total_record);
							  if((i+1)==obj2.total_record)
							  {
								  i=0;
								$('#next_image_id').val(obj2.image_id[i]);
								$('#next_image_show').attr('data-pid',obj2.image_id[i]);
							  }
							  else
							  {
								$('#next_image_id').val(obj2.image_id[i+1]);
								$('#next_image_show').attr('data-pid',obj2.image_id[i+1]);
							  }

							  if(i==0)
							  {
								  i=(obj2.total_record-1);
								  $('#previous_image_id').val(obj2.image_id[i]);
								  $('#previous_image_show').attr('data-pid',obj2.image_id[i]);
							  }
							  else
							  {
								$('#previous_image_id').val(obj2.image_id[i-1]);
								$('#previous_image_show').attr('data-pid',obj2.image_id[i-1]);
							  }
							  $('#choose_photo').hide();
							  $('#Remove_uploadFile2').attr('data-pid',obj2.image_id[i]).show();
							  $('#single_image_upload_album_id').val(obj2.album_id);
							  $('.prev_file_show').css('display','block');
							  $('.next_file_show').css('display','block');
							}
						}
						else
						{
						  upload_file_url=base_url+obj2.image_upload_path+obj2.image_name;
						  $('#imagePreview2').attr("src",upload_file_url).css('display','block');
                            $('.editImageIconForAdd').css('display','block');

						  $('#present_image_id').val(obj2.image_id);
						  $('#single_image_upload_album_id').val(obj2.album_id);
						  $('#choose_photo').hide();
						  $('#Remove_uploadFile2').attr('data-pid',obj2.image_id).show();
						}
					}

                });//end of ajax loop
				
	}
});


$('#previous_image_show').click(function(e){
	e.preventDefault();
	var imgid=$(this).attr('data-pid');
	var base_url='<?php echo base_url();?>';
	var previous_image_show_url='<?php echo base_url()?>ImageUpload/getSingleimage';
	$.post(previous_image_show_url,{'imgId':imgid},function(res){
		var obj= JSON.parse(res);
		//console.log(obj);
		upload_file_url2=base_url+obj.image_upload_path+obj.image_name;
		  /*$('#next_image_id').val(obj.next_image_id);
		  $('#previous_image_id').val(obj.prev_image_id);*/
		  $('#present_image_id').val(obj.image_id);
		  $('#imagePreview2').attr('src',upload_file_url2);
		  $('#previous_image_show').attr('data-pid',obj.prev_image_id);
		  $('#next_image_show').attr('data-pid',obj.next_image_id);
		  $('#Remove_uploadFile2').attr('data-pid',obj.image_id).show();
	});
});

$('#next_image_show').click(function(e){
	e.preventDefault();
	var image_id=$(this).attr('data-pid');
	var base_url='<?php echo base_url();?>';
	var next_image_show_url='<?php echo base_url()?>ImageUpload/getSingleimage';
	$.post(next_image_show_url,{'imgId':image_id},function(res){
		var obj= JSON.parse(res);
		//console.log(obj);
		upload_file_url3=base_url+obj.image_upload_path+obj.image_name;	
		  /*$('#next_image_id').val(obj.next_image_id);
		  $('#previous_image_id').val(obj.prev_image_id);*/
		  $('#present_image_id').val(obj.image_id);
		  $('#imagePreview2').attr('src',upload_file_url3);
		  $('#previous_image_show').attr('data-pid',obj.prev_image_id);
		  $('#next_image_show').attr('data-pid',obj.next_image_id);
		  $('#Remove_uploadFile2').attr('data-pid',obj.image_id).show();
	});
	
});


$('document').ready(function(){
	/*$('#show_friend_list').hide();*/
	$('.select2').select2();
	$('#imagePreview2').hide();
	$('#Remove_uploadFile2').hide();
	$('.prev_file_show').hide();
	$('.next_file_show').hide();
	
	$('#create_album').click(function(){
		$('#addalbumUpload').modal('show');
		$('#basicModalEmmUpdated2').modal('hide');	
	});

	$('#btn_back_image').click(function(){
		$('#addSingleImageUpload').modal('hide');
		$('#basicModalEmmUpdated2').modal('show');
	});
	$('#Remove_uploadFile2').click(function(e){
	    var image_id=$(this).attr('data-pid');
		//console.log(image_id);
		var base_url='<?php echo base_url();?>';
		var user_id=$('#user_id').val();
        $.ajax({
            type: 'POST',
            url: base_url+'ImageUpload/deleteImageBeforefullInsert',
            data: {'imgId':image_id,'user_id':user_id},
            beforeSend: function(){
                $(".lds-ring").show();
            },
            complete: function() {
                $(".lds-ring").hide();
            },
            success: function (res)
            {

                var obj2= JSON.parse(res);
               // console.log(obj2);
                if(obj2.total_record===0)
                {
                    $('#addSingleImageUpload').hide();
                    location.reload();
                }
                if(obj2.total_record>1)
                {
                    for(i=0;i<obj2.total_record;i++)
                    {
                        upload_file_url[i]=base_url+obj2.image_upload_path[i]+obj2.image_name[i];
                        slider_detais['image_'+i]=obj2.image_id[i];
                        slider_detais['image_url_'+i]=upload_file_url[i];

                        $('#imagePreview2').attr("src",upload_file_url[i]).css('display','block');
                       
                        $('#present_image_id').val(obj2.image_id[i]);
                        $('#total_upload_image').val(obj2.total_record);
                        if((i+1)==obj2.total_record)
                        {
                            i=0;
                            $('#next_image_id').val(obj2.image_id[i]);
                            $('#next_image_show').attr('data-pid',obj2.image_id[i]);
                        }
                        else
                        {
                            $('#next_image_id').val(obj2.image_id[i+1]);
                            $('#next_image_show').attr('data-pid',obj2.image_id[i+1]);
                        }

                        if(i==0)
                        {
                            i=(obj2.total_record-1);
                            $('#previous_image_id').val(obj2.image_id[i]);
                            $('#previous_image_show').attr('data-pid',obj2.image_id[i]);
                        }
                        else
                        {
                            $('#previous_image_id').val(obj2.image_id[i-1]);
                            $('#previous_image_show').attr('data-pid',obj2.image_id[i-1]);
                        }
                        $('#choose_photo').hide();
                        $('#Remove_uploadFile2').attr('data-pid',obj2.image_id[i]).show();
                        $('.prev_file_show').css('display','block');
                        $('.next_file_show').css('display','block');
                    }
                }
                else
                {
                    upload_file_url=base_url+obj2.image_upload_path+obj2.image_name;
                    $('#imagePreview2').attr("src",upload_file_url).css('display','block');
                    $('#choose_photo').hide();
                    $('#Remove_uploadFile2').attr('data-pid',obj2.image_id).show();
                    $('.prev_file_show').css('display','none');
                    $('.next_file_show').css('display','none');
                }
            }
        });

		/*
		$('#imagePreview2').attr('src', '');
		$('#imagePreview2').hide();
		$('#Remove_uploadFile2').hide();
        $('#choose_photo').show();
        $("#upload_File2").val('');*/
	});
    // Choose photo click
    $(document).on('click', '#choose_photo', function()
    {
        $("#upload_File2").click();
    });
	//for publishing the content
	$('#add_single_img_btn_publish').click(function(e){ 
    e.preventDefault();
    //alert('Hi');
    var form_data = new FormData();
    var base_url='<?php echo base_url();?>';
    var form_data_update_url='<?php echo base_url()?>ImageUpload/imageUpload/';
    var user_id=$('#user_id').val(); 
    $.post(form_data_update_url, $('form#upload_file_image_content').serialize(), function(res) {
       var obj2= JSON.parse(res);
        //console.log(obj2);
        if(obj2.total_record===0)
            {
                $('#addSingleImageUpload').hide();
                location.reload();
               /* var error_free=true;
                if($('#upload_File2').val()=='')
                {
                    $.alert({
                        title: 'Alert!',
                        content: 'Image section cannot be blank!',
                    });
                    error_free=false;
                }
                return error_free;*/
            }
            if(obj2.total_record>1)
            {
                for(i=0;i<obj2.total_record;i++)
                {
                  upload_file_url[i]=base_url+obj2.image_upload_path[i]+obj2.image_name[i];    
                  slider_detais['image_'+i]=obj2.image_id[i];
                  slider_detais['image_url_'+i]=upload_file_url[i];
                  
                  $('#imagePreview2').attr("src",upload_file_url[i]).css('display','block');
                   
                  $('#present_image_id').val(obj2.image_id[i]);
                  $('#total_upload_image').val(obj2.total_record);
                  if((i+1)==obj2.total_record)
                  {
                      i=0;
                    $('#next_image_id').val(obj2.image_id[i]);
                    $('#next_image_show').attr('data-pid',obj2.image_id[i]);    
                  }
                  else
                  {
                    $('#next_image_id').val(obj2.image_id[i+1]);
                    $('#next_image_show').attr('data-pid',obj2.image_id[i+1]);        
                  }
                  
                  if(i==0)
                  {
                      i=(obj2.total_record-1);
                      $('#previous_image_id').val(obj2.image_id[i]);
                      $('#previous_image_show').attr('data-pid',obj2.image_id[i]);
                  }
                  else
                  {
                    $('#previous_image_id').val(obj2.image_id[i-1]);
                    $('#previous_image_show').attr('data-pid',obj2.image_id[i-1]);    
                  }
                  $('#choose_photo').hide();
                  $('#Remove_uploadFile2').attr('data-pid',obj2.image_id[i]).show();
                  $('.prev_file_show').css('display','block');
                  $('.next_file_show').css('display','block');
				  $('.select2').val();
				  $('#txtEditor').val('');
				  $('#title').val('');
                }
            }
            else
            {
              upload_file_url=base_url+obj2.image_upload_path+obj2.image_name;        
              $('#imagePreview2').attr("src",upload_file_url).css('display','block');
              $('#present_image_id').val(obj2.image_id);
              $('#choose_photo').hide();
              $('#Remove_uploadFile2').attr('data-pid',obj2.image_id).show();
              $('.prev_file_show').css('display','none');
              $('.next_file_show').css('display','none');
			  $('.select2').val('');
		      $('#txtEditor').val('');
			  $('#title').val('');	
            }
    });
});


});


</script>
