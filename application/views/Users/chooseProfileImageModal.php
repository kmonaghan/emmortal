<script src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/js/chooseProfileBackground.js"></script>
<div class="modal fade" id="chooseProfileImageModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-box">
        <div class="modal-content modal-outer">
            <div class="modal-header modal-headernew">
                <button type="button" class="close close-new" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Upload Profile & Background Image</h3>
            </div>
            <div class="modal-body modal-main-body">
                <form id="chooseProfileBackgroundImage" method="post" enctype="multipart/form-data" >
                <div class="row">
                    <div class="col-md-6">
                        <?php if($userdata['profileimage']){?>
                            <div class="canvas-placeholder_choose" style="height: 134px;">
                                <i class="fa fa-picture-o" id="blank_avatar1" style="display:none"></i>
                                <img alt="image" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['profileimage']?>" id="img_choose_avatar1" style="height: 134px; width: 100%;">
                            </div>
                            <span class="btn btn-danger choose_avatar" id="remove_avatar1"><i class="glyphicon glyphicon-remove-circle"></i>  Remove avatar</span>
                            <input id="choose_avatar_file1" type="file" name="choose_avatar" class="avatar">
                            <div class="error" id="avatar_image_error1" style="display:none">Invalid Image</div>
                            <?php
                        }
                        else
                        {?>
                            <div class="canvas-placeholder_choose" style="height: 134px;">
                                <i class="fa fa-picture-o" id="blank_avatar1"></i>
                                <img alt="image" src="" id="img_choose_avatar1" style="height: 134px; width: 100%; display: none">
                            </div>
                            <span class="btn btn-success choose_avatar" id="choose_avatar1"><i class="glyphicon glyphicon-upload"></i>  Choose avatar</span>
                            <input id="choose_avatar_file1" type="file" name="choose_avatar" class="avatar">
                            <div class="error" id="avatar_image_error1" style="display:none">Invalid Image</div>
                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <?php if($userdata['backgroundimage']){?>
                            <div class="canvas-placeholder_choose" style="height: 134px;">
                                <i class="fa fa-picture-o" id="blank_background1" style="display:none"></i>
                                <img alt="image" src="<?php echo $this->config->item("cloudfront_base_url").$userdata['backgroundimage']?>" id="img_choose_background1" style="height: 134px; width: 100%">
                            </div>
                            <span class="btn btn-danger choose_background" id="remove_background1"><i class="glyphicon glyphicon-remove-circle"></i>  Remove background</span>
                            <input id="choose_background_file1" type="file" name="choose_background" class="avatar">
                            <div class="error" id="background_image_error1" style="display:none">Invalid Image</div>
                            <?php
                        }
                        else
                        {?>
                            <div class="canvas-placeholder_choose" style="height: 134px;">
                                <i class="fa fa-picture-o" id="blank_background1"></i>
                                <img alt="image" src="" id="img_choose_background1" style="height: 134px; width: 100%; display: none">
                            </div>
                            <span class="btn btn-success choose_background" id="choose_background1"><i class="glyphicon glyphicon-upload"></i>  Choose background</span>
                            <input id="choose_background_file1" type="file" name="choose_background" class="avatar">
                            <div class="error" id="background_image_error1" style="display:none">Invalid Image</div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <ul class="button-part pull-right">
                        <li><button type="button" class="btn skipr" data-dismiss="modal">Skip</button></li>
                        <li><button type="submit" class="btn save" >Save</button></li>
                    </ul>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>