<div id="modaloutr">
    <div class="modal fade" id="editSingleImageUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<div class="modal-body text-center add-paddingnw">
                    <form name="image_upload" method="post" action="<?php echo base_url()?>ImageUpload/editImage" enctype="multipart/form-data">
					 <div class="profilearear">
					  <div class="profilearea_innewr">
					   <div class="col-md-12">
						  <div class="row">
							<div class="pophd">
							 <h3 id="edit_headerCaption">Edit Photo</h3>
							</div>
						  </div>
						  <div class="row canvarea">
							<div class="col-md-6">
							<div id="image_container2" class="canvas-placeholdernewr">
                                <div class="rotation editImageIconForEdit" id="editImagePreview"><i class="fa fa-edit"></i></div>
								<img alt="image" id="edit_imagePreview2" name="upload_image" src="#">
                                <input type="hidden" class="url" name="url" value="">
							</div>
								<button id="edit_choosephoto2" class="modal-submit-btn" type="button">Choose Photo</button>
							 <input id="edit_uploadFile2" name="editimageUpload" class="modal-submit-btn" type="file" style="display: none"/>
								<button id="edit_Remove_uploadFile2" class="remvphoto" type="button">Remove Photo</button>
								<div class="error"></div>
							</div>
							<div class="col-md-6">
								<input name="Title" id="edit_title" type="text" class="form-control" placeholder="Title">
								<input type="hidden" id="edit_inner_image_id" name="image_id" value="">
								<input type="hidden" id="single_image_album_id" name="single_image_album_id">
								<textarea id="txtEditor" class="edit_txtEditor" name="desc" placeholder="Description"></textarea>
								<h6>Type Your Friend's Name</h6>
								<div class="outer_select">
								   <select name="tag_friend_name[]" class="select2 friend_list_edit_single" style="width:500px;" multiple="multiple">
								  </select>
								</div>
							</div>
							<select name="album_name" class="txtfl album_list_id">
		
							</select>
		
							<div class="row text-right">
								<button type="submit" name="btn_publish" class="bckbtn">Update</button>
							</div>
							</div>
						  </div>
						</div>
					  </div> 
			  		</form>
            	</div>
            </div>
        </div>
    </div>
</div>

<script>
    var currentImage;
    var featherEditor = new Aviary.Feather({
        apiKey: '92a0d4b40edd49318153eb3f38c788de',
        theme: 'dark', // Check out our new 'light' and 'dark' themes!
        tools: 'effects,frames,crop,orientation',
        onSave: function(imageID, newURL) {
           
            $('.url').val(newURL);
            currentImage.src = newURL;
            featherEditor.close();
        },
        onError: function(errorObj) {
            alert(errorObj.message);
        }
    });
    function convertImgToBase64URL(url, callback, outputFormat){
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
            var canvas = document.createElement('CANVAS'),
            ctx = canvas.getContext('2d'), dataURL;
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            callback(dataURL);
            canvas = null; 
        };
        img.src = url;
    }
    function launchEditor() {
        convertImgToBase64URL($('#edit_imagePreview2').attr('src'), function(base64Img){
            console.log(base64Img);
            $('#currentImage').attr('src', base64Img);  
        });
        currentImage = $('#edit_imagePreview2')[0];
        featherEditor.launch({
            image: currentImage.id
        });
        return false;
    }
    $(document).on('click', '#editImagePreview', function() {
        launchEditor();
    });
    $(function() {

        $(document).on('change', '#edit_uploadFile2',function() {
            readURLImage(this);
        });
        function readURLImage(input)
        {

            if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    launchEditor('edit_imagePreview2', this.result)
                    $('#edit_imagePreview2').attr('src', this.result);
                    $('.editImageIconForEdit').css('display','block');
                    $('#edit_imagePreview2').show();
                    $('#edit_Remove_uploadFile2').show();
                    $('#edit_choosephoto2').hide();

                }

                reader.readAsDataURL(input.files[0]);
            }
            else
            {
                $('#edit_uploadFile2').val('');
                $.alert({
                    title: 'Alert!',
                    content: 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!',
                });
            }
        }
    });
    $('document').ready(function(){

        $('#display_friend_list').show();
        $('#create_album').click(function(){
            $('#addalbumUpload').modal('show');
            $('#basicModalEmmUpdated2').modal('hide');
        });

        $('#edit_btn_back').click(function(){
            $('#editSingleImageUpload').modal('hide');
        });
        $('#edit_Remove_uploadFile2').click(function(){
            $('.editImageIconForEdit').css('display','none');
            $('#edit_choosephoto2').css('display','block');
            $('#edit_imagePreview2').attr('src', '');
            $('#edit_imagePreview2').hide();
            $('#edit_Remove_uploadFile2').hide();
            $("#edit_uploadFile2").val('');
        });
        // Choose photo click
        $(document).on('click', '#edit_choosephoto2', function()
        {
            $("#edit_uploadFile2").click();

        });


    });


</script>
