<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var image_name="<?php echo $fetchForegroundStyle['image_name']; ?>";
        var upload_path="<?php echo $fetchForegroundStyle['upload_path']; ?>";
        if(image_name&&upload_path)
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
        }
        else
        {
            $(".foreground_dynamic").css("background-image", "url("+cloudfront_base_url+"assets/img/content-bg.jpg"+")");
        }

    });
</script>
<script type="text/javascript">
    $(document).ready(function()
    {
        var cloudfront_base_url=$('#cloudfront_base_url').val();
        var album_background_image=$('#background_img_src').val();
        if(album_background_image)
        {
            $('body').css('background-image', 'url(' + album_background_image + ')');
        }
        else
        {
            var default_background_image="<?php echo $userdata['backgroundimage']; ?>";
            if(default_background_image)
            {
                $('body').css("background-image", "url("+cloudfront_base_url+escape(default_background_image)+")");
            }
            else
            {
                var image_name="<?php echo $fetchBackgroundStyle['image_name']; ?>";
                var upload_path="<?php echo $fetchBackgroundStyle['upload_path']; ?>";
                if(image_name && upload_path)
                {
                    $('body').css("background-image", "url("+cloudfront_base_url+upload_path+"/"+escape(image_name)+")");
                }
                else
                {
                    $('body').css("background-image", "url("+cloudfront_base_url+"assets/img/bg2.jpg"+")");
                }
            }

        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/search.js"></script>
<body>
    <div id="main-wrapper">
        <?php $this->load->view('Users/innerHeader') ?>
        <div class="container">
            <div class="profilearea">
                <div class="profilearea_in_set foreground_dynamic">
                    <div class="col-md-12">
                        <h4><?php echo $userdata['firstname'].'\'s';?> Connections</h4>
                        <input type="text" name="search" placeholder="Type here to search...." id="searchUser">
                        <input type="hidden" id="get_user_id" value="<?php echo $userdata['userid']; ?>">
                        <div class="col-md-12 conctn" id="allRelationshipUser">

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade relationshipsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-8 no-padding">
                    <a href="" class="full_image_view example-image-link" data-lightbox="example-set">
					<img alt="image" class="example-image left-image content_image_modal" src="" style="display: none"/>
					<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen src="" class="left-image content_video_modal" frameborder="0" style="     	                    display: none"></iframe>
					</a>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer add-margintop">
                            <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade relationshipsModal1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-outer">

                <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="modrel">
                    <?php if($this->session->userdata('userid'))
                    { ?>
                        <li>
                            <a class="pinModal" href="#" title="Pin!" data-toggle="tooltip" onClick="pinModalEditDelete(id)">
                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a class="editContentId" href="#" title="Edit!" data-toggle="tooltip" onClick="editContentPost(id)">
                                <i class="glyphicon glyphicon-pencil EditText"></i>
                            </a>
                        </li>
                        <li>
                            <a class="deleteContentId" href="#" title="Delete!" data-toggle="tooltip" onClick="deleteContentPost(id)">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        <?php
                    } ?>
                </div>
                <div class="modal-body">

                    <div class="col-lg-12 no-padding">
                            <p class="modal-text content_title_modal"></p>
                            <p class="left-image content_text_modal" style="display: none"></p>
                    </div>
                    <div class="col-lg-12 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text content_description_modal"></p>

                        </div>

                        <div class="modal-footer text-footer txttopmrgn">
                            <h3>
                                    <span class="square-icon">
                                    <img alt="image" src="" class="tagger_image_modal img-circle" style="height: 30px;width: 30px"/>
                                    </span>
                                <a href="" class="tagger_profile_link">
                                    <span class="tagger_name_modal"></span>
                                </a>
                            </h3>
                            <input type="hidden" class="tagger_user_id_modal">
                            <h5><a href="" class="tagger_album_link"><i class="album_name_modal"></i></a></h5>
                            <ul class="footer-icon">
                                <li>
                                    <a href="#" title="See Tagged!" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-circle"></i></span><span class="count_tag">0</span><b class="caret"></b></a>
                                    <ul class="dropdown-menu see_tagged_name">

                                    </ul>
                                </li>
                                <li><a href="" class="tagger_album_link" title="See Album!" data-toggle="tooltip"><span><img alt="image" src="<?php echo $this->config->item("cloudfront_base_url"); ?>assets/img/icon3.png" style="width: 100%" /></span><span class="count_album_content">0</span></a></li>
                                <li><a href="#" class="modal_comment_id user_like_dislike_title" id="" data-toggle="tooltip" title="Like!" onClick="like(id)"><span><i class="fa fa-heart user_like_dislike" aria-hidden="true"></i></span><span class="count_like"></span></a></li>
                                <li><a href="#" class="modal_comment_id" id="" data-toggle="tooltip" title="Tribute!" onClick="openComment(id)"><span><i class="fa fa-comment" aria-hidden="true"></i></span><span class="count_comment"></span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('Users/showComment'); ?>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-new" data-dismiss="modal">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Pin to which page?</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="pinbtnarealft">
                                <p>Pin to page:
                                    <select class="selectPageId" name="selectPageId">

                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="pinbtnarea">
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn updateContentPage" onClick="updateContentPage(id)">Pin</button>
                                </div>
                                <div class="pinbtn">
                                    <button type="button" class="modal-submit-btn deleteContentPage" onClick="deleteContentPage(id)">Unpin</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <?php $this->load->view('Users/editImageModal'); ?>
    <?php $this->load->view('Users/editTextModal'); ?>
    <?php $this->load->view('Users/EditvideoUpload'); ?>
</div>
</body>


