<!-- line modal -->
    <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-box">
            <div class="modal-content modal-outer">
                <div class="modal-header modal-headernew">
                    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Create a Profile and Become Emmortal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form id="signup" method="post">
                        <div class="form-group input-box">
                            <input type="text" class="form-control" id="firstname" placeholder="First Name" name="firstname" autocomplete="off">
                        </div>
                        <div class="form-group input-box">
                            <input type="text" class="form-control" id="lastname" placeholder="Last Name" name="lastname" autocomplete="off">
                        </div>
                        <div class="form-group  full-width">
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" autocomplete="off">
                        </div>
                       <div class="alert alert-danger alert-dismissable" id="email_alert" style="display:none">
  						  <a href="#" id="email_alert_close" class="close">×</a>
    						<strong>Sorry</strong> Email Id already Exist
  						</div>
                        <div class="form-group full-width">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" autocomplete="off">
                        </div>
                        <div class="form-group full-width">
                            <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" autocomplete="off">
                        </div>
                        <div class="form-group full-width">
                            <div class="input-group date datepicker">
                                <input type="text" class="form-control" id="dob" placeholder="YYYY-MM-DD" name="dob" value="<?php echo date('Y-m-d'); ?>">
                                <span class="input-group-addon">
                                     <span class="fa fa-birthday-cake"></span>
                                </span>
                            </div>
                        </div>
                   		<div class="modal-footersec">
                        	<p>By clicking Sign Up, you agree to our <a href="#" class="link-color" id="terms-conditions-link" >Terms and conditions</a></p>
                        	<button type="submit" class="btn modal-button btn-success" id="btn_signup">Sign Up <i class="fa fa-spinner icon_signup_spinner" style="display: none"></i> </button>
                             <div class="alert alert-success alert-dismissable" id="signup_success_alert" style="display:none">
                             	 <a href="#" id="signup_success_alert_close" class="close">×</a>
                               	 Registered Successfully...Please Verify Your Email 
  							</div>
                        	<p>Already have an account? Please, <a href="#" class="link-color" id="sign-in-link">Sign In</a></p>
                    	</div>
                     </form>
                </div>
            </div>
        </div>
    </div>