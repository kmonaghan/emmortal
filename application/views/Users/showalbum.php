<!-- Css Link -->
	<?php $this->load->view('Users/css_link') ?>
	<!-- js Script -->
<?php $this->load->view('Users/js_script') ?>
<?php $this->load->view('Users/innerHeader') ?>
 <?php $this->load->view('Users/file_upload');?>
        <div class="container">
            <div class="content-bg">
                <div class="top-area">
                    <div class="col-lg-2">
                        <span><img src="<?php echo base_url()?>assets/img/h11.jpg" /></span>
                    </div>
                    <div class="col-lg-8">
                        <i><?php echo $emm_album_title;?></i>
                        <p><?php echo $emm_album_desc;?></p>
                    </div>
                </div>
                <div class="body-area">
                    <div class="albumbtnarea">
                      <div class="albumbtnarea_in">
					  <form id="upload_album_image" name="upload_album_image" method="post" action="<?php echo base_url();?>imageUpload/albumimageUpload/<?php echo $album_id; ?>" enctype="multipart/form-data" />
                        <div class="center-align">
						    <input class="imgupld" type="file"  name="userFiles[]" placeholder="Image Upload"/>
						<input class="modal-submit-btn" type="submit" value="Submit"/>
						</div>
						</form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!---------------------for image----------------------->

    <div class="modal fade" id="basicModalimg" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="modal-body">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>-->
                    <div class="col-lg-8 no-padding">
                        <a href="#" class="" data-toggle="modal" data-target="#basicModalbigimg"><img class="left-image" src="img/modalimg1.jpg" /></a>
                    </div>
                    <div class="col-lg-4 no-padding padding-left">
                        <div class="modal-body no-padding">
                            <p class="modal-text" id="myModalLabel">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            <p class="modal-text" id="myModalLabel">Sed do eiusmod tempor incididunt ut.</p>
                            <p class="modal-text" id="myModalLabel">Sed do eiusmod tempor incididunt ut.</p>
                        </div>
                        <div class="modal-footer text-footer add-margintop">
                            <h3><span class="square-icon"><i class="fa fa-square" aria-hidden="true"></i></span>Alex Block</h3>
                            <i>2017 Summer Adventures</i>
                            <ul class="footer-icon">
                                <li><a href="#"><span><i class="fa fa-circle" aria-hidden="true"></i></span>1</a></li>
                                <li><a href="#"><span class=" add-img"><img src="images/ffg.png"/></span>42</a></li>
                                <li><a href="#" onclick="openNav()"><span><i class="fa fa-comment" aria-hidden="true"></i></span>20</a></li>
                                <li><a href="#"><span><i class="fa fa-heart" aria-hidden="true"></i></span>328</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---------------------for image----------------------->


    <!---------------------------------side popup------------------------------>

    <div class="modal fade" id="basicModalbigimg" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="popup-fullimg">
                    <img src="images/img1.jpg" />
                </div>
            </div>
        </div>
    </div>

    <div id="mySidenav" class="sidenav">
        <a href="#" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="use-padding use-border">
            <div class="left-box">
                <p><span class="square-icon"><i class="fa fa-square" aria-hidden="true"></i></span> <b>Kevin Monaghan</b><span class="right-icons">
               <!--<a href="#"><span><img src="images/icon.png" /></span></a>
                    <a href="#"><span><i class="fa fa-heart" aria-hidden="true"></i></span>730</a></span>-->

                </p>

                <p>It was worth all the effort!!! You look beautiful!</p>
            </div>
            <div class="right-box">
                <img src="img/icon1.png">
                <i class="fa fa-heart right-box-i" aria-hidden="true"></i> 730
            </div>
        </div>
        <div class="use-padding  use-border">
            <div class="left-box">
                <p><span class="square-icon"><i class="fa fa-square" aria-hidden="true"></i></span> <b>Patrick Monaghan</b></p>
                <p>You should see how Kevin and I made a whale small. Way harder</p>
            </div>
            <div class="right-box">
                <img src="img/icon1.png">
                <i class="fa fa-heart" aria-hidden="true"></i> 550
            </div>
        </div>
        <div class="use-padding">
            <div class="left-box">
                <p><span class="square-icon"><i class="fa fa-square" aria-hidden="true"></i></span> <b>Noelle Burchant
</b></p>
                <p>I’m so jealous I love Giraffees!!!</p>
            </div>
            <div class="right-box">
                <img src="img/icon1.png">
                <i class="fa fa-heart right-box-i" aria-hidden="true"></i> 550
            </div>
            <img class="men-in-black" src="img/sidepopupimg.jpg" />
        </div>
        <div class="use-padding">
            <form action="/action_page.php" method="get">
                <input type="checkbox" name="vehicle" value="">
                <textarea rows="2" cols="24">Write a Comment
                </textarea>
                <input type="submit" value="Submit">
                <br>
            </form>

        </div>
    </div>

    <!---------------------------------------------------------------------------->



    <!---------------------for image type2----------------------->

    <div class="modal fade" id="basicModalimg2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="modal-body">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>-->
                    <a href="#" class="" data-toggle="modal" data-target="#basicModalbigimg2"><img class="left-image" src="img/Elisha-Cuthbert-Beautiful-Lady.jpg" /></a>

                    <div class="modal-footer text-footer">

                        <h3><span class="side-img"><img src="images/Elisha-Cuthbert-Beautiful-Lady.jpg" /></span> Alyssa Hause</h3>
                        <i>2017 Summer Adventures</i>
                        <ul class="footer-icon">
                            <li><a href="#"><span><i class="fa fa-circle" aria-hidden="true"></i></span>1</a>
                            </li>
                            <li><a href="#"><span class=" add-img"><img src="images/ffg.png"/></span>42</a></li>
                            <li><a href="#"><span><i class="fa fa-comment" aria-hidden="true"></i></span>20</a></li>
                            <li><a href="#"><span><i class="fa fa-heart" aria-hidden="true"></i></span>328</a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="basicModalbigimg2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="popup-fullimg">
                    <img src="images/Elisha-Cuthbert-Beautiful-Lady.jpg" />
                </div>
            </div>
        </div>
    </div>

    <!---------------------for image type2----------------------->

    <!---------------------------------for text-------------------------->
    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="modal-body">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>-->
                    <p class="modal-text" id="myModalLabel">Our day out</p>
                    <p class="modal-text" id="myModalLabel">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                        <br>
                        <br>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibhate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                        <br>
                        <br> Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod </p>
                    <!--   <p class="modal-text" id="myModalLabel">Ruis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute ullamco laboris nisi utirure dolor in. </p>
                    <p class="modal-text" id="myModalLabel">Lorem ipsum dolor sit amet, consectetur. </p>-->
                </div>
                <div class="modal-footer texts-footer">
                    <h3><span class="side-img"><img src="images/Elisha-Cuthbert-Beautiful-Lady.jpg" /></span> Jackie Chan</h3>
                    <i>2017 Summer Adventures</i>
                    <ul class="footer-icon">
                        <li><a href="#"><span><i class="fa fa-circle" aria-hidden="true"></i></span>1</a></li>
                        <li><a href="#"><span class="add-img"><img src="images/ffg.png"/></span>42</a></li>
                        <li><a href="#"><span><i class="fa fa-comment" aria-hidden="true"></i></span>20</a></li>
                        <li><a href="#"><span><i class="fa fa-heart" aria-hidden="true"></i></span>328</a></li>
                    </ul>
                </div>


            </div>
        </div>
    </div>

    <!---------------------for text----------------------->

    <!-----------------------small img---------------------->

    <div class="modal fade" id="basicModalimg3" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog unique-width">
            <div class="modal-content for-text">
                <div class="modal-body">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>-->
                    <a href="#" class="" data-toggle="modal" data-target="#basicModalbigimg3"><img class="left-image" src="img/img20130504141421342s.jpg" /></a>

                    <div class="modal-footer text-footer">
                        <h3><span class="side-img new-side-img"><img src="images/Elisha-Cuthbert-Beautiful-Lady.jpg" /></span> Emily Monaghan</h3>
                        <i>Patricks Graduation</i>
                        <ul class="footer-icon">
                            <li><a href="#"><span><i class="fa fa-circle" aria-hidden="true"></i></span>1</a></li>
                            <li><a href="#"><span class=" add-img"><img src="images/ffg.png"/></span>42</a></li>
                            <li><a href="#"><span><i class="fa fa-comment" aria-hidden="true"></i></span>20</a></li>
                            <li><a href="#"><span><i class="fa fa-heart" aria-hidden="true"></i></span>328</a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!----------------------------------------------------------------------->

    <!---------------------EmmUpdated2----------------------->

    <!--<div class="container">
    <div class="row text-center">
        <a href="#" class="" data-toggle="modal" data-target="#basicModalEmmUpdated2"><b>10. Add</b></a>
    </div>
</div>-->

    <div class="modal fade" id="basicModalEmmUpdated2" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content for-text">
                <div class="modal-body text-center">
                    <div class="modal-header">
                        <h3>What Would You Like to Add?</h3>
                    </div>
                    <div class="col-lg-6">
                        <div class="left-block">
                            <span><i class="fa fa-square" aria-hidden="true"></i><a href="#"><p>New Photos</p></a></span>
                        </div>
                    </div>
                    <div class="col-lg-6 no-padding new-add-margin">
                        <div class="roght-block">
                            <div class="right-top-block">
                                <ul class="modal-list">
                                    <li><span><img src="img/icon3.png" /><a href="#"><p>Album</p></a></span></li>
                                    <li><span><img src="img/icon2.png" /><a href="#"><p>Page</p></a></span></li>
                                </ul>
                            </div>
                            <div class="right-bottom-block">
                                <ul class="modal-list">
                                    <li><span><img src="img/icon1.png" /><a href="#"><p>Relationship</p></a></span></li>
                                    <li><span><i class="fa fa-heart" aria-hidden="true"></i><a href="#"><p>Tribute</p></a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
<!---------------------EmmUpdated2----------------------->
<script>
$('document').ready(function(){
	$('#album_image_upload').click(function(){
		
		var imgVal = $('#upload').val(); 
		if(imgVal=='') 
		{ 
		  alert("Please Select Any Image");
		}
		else{
		 $("#upload_album_image").submit();	
		}
	});
	
	/*$('#album_image_upload').bind("click",function(){ 
		var imgVal = $('#upload').val(); 
		if(imgVal=='') 
		{ 
		  alert("Please Select Any Image");
		} 
		return false; 

	});*/ 
	
	
	
});
</script>