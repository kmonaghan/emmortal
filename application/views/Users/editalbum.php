
	<div id="modaloutr">	
    <div class="modal fade" id="editalbumUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog txtcls">
           <div class="modal-content modal-outer">
                <button type="button" class="close close-new" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
               <div class="modal-body text-center add-paddingnw">
			<form id="edit_upload_file_image" name="edit_upload_file_image" method="post" action="<?php echo base_url()?>ImageUpload/editAlbumDetails/" enctype="multipart/form-data" />
             <div class="profilearear">
              <div class="profilearea_innewr">
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3 id="headerCaption">Edit Album</h3>
					</div>
				  </div>
				  <div class="row canvarea">
				    <div class="col-md-6">
					<div class="canvas-placeholdernewr">
						<img alt="image" id="edit_album_imagePreview1" name="upload_image" src="#">
					</div>
                        <button id="edit_choosephoto1" class="modal-submit-btn" type="button">Choose Photo</button>
					<input id="edit_uploadFile1" name="imageUpload" class="modal-submit-btn" type="file" style="display: none"/>
                        <button id="edit_Remove_uploadFile1" class="remvphoto" type="button">Remove Photo</button>
                        <div class="error_image"></div>
					 <input type="hidden" value="" class="edit_album_id" name="album_id"/>
					 <input type="hidden" value="" name="image_id" id="image_id"/>
                     <input type="hidden" value="" name="choose_album_image_name" id="choose_album_image_name"/>
                     <input type="hidden" value="" name="choose_album_upload_path" id="choose_album_upload_path"/>
                        <input type="hidden" value="" name="selected_choose_album_image_name" id="selected_choose_album_image_name"/>
                        <input type="hidden" value="" name="selected_choose_album_upload_path" id="selected_choose_album_upload_path"/>
					</div>
					<div class="col-md-6">
					<input name="title" id="edit_album_title" type="text" class="form-control" placeholder="Album Title" value="" required>
                    <!--<label>Content</label>-->
					<textarea id="txtEditor1" class="edit_album_txtEditor1" name="desc" placeholder="Album Description">
                    </textarea>
                    <b>Album Background Style</b>
                    <div class="emm-page-style_txt edit_album_back_image_outr">
                    </div>
                    <b class="album_foreground_header"></b>
                    <div class="emm-page-style_txt edit_album_foreground_image_outr">
                    </div>
                    <input type="hidden" class="editAlbumForegroundStyleId" name="editAlbumForegroundStyleId" value=""/>
					<input type="hidden" class="edit_background_image_id" name="edit_background_image_id" value=""/>

					<select name="permission" class="txtfl album_view_permission">
					  <option value="all">Public</option>
					  <option value="friends">Friends</option>
					 </select>
					<!--<a href="#" id="create_album" class="addalbm"><i class="fa fa-plus" aria-hidden="true"></i> Add Album</a>-->
					<div class="row text-right">
                        <button type="submit" name="edit_album_btn_publish" id="updateFormAlbum" class="bckbtn">Update</button>
					  <!--<a href="#" class="bckbtn"></a>-->
					</div>
					</div>
				  </div>
				</div>
              </div>
            </div>
		</form>
                </div>
              
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="choose_album_photo" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog txtcls_thumb">
        <div class="modal-content for-text">
            <div class="modal-body text-center add-padding-thimb">
                <div class="profilearear_thimb">
                    <div class="profilearea_innewr_thimb">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="pophd_thimb">
                                    <h3 id="headerCaption">Choose From Album Photos</h3>
                                </div>
                                <div class="canvarea_thimb">
                                    <div class="emm-page-style_txt_thimb">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer color_footer_thumb">
                <button class="pull-left" id="browse_edit_album_photo">Browse</button>
                <button class="pull-right" id="next_edit_album_photo">Next</button>
            </div>
        </div>
    </div>
</div>

<script>
var cloudfront_base_url=$('#cloudfront_base_url').val();
function readURLAlbumImage1(input)
{

    if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
    {
        var reader = new FileReader();

        reader.onload = function(e)
        {

            $('#edit_album_imagePreview1').attr('src', e.target.result);
            $('#edit_album_imagePreview1').show();
            $('#edit_Remove_uploadFile1').show();
            $('#edit_choosephoto1').hide();
            $('.error_image').hide();
        }

        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        $('.error_image').html('Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!').show();
        $("#edit_uploadFile1").val('');
    }
}
function editAlbumForegroundImage(id) {
    $('.editAlbumForegroundStyleId').val(id);
    $('.toggle').toggleClass('toggle');
    $('#edit_album_foreground_image_inner_div_'+id).toggleClass('toggle');
}
//show image in choose Image onchange
$(document).on('change', '#edit_uploadFile1',function() {
   readURLAlbumImage1(this);
});
function edit_readme(id)
{
    $('.edit_background_image_id').val(id);
	$('.hli').toggleClass('hli');
	$('#edit_album_back_image_inner_div_'+id).toggleClass('hli');
    $.ajax({
        url: "<?php echo base_url('ImageUpload/select_foreground_images') ?>",
        type: 'post',
        data:{albumBackgroundStyleId:id},
        success: function(data)
        {
            var base_url='<?php echo base_url();?>';
            var obj2 = JSON.parse(data);
            var total_foreground_images=obj2.total_foreground_images;
            var option=new Array();
            if(total_foreground_images>0)
            {
                $('.album_foreground_header').html('Album Foreground Style');
            }
            else
            {
                $('.album_foreground_header').html('');
            }
            for(var i=0;i<total_foreground_images;i++)
            {
                option[i]='<div class="page-style-item" id="edit_album_foreground_image_inner_div_'+obj2.id[i]+'" onclick="editAlbumForegroundImage('+obj2.id[i]+')";><img alt="image" class="album_foreground_image" data-pid="'+obj2.id[i]+'" src="'+cloudfront_base_url+obj2.upload_path[i]+'/'+obj2.image_name[i]+'" /></div>';
            }
            $('.edit_album_foreground_image_outr').html(option);
        }
    });
}
$('document').ready(function(){
	$('#edit_album_btn_back1').click(function(){
		$('#editalbumUpload').modal('hide');
	});
	$('#edit_Remove_uploadFile1').click(function(){
		$('#edit_album_imagePreview1').attr('src', '');
		$('#edit_album_imagePreview1').hide();
		$('#edit_Remove_uploadFile1').hide();
		$('#edit_choosephoto1').show();
	});
    // Choose photo click
    $(document).on('click', '#edit_choosephoto1', function()
    {
        var album_id=$('.edit_album_id').val();
        $.ajax({
            url: "<?php echo base_url('ImageUpload/fetchAllImageInAlbum') ?>",
            type: 'post',
            data:{album_id:album_id},
            success: function(data)
            {
                var base_url='<?php echo base_url();?>';
                var obj = JSON.parse(data);
                var option=new Array();
                if(obj.fetchAllImageInAlbum)
                {
                    for(var i=0;i<obj.fetchAllImageInAlbum.length;i++)
                    {

                        if(obj.fetchAllImageInAlbum[i].thumbnail_image_name!="" && obj.fetchAllImageInAlbum[i].thumbnail_upload_path!="")
                        {
                            option[i]='<div class="page-style-item_thimb" id="choose_album_image_inner_div_'+obj.fetchAllImageInAlbum[i].id+'" onclick="chooseAlbumImage('+obj.fetchAllImageInAlbum[i].id+',\''+obj.fetchAllImageInAlbum[i].thumbnail_upload_path+'\',\''+obj.fetchAllImageInAlbum[i].thumbnail_image_name+'\')";>' +
                                '<img alt="image" src="' +cloudfront_base_url+obj.fetchAllImageInAlbum[i].thumbnail_upload_path+obj.fetchAllImageInAlbum[i].thumbnail_image_name+'" />' +
                                '</div>';
                        }
                        else
                        {
                            option[i]='<div class="page-style-item_thimb" id="choose_album_image_inner_div_'+obj.fetchAllImageInAlbum[i].id+'" onclick="chooseAlbumImage('+obj.fetchAllImageInAlbum[i].id+',\''+obj.fetchAllImageInAlbum[i].uploadPath+'\',\''+obj.fetchAllImageInAlbum[i].image_name+'\')";>' +
                                '<img alt="image" src="' +cloudfront_base_url+obj.fetchAllImageInAlbum[i].uploadPath+obj.fetchAllImageInAlbum[i].image_name+'" />' +
                                '</div>';
                        }

                    }
                }
                $('.emm-page-style_txt_thimb').html(option);
            }
        });
        $('#choose_album_photo').modal('show');

    });
    $(document).on('click', '#browse_edit_album_photo', function()
    {
        $("#edit_uploadFile1").click();
        $('#choose_album_photo').modal('hide');

    });
    $(document).on('click', '#next_edit_album_photo', function()
    {
        var base_url=$('#base_url').val();
        var choose_album_upload_path=$('#choose_album_upload_path').val();
        var  choose_album_image_name=$('#choose_album_image_name').val();
        if(choose_album_upload_path && choose_album_image_name)
        {
            $('#selected_choose_album_upload_path').val(choose_album_upload_path);
            $('#selected_choose_album_image_name').val(choose_album_image_name);
            $('#edit_album_imagePreview1').attr('src',cloudfront_base_url+choose_album_upload_path+choose_album_image_name);
            $('#edit_album_imagePreview1').show();
            $('#edit_Remove_uploadFile1').show();
            $('#edit_choosephoto1').hide();
            $('.error_image').hide();
            $('#choose_album_photo').modal('hide');
        }
        else
        {
            $.alert({
                title: 'Alert!',
                content: 'Please choose from album photos or browse!!!',
            });
        }

    });
	$('#edit_album_btn_back1').click(function(){
		$('#editSingleImageUpload').modal('hide');
	});

    $('#updateFormAlbum').on('submit', function()
    {
        var error_free=true;
        if($('#edit_uploadFile1').val()=='')
        {
            $('.error_image').html('Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!').show();
            error_free=false;
        }
        return error_free;
    })
});
function chooseAlbumImage(id,choose_album_upload_path,choose_album_image_name) {
    $('.hli_thimb').toggleClass('hli_thimb');
    $('#choose_album_image_inner_div_' + id).toggleClass('hli_thimb');
    $('#choose_album_upload_path').val(choose_album_upload_path);
    $('#choose_album_image_name').val(choose_album_image_name);
}

</script>
    