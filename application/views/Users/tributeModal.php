<div class="modal fade" id="tributeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog txtcls">
        <div class="modal-content modal-outer">
            <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <div class="modal-body text-center add-paddingnw">
                <div class="profilearea_txt">
                    <div class="profilearea_in_txt">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="pophd">
                                    <h3>Create new tribute</h3>
                                </div>
                            </div>
                            <form id="formTribute" method="post" enctype="multipart/form-data">
                                <div class="row canvarea2">
                                    <div class="col-md-12">
                                        <div class="outer_select_tribute">
                                            <select id="tribute_friends_id" name="tribute_friends_id[]" class="select2 form-control" data-placeholder="Type in a persons name to send a tribute..." multiple>
                                            </select>
                                        </div>
                                        <textarea id="tributeDescription" name="tributeDescription" class="form-control" placeholder="Tribute Description"></textarea>
                                        <div class="row text-right">
                                            <button class="bckbtn" type="submit">Publish</button>
                                            <button type="button" class="bckbtn" id="backTribute">Back</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  Tribute js -->
<script src="<?php echo base_url(); ?>assets/js/tributes.js"></script>


