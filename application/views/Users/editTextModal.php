<div class="modal fade" id="edittextcontent" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
 <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <div class="profilearea_txt">
              <div class="profilearea_in_txt">
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3>Edit text Entry</h3>
					</div>
				  </div>
                  <form name="text_add" method="post" action="<?php echo base_url()?>ImageUpload/editText/" enctype="multipart/form-data">
				  <div class="row canvarea2">
				    <div class="col-md-12">
					 <input name="edit_text_title" id="show_edit_text_title" type="text" class="form-control" placeholder="Title" maxlength="140">
					<textarea id="txtEditor" class="edit_txtEditor" name="text_desc"></textarea>
					  <input type="hidden" value="" name="edit_text_id" id="edit_text_id"/>
					 <div class="col-md-12 selectrtxt">
                      <div class="row">
					    <select name="album_name" class="txtfl album_list_id">

                        </select>
                    </div>
                    </div>
					   <div class="outer_select">
                           <h6>Type Your Friend's Name</h6>
						   <select name="tag_friend_name[]" class="select2 friend_list_edit_text" style="width:500px;" multiple="multiple">
						  </select>
						 </div> 
					<div class="row text-right">
                        <button class="bckbtn" type="submit" name="submit_text">Update</button>
					</div>
					</div>
                    </form>
				  </div>
				</div>
              </div>
            </div>
        </div>
    </div>
</div>
