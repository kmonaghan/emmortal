<div id="modaloutr">
    <div class="modal fade" id="editvideoUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			    <button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-body text-center add-paddingnw">
			    <form method="post" action="<?php echo base_url()?>ImageUpload/editvideoUpload/" enctype="multipart/form-data">
                    <div class="profilearear">
                        <div class="editvid_innewr">
                            <div class="col-md-12">
				                    <div class="row">
				                        <div class="pophd">
					                        <h3 id="headerCaption">Edit new Video</h3>
					                    </div>
				                    </div>
				                    <div class="row canvarea">
				                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <!-- add inner video start--->
                                                <input type="hidden" value="" name="edit_video_id" id="edit_video_id"/>
                                                <input type="hidden" value="" name="album_owner_id" id="album_owner_id"/>
                                                 <div class="thumbalbumedtvd">
                                                     <div class="col-md-12">
                                                        <div class="record_vd">
                                                            <div id="secimgvd" class="img-responsive show_edit_video"></div>
											   	            <button id="edit_Remove_video" class="remvphoto" type="button">Remove Video</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- add inner video end--->
                                            </div>
                                            <div class="col-md-6">
                                           <h6>Type Your Friend's Name</h6>
                                            <div class="outer_selectvd">
                                                <select name="friend_name[]" class="select2 friend_list_edit_video" style="width:464px;" multiple="multiple"></select>
                                            </div>
                                            <select name="album_name" id="album_name" class="txtflvd">
                                                <?php
                                                 if($total_record_album>0)
                                                 {
                                                     for($i=0;$i<$total_record_album;$i++)
                                                     {
                                                         if(!empty($present_album_id))
                                                         {
                                                            ?>
                                                            <option value="<?php echo $albumeid[$i]?>" <?php if($present_album_id==$albumeid[$i]){?>selected=selected<?php }?>><?php echo $album_title[$i];?></option>
                                                            <?php
                                                         }
                                                         else
                                                         {
                                                        ?>
                                                            <option value="<?php echo $albumeid[$i]?>"><?php echo $album_title[$i];?></option>
                                                        <?php
                                                        }
                                                }
                                            }
                                             else
                                             {
                                                ?>
                                                <option value="-1">My Chronicles</option>
                                                <?php
                                            }
                                            ?>
					                    </select>
                                    <div class="row text-right">
                                        <button type="submit" name="btn_publish" class="bckbtn">Publish</button>
                                        <button type="button" id="btn_edit_back_video" class="bckbtn">Back</button>
                                    </div>
                                </div>
                            </div>
				        </div>
                      </div>
				    </div>
				</div>
                </form>
                </div>
              </div>
            </div>
        </div>
    </div>
<script>
$('document').ready(function(){
$('#btn_edit_back_video').click(function(){
		$('#editvideoUpload').modal('hide');
		$('#basicModalvideo').modal('show');
});
$('#edit_Remove_video').click(function(){
  $('.show_edit_video').html('');
  var db_video_id=$('#edit_video_id').val();
  var owner_id=$('#album_owner_id').val();
  var album_id=$('#album_name').val();
  var frd_list_name=$('.friend_list_edit_video').val();
  console.log(frd_list_name);
  $.post("<?php echo base_url()?>ImageUpload/videoRemove",{'album_id':album_id,'video_id':db_video_id,'user_id':owner_id},function(res){
		var obj = JSON.parse(res);
		console.log(obj);
		
		 $('#album_id').val(album_id);
         $('#video_user_id').val(owner_id);
         $('#previous_page').val('frmEditpage');
         if(obj.friend_fullName!=null)
			{
				$.each(obj.friend_fullName, function(index, value) {
					option= $('<option></option>').attr("value", obj.friend_userid[index]).text(obj.friend_fullName[index]);
					$(".friend_list_add_video").append(option);
				});
				
				for (i=0;i<frd_list_name.length;i++){
					  $(".friend_list_add_video option[value='"+frd_list_name[i]+"']").attr('selected', 'selected');
			  }
			}
		 
		 $('#editvideoUpload').modal('hide');
         $('#addvideoUpload').modal('show');
	}); //end of delete -post method
}); //end of click remove button 
});
</script>
