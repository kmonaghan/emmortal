<!---------------------------------------12.add post------------------------------>
	<div id="modaloutr">	
    <div class="modal fade" id="addalbumUpload" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog txtcls">
            <div class="modal-content modal-outer">
			<button type="button" class="close close-new" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-body text-center add-paddingnw">
			<form id="upload_file_album" name="image_upload" method="post" action="<?php echo base_url()?>ImageUpload/album_creation/" enctype="multipart/form-data" />
             <div class="profilearear">
              <div class="profilearea_innewr">
               <div class="col-md-12">
				  <div class="row">
				    <div class="pophd">
					 <h3 id="headerCaption">Create new Album</h3>
					</div>
				  </div>
				  <div class="row canvarea">
				    <div class="col-md-6">
					<div class="canvas-placeholdernewr">
						<img alt="image" id="imagePreview1" name="upload_image" src="#">
					</div>
                        <button id="choosephoto1" class="modal-submit-btn" type="button">Choose Photo</button>
					<input id="uploadFile1" name="imageUpload" class="modal-submit-btn" type="file" style="display: none"/>
                        <button id="Remove_uploadFile1" class="remvphoto" type="button">Remove Photo</button>
                        <div id="album_image_error" class="error_image"></div>
					 <!--<input name="" type="text" class="form-control" placeholder="Type Friend Name">-->
					</div>
					<div class="col-md-6">
                    <!--<label>Album Title</label>-->
					<input name="title" type="text" class="form-control" placeholder="Album Title" required="required">
                    
					<textarea id="txtEditor1" name="desc" placeholder="Album Description"></textarea>
                    <b>Album Background Style</b>
					<div class="emm-page-style_txt album_back_image_outr">
                     </div>
                    <b class="album_foreground_header"></b>
                    <div class="emm-page-style_txt album_foreground_image_outr">
                    </div>
					<input type="hidden" class="background_image_id" name="background_image_id" value=""/>
                    <input type="hidden" class="albumForegroundStyleId" name="albumForegroundStyleId" value=""/>
					<select name="permission" class="txtfl">
					  <option value="all">Public</option>
					  <option value="friends">Friends</option>
					 </select>
					<!--<a href="#" id="create_album" class="addalbm"><i class="fa fa-plus" aria-hidden="true"></i> Add Album</a>-->
					<div class="row text-right">
                        <button type="submit" name="btn_publish" class="bckbtn">Publish</button>
					  <!--<a href="#" class="bckbtn"></a>-->
					  <button type="button" href="#" id="btn_back1" class="bckbtn">Back</button>
					</div>
					</div>
				  </div>
				</div>
              </div>
            </div>
		</form>
                </div>
              
            </div>
        </div>
    </div>
</div>

<script>
 var cloudfront_base_url=$('#cloudfront_base_url').val();
function readURLAlbumImage(input)
{

    if (input.files && input.files[0] &&  input.files[0].name.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/))
    {
        var reader = new FileReader();

        reader.onload = function(e)
        {

            $('#imagePreview1').attr('src', e.target.result);
            $('#imagePreview1').show();
            $('#Remove_uploadFile1').show();
            $('#choosephoto1').hide();
            $('#album_image_error').hide();
        }

        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        $('#album_image_error').html('Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.!').show();
        $("#uploadFile1").val('');
    }
}
//show image in choose Image onchange
$(document).on('change', '#uploadFile1',function() {
    readURLAlbumImage(this);
});

function selectAlbumForegroundImage(id) {
    $('.albumForegroundStyleId').val(id);
    $('.toggle').toggleClass('toggle');
    $('#album_foreground_image_inner_div_'+id).toggleClass('toggle');
}
function readme(id)
{
	$('.background_image_id').val(id);
	$('.hli').toggleClass('hli');
	$('#album_back_image_inner_div_'+id).toggleClass('hli');
    $.ajax({
        url: "<?php echo base_url('ImageUpload/select_foreground_images') ?>",
        type: 'post',
        data:{albumBackgroundStyleId:id},
        success: function(data)
        {
            var base_url='<?php echo base_url();?>';
            var obj2 = JSON.parse(data);
            var total_foreground_images=obj2.total_foreground_images;
            var option=new Array();
            if(total_foreground_images>0)
            {
                $('.album_foreground_header').html('Album Foreground Style');
            }
            else
            {
                $('.album_foreground_header').html('');
            }
            for(var i=0;i<total_foreground_images;i++)
            {
                option[i]='<div class="page-style-item" id="album_foreground_image_inner_div_'+obj2.id[i]+'" onclick="selectAlbumForegroundImage('+obj2.id[i]+')";><img alt="image" alt="image" class="album_foreground_image" data-pid="'+obj2.id[i]+'" src="'+cloudfront_base_url+obj2.upload_path[i]+'/'+obj2.image_name[i]+'" /></div>';
            }
            $('.album_foreground_image_outr').html(option);
        }
    });
}

$('document').ready(function(){
	$('#imagePreview1').hide();
	$('#Remove_uploadFile1').hide();
	$('#btn_back1').click(function(){
		$('#addalbumUpload').modal('hide');
		$('#basicModalEmmUpdated2').modal('show');
	});
	$('#Remove_uploadFile1').click(function(){
		$('#imagePreview1').attr('src', '');
		$('#imagePreview1').hide();
		$('#Remove_uploadFile1').hide();
		$('#choosephoto1').show();
        $("#uploadFile1").val('');
	});
    // Choose photo click
    $(document).on('click', '#choosephoto1', function()
    {
        $("#uploadFile1").click();

    });
    $('#upload_file_album').on('submit', function()
    {
        var error_free=true;
        if($('#uploadFile1').val()=='')
        {
            $('#album_image_error').html('Image file can not be blank!').show();
            error_free=false;
        }
        return error_free;
    })
});
</script>
    