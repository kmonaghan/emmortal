<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * MediaRenderer_youtube Class
 */
class MediaRenderer_youtube extends CI_Driver implements MediaRendererInterface {
 
  /**
   * @param string $id Media ID
   * @see MediaRendererInterface::display()
   */
  public function display($id)
  {
    if ($id)
    {
      return '<iframe width="420" height="315" src="//www.youtube.com/embed/'.$id.'" frameborder="0"
allowfullscreen></iframe>';
    }
  }
 
}