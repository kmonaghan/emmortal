<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * MediaRenderer_vimeo Class
 */
class MediaRenderer_vimeo extends CI_Driver implements MediaRendererInterface {
     
    /**
     * @param string $id Media ID
     * @see MediaRendererInterface::display()
     */
  public function display($id)
  {
      if ($id)
      {
      return '<iframe width="420" height="247" src="//player.vimeo.com/video/'.$id.'"></iframe>';
      }
  }
 
}