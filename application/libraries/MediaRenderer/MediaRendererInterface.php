<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * MediaRendererInterface
 */
interface MediaRendererInterface
{
    public function display($id);
}