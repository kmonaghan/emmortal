<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
interface_exists('MediaRendererInterface', FALSE) OR require_once(APPPATH.'/libraries/MediaRenderer/MediaRendererInterface.php');
 
/**
 * MediaRenderer Class
 */
class MediaRenderer extends CI_Driver_Library {
     
  public $valid_drivers;
  public $CI;
  protected $_adapter = 'youtube';
 
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->CI =& get_instance();
    $this->CI->config->load('mediarenderer');
    $this->valid_drivers = $this->CI->config->item('media_services');
    $this->_adapter = $this->CI->config->item('media_default');
  }
 
  /**
   * Overrided __get method to check if the adapter implements MediaRendererInterface interface
   * @see CI_Driver_Library::__get()
   */
  public function __get($child)
  {
      if (in_array($child, $this->valid_drivers))
      {
          $object = $this->load_driver($child);
           
          if ($object instanceof MediaRendererInterface)
          {
              return $object;
          }
          else
          {
              show_error("MediaRenderer: Adapter '".$child."' doesn't implement MediaRendererInterface. Aborting.");
              return;
          }
      }
      else
      {
                show_error('Unable to load the requested adapter: '.$child);
                return;
      }
  }
 
  /**
   * @param string $adapter Adapter name
   * @return MediaRenderer
   */
  public function setAdapter($adapter)
  {
      $this->_adapter = $adapter;
      return $this;
  }
 
  /**
   * @param string $id Media ID
   */
  public function display($id)
  {
      return $this->{$this->_adapter}->display($id);
  }
 
}