<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include("./vendor/autoload.php");
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
class Aws3
{
    private $s3;
    public function __construct()
    {
        $this->s3=s3client::factory([
            'key'=>'AKIAJVGBTB5U6Y6IJEBA',
            'secret'=>'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',
            'signature' => 'v4',
            'region'=>'us-east-2'
        ]);
    }
    public function sendFile($bucket_name,$source_file,$image_directory)
    {
        $this->s3->putObject(array(
            'Bucket'=>$bucket_name,
            'Key'=>$image_directory,
            'SourceFile'=>$source_file,
            'ContentType'=>'image',
            'StorageClass'=>'STANDARD',
            'ACL' => 'public-read',
            ));

    }
    public function deleteFile($bucket_name,$image_file)
    {
        $this->s3->deleteObject(array(
            'Bucket' => $bucket_name,
            'Key'    => $image_file
        ));
    }
    public function copyFile($bucket_name,$source_file,$image_directory)
    {
        $content = file_get_contents($source_file);
        $this->s3->putObject(array(
            'Bucket' => $bucket_name,
            'Key'    => $image_directory,
            'Body' => $content,
            'ContentType'=>'image',
            'StorageClass'=>'STANDARD',
            'ACL' => 'public-read',
        ));

    }
    public function deleteDirectory($bucket_name,$image_directory)
    {

        $this->s3->deleteObject(array(
            'Bucket' => $bucket_name,
            'Key'    => $image_directory
        ));
    }
}
?>