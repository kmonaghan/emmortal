<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImageUploadModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
	}
	
	public function select_album_name($user_id)
	{
		$this->db->select("title,albumid");
		$this->db->from("albumdetails");
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function select_image($id)// this function is call from users controller
	{
		
		$this->db->select("*");
		$this->db->from("singleimageuploaddetails");
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function edit_tag_friend_list($tag_friend_name,$image_id)
	{
		/*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $tag_friend_name['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
		if($image_id)
		{
		  $this->db->where('image_id', $image_id);
		  $this->db->where('user_id', $tag_friend_name['user_id']);
		  $this->db->delete('friendsTagContent');
		  
		  $this->db->where('notify_type_id', $image_id);
		  $this->db->where('UID', $tag_friend_name['user_id']);
          $this->db->delete('notificationdetails');
		  
		}
		for($i=0;$i<$tag_friend_name['total_frd_tag'];$i++)
		 {
			$userdata1['image_id']=$image_id;
            $userdata1['user_id']=$tag_friend_name['user_id'];
            $userdata1['friends_id']=$tag_friend_name['tag_friend_name'][$i];
		    $userdata1['created_at']=$tag_friend_name['created_at']; 
		    $this->db->set($userdata1);
             $this->db->insert('friendsTagContent');
            //echo $this->db->last_query();

            $userdata2['notify_type_id']=$image_id;
            $userdata2['UID']=$tag_friend_name['user_id'];
            $userdata2['FID']=$tag_friend_name['tag_friend_name'][$i];
		    $userdata2['notificationdate']=$tag_friend_name['created_at']; 
		    $userdata2['notify_type']='tagImage'; 
		    $userdata2['notify_details']=' Tagged you  on a Image';
			$this->db->set($userdata2);
			 $this->db->insert('notificationdetails');
			
		 }
		return true;
	}
	
	
	
    
	public function album_insertdata($value) //for album data only
	{
	     $userdata['user_id']=$value['user_id'];
         $userdata['description']=$value['desc'];
         $userdata['title']=$value['title'];
		 $userdata['viewstatus']=$value['view_status'];
		 $userdata['created_at']=$value['created_at'];
		 $userdata['album_background_id']=$value['background_image_id'];
        $userdata['album_foreground_id']=$value['albumForegroundStyleId'];
         $this->db->set($userdata);
         $this->db->insert('albumdetails');
         return $this->db->insert_id();	  
	}
	
	public function album_cover_image($value) //for album_cover_image
	{
		 $userdata['albumid']=$value['albumid'];
		 $userdata['user_id']=$value['user_id'];
         $userdata['image_name']=$value['image_name'];
         $userdata['uploadPath']=$value['uploadPath'];
		 $userdata['created_at']=$value['created_at'];
		 $userdata['is_cover_image']=$value['is_cover_image'];
		 $this->db->set($userdata);
         $this->db->insert('albumimageuploaddetails');
         return $this->db->insert_id();	
	}
	
	public function select_cover_image($id)// for select cover image for album creation
	{
	  	$this->db->select("*");
		$this->db->from("albumimageuploaddetails");
		$this->db->join('albumdetails', 'albumdetails.albumid = albumimageuploaddetails.albumid'); 
		$this->db->where('albumimageuploaddetails.albumid',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
    
    public function select_album_details($album_id)
    {
       $this->db->select("*");
       $this->db->from("albumdetails"); 
       $this->db->where('albumid',$album_id);
       $query = $this->db->get();
       return $query->result_array();
    }
	
    public function albumdetails_insert($value)
    {

        if($value['album_name']=='-1')
        {
          $this->db->select("*");
          $this->db->from("albumdetails");
          $this->db->where('user_id',$value['user_id']);
          $this->db->where('is_default_album',$value['is_default_album']);
          $query = $this->db->get();
          $result=$query->row_array();
		  //echo $this->db->last_query();
        }
        else
        {
          $this->db->select("*");
          $this->db->from("albumdetails");
          $this->db->where('albumid',$value['album_name']);
          $this->db->where('user_id',$value['user_id']);
          $query = $this->db->get();
          $result=$query->row_array();
		  //echo $this->db->last_query();
        }

        if(empty($result))
        {
           $userdata['user_id']=$value['user_id'];
           $userdata['description']='';
           $userdata['title']=$value['albumname'];;
           $userdata['viewstatus']='all';
           $userdata['created_at']=$value['created_at'];
           $userdata['is_default_album']=$value['is_default_album'];
           $this->db->set($userdata);
           $this->db->insert('albumdetails');
           //echo $this->db->last_query();
		   $album_id=$this->db->insert_id();

           $img_data['albumid']=$album_id;
           $img_data['user_id']=$value['user_id'];
		   $img_data['image_name']='no_cover.jpg';
		   $img_data['uploadPath']='assets/img/';
		   $img_data['created_at']=$value['created_at'];
		   $img_data['is_cover_image']='y';
		   $this->db->set($img_data);
           $this->db->insert('albumimageuploaddetails');
		   return $album_id;
        }
        else
        {

            return $result['albumid'];

        }
    }
    public function select_default_album_name($value)
	{
		  $this->db->select("*");
          $this->db->from("albumdetails");
          $this->db->where('albumid',$value);
          $query = $this->db->get();
          return $query->result_array();  
    }
    
    public function update_album_image_data($value)
    {

         $userdata['image_title']=$value['title'];
         $userdata['image_desc']=$value['desc'];
         $userdata['albumid']=$value['albumid'];
         $userdata['modified_at']=$value['modified_at'];
         $userdata['is_cover_image']=$value['is_cover_image'];
        $userdata['thumbnail_image_name']=$value['thumbnail_image_name'];
        $userdata['thumbnail_upload_path']=$value['thumbnail_upload_path'];


         $this->db->set($userdata);


         $this->db->where('id',$value['present_image_id']);
         $this->db->where('user_id',$value['user_id']);
         $this->db->update('albumimageuploaddetails');

         return true;
    }
	
    
	public function insertalbum_image($value) //for insert all images in a album 
	{
	     $userdata['user_id']=$value['user_id'];
         $userdata['image_title']=$value['title'];
         $userdata['image_desc']=$value['desc'];
         $userdata['albumid']=$value['albumid'];
         $userdata['image_name']=$value['image_name'];
         $userdata['uploadPath']=$value['uploadPath'];
         $userdata['created_at']=$value['created_at'];
         $userdata['is_cover_image']=$value['is_cover_image'];
         $this->db->set($userdata);
         $this->db->insert('albumimageuploaddetails');
         return $this->db->insert_id();
    }
    
    public function get_inserted_image_details($value)
    {
           $where_clause="created_at >=(now() - interval 10 minute) and user_id=".$value." AND `modified_at` = '0000-00-00 00:00:00' AND `is_cover_image`='n'";
		   $this->db->select("*");
           $this->db->from("albumimageuploaddetails");
           $this->db->where($where_clause);
           $query = $this->db->get();
		   return $query->result();
    }
	
	public function getImageDetails($value)
	{
		   $this->db->select("*");
           $this->db->from("albumimageuploaddetails");
           $this->db->where('id',$value);
           $query = $this->db->get(); 
           //echo $this->db->last_query();
		   //echo "<br/>".$query->num_rows();
		   return $query->result();
	}
	
	public function deleteImageBeforeInsert($image_id,$user_id)
	{
	     $this->db->where('id', $image_id);
		 $this->db->where('user_id', $user_id);
		 $this->db->delete('albumimageuploaddetails');
		 //echo $this->db->last_query();	
	     return true;
	}
	public function getdeletedImagePath($image_id,$user_id)
	{
	     $this->db->select("*");
         $this->db->from("albumimageuploaddetails");
		 $this->db->where('id', $image_id);
		 $this->db->where('user_id', $user_id);
		 $query = $this->db->get(); 
         return $query->result();
	}
	public function insert_albuminnerrecordsdragdrop($value)
	{
        $this->db->insert('albuminnerrecordsdragdrop',$value);
	}
    public function update_albuminnerrecordsdragdrop($value)
    {
        $this->db->set('albumid',$value['albumid']);
        $this->db->where('image_id',$value['image_id']);
        $this->db->update('albuminnerrecordsdragdrop');
    }
    public function update_album_image($value)
	{
		 $userdata['image_title']=$value['title'];
         $userdata['image_desc']=$value['desc'];
         $userdata['albumid']=$value['albumid'];
		 if(!empty($value['image_name']))
		 {
		   $userdata['image_name']=$value['image_name'];
		 }
		 if(!empty($value['uploadPath']))
		 {
		   $userdata['uploadPath']=$value['uploadPath'];

		 }
        if(!empty($value['thumbnail_image_name']))
        {
            $userdata['thumbnail_image_name']=$value['thumbnail_image_name'];
        }
        if(!empty($value['thumbnail_upload_path']))
        {
            $userdata['thumbnail_upload_path']=$value['thumbnail_upload_path'];
        }
         $userdata['modified_at']=$value['modified_at']; 
		 $userdata['is_cover_image']=$value['is_cover_image'];
		 $this->db->set($userdata);
         $this->db->where('id',$value['image_id']);
		 $this->db->update('albumimageuploaddetails');

        $this->db->set('albumid',$value['albumid']);
        $this->db->where('image_id',$value['image_id']);
        $this->db->update('albuminnerrecordsdragdrop');

		 return true;
	}
	public function select_tagFrd_details($image_id,$album_id,$userId)
	{
	        $this->db->select("`pagedetails`.`pageid` AS frdTagPageId, user.fullname AS frdTagname, user.userid AS frdTagId");
            $this->db->from("albumimageuploaddetails");
			$this->db->join("friendsTagContent","`albumimageuploaddetails`.`id`=`friendsTagContent`.`image_id`");
			$this->db->join("user", "`user`.`userid` = `friendsTagContent`.`friends_id`");
			$this->db->join("pagedetails", "`user`.`userid` = `pagedetails`.`user_id`");
            $this->db->where("`albumimageuploaddetails`.`id` =".$image_id."
AND  `albumimageuploaddetails`.`albumid` =".$album_id." AND pagedetails.default_page=1");
            $query = $this->db->get();
			//echo $this->db->last_query();
			//die('aaaa');
			return $query->result_array();
	}
	
	
	public function select_image_details($image_id,$album_id,$userId)
	{
            $this->db->select("*");
            $this->db->from("albumimageuploaddetails");
			$this->db->join("albumdetails","`albumdetails`.`albumid`=`albumimageuploaddetails`.`albumid`");
			$this->db->join("user", "`user`.`userid` = `albumdetails`.`user_id`");
            $this->db->where("`albumimageuploaddetails`.`id` =".$image_id."
AND  `albumimageuploaddetails`.`albumid` =".$album_id."
AND  `user`.`userid` =".$userId);
            $query= $this->db->get();
			//echo $this->db->last_query();
			return $query->result_array();
	}
	
	
	public function getAlbumtitleDesc($user_id)
	{
		$this->db->select("*");
		$this->db->from("albumdetails");
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function getparticularAlbumtitleDesc($user_id,$album_id)
	{
		$this->db->select("*");
		$this->db->from("albumdetails");
		$this->db->where('user_id',$user_id);
		$this->db->where('albumid',$album_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function insert_album_image($value)
    {
		 $userdata['albumid']=$value['album_id'];
         $userdata['image_name']=$value['image_name'];
         $userdata['uploadPath']=$value['image_path'];
         $userdata['created_at']=$value['created_at'];
         $this->db->set($userdata);
         $this->db->insert('albumimageuploaddetails');
         return $this->db->insert_id(); 
    }
	
	public function getAlbumimage($album_id)
	{
		$this->db->select("*");
		$this->db->from("albumimageuploaddetails");
		$this->db->where('albumid',$album_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getAlbum_details($user_id)
    {
       $this->db->select("*");
       $this->db->from("albumdetails");
       $this->db->where('user_id',$user_id);
	   //$this->db->limit(4,$start);
       $query = $this->db->get();
       //echo $this->db->last_query();
       return $query->result_array();  
    }
	
	
    /* public function getAlbum_details($user_id,$start)
    {
       $this->db->select("*");
       $this->db->from("albumdetails");
       $this->db->where('user_id',$user_id);
	   $this->db->limit(4,$start);
       $query = $this->db->get();
       //echo $this->db->last_query();
       return $query->result_array();  
    } */
 	public function getAlbumdetailswithcoverImage($albumid)
	{
		$this->db->select("*");
		$this->db->from("albumimageuploaddetails");
		$this->db->where('albumid',$albumid);
		$this->db->where('is_cover_image','y');
		$query = $this->db->get();
        //echo $this->db->last_query();
		return $query->result_array();
	}
	
	public function getAlbumdetailswithinnerImage($album_id,$user_id)
	{
	    $image_where="`albumdetails`.`albumid`=".$album_id." and `albumdetails`.`user_id`=".$user_id." and albumimageuploaddetails.is_cover_image='n'";
        
        $this->db->select("albumimageuploaddetails.image_name as image_name,albumimageuploaddetails.uploadPath as image_uploadPath,albumimageuploaddetails.created_at as created_at,albumimageuploaddetails.thumbnail_image_name,albumimageuploaddetails.thumbnail_upload_path");
		$this->db->from("albumdetails");
        $this->db->join('albumimageuploaddetails', '`albumdetails`.`albumid`=`albumimageuploaddetails`.`albumid`');
        $this->db->join('albuminnerrecordsdragdrop', 'albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
        $this->db->where($image_where);
		$this->db->order_by("`albumimageuploaddetails`.`created_at`", "desc");
        $this->db->limit(3,0);
        $query1 = $this->db->get();
        //echo $this->db->last_query();
        $res1=$query1->result_array();
        
        $video_where="`albumdetails`.`albumid`=".$album_id." and `albumdetails`.`user_id`=".$user_id." and albumvideouploaddetails.is_deleted='0'";
        $this->db->select("albumvideouploaddetails.vimeo_video_id as vimeo_video_id,albumvideouploaddetails.created_at as created_at");
        $this->db->from("albumdetails");
        $this->db->join('albumvideouploaddetails', '`albumdetails`.`albumid`=`albumvideouploaddetails`.`albumid`');
        $this->db->join('albuminnerrecordsdragdrop', 'albumvideouploaddetails.id=albuminnerrecordsdragdrop.video_id');
        $this->db->where($video_where);
        $this->db->order_by("`albumvideouploaddetails`.`created_at`", "desc");
        $this->db->limit(3,0);
        $query2 = $this->db->get();
        //echo $this->db->last_query();
        $res2=$query2->result_array();
        
        $text_where="`albumdetails`.`albumid`=".$album_id." and `albumdetails`.`user_id`=".$user_id;
        $this->db->select("textdetails.title as text_title, textdetails.description as text_desc,textdetails.addeddate as created_at");
        $this->db->from("albumdetails");
        $this->db->join('textdetails', '`albumdetails`.`albumid`=`textdetails`.`album_id`');
        $this->db->join('albuminnerrecordsdragdrop', 'textdetails.textid=albuminnerrecordsdragdrop.text_id');
        $this->db->where($text_where);
        $this->db->order_by("`textdetails`.`addeddate`", "desc");
        $this->db->limit(3,0);
        $query3 = $this->db->get();
        //echo $this->db->last_query();
        $res3=$query3->result_array();
        
        $result=array();
        foreach($res1 as $value)
        {
            $value['type']='image';
            array_push($result,$value);
        }
        foreach($res2 as $value)
        {
            $value['type']='video';
            array_push($result,$value);

        }
        foreach($res3 as $value)
        {
            $value['type']='text';
            array_push($result,$value);
        }
        array_multisort($result, SORT_DESC);
         return $result;
	}
	 public function insertAlbumlike($value)
     {
         $this->db->select("*");
         $this->db->from("album_like");
         $this->db->where('album_id', $value['album_id']);
         $this->db->where('user_id', $value['user_id']);
         $query = $this->db->get();
         $result=$query->result_array();
		 //echo $this->db->last_query();
         if(empty($result))
         { 
             $userdata['album_id']=$value['album_id'];
             $userdata['user_id']=$value['user_id'];
             $userdata['created_at']=$value['created_at'];
             $this->db->set($userdata);
             $this->db->insert('album_like');
			 //echo $this->db->last_query();

             if(!($value['session_user_id']))
             {
                 $userdata2['UID']=$value['user_id'];
                 $userdata2['FID']=$value['album_user_id'];
                 $userdata2['notify_type']='albumLike';
                 $userdata2['notify_type_id']=$value['album_id'];
                 $userdata2['notify_details']='Likes on your album';
                 $this->db->set($userdata2);
                 $this->db->insert('notificationdetails');
             }
			 $total_like=$this->gettotalAlbumLike($value['album_id']);

			 return $total_like;
         }else
         {
            $this->db->where('album_id', $value['album_id']);
            $this->db->where('user_id', $value['user_id']);
            $this->db->delete('album_like');
             if(!($value['session_user_id']))
             {
                 $this->db->where('UID', $value['user_id']);
                 $this->db->where('FID', $value['album_user_id']);
                 $this->db->where('notify_type_id', $value['album_id']);
                 $this->db->where('notify_type', 'albumLike');
                 $this->db->delete('notificationdetails');
             }
            $total_like=$this->gettotalAlbumLike($value['album_id']);
		    return $total_like;
         }
     }
	 
	 public function insertImagelike($value)
     {
         $this->db->select("*");
         $this->db->from("image_like");
         $this->db->where('image_id', $value['image_id']);
         $this->db->where('user_id', $value['user_id']);
         $query = $this->db->get();
		 //echo $this->db->last_query();	
         $result=$query->result_array();
		 if(empty($result))
         { 
             $userdata['image_id']=$value['image_id'];
             $userdata['user_id']=$value['user_id'];
             $userdata['like_date']=$value['created_at'];
             $this->db->set($userdata);
             $this->db->insert('image_like');
             if(!($value['session_user_id']))
             {
                 $userdata2['UID']=$value['user_id'];
                 $userdata2['FID']=$value['album_user_id'];
                 $userdata2['notify_type']='imageLike';
                 $userdata2['notify_type_id']=$value['image_id'];
                 $userdata2['notify_details']='Likes on your image';
                 $this->db->set($userdata2);
                 $this->db->insert('notificationdetails');
             }
			 $total_like=$this->gettotalImageLike($value['image_id']);
			 return $total_like;
         }else
         {
            $this->db->where('image_id', $value['image_id']);
            $this->db->where('user_id', $value['user_id']);
            $this->db->delete('image_like'); 
            if(!($value['session_user_id']))
             {
                 $this->db->where('UID', $value['user_id']);
                 $this->db->where('FID', $value['album_user_id']);
                 $this->db->where('notify_type_id', $value['image_id']);
                 $this->db->where('notify_type', 'imageLike');
                 $this->db->delete('notificationdetails');
             }
             $total_like=$this->gettotalImageLike($value['image_id']);
             return $total_like;
         }
     }
     
     
     
     public function insertTextlike($value)
     {
         $this->db->select("*");
         $this->db->from("text_like");
         $this->db->where('text_id', $value['text_id']);
         $this->db->where('user_id', $value['user_id']);
         $query = $this->db->get();
         //echo $this->db->last_query();    
         $result=$query->result_array();
         if(empty($result))
         { 
             $userdata['text_id']=$value['text_id'];
             $userdata['user_id']=$value['user_id'];
             $userdata['like_date']=$value['created_at'];
             $this->db->set($userdata);
             $this->db->insert('text_like');
             if(!($value['session_user_id']))
             {
                 $userdata2['UID'] = $value['user_id'];
                 $userdata2['FID'] = $value['album_user_id'];
                 $userdata2['notify_type'] = 'textLike';
                 $userdata2['notify_type_id'] = $value['text_id'];
                 $userdata2['notify_details'] = 'Likes on your text';
                 $this->db->set($userdata2);
                 $this->db->insert('notificationdetails');
             }
             //echo $this->db->last_query();
             $total_like=$this->gettotalTextLike($value['text_id']);
             return $total_like;
         }else
         {
            $this->db->where('text_id', $value['text_id']);
            $this->db->where('user_id', $value['user_id']);
            $this->db->delete('text_like');
             if(!($value['session_user_id']))
             {
                 $this->db->where('UID', $value['user_id']);
                 $this->db->where('FID', $value['album_user_id']);
                 $this->db->where('notify_type_id', $value['text_id']);
                 $this->db->where('notify_type', 'textLike');
                 $this->db->delete('notificationdetails');
             }
            $total_like=$this->gettotalTextLike($value['text_id']);
            return $total_like;
         }
     }
	
    public function insertVideolike($value)
     {
         $this->db->select("*");
         $this->db->from("video_like");
         $this->db->where('video_id', $value['videoid']);
         $this->db->where('user_id', $value['user_id']);
         $query = $this->db->get();
         //echo $this->db->last_query();    
         $result=$query->result_array();
         if(empty($result))
         { 
             $userdata['video_id']=$value['videoid'];
             $userdata['user_id']=$value['user_id'];
             $userdata['like_date']=$value['created_at'];
             $this->db->set($userdata);
             $this->db->insert('video_like');
             if(!($value['session_user_id']))
             {
                 $userdata2['UID'] = $value['user_id'];
                 $userdata2['FID'] = $value['album_user_id'];
                 $userdata2['notify_type'] = 'videoLike';
                 $userdata2['notify_type_id'] = $value['videoid'];
                 $userdata2['notify_details'] = 'Likes on your video';
                 $this->db->set($userdata2);
                 $this->db->insert('notificationdetails');
             }
             //echo $this->db->last_query();
             $total_like=$this->gettotalVideoLike($value['videoid']);
             return $total_like;
         }else
         {
            $this->db->where('video_id', $value['videoid']);
            $this->db->where('user_id', $value['user_id']);
            $this->db->delete('video_like');
             if(!($value['session_user_id']))
             {
                 $this->db->where('UID', $value['user_id']);
                 $this->db->where('FID', $value['album_user_id']);
                 $this->db->where('notify_type_id', $value['videoid']);
                 $this->db->where('notify_type', 'videoLike');
                 $this->db->delete('notificationdetails');
             }
            $total_like=$this->gettotalVideoLike($value['videoid']);
            return $total_like;
         }
     }
    public function isUserVideoLike($video_id,$user_id)
    {
        $this->db->select('*');
        $this->db->from("video_like");
        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
        //echo $this->db->last_query();
    }
	public function gettotalVideoLike($video_id)
     {
         $this->db->select('count("like_id") as total_like');
         $this->db->from("video_like");
         $this->db->where('video_id', $video_id);
         //$this->db->where('user_id', $user_id);
         $query = $this->db->get();
         $result=$query->row();
         //echo $this->db->last_query();
         return $result->total_like;
     }
    public function isUserTextLike($text_id,$user_id)
    {
        $this->db->select('*');
        $this->db->from("text_like");
        $this->db->where('text_id', $text_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
        //echo $this->db->last_query();
    }
    public function gettotalTextLike($text_id)
     {
         $this->db->select('count("like_id") as total_like');
         $this->db->from("text_like");
         $this->db->where('text_id', $text_id);
         //$this->db->where('user_id', $user_id);
         $query = $this->db->get();
         $result=$query->row();
         //echo $this->db->last_query();
         return $result->total_like;
     }

    public function isUserImageLike($image_id,$user_id)
    {
        $this->db->select('*');
        $this->db->from("image_like");
        $this->db->where('image_id', $image_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
        //echo $this->db->last_query();
    }

	public function gettotalImageLike($image_id)
	 {
		 $this->db->select('count("like_id") as total_like');
         $this->db->from("image_like");
         $this->db->where('image_id', $image_id);
		 //$this->db->where('user_id', $user_id);
         $query = $this->db->get();
		 $result=$query->row();
		 //echo $this->db->last_query();
		 return $result->total_like;
	 }
	 
	 public function isUserAlbumLike($album_id,$user_id)
	 {
		 $this->db->select('*');
         $this->db->from("album_like");
		 $this->db->where('album_id', $album_id);
		 $this->db->where('user_id', $user_id);
         $query = $this->db->get();
		 return $query->row();
		 //echo $this->db->last_query();
	 }
	 
	 public function gettotalAlbumLike($album_id)
	 {
		 $this->db->select('count("like_id") as total_like');
         $this->db->from("album_like");
		 $this->db->where('album_id', $album_id);
		 //$this->db->where('user_id', $user_id);
         $query = $this->db->get();
		 $result=$query->row();
		 //echo $this->db->last_query();
		 return $result->total_like;
	 }
	 
	 public function getAlbumdetails($user_id,$album_id)
	 {
		$this->db->select("*");
		$this->db->from("albumdetails");
		$this->db->join('album_like', '`albumdetails`.`albumid` = `album_like`.`album_id`'); 
		$this->db->where('albumdetails.user_id',$user_id);
		$this->db->where('albumdetails.albumid',$album_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die('aaaa');
		if(empty($query->result_array()))
		{
		  $this->db->select("*");
		  $this->db->from("albumdetails");
		  $this->db->where('albumdetails.user_id',$user_id);
		  $this->db->where('albumdetails.albumid',$album_id);
		  $query = $this->db->get();
		  return $query->result_array(); 	
		}
		return $query->result_array();
	 }
	 
     public function get_total_comment($album_id)
     {
          $this->db->select("count(*) as total_count");
          $this->db->from("tributesdetails");
          //$this->db->where('tributesdetails.user_id',$user_id);
          $this->db->where('tributesdetails.album_id',$album_id);
          $query = $this->db->get();
          return $query->row();       
     }
     
     
     
	 public function insertImageOrder($value)
	 {
		$count = 1;
		foreach ($value as $id){
		 $userdata['records_order']=$count; 
		 $this->db->set($userdata);
         $this->db->where('dragdrop_id',$id);
		 $this->db->update('albuminnerrecordsdragdrop');
         echo $this->db->last_query();
		 $count++;	 
		}
		return true;
	 }
	 
	 
	 public function getAlbumdetailsWidthImageuserwise($user_id,$album_id,$image_id) 
	 {
		$this->db->select("*");
		$this->db->from("albumimageuploaddetails");
		$this->db->join('albumdetails', 'albumdetails.albumid = albumimageuploaddetails.albumid'); 
		$this->db->where('albumdetails.user_id',$user_id);
		$this->db->where('albumimageuploaddetails.albumid',$album_id);
		$this->db->where('albumimageuploaddetails.id',$image_id);
		$query = $this->db->get();
		//echo $this->db->last_query();
		//die('aaaa');
		return $query->result_array();
	 }
     public function getAllAlbumDetailsUserwise($start,$user_id,$album_id)
     {
         $this->db->select('*');
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id);
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query1 = $this->db->get();
         $res1=$query1->result_array();

         $this->db->select('*');  //
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=albuminnerrecordsdragdrop.video_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id." AND albumvideouploaddetails.is_deleted='0'");
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query2 = $this->db->get();
         //echo $this->db->last_query();
         $res2=$query2->result_array();
         //die('aaaa');

         $this->db->select('*');
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('textdetails', 'textdetails.textid=albuminnerrecordsdragdrop.text_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id);
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query3 = $this->db->get();
         $res3=$query3->result_array();

         $result=array();
         foreach($res1 as $value)
         {
             $comment=$this->gettotal_image_comment($value['image_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_image_like($value['image_id']);
             $value['total_like']=$like->total_like;
             $page_id=$this->get_page_id_for_image($value['image_id']);
             $value['page_ids']=@$page_id->page_id;
             array_push($result,$value);
         }
         foreach($res2 as $value)
         {
             $comment=$this->gettotal_video_comment($value['video_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_video_like($value['video_id']);
             $value['total_like']=$like->total_like;

             $page_id=$this->get_page_id_for_video($value['video_id']);
             $value['page_ids']=@$page_id->page_id;

             array_push($result,$value);

         }
         foreach($res3 as $value)
         {

             $comment=$this->gettotal_text_comment($value['text_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_text_like($value['text_id']);
             $value['total_like']=$like->total_like;
             $page_id=$this->get_page_id_for_text($value['text_id']);
             $value['page_ids']=@$page_id->page_id;

             array_push($result,$value);

         }
         usort($result, function ($one, $two) {
             if ($one['records_order'] === $two['records_order']) {
                 return 0;
             }
             return $one['records_order'] < $two['records_order'] ? -1 : 1;
         });

         return array_slice($result, $start, 15);
     }
	 public function getAlbumdetailsuserwise($user_id,$album_id)
	 {
         $this->db->select('*');
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id);
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query1 = $this->db->get();
         $res1=$query1->result_array();
                 
         $this->db->select('*');  //
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=albuminnerrecordsdragdrop.video_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id." AND albumvideouploaddetails.is_deleted='0'");
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query2 = $this->db->get();
		 //echo $this->db->last_query();
         $res2=$query2->result_array();
		 //die('aaaa');	

         $this->db->select('*');
         $this->db->from('albuminnerrecordsdragdrop');
         $this->db->join('textdetails', 'textdetails.textid=albuminnerrecordsdragdrop.text_id');
         $this->db->where("albuminnerrecordsdragdrop.user_id='".$user_id."' AND  albuminnerrecordsdragdrop.albumid=".$album_id);
         $this->db->order_by("albuminnerrecordsdragdrop.records_order","ASC");
         $query3 = $this->db->get();
         $res3=$query3->result_array();

         $result=array();
		 foreach($res1 as $value)
         {
             $comment=$this->gettotal_image_comment($value['image_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_image_like($value['image_id']);
			 $value['total_like']=$like->total_like;
			 $page_id=$this->get_page_id_for_image($value['image_id']);
			 $value['page_ids']=@$page_id->page_id;
             array_push($result,$value);
         }	         
         foreach($res2 as $value)
         {
             $comment=$this->gettotal_video_comment($value['video_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_video_like($value['video_id']);
             $value['total_like']=$like->total_like;
			 
			 $page_id=$this->get_page_id_for_video($value['video_id']);
			 $value['page_ids']=@$page_id->page_id;

			 array_push($result,$value);

         }
         foreach($res3 as $value)
         {
             
             $comment=$this->gettotal_text_comment($value['text_id']);
             $value['total_comment']=$comment->total_comment;
             $like=$this->gettotal_text_like($value['text_id']);
             $value['total_like']=$like->total_like;
			 $page_id=$this->get_page_id_for_text($value['text_id']);
			 $value['page_ids']=@$page_id->page_id;

			 array_push($result,$value);

         }
         usort($result, function ($one, $two) {
             if ($one['records_order'] === $two['records_order']) {
                 return 0;
             }
             return $one['records_order'] < $two['records_order'] ? -1 : 1;
         });

         return $result;
	 }
	 
	 public function selectCurrentPage($user_id,$album_id)
	 {
		 $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("album_id",$album_id);
         $this->db->where("user_id",$user_id);
         $query = $this->db->get();
         return $query->row();  
		 
	 }
	 public function delete_pageId($user_id,$album_id,$page_id)
     {
        $this->db->where("album_id",$album_id);
        $this->db->where("user_id",$user_id);
        $this->db->where('page_id', $page_id);
        $this->db->delete('pagerecordsdragdrop');
        return true;    
     }
     public function delete_page_id_to_image($image_id,$user_id,$page_id)
     {
        $this->db->where("image_id",$image_id);
        $this->db->where("user_id",$user_id);
        $this->db->where('page_id', $page_id);
        $this->db->delete('pagerecordsdragdrop');
		//echo $this->db->last_query();
		//die('aaaa');
        return true;    
     }
     public function delete_page_id_to_video($video_id,$user_id,$page_id)
     {
        $this->db->where("video_id",$video_id);
        $this->db->where("user_id",$user_id);
        $this->db->where('page_id', $page_id);
        $this->db->delete('pagerecordsdragdrop');
		//echo $this->db->last_query();
		//die('aaaa');
        return true;    
     }
     public function delete_page_id_to_text($text_id,$user_id,$page_id)
	 {
		$this->db->where("text_id",$text_id);
        $this->db->where("user_id",$user_id);
		$this->db->where('page_id', $page_id);
        $this->db->delete('pagerecordsdragdrop');
		return true;	
	 }
	 
	 public function getPageId($album_id)
	 {
		 $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("album_id",$album_id);
         $query = $this->db->get();
         return $query->row(); 
	 }
	 
	 public function get_page_id_for_text($text_id)
	 {
		 $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("text_id",$text_id);
         $query = $this->db->get();
         return $query->row();   
	 }
	 public function get_page_id_for_video($video_id)
	 {
		 $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("video_id",$video_id);
         $query = $this->db->get();
         return $query->row();   
	 }
	 public function get_page_id_for_image($image_id)
	 {
		 $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("image_id",$image_id);
         $query = $this->db->get();
         return $query->row();   
	 }
	 
	 
	 
	 
	 
     public function  gettotal_image_comment($image_id)
     {
         $this->db->select('count(*) as total_comment');
         $this->db->from('tributesdetails');
         $this->db->where("image_id",$image_id);
         $query = $this->db->get();
         return $query->row();  
     }
     
     public function gettotal_image_like($image_id)
     {
         $this->db->select('count(*) as total_like');
         $this->db->from('image_like');
         $this->db->where("image_id",$image_id);
         $query = $this->db->get();
         return $query->row();  
     }
     
     
     public function  gettotal_text_comment($text_id)
     {
         $this->db->select('count(*) as total_comment');
         $this->db->from('tributesdetails');
         $this->db->where("text_id",$text_id);
         $query = $this->db->get();
         return $query->row();  
     }
     
     public function gettotal_text_like($text_id)
     {
         $this->db->select('count(*) as total_like');
         $this->db->from('text_like');
         $this->db->where("text_id",$text_id);
         $query = $this->db->get();
         return $query->row();  
     }
    
    public function  gettotal_video_comment($video_id)
     {
         $this->db->select('count(*) as total_comment');
         $this->db->from('tributesdetails');
         $this->db->where("video_id",$video_id);
         $query = $this->db->get();
         return $query->row();  
     }
     
     public function gettotal_video_like($video_id)
     {
         $this->db->select('count(*) as total_like');
         $this->db->from('video_like');
         $this->db->where("video_id",$video_id);
         $query = $this->db->get();
         return $query->row();  
     } 
     
     public function remove_album_data($album_id,$user_id)
     {  
			$this->db->select('tributesid');
			$this->db->from('tributesdetails');
			$this->db->where("album_id",$album_id);
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();	
			$result=$query->result_array();
			$comment_id_array=array();
			if(!empty($result))
			{
				 foreach($result as $value)
				 {
					 array_push($comment_id_array, $value['tributesid']);	
				 }
				 $this->db->where_in('comment_id', $comment_id_array);
				 $this->db->delete('comment_like');
			}
			 $this->db->where('album_id', $album_id);
			 $this->db->where('user_id', $user_id);
			 $this->db->delete('pagerecordsdragdrop');
			 

			$this->db->select('textid');
			$this->db->from('textdetails');
			$this->db->where("album_id",$album_id);
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();	
			$result=$query->result_array();
			$image_id_array=array();
			if(!empty($result))
			{
				 foreach($result as $value)
				 {
				    $this->remove_perticular_text($album_id,$user_id,$value['textid']);		
				 }
			}
			
			$this->db->select('id');
			$this->db->from('albumimageuploaddetails');
			$this->db->where("albumid",$album_id);
			$query = $this->db->get();	
			$result=$query->result_array();
			$image_id_array=array();
			if(!empty($result))
			{
				 foreach($result as $value)
				 {
					//$value['id'];
				    $this->remove_perticular_image($album_id,$user_id,$value['id']);		
				 }
			}	
			 $this->db->where('albumid', $album_id);
             $this->db->where('user_id', $user_id);
             $this->db->delete('albumdetails');
             
             $this->db->where('album_id', $album_id);
             $this->db->where('user_id', $user_id);
             $this->db->delete('album_like'); 
             
             $this->db->where('albumid', $album_id);
             $this->db->where('user_id', $user_id);
             $this->db->delete('albuminnerrecordsdragdrop');
             
             $this->db->where('album_id', $album_id);
             $this->db->where('user_id', $user_id);
             $this->db->delete('tributesdetails');
             
			 $userdata['is_deleted']='1';
			 $userdata['modified_at']=date('Y-m-d H:i:s');
             $this->db->set($userdata);
		     $this->db->where('albumid',$album_id);
             $this->db->where('user_id',$user_id);
		     $this->db->update('albumvideouploaddetails');
			 return true;
     }
     public function remove_perticular_image($album_id,$user_id,$image_id)
     {
        
		$this->db->select('tributesid');
        $this->db->from('tributesdetails');
        $this->db->where("image_id",$image_id);
		$this->db->where('user_id', $user_id);
        $query = $this->db->get();	
        $result=$query->result_array();
		$comment_id_array=array();
        if(!empty($result))
        {
			 foreach($result as $value)
			 {
			     array_push($comment_id_array, $value['tributesid']);	
			 }
			 $this->db->where_in('comment_id', $comment_id_array);
			 $this->db->delete('comment_like');
		}
		 
		 $this->db->where('notify_type_id', $image_id);
		 $this->db->where('UID',$user_id);
         $this->db->like('notify_type', "%imageLike%");
         $this->db->like('notify_type', "%tagImage%");
         $this->db->delete('notificationdetails');
		 
		 
		 $this->db->where('image_id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('tributesdetails');
				
		 $this->db->where('image_id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('friendsTagContent');
         
         $this->db->where('image_id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->where('albumid', $album_id);
         $this->db->delete('albuminnerrecordsdragdrop');
         
		 $this->db->where('image_id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('image_like');
		 
		 $this->db->where('image_id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('pagerecordsdragdrop');
		 
		 $this->db->where('id', $image_id);
         $this->db->where('user_id', $user_id);
         $this->db->where('albumid', $album_id);
         $this->db->delete('albumimageuploaddetails');	
		 return true;
     }
     
     public function collect_album_data($album_id,$user_id)
     {
        $this->db->select("*");
		$this->db->from("albumdetails");
        $this->db->where('albumid',$album_id);
		$this->db->where('user_id',$user_id);
	    $query = $this->db->get();
		$album_array=$query->result_array();
		return $album_array;
     }
	 
	 public function collect_album_cover_image_data($album_id)
	 {
		$this->db->select("*");
		$this->db->from("albumimageuploaddetails");
        $this->db->where('albumid',$album_id);
		$this->db->where('is_cover_image','y');
	    $query = $this->db->get();
		return $query->result_array(); 
	 }
     
     public function collect_inner_image_data($album_id,$image_id,$user_id)
     {
        $this->db->select("*");
        $this->db->from("albumimageuploaddetails");
		$this->db->join('friendsTagContent', 'albumimageuploaddetails.id = friendsTagContent.image_id');
		$this->db->join('user', 'friendsTagContent.friends_id=user.userid');
        $this->db->where('albumimageuploaddetails.id',$image_id);
		$this->db->where('friendsTagContent.user_id',$user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();	
		//die('aaaa');
        if(empty($query->result_array()))
        {
          $this->db->select("*");
          $this->db->from("albumimageuploaddetails");
          $this->db->where('albumimageuploaddetails.id',$image_id);
          $query = $this->db->get();
          return $query->result_array();  
        }
		return $query->result_array();
        
     }
	 public function update_album($value)
	 {
		$userdata['title']=$value['title'];
         $userdata['description']=$value['desc'];
		 if($value['edit_background_image_id'])
		 {
		   $userdata['album_background_id']=$value['edit_background_image_id'];	 
		 }
		 $this->db->set($userdata);
         $this->db->where('albumid',$value['album_id']);
		 $this->db->where('user_id',$value['user_id']);
		 $this->db->update('albumdetails');
		 
		 if(!empty($value['image_id']))
		 {
			 $user_data['image_name']=$value['image_name'];	
			$user_data['uploadPath']=$value['uploadPath'];
			$user_data['modified_at']=$value['modified_at'];
			$this->db->set($user_data);
            $this->db->where('albumid',$value['album_id']);
		    $this->db->where('id',$value['image_id']);
		    $this->db->update('albumimageuploaddetails');
		 }
		 else
		 {
			$userdata1['albumid']=$value['album_id'];
            $userdata1['image_name']=$value['image_name'];
            $userdata1['uploadPath']=$value['uploadPath'];
		    $userdata1['created_at']=$value['modified_at']; 
		    $userdata1['is_cover_image']=$value['is_cover_image'];
			$this->db->set($userdata1);
			$this->db->insert('albumimageuploaddetails');
		 }			 
		 return true;	
	 }
	
	 public function update_album_details($value)
	 {
		/* echo "<pre>";
		 print_r($value);
		 die('frm model');*/
		 $userdata['title']=$value['title'];
         $userdata['description']=$value['desc'];
         $userdata['viewstatus']=$value['viewstatus'];
		 if(!empty($value['edit_background_image_id']))
		 {
		   $userdata['album_background_id']=$value['edit_background_image_id'];	 
		 }
         if(!empty($value['editAlbumForegroundStyleId']))
         {
             $userdata['album_foreground_id']=$value['editAlbumForegroundStyleId'];
         }
		 $this->db->set($userdata);
         $this->db->where('albumid',$value['album_id']);
		 $this->db->where('user_id',$value['user_id']);
		 $this->db->update('albumdetails');
		 
		 if(!empty($value['image_name']))
		 {
			 $user_data['image_name']=$value['image_name'];	
			$user_data['uploadPath']=$value['uploadPath'];
			$user_data['modified_at']=$value['modified_at'];
			$this->db->set($user_data);
            $this->db->where('albumid',$value['album_id']);
		    $this->db->where('id',$value['image_id']);
		    $this->db->update('albumimageuploaddetails');
			echo $this->db->last_query();
		 }
         if(empty($value['image_id']))
		 {
			$userdata1['albumid']=$value['album_id'];
			$userdata1['user_id']=$value['user_id'];
            if(!empty($value['image_name']))
			{
			 $userdata1['image_name']=$value['image_name'];	
			}
			else
			{
			  $userdata1['image_name']='no_cover.jpg';	
			}
			
			if(!empty($value['uploadPath']))
			{
			  $userdata1['uploadPath']=$value['uploadPath'];	
			}
			else
			{
			  $userdata1['uploadPath']='assets/img/';	
			}
			
			if(!empty($value['is_cover_image']))
			{
			 $userdata1['is_cover_image']=$value['is_cover_image'];	
			}
			else
			{
			 $userdata1['is_cover_image']='y';	
			}
			if(!empty($value['modified_at']))
			{
			 $userdata1['created_at']=$value['modified_at'];	
			}
			else
			{
			  $userdata1['created_at']=date('Y-m-d H:i:s');	
			}
			$this->db->set($userdata1);
			$this->db->insert('albumimageuploaddetails');
		 }			 
		 return true;	
	 }
     
	 public function selectCover_image($album_id,$user_id)
	 {
		 $this->db->select("*");
         $this->db->from("albumimageuploaddetails");
         $this->db->where('albumid',$album_id);
         $this->db->where('user_id',$user_id);
         $this->db->where('is_cover_image','y');
         $query = $this->db->get();
        return $query->result_array(); 
	 }
	 
     public function add_text_insert($value)
     {
         //echo "<pre>";
         //print_r($value);
         
        $userdata1['user_id']=$value['user_id'];
        $userdata1['title']=$value['text_title'];
        $userdata1['description']=$value['text_desc'];
        $userdata1['album_id']=$value['album_name']; 
        $userdata1['addeddate']=$value['created_at'];
        $this->db->set($userdata1);
        $this->db->insert('textdetails');
        //echo $this->db->last_query();
        return $this->db->insert_id(); 
     }
     
     public function getAlbumdetailsWidthTextuserwise($user_id,$album_id,$text_id)
     { 
         $this->db->select("*");
         $this->db->from("textdetails");
         $this->db->where('album_id',$album_id);
         $this->db->where('user_id',$user_id);
		 $this->db->where('textid',$text_id);
         $query = $this->db->get();
        return $query->result_array();
     }
     
     public function select_text($edit_text_id,$album_id,$user_id)
     {
         $this->db->select("*,textdetails.title as text_title,textdetails.description as text_description "); 
         $this->db->from("textdetails");
		 $this->db->join("albumdetails","`albumdetails`.`albumid`=`textdetails`.`album_id`");
         $this->db->join("user", "`user`.`userid` = `textdetails`.`user_id`");
		 $this->db->join("pagedetails", "`pagedetails`.`user_id` =  `textdetails`.`user_id`");
         $this->db->where('textdetails.album_id',$album_id);
         $this->db->where('textdetails.user_id',$user_id);
         $this->db->where('textdetails.textid',$edit_text_id);
         $this->db->where('`pagedetails`.`default_page`=1');
         $query = $this->db->get();
		 //echo $this->db->last_query();
         //die('aaaa');
		 return $query->result_array();
     }
     
     public function select_edit_text_details($edit_text_id,$album_id,$user_id)
     {
         $this->db->select("*");
        $this->db->from("textdetails");
        $this->db->join('friendsTagContent', 'textdetails.textid = friendsTagContent.text_id');
        $this->db->join('user', 'friendsTagContent.friends_id=user.userid');
        $this->db->where('textdetails.textid',$edit_text_id);
        $this->db->where('friendsTagContent.user_id',$user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();    
        //die('aaaa');
        if(empty($query->result_array()))
        {
          $this->db->select("*");
          $this->db->from("textdetails");
          $this->db->where('textdetails.textid',$edit_text_id);
          $query = $this->db->get();
          return $query->result_array();  
        }
        return $query->result_array();
     }
	 
	 public function select_total_like_text($edit_text_id)
	 {
		  $this->db->select("count(like_id) as total_like");
          $this->db->from("text_like");
          $this->db->where('text_like.text_id',$edit_text_id);
          $query = $this->db->get();
		  //echo $this->db->last_query();
          return $query->result_array(); 
	 }
	 
	 public function select_total_like_image($image_id)
	 {
		  $this->db->select("count(like_id) as total_like");
          $this->db->from("image_like");
          $this->db->where('image_like.image_id',$image_id);
          $query = $this->db->get();
		  //echo $this->db->last_query();
          return $query->result_array(); 
	 }
	 
	 public function search_text_Tag_friendName($user_id,$edit_text_id)
	 {
		    $this->db->select("`pagedetails`.`pageid` AS frdTagPageId, user.fullname AS frdTagname, user.userid AS frdTagId");
            $this->db->from("textdetails");
            $this->db->join("friendsTagContent","`textdetails`.`textid`=`friendsTagContent`.`text_id`");
            $this->db->join("user", "`user`.`userid` = `friendsTagContent`.`friends_id`");
            $this->db->join("pagedetails", "`user`.`userid` = `pagedetails`.`user_id`");
            $this->db->where("`textdetails`.`textid` =".$edit_text_id."
AND  `textdetails`.`user_id` =".$user_id." AND pagedetails.default_page=1");
            $query = $this->db->get();
            //echo $this->db->last_query();
            return $query->result_array();
	 }
	 
	 
	 
	 public function update_text($value)
	 {
		$user_data['title']=$value['edit_text_title'];
		$user_data['description']=$value['text_desc'];
		$user_data['album_id']=$value['album_id'];
		$this->db->set($user_data);
		//$this->db->where('album_id',$value['album_id']);
		$this->db->where('textid',$value['text_id']);
		$this->db->update('textdetails');
		$new_album_id['albumid']=$value['album_id'];
		$this->db->set($new_album_id);
		$this->db->where('text_id',$value['text_id']);
		$this->db->update('albuminnerrecordsdragdrop');
		//echo $this->db->last_query();
		//die();
        return true;		
	 }
	 public function update_video($value)
	 {
		/*echo "<pre>";
		print_r($value);
		die('frm model');*/
		$user_data['albumid']=$value['album_id'];
		$this->db->set($user_data);
		//$this->db->where('album_id',$value['album_id']);
		$this->db->where('id',$value['video_id']);
		$this->db->update('albumvideouploaddetails');
		$new_album_id['albumid']=$value['album_id'];
		$this->db->set($new_album_id);
		$this->db->where('video_id',$value['video_id']);
		$this->db->update('albuminnerrecordsdragdrop');
		//echo $this->db->last_query();
		//die();
        return true;		
	 }
	 
	 public function search_friendName($userid)
	 {
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where('user.userid !=', $userid);
		$this->db->join('friends','(friends.userid=user.userid OR friends.friendsid=user.userid)');
		$this->db->where(" (friends.userid=$userid OR friends.friendsid=$userid) ");
		$this->db->where('friends.relationshipstatus','accepted');
		$this->db->order_by("user.firstname", "asc");
		$this->db->distinct();
		$query=$this->db->get();
		return $query->result();
	 }
	 
     public function find_text_Tag_friendName($userid)
     {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where('user.userid !=', $userid);
        $this->db->join('friends','(friends.userid=user.userid OR friends.friendsid=user.userid)');
        $this->db->where(" (friends.userid=$userid OR friends.friendsid=$userid) ");
        $this->db->where('friends.relationshipstatus','accepted');
        $this->db->order_by("user.firstname", "asc");
        $this->db->distinct();
        $query=$this->db->get();
        return $query->result();
     }
     public function select_video_tagFrd_details($value)
     {
		 $this->db->select("`pagedetails`.`pageid` AS frdTagPageId, user.fullname AS frdTagname, user.userid AS frdTagId"); 
         $this->db->from("albumvideouploaddetails");
		 $this->db->join("friendsTagContent","`albumvideouploaddetails`.`id`=`friendsTagContent`.`video_id`");
         $this->db->join("user", "`user`.`userid`=`friendsTagContent`.`friends_id`");
		 $this->db->join("pagedetails", "`pagedetails`.`user_id`=`friendsTagContent`.`friends_id`");
         $this->db->where('`albumvideouploaddetails`.`user_id`',$value['user_id']);
         $this->db->where('`albumvideouploaddetails`.`id`',$value['video_id']);
         $this->db->where('`albumvideouploaddetails`.`albumid`',$value['album_id']);
         $this->db->where('`pagedetails`.`default_page`=1');
         $query = $this->db->get(); 
		 //echo $this->db->last_query();
		 $result=$query->result_array();
		 return $result;	
	 }     
     
	 public function select_video_details_in_popup($value)
	 {
		 $this->db->select("*"); 
         $this->db->from("albumvideouploaddetails");
		 $this->db->join("user", "`user`.`userid`=`albumvideouploaddetails`.`user_id`");
		 $this->db->join("albumdetails", "`albumdetails`.`albumid`=`albumvideouploaddetails`.`albumid`");
		 $this->db->join("pagedetails", "`pagedetails`.`user_id`=`albumvideouploaddetails`.`user_id`");
         $this->db->where('`albumvideouploaddetails`.`user_id`',$value['user_id']);
         $this->db->where('`albumvideouploaddetails`.`id`',$value['video_id']);
         $this->db->where('`albumvideouploaddetails`.`albumid`',$value['album_id']);
         $this->db->where('`pagedetails`.`default_page`=1');
         $query = $this->db->get(); 
		 //echo $this->db->last_query();
		 $result=$query->result_array();
		 return $result; 
	 }
     
     
	 public function insertfrd_tag($value)
	 {
		 /*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $value['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
        //echo $this->db->last_query();
        //print_r($result[0]['fullname']);
        //die('aaa');
         for($i=0;$i<$value['total_frd_tag'];$i++)
		 {
			$userdata1['image_id']=$value['image_id'];
            $userdata1['user_id']=$value['user_id'];
            $userdata1['friends_id']=$value['frd_ids'][$i];
		    $userdata1['created_at']=$value['created_at']; 
		    $this->db->set($userdata1);
			 $this->db->insert('friendsTagContent');
            //echo $this->db->last_query();
			
            $userdata2['notify_type_id']=$value['image_id'];
            $userdata2['UID']=$value['user_id'];
            $userdata2['FID']=$value['frd_ids'][$i];
		    $userdata2['notificationdate']=$value['created_at']; 
		    $userdata2['notify_type']='tagImage'; 
		    $userdata2['notify_details']=' Tagged you  on a Image';
			$this->db->set($userdata2);
			 $this->db->insert('notificationdetails');
            //echo $this->db->last_query();
				
		 }
		 return true;
	 }
	 
	 public function remove_perticular_text($album_id,$user_id,$text_id)
	 {
		 
		$this->db->select('tributesid');
        $this->db->from('tributesdetails');
        $this->db->where("text_id",$text_id);
		$this->db->where('user_id', $user_id);
        $query = $this->db->get();	
        $result=$query->result_array();
		$comment_id_array=array();
        if(!empty($result))
        {
			 foreach($result as $value)
			 {
			     array_push($comment_id_array, $value['tributesid']);	
			 }
			 $this->db->where_in('comment_id', $comment_id_array);
			 $this->db->delete('comment_like');
		}
		 
		 $this->db->where('notify_type_id', $text_id);
		 $this->db->where('UID',$user_id);
         $this->db->like('notify_type', "%textLike%");
         $this->db->like('notify_type', "%tagText%");
         $this->db->delete('notificationdetails');
		 
		 $this->db->where('text_id', $text_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('text_like');
		
		 $this->db->where('text_id', $text_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('pagerecordsdragdrop');

		 $this->db->where('textid', $text_id);
         $this->db->where('album_id', $album_id);
         $this->db->delete('textdetails');
		 
		 $this->db->where('text_id', $text_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('friendsTagContent');
                
         $this->db->where('text_id', $text_id);
         $this->db->where('user_id', $user_id);
         $this->db->where('albumid', $album_id);
         $this->db->delete('albuminnerrecordsdragdrop');
              
         $this->db->where('text_id', $text_id);
         $this->db->where('user_id', $user_id);
         $this->db->delete('tributesdetails');
         
         return true;
 
	 }
	 
	 public function friend_list_userwise($user_id)
	 {
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where('user.userid !=', $user_id);
		$this->db->join('friends','(friends.userid=user.userid OR friends.friendsid=user.userid)');
		$this->db->where(" (friends.userid=$user_id OR friends.friendsid=$user_id) ");
        $this->db->where('friends.relationshipstatus','accepted');
		$this->db->where('user.userstype="user"');
        $this->db->order_by("user.firstname", "asc");
		$this->db->distinct();
		$query=$this->db->get();
		//echo $this->db->last_query();
        //die('aaaaaaa');
		return $query->result();
	 }
	 
	 public function insertfrd_tag_text($value)
	 {
	    /*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $value['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
         
         for($i=0;$i<$value['total_frd_tag'];$i++)
         {
            $userdata1['text_id']=$value['text_id'];
            $userdata1['user_id']=$value['user_id'];
            $userdata1['friends_id']=$value['frd_ids'][$i];
            $userdata1['created_at']=$value['created_at']; 
            $this->db->set($userdata1);
            $this->db->insert('friendsTagContent');
            //echo $this->db->last_query();
            
            $userdata2['notify_type_id']=$value['text_id']; 
            $userdata2['UID']=$value['user_id'];
            $userdata2['FID']=$value['frd_ids'][$i];
            $userdata2['notificationdate']=$value['created_at']; 
            $userdata2['notify_type']='tagText'; 
            $userdata2['notify_details']=' Tagged you  on a Text';
            $this->db->set($userdata2);
             $this->db->insert('notificationdetails');
                
         }
         return true;
	 }
	 
	 public function edit_tag_friend_list_text($tag_friend_name,$text_id)
	{
		/*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $tag_friend_name['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
				
		if($text_id)
		{
		 $this->db->where('text_id', $text_id);
		 $this->db->where('user_id', $tag_friend_name['user_id']);
         $this->db->delete('friendsTagContent');
		 
		 $this->db->where('notify_type_id', $text_id);
		 $this->db->where('UID', $tag_friend_name['user_id']);
         $this->db->delete('notificationdetails');
		 
		}
	
		for($i=0;$i<$tag_friend_name['total_frd_tag'];$i++)
		 {
			$userdata1['text_id']=$text_id;
            $userdata1['user_id']=$tag_friend_name['user_id'];
            $userdata1['friends_id']=$tag_friend_name['frd_ids'][$i];
		    $userdata1['created_at']=$tag_friend_name['created_at']; 
		    $this->db->set($userdata1);
             $this->db->insert('friendsTagContent');
            //echo $this->db->last_query();			
			
			$userdata2['notify_type_id']=$text_id; 
            $userdata2['UID']=$tag_friend_name['user_id'];
            $userdata2['FID']=$tag_friend_name['frd_ids'][$i];;
            $userdata2['notificationdate']=$tag_friend_name['created_at']; 
            $userdata2['notify_type']='tagText'; 
            $userdata2['notify_details']=' Tagged you  on a Text';
            $this->db->set($userdata2);
            $this->db->insert('notificationdetails');
			
			
		 }
		return true;
	}
	public function select_background_images()
    {
	     $this->db->select("*");
         $this->db->from("album_background_style");
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();		
	}
    public function select_foreground_image($foreground_image_id)
    {
        $this->db->select("*");
        $this->db->from("albumForegroundStyle");
        $this->db->where('id',$foreground_image_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_foreground_images($albumBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("albumForegroundStyle");
        $this->db->where('albumBackgroundStyleId',$albumBackgroundStyleId);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_page_foreground_images($pageBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("pageForegroundStyle");
        $this->db->where('pageBackgroundStyleId',$pageBackgroundStyleId);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_background_image($background_id)
    {
		$this->db->select("*");
        $this->db->from("album_background_style");
		$this->db->where('id',$background_id);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();
	}
	public function select_page_background_images()
	{
		$this->db->select("*");
		$this->db->from("page_background_style");
		$query = $this->db->get();
		return $query->result();
	} 
    public function insert_video_details($value)
    {
		/*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $value['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
        
        $userdata1['user_id']=$value['user_id'];
		$userdata1['albumid']=$value['album_id'];
		$userdata1['video_title']=$value['video_title'];
		$userdata1['video_desc']=$value['video_desc'];
		$userdata1['vimeo_video_id']=$value['video_id'];
		$userdata1['created_at']=$value['created_at'];
		$this->db->set($userdata1);
		$this->db->insert('albumvideouploaddetails');
		//echo $this->db->last_query();
		$db_video_id=$this->db->insert_id(); 
		if($db_video_id)
		{
			 for($i=0;$i<$value['total_frd_tag'];$i++)
			 {
				$video_tag['video_id']=$db_video_id;
				$video_tag['user_id']=$value['user_id'];
				$video_tag['created_at']=$value['created_at'];
				$video_tag['friends_id']=$value['video_friends_tag'][$i];
				$this->db->set($video_tag);
                $this->db->insert('friendsTagContent');
				
				$userdata2['notify_type_id']=$db_video_id; 
                $userdata2['UID']=$value['user_id'];
                $userdata2['FID']=$value['video_friends_tag'][$i];
                $userdata2['notificationdate']=$value['created_at']; 
                $userdata2['notify_type']='tagVideo'; 
                $userdata2['notify_details']='Tagged you  on a Video';
                $this->db->set($userdata2);
                $this->db->insert('notificationdetails');
				 
			 }
		}
        return $db_video_id;
	}
	public function showCommentsAlbumImage($image_id)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.image_id',$image_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function showCommentsAlbumText($text_id)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.text_id',$text_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function showCommentsAlbumVideo($video_id)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.video_id',$video_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
	public function showCommentsAlbum($album_id)
	{
		$this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.album_id',$album_id);
        $query = $this->db->get();
		//echo $this->db->last_query();
		//die('aaaaa');
        return $query->result();
	}
	public function CommentsAlbum($value)
	{
	    $this->db->insert('tributesdetails',$value);
		//echo $this->db->last_query();
		//die('aaa');
	}
	//for pinging start
	public function selectAllPageList($user_id)
	{
		$this->db->select("*");
		$this->db->from('pagedetails');
		$this->db->where("user_id",$user_id);
		$this->db->order_by("pageid", "asc");
		$query=$this->db->get();
		//echo $this->db->last_query();
        return $query->result_array();
	}
	
	public function insert_page_id($user_id,$album_id,$page_id)
	{
	    $this->db->select("page_id");
		$this->db->from('pagerecordsdragdrop');
		$this->db->where("user_id",$user_id);
		$this->db->where("album_id",$album_id);
		$query=$this->db->get();
        $result=$query->result_array();
		if(!empty($result))
		{

            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata1['row']=$maxRow+1;
                $userdata1['col']=1;
            }
            else {
                $userdata1['row']=$maxRow;
                $userdata1['col']=$maxCol+1;
            }
            $userdata1['page_id']=$page_id;
			$this->db->set($userdata1);
			$this->db->where("user_id",$user_id);
		    $this->db->where("album_id",$album_id);
			$this->db->update('pagerecordsdragdrop');
		}
		else
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata2['row']=$maxRow+1;
                $userdata2['col']=1;
            }
            else {
                $userdata2['row']=$maxRow;
                $userdata2['col']=$maxCol+1;
            }

            $userdata2['user_id']=$user_id;
			$userdata2['album_id']=$album_id;
			$userdata2['page_id']=$page_id;
			$this->db->set($userdata2);
			$this->db->insert('pagerecordsdragdrop');
		}
		/*if($query->row()->album_page_id===0)
		{
		    $userdata1['album_page_id']=$page_id;
			$this->db->set($userdata1);
			$this->db->where("user_id",$user_id);
		    $this->db->where("albumid",$album_id);
			$this->db->update('albumdetails');
		}
		else
		{
		    $user_data['album_page_id']=$page_id;
			$this->db->set($user_data);
            $this->db->where("user_id",$user_id);
		    $this->db->where("albumid",$album_id);
		    $this->db->update('albumdetails');	
		}*/
        		
	}
	public function insert_page_id_to_image($image_id,$user_id,$page_id)
	{
	    $this->db->select("page_id");
		$this->db->from('pagerecordsdragdrop');
		$this->db->where("user_id",$user_id);
		$this->db->where("image_id",$image_id);
		$query=$this->db->get();
        $result=$query->result_array();
		if(!empty($result))
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata1['row']=$maxRow+1;
                $userdata1['col']=1;
            }
            else {
                $userdata1['row']=$maxRow;
                $userdata1['col']=$maxCol+1;
            }

			$userdata1['page_id']=$page_id;
			$this->db->set($userdata1);
			$this->db->where("user_id",$user_id);
		    $this->db->where("image_id",$image_id);
			$this->db->update('pagerecordsdragdrop');
		}
		else
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata2['row']=$maxRow+1;
                $userdata2['col']=1;
            }
            else {
                $userdata2['row']=$maxRow;
                $userdata2['col']=$maxCol+1;
            }
		    $userdata2['user_id']=$user_id;
			$userdata2['image_id']=$image_id;
			$userdata2['page_id']=$page_id;
			$this->db->set($userdata2);
			$this->db->insert('pagerecordsdragdrop');
		}
	}
	public function insert_page_id_to_video($video_id,$user_id,$page_id)
	{
	    $this->db->select("page_id");
		$this->db->from('pagerecordsdragdrop');
		$this->db->where("user_id",$user_id);
		$this->db->where("video_id",$video_id);
		$query=$this->db->get();
        $result=$query->result_array();
		if(!empty($result))
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata1['row']=$maxRow+1;
                $userdata1['col']=1;
            }
            else {
                $userdata1['row']=$maxRow;
                $userdata1['col']=$maxCol+1;
            }
			$userdata1['page_id']=$page_id;
			$this->db->set($userdata1);
			$this->db->where("user_id",$user_id);
		    $this->db->where("video_id",$video_id);
			$this->db->update('pagerecordsdragdrop');
		}
		else
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata2['row']=$maxRow+1;
                $userdata2['col']=1;
            }
            else {
                $userdata2['row']=$maxRow;
                $userdata2['col']=$maxCol+1;
            }
		    $userdata2['user_id']=$user_id;
			$userdata2['video_id']=$video_id;
			$userdata2['page_id']=$page_id;
			$this->db->set($userdata2);
			$this->db->insert('pagerecordsdragdrop');
		}
        		
	}
	public function insert_page_id_to_text($text_id,$user_id,$page_id)
	{
	    $this->db->select("page_id");
		$this->db->from('pagerecordsdragdrop');
		$this->db->where("user_id",$user_id);
		$this->db->where("text_id",$text_id);
		$query=$this->db->get();
        $result=$query->result_array();
		if(!empty($result))
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata1['row']=$maxRow+1;
                $userdata1['col']=1;
            }
            else {
                $userdata1['row']=$maxRow;
                $userdata1['col']=$maxCol+1;
            }
			$userdata1['page_id']=$page_id;
			$this->db->set($userdata1);
			$this->db->where("user_id",$user_id);
		    $this->db->where("text_id",$text_id);
			$this->db->update('pagerecordsdragdrop');
		}
		else
		{
            $this->db->select('max(row) as maxRow');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $query = $this->db->get();
            $maxRow =$query->row('maxRow');

            $this->db->select('max(col) as maxCol');
            $this->db->from('pagerecordsdragdrop');
            $this->db->where('user_id', $user_id);
            $this->db->where('page_id', $page_id);
            $this->db->where('row', $maxRow);
            $query = $this->db->get();
            $maxCol =$query->row('maxCol');

            if($maxCol==5)
            {
                $userdata2['row']=$maxRow+1;
                $userdata2['col']=1;
            }
            else {
                $userdata2['row']=$maxRow;
                $userdata2['col']=$maxCol+1;
            }
		    $userdata2['user_id']=$user_id;
			$userdata2['text_id']=$text_id;
			$userdata2['page_id']=$page_id;
			$this->db->set($userdata2);
			$this->db->insert('pagerecordsdragdrop');
		}
	}
	//pinging end
    /* following is used for home page of the project ---Start*/
    
    public function get_total_records()
    {
        $this->db->select('count(*) as total_image');
        $this->db->from('albuminnerrecordsdragdrop');
        $query=$this->db->get();
        //return $query->result_array();
        return $query->row();  
    }
    
    public function getAlbum_records_details($start)
    {
       $this->db->select("*");
       $this->db->from("albuminnerrecordsdragdrop");
       $this->db->limit(5,$start);
       $query = $this->db->get();
       //echo $this->db->last_query();
       //die('aaaa');
       return $query->result_array();  
    }
    
    /* end*/
	public function get_all_image_details()
	{
		$this->db->select("*");
		$this->db->from('albumdetails');
		$this->db->join('albumimageuploaddetails','albumdetails.albumid=albumimageuploaddetails.albumid');
		$this->db->where("albumdetails.viewstatus='all'");
		$this->db->order_by("albumimageuploaddetails.img_order", "asc");
		$query=$this->db->get();
        return $query->result_array();
		//return $query->row();;
	}
	public function getAlbumdetailsWidthvideouserwise($user_id,$album_id,$video_id)
	{
	    $this->db->select("*");
        $this->db->from("albumvideouploaddetails");
		$this->db->where('albumid',$album_id);
		$this->db->where('user_id',$user_id);
		$this->db->where('id',$video_id);
		$this->db->where('is_deleted','0');
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();	
	}
	
	public function select_video($edit_video_id,$album_id,$user_id)
	{
		$this->db->select("*");
         $this->db->from("albumvideouploaddetails");
         $this->db->where('albumid',$album_id);
         $this->db->where('user_id',$user_id);
         $this->db->where('id',$edit_video_id);
         $query = $this->db->get();
		 //echo $this->db->last_query();
         return $query->result_array(); 
	}
	
	public function search_video_Tag_friendName($user_id,$edit_video_id)
    {
		 $this->db->select("*");
         $this->db->from("friendsTagContent");
         $this->db->join('user','`user`.`userid` =  `friendsTagContent`.`friends_id`');
		 $this->db->where('friendsTagContent.user_id',$user_id);
         $this->db->where('friendsTagContent.video_id',$edit_video_id);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();

		
	}	
	
	public function edit_tag_friend_list_video($frd_tag)
	{
		/*$this->db->select('fullname');
        $this->db->from('user');
        $this->db->where('userid', $tag_friend_name['user_id']);
        $query = $this->db->get();    
        $result=$query->result_array();
        $user_fullname_array=$result[0]['fullname'];*/
		
		if($frd_tag['video_id'])
		{
		  $this->db->where('video_id', $frd_tag['video_id']);
		  $this->db->where('user_id', $frd_tag['user_id']);
          $this->db->delete('friendsTagContent');

          $this->db->where('notify_type','tagVideo');
		  $this->db->where('notify_type_id', $frd_tag['video_id']);
		  $this->db->where('UID', $frd_tag['user_id']);
          $this->db->delete('notificationdetails');
		}
	
		for($i=0;$i<$frd_tag['total_frd_tag'];$i++)
		 {
			$userdata1['video_id']=$frd_tag['video_id'];
            $userdata1['user_id']=$frd_tag['user_id'];
            $userdata1['friends_id']=$frd_tag['frd_ids'][$i];
		    $userdata1['created_at']=$frd_tag['created_at']; 
		    $this->db->set($userdata1);
             $this->db->insert('friendsTagContent');
            //echo $this->db->last_query();
		    
			$userdata2['notify_type_id']=$frd_tag['video_id'];
            $userdata2['UID']=$frd_tag['user_id'];
            $userdata2['FID']=$frd_tag['frd_ids'][$i];
		    $userdata2['notificationdate']=$frd_tag['created_at']; 
		    $userdata2['notify_type']='tagVideo';
		    $userdata2['notify_details']=' Tagged you  on a Video';
			$this->db->set($userdata2);
			 $this->db->insert('notificationdetails');		
		 }
		return true;
	}
    public function delete_video($album_id,$video_id,$user_id)
	{
		$userdata['is_deleted']='1';
		$userdata['modified_at']=date('Y-m-d H:i:s');
        $this->db->set($userdata);
		$this->db->where('albumid',$album_id);
        $this->db->where('id',$video_id);
        $this->db->where('user_id',$user_id);
		$this->db->update('albumvideouploaddetails');

        $this->db->select('tributesid');
        $this->db->from('tributesdetails');
        $this->db->where("video_id",$video_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $result=$query->result_array();
        $comment_id_array=array();
        if(!empty($result))
        {
            foreach($result as $value)
            {
                array_push($comment_id_array, $value['tributesid']);
            }
            $this->db->where_in('comment_id', $comment_id_array);
            $this->db->delete('comment_like');
        }
        $this->db->where('notify_type_id', $video_id);
        $this->db->where('UID',$user_id);
        $this->db->like('notify_type', "%videoLike%");
        $this->db->like('notify_type', "%tagVideo%");
        $this->db->delete('notificationdetails');

        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('video_like');

        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('pagerecordsdragdrop');

        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('friendsTagContent');

        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('albumid', $user_id);
        $this->db->delete('albuminnerrecordsdragdrop');

        $this->db->where('video_id', $video_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('tributesdetails');



        return true;
	}
    public function getdashboardImageDetails($image_id)
	{
	     $this->db->select("*");
         $this->db->from("albumimageuploaddetails");
         $this->db->where('albumimageuploaddetails.id',$image_id);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();
	}
	public function addPage($data)
	{
		 $this->db->insert('pagedetails',$data);
	}
	
	public function selectCurrentPageforText($user_id,$text_id)
	{
	     $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("text_id",$text_id);
         $this->db->where("user_id",$user_id);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		 //die('aaaaa');
         return $query->row();  
	}
	public function selectCurrentPageforvideo($user_id,$video_id)
	{
	     $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("video_id",$video_id);
         $this->db->where("user_id",$user_id);
         $query = $this->db->get();
         return $query->row();  
	}
	public function selectCurrentPageforimage($user_id,$image_id)
	{
	     $this->db->select('page_id');
         $this->db->from('pagerecordsdragdrop');
         $this->db->where("image_id",$image_id);
         $this->db->where("user_id",$user_id);
         $query = $this->db->get();
         return $query->row();  
	}
	public function check_album_user($user_id,$album_id)
    {
        $this->db->select('*');
        $this->db->from('albumdetails');
        $this->db->where("albumid",$album_id);
        $this->db->where("user_id",$user_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_next_image_id($image_id,$album_id,$userId)
    {
        $this->db->select("id");
        $this->db->from("albumimageuploaddetails");
        $this->db->where("id=(select max(id) from albumimageuploaddetails where id < '".$image_id."' and albumid='".$album_id."' and user_id='".$userId."' and is_cover_image='n')");
        $query= $this->db->get();
        return $query->row('id');
    }
    public function get_prev_image_id($image_id,$album_id,$userId)
    {
        $this->db->select("id");
        $this->db->from("albumimageuploaddetails");
        $this->db->where("id=(select min(id) from albumimageuploaddetails where id > '".$image_id."' and albumid='".$album_id."' and user_id='".$userId."' and is_cover_image='n')");
        $query= $this->db->get();
        return $query->row('id');
    }
    public function get_next_video_id($video_id,$album_id,$userId)
    {
        $this->db->select("id");
        $this->db->from("albumvideouploaddetails");
        $this->db->where("id=(select max(id) from albumvideouploaddetails where id < '".$video_id."' and albumid='".$album_id."' and user_id='".$userId."' and 	is_deleted=0)");
        $query= $this->db->get();
        return $query->row('id');
    }
    public function get_prev_video_id($video_id,$album_id,$userId)
    {
        $this->db->select("id");
        $this->db->from("albumvideouploaddetails");
        $this->db->where("id=(select min(id) from albumvideouploaddetails where id > '".$video_id."' and albumid='".$album_id."' and user_id='".$userId."' and 	is_deleted=0)");
        $query= $this->db->get();
        return $query->row('id');
    }
    public function get_next_text_id($text_id,$album_id,$userId)
    {
        $this->db->select("textid");
        $this->db->from("textdetails");
        $this->db->where("textid=(select max(textid) from textdetails where textid < '".$text_id."' and album_id='".$album_id."' and user_id='".$userId."')");
        $query= $this->db->get();
        return $query->row('textid');
    }
    public function get_prev_text_id($text_id,$album_id,$userId)
    {
        $this->db->select("textid");
        $this->db->from("textdetails");
        $this->db->where("textid=(select min(textid) from textdetails where textid > '".$text_id."' and album_id='".$album_id."' and user_id='".$userId."')");
        $query= $this->db->get();
        return $query->row('textid');
    }
    public function fetchAllImageInAlbum($user_id,$album_id)
    {
        $this->db->select("*");
        $this->db->from("albumimageuploaddetails");
        $this->db->where('albumid',$album_id);
        $this->db->where('user_id',$user_id);
        $this->db->where('is_cover_image','n');
        $query=$this->db->get();
        return $query->result();
    }
    public function fetchAlbumDetails($album_id)
    {
        $this->db->select("*");
        $this->db->from("albumdetails");
        $this->db->where('albumid',$album_id);
        $query=$this->db->get();
        return $query->row_array();
    }
}