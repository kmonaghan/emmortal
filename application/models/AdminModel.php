<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
	}
	
    public function getAdminuserDetails($user_id)
    {
         $this->db->select("*");
        $this->db->from("user");
        $this->db->where('userid',$user_id);
        $query = $this->db->get();
        return $query->result_array();   
    }
    
    
	public function getUserListDetails()
	{
		$this->db->select("*");
		$this->db->from("user");
		$query = $this->db->get();
		return $query->result_array();

	}
    
    public function get_userDetails($limit, $start)
    { 
        $this->db->select("*");
        $this->db->from("user");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false; 
    }
    public function get_albumStyleDetails($limit, $start)
    { 
        $this->db->select("*");
        $this->db->from("album_background_style");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false; 
    }
    public function get_pagestyleDetails($limit, $start)
    { 
        $this->db->select("*");
        $this->db->from("page_background_style");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false; 
    }
	public function get_supportquestionDetails($limit, $start)
    { 
        $this->db->select("*");
        $this->db->from("supportquestion");
		$this->db->join('user','`user`.`userid`=`supportquestion`.`userid`');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false; 
    }
	
	public function get_deletevideoDetails($limit, $start)
    { 
        $where="albumvideouploaddetails.is_deleted ='1'";
		$this->db->select("*");
        $this->db->from("albumvideouploaddetails");
		$this->db->join('user','`user`.`userid`=`albumvideouploaddetails`.`user_id`');
		$this->db->where($where);
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }else
		{
		  return false;	
		}
         
    }
	
    
    public function album_background_image($value)
    {
         $userdata['image_name']=$value['image_name'];
         $userdata['upload_path']=$value['uploadPath'];
         $userdata['created_at']=$value['created_at'];
         $this->db->set($userdata);
         $this->db->insert('album_background_style');
         return $this->db->insert_id();
    }
    public function addAlbumForegroundStyle($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['created_at']=$value['created_at'];
        $userdata['albumBackgroundStyleId']=$value['albumBackgroundStyleId'];
        $this->db->set($userdata);
        $this->db->insert('albumForegroundStyle');
        return $this->db->insert_id();
    }
    public function addPageForegroundStyle($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['created_at']=$value['created_at'];
        $userdata['pageBackgroundStyleId']=$value['pageBackgroundStyleId'];
        $this->db->set($userdata);
        $this->db->insert('pageForegroundStyle');
        return $this->db->insert_id();
    }
    public function addBackgroundStyle($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['created_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->insert('backgroundStyle');
        return $this->db->insert_id();
    }
    public function addDefaultForegroundStyle($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['created_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->insert('foregroundStyle');
        return $this->db->insert_id();
    }
    public function page_background_image($value)
    {
         $userdata['image_name']=$value['image_name'];
         $userdata['upload_path']=$value['uploadPath'];
         $userdata['created_at']=$value['created_at'];
         $this->db->set($userdata);
         $this->db->insert('page_background_style');
         return $this->db->insert_id();
    }
    
    public function getAlbumBackgroundStyle()
    {
       $this->db->select("*");
       $this->db->from("album_background_style");
       $this->db->order_by('created_at','desc');
       $query = $this->db->get();
       return $query->result_array(); 
    }
    public function getPageForegroundStyle()
    {
        $this->db->select("pfs.*,pbs.id as page_back_id,pbs.image_name as page_image_name,pbs.upload_path as page_upload_path");
        $this->db->from("pageForegroundStyle pfs");
        $this->db->join('page_background_style pbs','pfs.pageBackgroundStyleId=pbs.id');
        $this->db->order_by('pfs.created_at','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getforegroundStyle()
    {
        $this->db->select("fs.*,abs.id as album_back_id,abs.image_name as album_image_name,abs.upload_path as album_upload_path");
        $this->db->from("albumForegroundStyle fs");
        $this->db->join('album_background_style abs','fs.albumBackgroundStyleId=abs.id');
        $this->db->order_by('fs.created_at','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getDefaultForegroundStyle()
    {
        $this->db->select("*");
        $this->db->from("foregroundStyle");
        $this->db->order_by('created_at','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getBackgroundStyle()
    {
        $this->db->select("*");
        $this->db->from("backgroundStyle");
        $this->db->order_by('created_at','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
     public function getPageBackgroundStyle()
    {
       $this->db->select("*");
       $this->db->from("page_background_style");
       $this->db->order_by('created_at','desc');
       $query = $this->db->get();
       return $query->result_array(); 
    }
    
	public function total_deleted_video()
	{
		 $where="albumvideouploaddetails.is_deleted ='1'";
		 $this->db->select("*");
         $this->db->from("albumvideouploaddetails");
         $this->db->join('user','`user`.`userid`=`albumvideouploaddetails`.`user_id`');
		 $this->db->where($where);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();
	}
	
	public function total_user_given_question()
	{
		 $this->db->select("*");
         $this->db->from("supportquestion");
         $this->db->join('user','`user`.`userid`=`supportquestion`.`userid`');
		 $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();
	}
	public function select_question_details($qid)
	{
		 $this->db->select("*");
         $this->db->from("supportquestion");
         $this->db->where('id',$qid);
		 $query = $this->db->get();
		 //echo $this->db->last_query();
		return $query->result_array();
	}
	public function delete_question($qid)
	{
	    $this->db->where('id', $qid);
		$this->db->delete('supportquestion');
        return true;		
	}
	public function delete_page_background($background_image_id)
	{
        $this->db->where('pageBackgroundStyleId', $background_image_id);
        $this->db->delete('pageForegroundStyle');

	    $this->db->where('id', $background_image_id);
		$this->db->delete('page_background_style');
        return true;		
	}
	public function delete_album_background($background_image_id)
	{
	    $this->db->where('id', $background_image_id);
		$this->db->delete('album_background_style');
        //echo $this->db->last_query();
		//die('aaaa');
		
		return true;		
	}
    public function delete_album_forground_style($delete_forground_style)
    {
        $this->db->where('id', $delete_forground_style);
        $this->db->delete('albumForegroundStyle');
        //echo $this->db->last_query();
        //die('aaaa');

        return true;
    }
    public function delete_page_foreground_style($forground_style)
    {
        $this->db->where('id', $forground_style);
        $this->db->delete('pageForegroundStyle');
        return true;
    }
    public function delete_background_style($delete_background_style)
    {
        $this->db->where('id', $delete_background_style);
        $this->db->delete('backgroundStyle');
        //echo $this->db->last_query();
        //die('aaaa');

        return true;
    }
    public function delete_default_foreground_style($foregroundStyleId)
    {
        $this->db->where('id', $foregroundStyleId);
        $this->db->delete('foregroundStyle');
        //echo $this->db->last_query();
        //die('aaaa');

        return true;
    }
	public function update_page_background_image($value)
	{
	   	$userdata['image_name']=$value['image_name'];
		$userdata['upload_path']=$value['uploadPath'];
		$userdata['modified_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->where('id',$value['background_image_id']);
  	    $this->db->update('page_background_style');
        return true;		
	}
    public function update_default_foreground_style($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['modified_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->where('id',$value['editForegroundStyleId']);
        $this->db->update('foregroundStyle');
        return true;
    }
    public function update_album_foreground_style($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['modified_at']=$value['created_at'];
        $userdata['albumBackgroundStyleId']=$value['albumBackgroundStyleId'];
        $this->db->set($userdata);
        $this->db->where('id',$value['foregroundStyleId']);
        $this->db->update('albumForegroundStyle');
        return true;
    }
    public function update_page_foreground_style($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['modified_at']=$value['created_at'];
        $userdata['pageBackgroundStyleId']=$value['pageBackgroundStyleId'];
        $this->db->set($userdata);
        $this->db->where('id',$value['foregroundStyleId']);
        $this->db->update('pageForegroundStyle');
        return true;
    }
    public function update_background_style($value)
    {
        $userdata['image_name']=$value['image_name'];
        $userdata['upload_path']=$value['uploadPath'];
        $userdata['modified_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->where('id',$value['editBackgroundStyleId']);
        $this->db->update('backgroundStyle');
        return true;
    }
	public function update_album_background_image($value)
	{
	   	$userdata['image_name']=$value['image_name'];
		$userdata['upload_path']=$value['uploadPath'];
		$userdata['modified_at']=$value['created_at'];
        $this->db->set($userdata);
        $this->db->where('id',$value['background_image_id']);
  	    $this->db->update('album_background_style');
        return true;		
	}
	
	public function delete_video($value)
	{
		 $this->db->select("id");
         $this->db->from("albumvideouploaddetails");
         $this->db->where('vimeo_video_id',$value);
         $query = $this->db->get();
		 //echo $this->db->last_query();
		 $result=$query->result_array();
		 $video_id='';
		 if(!empty($result))
         {
			 foreach($result as $value)
			 {
				$video_id=$value['id']; 
			 }
			 
		$this->db->select('tributesid');
        $this->db->from('tributesdetails');
        $this->db->where("video_id",$video_id);
		$query = $this->db->get();	
        $result2=$query->result_array();
		$comment_id_array=array();
        if(!empty($result2))
        {
			 foreach($result2 as $value)
			 {
			     array_push($comment_id_array, $value['tributesid']);	
			 }
			 $this->db->where_in('comment_id', $comment_id_array);
			 $this->db->delete('comment_like');
		}
		 $this->db->where("video_id",$video_id);
         $this->db->delete('tributesdetails');
				
		          
         $this->db->where("video_id",$video_id);
         $this->db->delete('albuminnerrecordsdragdrop');
         
		 $this->db->where("video_id",$video_id);
         $this->db->delete('video_like');
		 
		 $this->db->where('video_id', $video_id);
         $this->db->delete('pagerecordsdragdrop');
		 
		 $this->db->where('video_id', $video_id);
		 $this->db->delete('friendsTagContent');
			 
		 $this->db->where('video_id', $video_id);
		 $this->db->delete('video_like');
			 
		 $this->db->where('id', $video_id);
		 $this->db->delete('albumvideouploaddetails');
		 return true;
		 }			 
		
		
	}
	public function total_foreground_style()
    {
        $this->db->select("count(*) as total_foreground_style");
        $this->db->from("albumForegroundStyle");
        $query = $this->db->get();
        return $query->row('total_foreground_style');
    }
    public function total_default_foreground_style()
    {
        $this->db->select("count(*) as total_foreground_style");
        $this->db->from("foregroundStyle");
        $query = $this->db->get();
        return $query->row('total_foreground_style');
    }
    public function total_page_foreground_style()
    {
        $this->db->select("count(*) as total_foreground_style");
        $this->db->from("pageForegroundStyle");
        $query = $this->db->get();
        return $query->row('total_foreground_style');
    }
    public function total_background_style()
    {
        $this->db->select("count(*) as total_background_style");
        $this->db->from("backgroundStyle");
        $query = $this->db->get();
        return $query->row('total_background_style');
    }
    public function activeStatusForegroundStyle($id)
    {
        $this->db->set('status','off');
        $this->db->where('status','on');
        $this->db->update("foregroundStyle");

        $this->db->set('status','on');
        $this->db->where('id',$id);
        $this->db->update("foregroundStyle");

    }
    public function deactiveStatusForegroundStyle($id)
    {
        $this->db->set('status','off');
        $this->db->where('id',$id);
        $this->db->update("foregroundStyle");

    }
    public function activeStatusBackgroundStyle($id)
    {
        $this->db->set('status','off');
        $this->db->where('status','on');
        $this->db->update("backgroundStyle");

        $this->db->set('status','on');
        $this->db->where('id',$id);
        $this->db->update("backgroundStyle");

    }
    public function deactiveStatusBackgroundStyle($id)
    {
        $this->db->set('status','off');
        $this->db->where('id',$id);
        $this->db->update("backgroundStyle");

    }
    public function getDeletedAlbumBackgroundStyle($albumBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("album_background_style");
        $this->db->where('id',$albumBackgroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function getDeletedDefaultBackgroundStyle($defaultBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("backgroundStyle");
        $this->db->where('id',$defaultBackgroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function getDeletedAlbumForegroundStyle($albumForegroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("albumForegroundStyle");
        $this->db->where('id',$albumForegroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function getDeletedPageBackgroundStyle($pageBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("page_background_style");
        $this->db->where('id',$pageBackgroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function deletedAllPageForegroundStyle($pageBackgroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("pageForegroundStyle");
        $this->db->where('pageBackgroundStyleId',$pageBackgroundStyleId);
        $query = $this->db->get();
        return $query->result();
    }
    public function getDeletedPageForegroundStyle($pageForegroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("pageForegroundStyle");
        $this->db->where('id',$pageForegroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function getDeletedDefaultForegroundStyle($defaultForegroundStyleId)
    {
        $this->db->select("*");
        $this->db->from("foregroundStyle");
        $this->db->where('id',$defaultForegroundStyleId);
        $query = $this->db->get();
        return $query->row();
    }
    public function update_page_style_id($page_style_id)
    {
        $this->db->set('page_style_id','0');
        $this->db->set('page_foreground_id','0');
        $this->db->where('page_style_id',$page_style_id);
        $this->db->update('pagedetails');
    }
    public function update_page_foreground_id($page_foreground_id)
    {
        $this->db->set('page_foreground_id','0');
        $this->db->where('page_foreground_id',$page_foreground_id);
        $this->db->update('pagedetails');
    }
    public function update_album_forground_style($album_foreground_id)
    {
        $this->db->set('album_foreground_id','0');
        $this->db->where('album_foreground_id',$album_foreground_id);
        $this->db->update('albumdetails');
    }
    public function update_album_background_style($album_background_id)
    {
        $this->db->set('album_foreground_id','0');
        $this->db->set('album_background_id','0');
        $this->db->where('album_background_id',$album_background_id);
        $this->db->update('albumdetails');
    }
    public function deletedAllAlbumForegroundStyle($album_background_id)
    {
        $this->db->select("*");
        $this->db->from("albumForegroundStyle");
        $this->db->where('albumBackgroundStyleId',$album_background_id);
        $query = $this->db->get();
        return $query->result();
    }
}
