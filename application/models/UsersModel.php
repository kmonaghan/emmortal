<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model
{
 	public function __construct()
     {
         parent::__construct(); 
				
	}
	// User Exist or not
	public function userExistence($email)
	{
		$this->db->select('*');
		$this->db->where('emailid',$email);
		$this->db->limit(1);
		$query = $this->db->get('user');
		$num = $query->num_rows();
		if($num==1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}
	//signup
	public function signup($data)
	{
		$this->db->insert('user',$data);
	}
	//Check User Exist or not
	public function checkUserLogin($data)
	{
		$this->db->select('*');
		$this->db->where('emailid',$data['emailid']);
		$this->db->where('password',$data['password']);
		$query = $this->db->get('user');
		$num = $query->num_rows();
		if($num==1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
		
	}
	//Confirm Email
	public function confirmEmail($email)
	{
		 $this->db->where('emailid', $email);
   		 $this->db->update('user', array('activation' => '1'));

	}
    /*update Forgot Get Password*/
    public function updateForgotGetPassword($email,$random_key)
    {
        $this->db->set('forgetpassword', $random_key);  //Set the column name and which value to set..
        $this->db->where('emailid', $email); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
    }
    /* User Existence for reset password */
    public function userExistenceResetPassword($email,$random_key)
    {
        $this->db->select('*');
        $this->db->where('emailid',$email);
        $this->db->where('forgetpassword',$random_key);
        $this->db->limit(1);
        $query = $this->db->get('user');
        $num = $query->num_rows();
        if($num==1)
        {
            return $query->row_array();
        }
        else
        {
            return FALSE;
        }
    }
    //New Password
    public function newPassword($data)
    {
        $this->db->set('password', $data['npassword']);  //Set the column name and which value to set..
        $this->db->where('emailid', $data['email']); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
        return TRUE;
    }
    //signin date time
    public function updateSigninDateTime($userid)
    {
        $this->db->set('signindate', date('Y-m-d H:i:s'));  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
    }
    //last logout date time
    public function updateLogoutDateTime($userid)
    {
        $this->db->set('lastlogout', date('Y-m-d H:i:s'));  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
    }
    public function fetchParticularUserData($userid)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user.userid',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function fetchPerticularUserData($userid)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('pagedetails.user_id',$userid);
        $this->db->where('user.userid',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function createDefaultPage($data)
    {
        $this->db->insert('pagedetails',$data);
    }
    public function updateGeneralSettings($userid,$data)
    {
        $this->db->set('firstname', $data['firstname']);  //Set the column name and which value to set..
        $this->db->set('lastname', $data['lastname']);  //Set the column name and which value to set..
        $this->db->set('fullname', $data['fullname']);  //Set the column name and which value to set..
        $this->db->set('dob', $data['dob']);  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
    }
	
	public function changePasswordSettings($userid,$data)
	{
        $this->db->set('password', $data['new_password']);  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
	}
	public function userExistChangePassword($userid,$data)
	{
		$this->db->select('*');
        $this->db->where('userid',$userid);
        $this->db->where('password',$data['current_password']);
        $this->db->limit(1);
        $query = $this->db->get('user');
        $num = $query->num_rows();
        if($num==1)
        {
            return $query->row_array();
        }
        else
        {
            return FALSE;
        }
	}
	public function updateProfileImage($userid,$data)
	{
		$this->db->set('profileimage', $data['profileimage']);  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
	}
	public function updateBackgroundImage($userid,$data)
	{
		$this->db->set('backgroundimage', $data['backgroundimage']);  //Set the column name and which value to set..
        $this->db->where('userid', $userid); //set column_name and value in which row need to update
        $this->db->update('user'); //Set your table name
	}
	public function fetchAllUserData($userid)
    {
        $this->db->select('count(*) as total_user');
        $this->db->from('user');
        $this->db->where('userid !=', $userid);
        $this->db->where('userstype', 'user');
        $query = $this->db->get();
        return $query->row();
    }

    public function fetchAllScrollLoad($userid,$start)
    {
        $this->db->select('*');
        $this->db->where('userid !=', $userid);
        $this->db->where('userstype', 'user');
        $this->db->order_by("firstname", "asc");
        $this->db->limit(5,$start);
        $query = $this->db->get('user');
        return $query->result();
    }

    public function searchUsers($userid,$search_users)
    {
        $this->db->select('*');
        $this->db->where('userid !=', $userid);
        $this->db->where('userstype', 'user');
        $this->db->like('fullname',$search_users);
        $this->db->order_by("firstname", "asc");
        $query = $this->db->get('user');
        return $query->result();
    }
	public function addFriend($data)
    {
        $this->db->insert('friends',$data);
        $this->db->select('id');
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $query=$this->db->get('friends');
        return $query->row();

    }
    public function fetchAllFriendRequest($userid)
    {
        $this->db->select('*');
        $this->db->where('userid', $userid);
        $this->db->where('relationshipstatus', 'outgoing');
        $query = $this->db->get('friends');
        return $query->result();
    }
    public function fetchAllUsers($userid)
    {
        $this->db->select('*');
        $this->db->where('userid !=', $userid);
        $this->db->where('userstype', 'user');
        $this->db->order_by("firstname", "asc");
        $query = $this->db->get('user');
        return $query->result();
    }

    public function fetchAllOutgoingUsers($userid)
    {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends','friends.friendsid=user.userid');
        $this->db->where('friends.userid', $userid);
        $this->db->where('friends.relationshipstatus','outgoing');
        $this->db->order_by("user.firstname", "asc");
        $this->db->distinct();
        $query=$this->db->get();
        return $query->result();
    }
    public function fetchAllIncomingUsers($userid)
    {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends','friends.userid=user.userid');
        $this->db->where('friends.friendsid', $userid);
        $this->db->where('friends.relationshipstatus','outgoing');
        $this->db->order_by("user.firstname", "asc");
        $this->db->distinct();
        $query=$this->db->get();
        return $query->result();
    }
    public function fetchAllAcceptedFriend($userid)
    {
        $this->db->select('*');
        $this->db->where(" (userid=$userid OR friendsid=$userid) ");
        $this->db->where('relationshipstatus', 'accepted');
        $query = $this->db->get('friends');
        return $query->result();
    }

    public function fetchAllRelationshipUsers($userid)
    {
        $this->db->select('user.*');
        $this->db->from('user');
       $this->db->where('user.userid !=', $userid);
        $this->db->where('user.userstype', 'user');
        $this->db->join('friends','(friends.userid=user.userid OR friends.friendsid=user.userid)');
        $this->db->where(" (friends.userid=$userid OR friends.friendsid=$userid) ");
        $this->db->where('friends.relationshipstatus','accepted');
        $this->db->order_by("user.firstname", "asc");
        $this->db->distinct();
        $query=$this->db->get();
        return $query->result();
    }
    public function acceptFriendRequest($data)
    {
        $this->db->set('requestaccept', $data['requestaccept']);
        $this->db->set('relationshipstatus', $data['relationshipstatus']);
        $this->db->where(" ((userid='".$data['userid']."' AND friendsid='".$data['friendsid']."') OR (userid='".$data['friendsid']."' AND friendsid='".$data['userid']."') )");
        $this->db->update('friends');

    }

    public function searchRelationships($userid,$searchUsers)
    {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends', '(friends.userid=user.userid OR friends.friendsid=user.userid)');
       $this->db->where('user.userid !=', $userid);
        $this->db->where('user.userstype', 'user');
        $this->db->like('fullname',$searchUsers);
        $this->db->where(" (friends.userid=$userid OR friends.friendsid=$userid) ");
        $this->db->where('friends.relationshipstatus', 'accepted');
        $this->db->order_by("user.firstname", "asc");
        $this->db->distinct();
        $query = $this->db->get();
        return $query->result();

    }
    public function searchOutgoingUsers($userid,$searchUsers)
    {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends','friends.friendsid=user.userid');
        $this->db->where('friends.userid', $userid);
        $this->db->where('friends.relationshipstatus','outgoing');
        $this->db->like('fullname',$searchUsers);
        $this->db->order_by("user.firstname", "asc");
        $query=$this->db->get();
        return $query->result();
        
    }
    public function declineFriendsRequest($data)
    {
        $this->db->delete('friends',$data);
    }

    public function friendsRelationships($userid,$friendsid)
    {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends', '(friends.userid=user.userid OR friends.friendsid=user.userid)');
       $this->db->where('user.userid !=', $userid);
        $this->db->where('user.userstype', 'user');
        $this->db->where(" ((friends.userid=$userid AND friends.friendsid=$friendsid) OR (friends.userid=$friendsid AND friends.friendsid=$userid))  ");
        $this->db->where('friends.relationshipstatus', 'accepted');
        $query = $this->db->get();
        return $query->row_array();
    }
    public function showCommentsImage($imageid)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.image_id',$imageid);
        $this->db->order_by('tributesdetails.addeddate','asc');
        $query = $this->db->get();
        return $query->result();
    }
    public function CommentsImage($data)
    {
        $this->db->insert('tributesdetails',$data);
    }
    public function sendQuestion($data)
    {
        $this->db->insert('supportquestion',$data);
    }
    public function tagFriendsContent($user_id,$friends_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        return $query->result();

    }
	
	public function friendsNewsfeed($userid)
	{
		$this->db->select('user.userid,user.fullname,user.profileimage,user.backgroundimage,pagedetails.pageid');
        $this->db->from('user');
       $this->db->where('user.userid !=', $userid);
        $this->db->where('user.userstype', 'user');
        $this->db->join('friends','(friends.userid=user.userid OR friends.friendsid=user.userid)');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" (friends.userid=$userid OR friends.friendsid=$userid) ");
        $this->db->where('friends.relationshipstatus','accepted');
        $this->db->order_by("friends.friendshipdate", "desc");
        $query=$this->db->get();
        return $query->result();	
	}
	public function fetchAllAcceptedUsers($userid)
	{
		$this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends','friends.userid=user.userid');
		$this->db->where('user.userid !=', $userid);
        $this->db->where('friends.friendsid',$userid);
        $this->db->where('friends.relationshipstatus','accepted');
        $this->db->order_by("friends.friendshipdate", "desc");
        $query=$this->db->get();
        return $query->result();
	}
	public function fetchAllAcceptedRelationshipsRequest($userid)
	{
		$this->db->select('user.*');
        $this->db->from('user');
        $this->db->join('friends','friends.friendsid=user.userid');
		$this->db->where('user.userid !=', $userid);
        $this->db->where('friends.userid',$userid);
        $this->db->where('friends.relationshipstatus','accepted');
        $this->db->order_by("friends.friendshipdate", "desc");
        $query=$this->db->get();
        return $query->result();
	}
	public function userExistOrNot($userid)
	{
		$this->db->select('*');
		$this->db->where('userid',$userid);
		$this->db->limit(1);
		$query = $this->db->get('user');
		$num = $query->num_rows();
		if($num==1)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}
	public function pageDetails($userid)
	{
		$this->db->select('*');
        $this->db->from('pagedetails');
		$this->db->where('user_id',$userid);
        $this->db->order_by('default_page','desc');
        $query=$this->db->get();
        return $query->result();
		
	}
	public function pageContent($page_id)
	{
		$this->db->select('*');
        $this->db->from('pagedetails');
        $this->db->join('user','user.userid=pagedetails.user_id');
		$this->db->where('pagedetails.pageid',$page_id);
        $query=$this->db->get();
        $res = $query->row();
		if($res->page_style_id==0)
		{

            return $res;

		}
		else
		{
			$this->db->select('*');
        	$this->db->from('pagedetails');
			$this->db->join('page_background_style','page_background_style.id=pagedetails.page_style_id');
            $this->db->join('user','user.userid=pagedetails.user_id');
			$this->db->where('pagedetails.pageid',$page_id);
        	$query1=$this->db->get();
			return $query1->row();
		}
	}
	
	public function totalCommentsImage($user_id,$friends_id)
	{
		
		$this->db->select('albumimageuploaddetails.id');
        $this->db->from('friendsTagContent');
		$this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['id']);
			
		}
		if($result)
		{
			$this->db->select('count(*) as total_comments');
			$this->db->where_in('image_id',$result);
			$query1 = $this->db->get('tributesdetails');
			return $query1->row();
		}
		else
		{
			$obj = (object) array('total_comments' => 0);
			return $obj;
		}
			
	}
	public function totalCommentsPerticularImage($user_id,$friends_id)
	{
		$this->db->select('albumimageuploaddetails.id');
        $this->db->from('friendsTagContent');
		$this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['id']);
			
		}
		if($result)
		{
			$this->db->select('image_id,COUNT(image_id) as total');
			$this->db->group_by('image_id');
			$this->db->where_in('image_id',$result);
			$query1 = $this->db->get('tributesdetails');
			return $query1->result();
		}
		else
		{
			$obj[] = (object) array('image_id'=>0,'total' => 0);
			return $obj;
		}
	}
	
	public function existUserLoginLikeImage($userid,$image_id)
	{
		$this->db->select('*');
		$this->db->from('image_like');
		$this->db->where('image_id',$image_id);
		$this->db->where('user_id',$userid);
		$this->db->limit(1);
		$query = $this->db->get();
		$num = $query->num_rows();
        if($num==1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
		
	}
	public function likeImage($data)
	{
		$this->db->insert('image_like',$data);
	}
	public function dislikeImage($userid,$image_id)
	{
		 $this->db->where('user_id', $userid);
		 $this->db->where('image_id', $image_id);
     	 $this->db->delete('image_like'); 
	}
	
	public function totalLikePerticularImage($image_id)
	{
		$this->db->select('count(*) as total');
		$this->db->from('image_like');
		$this->db->where('image_id',$image_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function totalLikesImage($user_id,$friends_id)
	{
		$this->db->select('albumimageuploaddetails.id');
        $this->db->from('friendsTagContent');
		$this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		
		foreach($res as $value)
		{
			array_push($result,$value['id']);
			
		}
		if($result)
		{
			$this->db->select('count(*) as total_like');
			$this->db->where_in('image_id',$result);
			$query1 = $this->db->get('image_like');
			return $query1->row();
		}
		else
		{
			$obj = (object) array('total_like' => 0);
			return $obj;
		}
	}
	
	public function totalLikesPerticularImage($user_id,$friends_id)
	{
		$this->db->select('albumimageuploaddetails.id');
        $this->db->from('friendsTagContent');
		$this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['id']);
			
		}
		if($result)
		{
			$this->db->select('image_id,COUNT(image_id) as total');
			$this->db->group_by('image_id');
			$this->db->where_in('image_id',$result);
			$query1 = $this->db->get('image_like');
			return $query1->result();
		}
		else
		{
			$obj[] = (object) array('image_id'=>0,'total' => 0);
			return $obj;
		}
	}
	
	 public function tagFriendsText($user_id,$friends_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        return $query->result();
		

    }
	
	public function showCommentsText($text_id)
	{
		 $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.text_id',$text_id);
        $this->db->order_by('tributesdetails.addeddate','asc');
        $query = $this->db->get();
        return $query->result();
	}
	 public function CommentsText($data)
    {
        $this->db->insert('tributesdetails',$data);
    }
	
	
	public function totalCommentsText($user_id,$friends_id)
	{
		
		$this->db->select('textdetails.textid');
        $this->db->from('friendsTagContent');
		$this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['textid']);
			
		}
		if($result)
		{
			$this->db->select('count(*) as total_comments');
			$this->db->where_in('text_id',$result);
			$query1 = $this->db->get('tributesdetails');
			return $query1->row();
		}
		else
		{
			$obj = (object) array('total_comments' => 0);
			return $obj;
		}
	}
	
	public function totalCommentsPerticularText($user_id,$friends_id)
	{
		$this->db->select('textdetails.textid');
        $this->db->from('friendsTagContent');
		$this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['textid']);
			
		}
		if($result)
		{
			$this->db->select('text_id,COUNT(text_id) as total');
			$this->db->group_by('text_id');
			$this->db->where_in('text_id',$result);
			$query1 = $this->db->get('tributesdetails');
			return $query1->result();
		}
		else
		{
			$obj[] = (object) array('text_id'=>0,'total' => 0);
			return $obj;
		}
	}
	
	public function totalLikesPerticularText($user_id,$friends_id)
	{
		$this->db->select('textdetails.textid');
        $this->db->from('friendsTagContent');
		$this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['textid']);
			
		}
		if($result)
		{
			$this->db->select('text_id,COUNT(text_id) as total');
			$this->db->group_by('text_id');
			$this->db->where_in('text_id',$result);
			$query1 = $this->db->get('text_like');
			return $query1->result();
		}
		else
		{
			$obj[] = (object) array('text_id'=> 0,'total' => 0);
			return $obj;
		}
	}
	public function totalLikesText($user_id,$friends_id)
	{
		$this->db->select('textdetails.textid');
        $this->db->from('friendsTagContent');
		$this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
		$result=array();
		foreach($res as $value)
		{
			array_push($result,$value['textid']);
			
		}
		if($result)
		{
			$this->db->select('count(*) as total_like');
			$this->db->where_in('text_id',$result);
			$query1 = $this->db->get('text_like');
			return $query1->row();
		}
		else
		{
			$obj = (object) array('total_like' => 0);
			return $obj;
		}
		
	}
	public function existUserLoginLikeText($userid,$text_id)
	{
		$this->db->select('*');
		$this->db->from('text_like');
		$this->db->where('text_id',$text_id);
		$this->db->where('user_id',$userid);
		$this->db->limit(1);
		$query = $this->db->get();
		$num = $query->num_rows();
        if($num==1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
		
	}
	
	public function likeText($data)
	{
		$this->db->insert('text_like',$data);
	}
	public function dislikeText($userid,$text_id)
	{
		 $this->db->where('user_id', $userid);
		 $this->db->where('text_id', $text_id);
     	 $this->db->delete('text_like'); 
	}
	
	public function totalLikePerticularText($text_id)
	{
		$this->db->select('count(*) as total');
		$this->db->from('text_like');
		$this->db->where('text_id',$text_id);
		$query = $this->db->get();
		return $query->row();
	}
	public function pageExistOrNot($user_id,$page_id)
    {
        $this->db->select('*');
        $this->db->where('user_id',$user_id);
        $this->db->where('pageid',$page_id);
        $this->db->limit(1);
        $query = $this->db->get('pagedetails');
        $num = $query->num_rows();
        if($num==1)
        {
            return $query->row_array();
        }
        else
        {
            return FALSE;
        }
    }
    public function pageImages($page_id)
    {
        $this->db->select('*');
        $this->db->from('albumimageuploaddetails');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('user', 'user.userid=albumdetails.user_id');
        $this->db->where('page_id',$page_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function existUserLoginLikeComment($userid,$comment_id)
    {
        $this->db->select('*');
        $this->db->from('comment_like');
        $this->db->where('comment_id',$comment_id);
        $this->db->where('user_id',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        $num = $query->num_rows();
        if($num==1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }
    public function likeComment($data)
    {
        $this->db->insert('comment_like',$data);
    }
    public function dislikeComment($userid,$comment_id)
    {
        $this->db->where('user_id', $userid);
        $this->db->where('comment_id', $comment_id);
        $this->db->delete('comment_like');
    }
    public function totalCommentLike($comment_id)
    {
        $this->db->select('COUNT(comment_id) as total');
        $this->db->where('comment_id',$comment_id);
        $query = $this->db->get('comment_like');
        return $query->row();
    }
    public function allLikecomment()
    {
        $this->db->select('*');
        $this->db->order_by('comment_id');
        $query = $this->db->get('comment_like');
        return $query->result();
    }
    public function fetchAllUser()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $query = $this->db->get();
        return $query->result();
    }

    public function tagFriendsVideo($user_id,$friends_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        return $query->result();

    }

    public function totalVideoComments($user_id,$friends_id)
    {

        $this->db->select('albumvideouploaddetails.id');
        $this->db->from('friendsTagContent');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['id']);

        }
        if($result)
        {
            $this->db->select('count(*) as total_comments');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->row();
        }
        else
        {
            $obj = (object) array('total_comments' => 0);
            return $obj;
        }

    }
    public function totalLikesVideo($user_id,$friends_id)
    {
        $this->db->select('albumvideouploaddetails.id');
        $this->db->from('friendsTagContent');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['id']);

        }
        if($result)
        {
            $this->db->select('count(*) as total_like');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('video_like');
            return $query1->row();
        }
        else
        {
            $obj = (object) array('total_like' => 0);
            return $obj;
        }
    }

    public function totalLikesParticularVideo($user_id,$friends_id)
    {
        $this->db->select('albumvideouploaddetails.id');
        $this->db->from('friendsTagContent');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['id']);

        }
        if($result)
        {
            $this->db->select('video_id,COUNT(video_id) as total');
            $this->db->group_by('video_id');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('video_like');
            return $query1->result();
        }
        else
        {
            $obj[] = (object) array('video_id' => 0,'total' => 0);
            return $obj;
        }
    }
    public function totalCommentsParticularVideo($user_id,$friends_id)
    {
        $this->db->select('albumvideouploaddetails.id');
        $this->db->from('friendsTagContent');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."')) ");
        $this->db->order_by("friendsTagContent.created_at",'desc');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['id']);

        }
        if($result)
        {
            $this->db->select('video_id,COUNT(video_id) as total');
            $this->db->group_by('video_id');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj[] = (object) array('video_id'=>0,'total' => 0);
            return $obj;
        }
    }
    public function showCommentsVideo($video_id)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.video_id',$video_id);
        $this->db->order_by('tributesdetails.addeddate','asc');
        $query = $this->db->get();
        return $query->result();
    }
    public function existUserLoginLikeVideo($userid,$video_id)
    {
        $this->db->select('*');
        $this->db->from('video_like');
        $this->db->where('video_id',$video_id);
        $this->db->where('user_id',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        $num = $query->num_rows();
        if($num==1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }
    public function totalLikeParticularVideo($video_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('video_like');
        $this->db->where('video_id',$video_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function CommentsVideo($data)
    {
        $this->db->insert('tributesdetails',$data);
    }
    public function likeVideo($data)
    {
        $this->db->insert('video_like',$data);
    }
    public function dislikeVideo($userid,$video_id)
    {
        $this->db->where('user_id', $userid);
        $this->db->where('video_id', $video_id);
        $this->db->delete('video_like');
    }
    public function seeTaggedImage($image_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.friends_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('friendsTagContent.image_id',$image_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function seeTaggedVideo($video_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.friends_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('friendsTagContent.video_id',$video_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function seeTaggedText($text_id)
    {
        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.friends_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('friendsTagContent.text_id',$text_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function countAlbumContent($album_id,$user_id)
    {
        $this->db->select('COUNT(*) as total');
        $this->db->from('albumdetails');
        $this->db->join('albumimageuploaddetails','albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('albuminnerrecordsdragdrop','albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
        $this->db->where('albumdetails.albumid',$album_id);
        $this->db->where('albumdetails.user_id',$user_id);
        $this->db->where('albumimageuploaddetails.is_cover_image','n');
        $query1 = $this->db->get();
        $res1=$query1->row();

        $this->db->select('COUNT(*) as total');
        $this->db->from('albumdetails');
        $this->db->join('albumvideouploaddetails','albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->where('albumdetails.albumid',$album_id);
        $this->db->where('albumdetails.user_id',$user_id);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $query2 = $this->db->get();
        $res2=$query2->row();

        $this->db->select('COUNT(*) as total');
        $this->db->from('albumdetails');
        $this->db->join('textdetails','albumdetails.albumid=textdetails.album_id');
        $this->db->where('albumdetails.albumid',$album_id);
        $this->db->where('albumdetails.user_id',$user_id);
        $query3 = $this->db->get();
        $res3=$query3->row();

        return $res1->total+$res2->total+$res3->total;
    }
    public function tagFriendsAllContent($user_id,$friends_id)
    {

        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query1 = $this->db->get();
        $res1=$query1->result_array();

        $this->db->select('*');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query2 = $this->db->get();
        $res2=$query2->result_array();

        $this->db->select('*,textdetails.title as text_title,textdetails.description as text_description');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query3 = $this->db->get();
        $res3=$query3->result_array();

        $result=array();
        foreach($res1 as $value)
        {
            array_push($result,$value);


        }

        foreach($res2 as $value)
        {
            $value['image_name']='';
            $value['uploadPath']='';
            array_push($result,$value);

        }
        foreach($res3 as $value)
        {
            $value['image_name']='';
            $value['uploadPath']='';
            array_push($result,$value);

        }
        usort($result, function ($one, $two) {
            if ($one['records_order'] === $two['records_order']) {
                return $one['created_at'] > $two['created_at'] ? -1 : 1;
            }
            return $one['records_order'] < $two['records_order'] ? -1 : 1;
        });
        return $result;
    }
    public function totalTagContent($user_id,$friends_id)
    {
        $this->db->select('count(*) as total_tag_content');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=friendsTagContent.image_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query1 = $this->db->get();
        $res1=$query1->row();

        $this->db->select('count(*) as total_tag_content');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=friendsTagContent.video_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query2 = $this->db->get();
        $res2=$query2->row();

        $this->db->select('count(*) as total_tag_content');
        $this->db->from('friendsTagContent');
        $this->db->join('user', 'user.userid=friendsTagContent.user_id');
        $this->db->join('textdetails', 'textdetails.textid=friendsTagContent.text_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('pagedetails','pagedetails.user_id=friendsTagContent.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where(" ((friendsTagContent.user_id='".$user_id."' AND friendsTagContent.friends_id='".$friends_id."') OR (friendsTagContent.user_id='".$friends_id."' AND friendsTagContent.friends_id='".$user_id."') )");
        $this->db->order_by("friendsTagContent.records_order");
        $query3 = $this->db->get();
        $res3=$query3->row();

        return ($res1->total_tag_content + $res2->total_tag_content + $res3->total_tag_content);
    }
    public function updateRelationshipsContentOrder($alltagid)
    {
        $count = 1;
        foreach ($alltagid as $tagid)
        {
            $this->db->set('records_order',$count);
            $this->db->where('tagid',$tagid);
            $this->db->update('friendsTagContent');
            $count++;
        }
        return true;
    }
    public function addTribute($data)
    {
        $this->db->insert('tributes',$data);
        $this->db->select('tribute_id');
        $this->db->order_by('tribute_id','desc');
        $this->db->limit(1);
        $query=$this->db->get('tributes');
        return $query->row();
    }
    public function deleteRelationshipsTagContent($tagId)
    {
        $this->db->where('tagid',$tagId);
        if($this->db->delete('friendsTagContent'))
            return true;
        else
            return false;

    }
    public function fetchRelationshipsTagContent($tagId)
    {
        $this->db->select('*');
        $this->db->where('tagid',$tagId);
        $this->db->limit(1);
        $query = $this->db->get('friendsTagContent');
        return $query->row();
    }
    public function deleteImageLike($image_id)
    {
        $this->db->where('image_id',$image_id);
        $this->db->delete('image_like');
    }
    public function deleteVideoLike($video_id)
    {
        $this->db->where('video_id',$video_id);
        $this->db->delete('video_like');
    }
    public function deleteTextLike($text_id)
    {
        $this->db->where('text_id',$text_id);
        $this->db->delete('text_like');
    }
    public function deleteTextTribute($text_id)
    {
        $this->db->where('text_id',$text_id);
        $this->db->delete('tributesdetails');
    }
    public function deleteImageTribute($image_id)
    {
        $this->db->where('image_id',$image_id);
        $this->db->delete('tributesdetails');
    }
    public function deleteVideoTribute($video_id)
    {
        $this->db->where('video_id',$video_id);
        $this->db->delete('tributesdetails');
    }
    public function fetchAllTributes($userid)
    {
        /*$sql="SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))";
        $this->db->query($sql);*/
        $this->db->select('tributes.*,user.*,pagedetails.*');
        $this->db->from('tributes');
        $this->db->join('user', 'user.userid=tributes.user_id');
        $this->db->join('pagedetails','tributes.user_id=pagedetails.user_id');
        $this->db->join('tribute_like','tribute_like.tribute_id=tributes.tribute_id','left');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributes.friends_id',$userid);
        $this->db->group_by('tributes.tribute_id');
        $this->db->order_by('COUNT(tribute_like.like_id)','desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function fetchTributesDetailsByTributeId($userid,$tribute_id)
    {
        $this->db->select('*');
        $this->db->from('tributes');
        $this->db->join('user', 'user.userid=tributes.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=tributes.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributes.tribute_id',$tribute_id);
        $this->db->where('tributes.friends_id',$userid);
        $query = $this->db->get();
        return $query->row();
    }
    public function editTribute($tribute_id,$tribute_description)
    {
        $this->db->set('tribute_description', $tribute_description);  //Set the column name and which value to set..
        $this->db->where('tribute_id', $tribute_id); //set column_name and value in which row need to update
        $this->db->update('tributes'); //Set your table name
    }
    public function deleteTribute($tribute_id)
    {
        $this->db->where('tribute_id', $tribute_id);
        $this->db->delete('tributes');
    }
    public function existUserLoginLikeTribute($userid,$tribute_id)
    {
        $this->db->select('*');
        $this->db->from('tribute_like');
        $this->db->where('tribute_id',$tribute_id);
        $this->db->where('user_id',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        $num = $query->num_rows();
        if($num==1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }
    public function likeTribute($data)
    {
        $this->db->insert('tribute_like',$data);
    }
    public function dislikeTribute($userid,$tribute_id)
    {
        $this->db->where('user_id', $userid);
        $this->db->where('tribute_id', $tribute_id);
        $this->db->delete('tribute_like');
    }
    public function totalTributeLike($tribute_id)
    {
        $this->db->select('COUNT(tribute_id) as total');
        $this->db->where('tribute_id',$tribute_id);
        $query = $this->db->get('tribute_like');
        return $query->row();
    }
    public function allTributeLikes($userid)
    {
        $this->db->select('tributes.tribute_id');
        $this->db->from(' tributes');
        $this->db->where('friends_id',$userid);
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['tribute_id']);

        }
        if($result)
        {

            $this->db->select('tribute_id,COUNT(tribute_id) as total');
            $this->db->group_by('tribute_id');
            $this->db->where_in('tribute_id',$result);
            $query1= $this->db->get('tribute_like');
            return $query1->result();
        }
        else
        {
            $obj[] = (object) array('tribute_id'=>0,'total' => 0);
            return $obj;
        }


    }
    public function existUserLoginLikeTributes($userid)
    {
        $this->db->select('*');
        $this->db->from('tribute_like');
        $this->db->where('user_id',$userid);
        $query= $this->db->get();
        return $query->result();
    }
    public function showCommentsTribute($tributeid)
    {
        $this->db->select('*');
        $this->db->from('tributesdetails');
        $this->db->join('user', 'user.userid=tributesdetails.user_id');
        $this->db->join('pagedetails','pagedetails.user_id=user.userid');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('tributesdetails.tribute_id',$tributeid);
        $this->db->order_by('tributesdetails.addeddate','asc');
        $query = $this->db->get();
        return $query->result();
    }
    public function CommentsTribute($data)
    {
        $this->db->insert('tributesdetails',$data);
    }

    public function CountAllHomeContent()
    {
        $this->db->select('count(*) as total');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query1 = $this->db->get();
        $res1=$query1->row();

        $this->db->select('count(*) as total');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=albuminnerrecordsdragdrop.video_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query2 = $this->db->get();
        $res2=$query2->row();

        $this->db->select('count(*) as total');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('textdetails', 'textdetails.textid=albuminnerrecordsdragdrop.text_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query3 = $this->db->get();
        $res3=$query3->row();

        return ($res1->total + $res2->total + $res3->total);


    }

    public function getAllHomeContent($start)
    {
        $this->db->select('*');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=albuminnerrecordsdragdrop.image_id');
        $this->db->join('user', 'user.userid=albuminnerrecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=albuminnerrecordsdragdrop.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query1 = $this->db->get();
        $res1=$query1->result_array();

        $this->db->select('*');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=albuminnerrecordsdragdrop.video_id');
        $this->db->join('user', 'user.userid=albuminnerrecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=albuminnerrecordsdragdrop.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query2 = $this->db->get();
        $res2=$query2->result_array();

        $this->db->select('*,textdetails.title as text_title,textdetails.description as text_description');
        $this->db->from('albuminnerrecordsdragdrop');
        $this->db->join('textdetails', 'textdetails.textid=albuminnerrecordsdragdrop.text_id');
        $this->db->join('user', 'user.userid=albuminnerrecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albuminnerrecordsdragdrop.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=albuminnerrecordsdragdrop.user_id');
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("albuminnerrecordsdragdrop.created_date_time","DESC");
        $query3 = $this->db->get();
        $res3=$query3->result_array();

        $result=array();
        foreach($res1 as $value)
        {
            array_push($result,$value);
        }
        foreach($res2 as $value)
        {
            $value['image_name']="";
            $value['uploadPath']="";
            array_push($result,$value);

        }
        foreach($res3 as $value)
        {
            $value['image_name']="";
            $value['uploadPath']="";
            array_push($result,$value);

        }
        usort($result, function ($one, $two) {
            if ($one['created_date_time'] === $two['created_date_time']) {
                return 0;
            }
            return $one['created_date_time'] > $two['created_date_time'] ? -1 : 1;
        });
        return array_slice($result, $start, 15);
    }
    public function tributeCommentsCount($userid)
    {
        $this->db->select('tributes.tribute_id');
        $this->db->from(' tributes');
        $this->db->where('friends_id',$userid);
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['tribute_id']);

        }
        if($result)
        {

            $this->db->select('tribute_id,COUNT(tribute_id) as total');
            $this->db->group_by('tribute_id');
            $this->db->where_in('tribute_id',$result);
            $query1= $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj[] = (object) array('tribute_id'=>0,'total' => 0);
            return $obj;
        }
    }
    public function tributeCommentsCountByTributeId($tribute_id)
    {
        $this->db->select('COUNT(tribute_id) as total');
        $this->db->where('tribute_id',$tribute_id);
        $query1= $this->db->get('tributesdetails');
        return $query1->row();
    }

    public function totCommentParticularImage($image_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tributesdetails');
        $this->db->where('image_id',$image_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function totCommentParticularVideo($video_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tributesdetails');
        $this->db->where('video_id',$video_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function totCommentParticularText($text_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tributesdetails');
        $this->db->where('text_id',$text_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function pageAllContentCount($page_id)
    {
        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=pagerecordsdragdrop.image_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumimageuploaddetails.albumid=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query1 = $this->db->get();
        $res1=$query1->row();

        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=pagerecordsdragdrop.video_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumvideouploaddetails.albumid=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
       
        $query2 = $this->db->get();
        $res2=$query2->row();

        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('textdetails', 'textdetails.textid=pagerecordsdragdrop.text_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'textdetails.album_id=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query3 = $this->db->get();
        $res3=$query3->row();


        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.albumid=pagerecordsdragdrop.album_id');
        $this->db->join('albumdetails', 'pagerecordsdragdrop.album_id=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumimageuploaddetails.is_cover_image','y');
       
        $query4 = $this->db->get();
        $res4=$query4->row();


        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('tributes', 'tributes.tribute_id=pagerecordsdragdrop.tribute_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query5 = $this->db->get();
        $res5=$query5->row();

        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_profile_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query6 = $this->db->get();
        $res6=$query6->row();


        $this->db->select('count(*) as total');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_description_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query7 = $this->db->get();
        $res7=$query7->row();

        return $res1->total+$res2->total+$res3->total+$res4->total+$res5->total+$res6->total+$res7->total;
    }

    public function pageAllContent($page_id)
    {

        $this->db->select('*,pagerecordsdragdrop.image_id as page_image_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=pagerecordsdragdrop.image_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumimageuploaddetails.albumid=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query1 = $this->db->get();
        $res1=$query1->result_array();

        $this->db->select('*,pagerecordsdragdrop.video_id as page_video_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('albumvideouploaddetails', 'albumvideouploaddetails.id=pagerecordsdragdrop.video_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'albumvideouploaddetails.albumid=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
       
        $query2 = $this->db->get();
        $res2=$query2->result_array();

        $this->db->select('*,textdetails.title as text_title,textdetails.description as text_description,pagerecordsdragdrop.text_id as page_text_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('textdetails', 'textdetails.textid=pagerecordsdragdrop.text_id');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'textdetails.album_id=albumdetails.albumid');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query3 = $this->db->get();
        $res3=$query3->result_array();


        $this->db->select('*,pagerecordsdragdrop.album_id as page_album_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('albumdetails', 'pagerecordsdragdrop.album_id=albumdetails.albumid');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.albumid=pagerecordsdragdrop.album_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
        $this->db->where('albumimageuploaddetails.is_cover_image','y');
       
        $query4 = $this->db->get();
        $res4=$query4->result_array();


        $this->db->select('*,pagerecordsdragdrop.tribute_id as page_tribute_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_id');
        $this->db->join('tributes', 'tributes.tribute_id=pagerecordsdragdrop.tribute_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query5 = $this->db->get();
        $res5=$query5->result_array();

        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_profile_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
       
        $query6 = $this->db->get();
        $res6=$query6->result_array();


        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->join('user', 'user.userid=pagerecordsdragdrop.user_description_id');
        $this->db->join('pagedetails','pagedetails.user_id=pagerecordsdragdrop.user_id');
        $this->db->where('pagerecordsdragdrop.page_id',$page_id);
        $this->db->where('pagedetails.default_page',1);
        
        $query7 = $this->db->get();
        $res7=$query7->result_array();


        $result=array();
        foreach($res1 as $value)
        {
            array_push($result,$value);
        }
        foreach($res2 as $value)
        {
            $value['image_name']="";
            $value['uploadPath']="";
            array_push($result,$value);

        }
        foreach($res3 as $value)
        {
            $value['image_name']="";
            $value['uploadPath']="";
            array_push($result,$value);

        }

        foreach($res4 as $value)
        {

            array_push($result,$value);

        }
        foreach($res5 as $value)
        {
            array_push($result,$value);

        }
        foreach($res6 as $value)
        {
            array_push($result,$value);

        }
        foreach($res7 as $value)
        {
            array_push($result,$value);

        }
        return $result;
     

    }
    public function addUserDetailsPage($data1,$data2)
    {
        $this->db->insert('pagerecordsdragdrop',$data2);
        $this->db->insert('pagerecordsdragdrop',$data1);
    }

    public function allPageId($userid)
    {
        $this->db->select('*');
        $this->db->from('pagedetails');
        $this->db->where('user_id',$userid);
        $query = $this->db->get();
        return $query->result();
    }
    public function selectedPageIdImage($userid,$image_id)
    {
        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id',$userid);
        $this->db->where('image_id',$image_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    public function selectedPageIdText($userid,$text_id)
    {
        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id',$userid);
        $this->db->where('text_id',$text_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
    public function selectedPageIdVideo($userid,$video_id)
    {
        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id',$userid);
        $this->db->where('video_id',$video_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
    public function selectedPageIdTribute($userid,$tribute_id)
    {
        $this->db->select('*');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id',$userid);
        $this->db->where('tribute_id',$tribute_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }
    public function existPagerecordsdragdrop($data)
    {
        $this->db->select('*');
        $query = $this->db->get_where('pagerecordsdragdrop',$data);
        return $query->row();
    }
    public function insertPagerecordsdragdrop($data)
    {
        $this->db->select('max(row) as maxRow');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $query = $this->db->get();
        $maxRow =$query->row('maxRow');

        $this->db->select('max(col) as maxCol');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $this->db->where('row', $maxRow);
        $query = $this->db->get();
        $maxCol =$query->row('maxCol');

        if($maxCol==5)
        {
            $data['row']=$maxRow+1;
            $data['col']=1;
        }
        else {
            $data['row']=$maxRow;
            $data['col']=$maxCol+1;
        }

        $this->db->insert('pagerecordsdragdrop',$data);
    }
    public function updatePagerecordsdragdropImage($data)
    {
        $this->db->select('max(row) as maxRow');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $query = $this->db->get();
        $maxRow =$query->row('maxRow');

        $this->db->select('max(col) as maxCol');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $this->db->where('row', $maxRow);
        $query = $this->db->get();
        $maxCol =$query->row('maxCol');

        if($maxCol==5)
        {
            $data['row']=$maxRow+1;
            $data['col']=1;
        }
        else {
            $data['row']=$maxRow;
            $data['col']=$maxCol+1;
        }
        $this->db->set('row', $data['row']);
        $this->db->set('col', $data['col']);

        $this->db->set('page_id', $data['page_id']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('image_id', $data['image_id']);
        $this->db->update('pagerecordsdragdrop');
    }
    public function updatePagerecordsdragdropText($data)
    {
        $this->db->select('max(row) as maxRow');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $query = $this->db->get();
        $maxRow =$query->row('maxRow');

        $this->db->select('max(col) as maxCol');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $this->db->where('row', $maxRow);
        $query = $this->db->get();
        $maxCol =$query->row('maxCol');

        if($maxCol==5)
        {
            $data['row']=$maxRow+1;
            $data['col']=1;
        }
        else {
            $data['row']=$maxRow;
            $data['col']=$maxCol+1;
        }
        $this->db->set('row', $data['row']);
        $this->db->set('col', $data['col']);
        $this->db->set('page_id', $data['page_id']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('text_id', $data['text_id']);
        $this->db->update('pagerecordsdragdrop');
    }
    public function updatePagerecordsdragdropVideo($data)
    {
        $this->db->select('max(row) as maxRow');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $query = $this->db->get();
        $maxRow =$query->row('maxRow');

        $this->db->select('max(col) as maxCol');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $this->db->where('row', $maxRow);
        $query = $this->db->get();
        $maxCol =$query->row('maxCol');

        if($maxCol==5)
        {
            $data['row']=$maxRow+1;
            $data['col']=1;
        }
        else {
            $data['row']=$maxRow;
            $data['col']=$maxCol+1;
        }
        $this->db->set('row', $data['row']);
        $this->db->set('col', $data['col']);
        $this->db->set('page_id', $data['page_id']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('video_id', $data['video_id']);
        $this->db->update('pagerecordsdragdrop');
    }
    public function updatePagerecordsdragdropTribute($data)
    {
        $this->db->select('max(row) as maxRow');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $query = $this->db->get();
        $maxRow =$query->row('maxRow');

        $this->db->select('max(col) as maxCol');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('page_id', $data['page_id']);
        $this->db->where('row', $maxRow);
        $query = $this->db->get();
        $maxCol =$query->row('maxCol');

        if($maxCol==5)
        {
            $data['row']=$maxRow+1;
            $data['col']=1;
        }
        else {
            $data['row']=$maxRow;
            $data['col']=$maxCol+1;
        }
        $this->db->set('row', $data['row']);
        $this->db->set('col', $data['col']);
        $this->db->set('page_id', $data['page_id']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('tribute_id', $data['tribute_id']);
        $this->db->update('pagerecordsdragdrop');
    }
    public function deleteContentPage($data)
    {
        $this->db->delete('pagerecordsdragdrop',$data);
    }
    public function existPinnedPage($user_id)
    {
        $this->db->select('tribute_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('user_id',$user_id);
        $this->db->where('tribute_id !=0');
        $query = $this->db->get();
        return $query->result();
    }

    public function totalCommentParticularText($page_id)
    {
        $this->db->select('pagerecordsdragdrop.text_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('text_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['text_id']);

        }
        if($result)
        {
            $this->db->select('text_id,COUNT(text_id) as total');
            $this->db->group_by('text_id');
            $this->db->where_in('text_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalLikeParticularText($page_id)
    {
        $this->db->select('pagerecordsdragdrop.text_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('text_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['text_id']);

        }
        if($result)
        {
            $this->db->select('text_id,COUNT(text_id) as total');
            $this->db->group_by('text_id');
            $this->db->where_in('text_id',$result);
            $query1 = $this->db->get('text_like');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalCommentParticularImage($page_id)
    {
        $this->db->select('pagerecordsdragdrop.image_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('image_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['image_id']);

        }
        if($result)
        {
            $this->db->select('image_id,COUNT(image_id) as total');
            $this->db->group_by('image_id');
            $this->db->where_in('image_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalLikeParticularImage($page_id)
    {
        $this->db->select('pagerecordsdragdrop.image_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('image_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['image_id']);

        }
        if($result)
        {
            $this->db->select('image_id,COUNT(image_id) as total');
            $this->db->group_by('image_id');
            $this->db->where_in('image_id',$result);
            $query1 = $this->db->get('image_like');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalCommentParticularVideo($page_id)
    {
        $this->db->select('pagerecordsdragdrop.video_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('video_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['video_id']);

        }
        if($result)
        {
            $this->db->select('video_id,COUNT(video_id) as total');
            $this->db->group_by('video_id');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalLikePerticularVideo($page_id)
    {
        $this->db->select('pagerecordsdragdrop.video_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('video_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['video_id']);

        }
        if($result)
        {
            $this->db->select('video_id,COUNT(video_id) as total');
            $this->db->group_by('video_id');
            $this->db->where_in('video_id',$result);
            $query1 = $this->db->get('video_like');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalCommentParticularAlbum($page_id)
    {
        $this->db->select('pagerecordsdragdrop.album_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('album_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['album_id']);

        }
        if($result)
        {
            $this->db->select('album_id,COUNT(album_id) as total');
            $this->db->group_by('album_id');
            $this->db->where_in('album_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalLikeParticularAlbum($page_id)
    {
        $this->db->select('pagerecordsdragdrop.album_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('album_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['album_id']);

        }
        if($result)
        {
            $this->db->select('album_id,COUNT(album_id) as total');
            $this->db->group_by('album_id');
            $this->db->where_in('album_id',$result);
            $query1 = $this->db->get('album_like');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalCommentParticularTribute($page_id)
    {
        $this->db->select('pagerecordsdragdrop.tribute_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('tribute_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['tribute_id']);

        }
        if($result)
        {
            $this->db->select('tribute_id,COUNT(tribute_id) as total');
            $this->db->group_by('tribute_id');
            $this->db->where_in('tribute_id',$result);
            $query1 = $this->db->get('tributesdetails');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function totalLikeParticularTribute($page_id)
    {
        $this->db->select('pagerecordsdragdrop.tribute_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('tribute_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['tribute_id']);

        }
        if($result)
        {
            $this->db->select('tribute_id,COUNT(tribute_id) as total');
            $this->db->group_by('tribute_id');
            $this->db->where_in('tribute_id',$result);
            $query1 = $this->db->get('tribute_like');
            return $query1->result();
        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }

    public function updatePageContentResizableDraggable($data)
    {

        $this->db->set('sizex',$data['sizex']);
        $this->db->set('sizey',$data['sizey']);
        $this->db->set('row',$data['row']);
        $this->db->set('col',$data['col']);
        $this->db->where('dragdrop_id',$data['dragdrop_id']);
        $this->db->update('pagerecordsdragdrop');
    }
    public function insertNotification($notifyData)
    {
        $this->db->insert('notificationdetails',$notifyData);
    }
    public function updatePrivacySettings($data)
    {
        $this->db->set('seeme',$data['seeme']);
        $this->db->set('findme',$data['findme']);
        $this->db->where('userid',$data['userid']);
        $this->db->update('user');
    }

    public function getNotification($userid)
    {
        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','friendsRequest');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query1=$this->db->get();
        $res1=$query1->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','friendsRequestAccept');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query2=$this->db->get();
        $res2=$query2->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','friendsAccept');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query3=$this->db->get();
        $res3=$query3->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','friendsRequestDecline');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query4=$this->db->get();
        $res4=$query4->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','friendsDecline');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query5=$this->db->get();
        $res5=$query5->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tagTribute');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query6=$this->db->get();
        $res6=$query6->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','deleteTributeRequest');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query7=$this->db->get();
        $res7=$query7->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','confirmDeleteTributeRequest');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query8=$this->db->get();
        $res8=$query8->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','deleteTributeRequestConfirm');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query9=$this->db->get();
        $res9=$query9->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','declineDeleteTributeRequest');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query10=$this->db->get();
        $res10=$query10->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','deleteTributeRequestDecline');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query11=$this->db->get();
        $res11=$query11->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumimageuploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','imageLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query12=$this->db->get();
        $res12=$query12->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,textdetails.title as text_title,textdetails.description as text_description,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('textdetails', 'textdetails.textid=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','textLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query13=$this->db->get();
        $res13=$query13->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumvideouploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumvideouploaddetails','albumvideouploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','videoLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query14=$this->db->get();
        $res14=$query14->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tributeLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query15=$this->db->get();
        $res15=$query15->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumimageuploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','imageComment');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query16=$this->db->get();
        $res16=$query16->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,textdetails.title as text_title,textdetails.description as text_description,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('textdetails', 'textdetails.textid=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','textComment');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query17=$this->db->get();
        $res17=$query17->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumvideouploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumvideouploaddetails','albumvideouploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->where('notificationdetails.notify_type','videoComment');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query18=$this->db->get();
        $res18=$query18->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tributeComment');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query19=$this->db->get();
        $res19=$query19->result_array();


        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumimageuploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('user u2','u2.userid=notificationdetails.UID');
        $this->db->join('user u1','u1.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.FID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tagImage');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query20=$this->db->get();
        $res20=$query20->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,textdetails.title as text_title,textdetails.description as text_description,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('textdetails', 'textdetails.textid=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('user u1','u1.userid=notificationdetails.FID');
        $this->db->join('user u2','u2.userid=notificationdetails.UID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.FID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tagText');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query21=$this->db->get();
        $res21=$query21->result_array();

        $this->db->select('notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumvideouploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumvideouploaddetails','albumvideouploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.FID');
        $this->db->join('user u2','u2.userid=notificationdetails.UID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.FID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tagVideo');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query22=$this->db->get();
        $res22=$query22->result_array();

        $this->db->select('u3.fullname as full_name_tag,u3.userid as userId_tag,p3.pageid as pageId_tag,notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumimageuploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumimageuploaddetails', 'albumimageuploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumimageuploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->join('user u3','u3.userid=albumimageuploaddetails.user_id');
        $this->db->join('pagedetails p3','p3.user_id=albumimageuploaddetails.user_id');
        $this->db->where('notificationdetails.notify_type','imageCommentLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('p3.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query23=$this->db->get();
        $res23=$query23->result_array();

        $this->db->select('u3.fullname as full_name_tag,u3.userid as userId_tag,p3.pageid as pageId_tag,notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,textdetails.title as text_title,textdetails.description as text_description,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('textdetails', 'textdetails.textid=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=textdetails.album_id');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->join('user u3','u3.userid=textdetails.user_id');
        $this->db->join('pagedetails p3','p3.user_id=textdetails.user_id');
        $this->db->where('notificationdetails.notify_type','textCommentLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('p3.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query24=$this->db->get();
        $res24=$query24->result_array();

        $this->db->select('u3.fullname as full_name_tag,u3.userid as userId_tag,p3.pageid as pageId_tag,notificationdetails.*,u1.userid,u1.fullname,u1.profileimage,u2.userid as userId,u2.fullname as full_name,u2.profileimage as profile_image,p1.pageid,p2.pageid as pageId,albumvideouploaddetails.*,albumdetails.albumid,albumdetails.title,albumdetails.description');
        $this->db->from('notificationdetails');
        $this->db->join('albumvideouploaddetails','albumvideouploaddetails.id=notificationdetails.notify_type_id');
        $this->db->join('albumdetails', 'albumdetails.albumid=albumvideouploaddetails.albumid');
        $this->db->join('user u1','u1.userid=notificationdetails.UID');
        $this->db->join('user u2','u2.userid=notificationdetails.FID');
        $this->db->join('pagedetails p1','p1.user_id=notificationdetails.UID');
        $this->db->join('pagedetails p2','p2.user_id=notificationdetails.FID');
        $this->db->join('user u3','u3.userid=albumvideouploaddetails.user_id');
        $this->db->join('pagedetails p3','p3.user_id=albumvideouploaddetails.user_id');
        $this->db->where('notificationdetails.notify_type','videoCommentLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('p1.default_page',1);
        $this->db->where('p2.default_page',1);
        $this->db->where('p3.default_page',1);
        $this->db->where('albumvideouploaddetails.is_deleted','0');
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query25=$this->db->get();
        $res25=$query25->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','tributeCommentLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query26=$this->db->get();
        $res26=$query26->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','albumComment');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query27=$this->db->get();
        $res27=$query27->result_array();

        $this->db->select('notificationdetails.*,user.userid,user.fullname,user.profileimage,pagedetails.pageid');
        $this->db->from('notificationdetails');
        $this->db->join('user','user.userid=notificationdetails.UID');
        $this->db->join('pagedetails','pagedetails.user_id=notificationdetails.UID');
        $this->db->where('notificationdetails.notify_type','albumLike');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('pagedetails.default_page',1);
        $this->db->order_by("notificationdetails.notificationdate","desc");
        $query28=$this->db->get();
        $res28=$query28->result_array();

        $result=array();
        foreach($res1 as $value)
        {
            array_push($result,$value);
        }
        foreach($res2 as $value)
        {
            array_push($result,$value);

        }
        foreach($res3 as $value)
        {
            array_push($result,$value);

        }
        foreach($res4 as $value)
        {
            array_push($result,$value);

        }
        foreach($res5 as $value)
        {
            array_push($result,$value);

        }
        foreach($res6 as $value)
        {
            array_push($result,$value);

        }
        foreach($res7 as $value)
        {
            array_push($result,$value);

        }
        foreach($res8 as $value)
        {
            array_push($result,$value);

        }
        foreach($res9 as $value)
        {
            array_push($result,$value);

        }
        foreach($res10 as $value)
        {
            array_push($result,$value);

        }
        foreach($res11 as $value)
        {
            array_push($result,$value);

        }
        foreach($res12 as $value)
        {
            array_push($result,$value);

        }
        foreach($res13 as $value)
        {
            array_push($result,$value);

        }
        foreach($res14 as $value)
        {
            array_push($result,$value);

        }
        foreach($res15 as $value)
        {
            array_push($result,$value);

        }
        foreach($res16 as $value)
        {
            array_push($result,$value);

        }
        foreach($res17 as $value)
        {
            array_push($result,$value);

        }
        foreach($res18 as $value)
        {
            array_push($result,$value);

        }
        foreach($res19 as $value)
        {
            array_push($result,$value);

        }
        foreach($res20 as $value)
        {
            array_push($result,$value);

        }
        foreach($res21 as $value)
        {
            array_push($result,$value);

        }
        foreach($res22 as $value)
        {
            array_push($result,$value);

        }
        foreach($res23 as $value)
        {
            array_push($result,$value);

        }
        foreach($res24 as $value)
        {
            array_push($result,$value);

        }
        foreach($res25 as $value)
        {
            array_push($result,$value);

        }
        foreach($res26 as $value)
        {
            array_push($result,$value);

        }
        foreach($res27 as $value)
        {
            array_push($result,$value);

        }
        foreach($res28 as $value)
        {
            array_push($result,$value);

        }
        usort($result, function ($one, $two) {
            if ($one['notificationdate'] === $two['notificationdate']) {
                return 0;
            }
            return $one['notificationdate'] > $two['notificationdate'] ? -1 : 1;
        });
        return $result;
    }
    public function countNotication($userid)
    {
        $this->db->select('count(*) as total');
        $this->db->from('notificationdetails');
        $this->db->where('notificationdetails.FID',$userid);
        $this->db->where('notificationdetails.notify_seen',0);
        $query=$this->db->get();
        return $query->row();
    }
    public function markAllRead($userid)
    {
        $this->db->set('notify_seen','1');
        $this->db->where('FID', $userid);
        $this->db->update('notificationdetails');
    }

    public function updateNotificationAcceptFriendRequest($data)
    {
        $this->db->set('notify_seen','1');
        $this->db->set('notify_type', $data['notify_type']);
        $this->db->set('notify_details', $data['notify_details']);
        $this->db->set('notificationdate', $data['notificationdate']);
        $this->db->where("((`UID`=".$data['UID']." AND `FID`=".$data['FID'].") OR (`UID`=".$data['FID']." AND `FID`=".$data['UID']."))");
        $this->db->where('notify_type','friendsRequest');
        $this->db->update('notificationdetails');

    }
    public function updateNotificationDeleteTributeRequest($data)
    {
        $this->db->set('notify_seen','1');
        $this->db->set('notify_type', $data['notify_type']);
        $this->db->set('notify_details', $data['notify_details']);
        $this->db->set('notificationdate', $data['notificationdate']);
        $this->db->where("((`UID`=".$data['UID']." AND `FID`=".$data['FID'].") OR (`UID`=".$data['FID']." AND `FID`=".$data['UID']."))");
        $this->db->where('notify_type','deleteTributeRequest');
        $this->db->where('notify_type_id',$data['notify_type_id']);
        $this->db->update('notificationdetails');
    }
    public function deleteNotificationTagTribute($data)
    {
        $this->db->where('UID', $data['UID']);
        $this->db->where('FID', $data['FID']);
        $this->db->where('notify_type_id',$data['notify_type_id']);
        $this->db->where('notify_type',$data['notify_type']);
        $this->db->delete('notificationdetails');
    }
    public function deleteNotification($data)
    {
        $this->db->where('UID', $data['UID']);
        $this->db->where('FID', $data['FID']);
        $this->db->where('notify_type_id',$data['notify_type_id']);
        $this->db->where('notify_type',$data['notify_type']);
        $this->db->delete('notificationdetails');
    }
    public function updateNotifySeen($notificationid)
    {
        $this->db->set('notify_seen','1');
        $this->db->where('notificationid',$notificationid);
        $this->db->update('notificationdetails');
    }
    public function createOrUpdateTrackUserDetailsAfterLogin($user_id,$ip)
    {
        $this->db->select('*');
        $this->db->from('trackUserDetailsAfterLogin');
        $this->db->where('user_id',$user_id);
        $this->db->limit(1);
        $query=$this->db->get();
        $num = $query->num_rows();
        if($num==1)
        {
            //update
            $this->db->set('machine_ip',$ip);
            $this->db->set('login_time',date("Y-m-d H:i:s"));
            $this->db->where('user_id',$user_id);
            $this->db->update('trackUserDetailsAfterLogin');
        }
        else
        {
            //create
            $data=array(
                            'user_id'=>$user_id,
                            'machine_ip'=>$ip,
                            'login_time'=>date("Y-m-d H:i:s")

                        );
            $this->db->insert('trackUserDetailsAfterLogin',$data);

        }
    }
    public function updatePage($data)
    {
        $this->db->set('page_title', $data['page_title']);
        $this->db->set('page_style_id', $data['page_style_id']);
        $this->db->set('page_foreground_id', $data['page_foreground_id']);
        $this->db->where('pageid', $data['pageid']);
        $this->db->update('pagedetails');
    }
    public function deletePage($page_id,$user_id)
    {
        $this->db->where('pageid',$page_id);
        $this->db->where('user_id',$user_id);
        $this->db->delete('pagedetails');

        $this->db->where('page_id',$page_id);
        $this->db->where('user_id',$user_id);
        $this->db->delete('pagerecordsdragdrop');
    }
    public function fetchSessionData($userid)
    {
        $this->db->select('user.emailid as session_email_id,user.password as session_pass');
        $this->db->from('user');
        $this->db->where('user.userid',$userid);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    // Email Exist or not
    public function emailExistence($session_emailid,$emailid)
    {
        $this->db->select('*');
        $this->db->where('emailid',$emailid);
        $this->db->where('emailid !=', $session_emailid);
        $this->db->limit(1);
        $query = $this->db->get('user');
        $num = $query->num_rows();
        if($num==1)
        {
            return $query->row_array();
        }
        else
        {
            return FALSE;
        }
    }
    public function changeEmail($oldemailid,$emailid)
    {
        $this->db->set('emailid', $emailid);
        $this->db->where('emailid', $oldemailid);
        $this->db->update('user');
    }
    public function isFriendsOrNot($userid,$session_id)
    {
        $this->db->select('*');
        $this->db->where(" ((userid=$userid AND friendsid=$session_id) OR (userid=$session_id AND friendsid=$userid))");
        $this->db->where('relationshipstatus', 'accepted');
        $query = $this->db->get('friends');
        return $query->result();
    }
    public function isFriendsRequestOrNot($userid,$session_id)
    {
        $this->db->select('*');
        $this->db->where("(userid=$session_id AND friendsid=$userid)");
        $this->db->where('relationshipstatus', 'outgoing');
        $query = $this->db->get('friends');
        return $query->result();
    }
    public function unfriend($data)
    {
        $this->db->where('relationshipstatus',$data['relationshipstatus']);
        $this->db->where(" ((userid='".$data['userid']."' AND friendsid='".$data['friendsid']."') OR (userid='".$data['friendsid']."' AND friendsid='".$data['userid']."'))");
        $this->db->delete('friends');
        $this->db->where(" ((user_id='".$data['userid']."' AND friends_id='".$data['friendsid']."') OR (user_id='".$data['friendsid']."' AND friends_id='".$data['userid']."'))");
        $this->db->delete('friendsTagContent');
        $this->db->where(" ((UID='".$data['userid']."' AND FID='".$data['friendsid']."') OR (UID='".$data['friendsid']."' AND FID='".$data['userid']."'))");
        $this->db->delete('notificationdetails');
    }
    public function upadteChangeEmail($email)
    {
        $this->db->where('emailid', $email);
        $this->db->update('user', array('changeEmail' => '1'));
    }
    public function checkChangeEmail($email)
    {
        $this->db->select('*');
        $this->db->where('emailid',$email);
        $this->db->where('changeEmail','1');
        $this->db->limit(1);
        $query = $this->db->get('user');
        $num = $query->num_rows();
        if($num==1)
        {
            return $query->row_array();
        }
        else
        {
            return FALSE;
        }
    }
    public function cancelSentRequest($data)
    {
        $this->db->where('relationshipstatus',$data['relationshipstatus']);
        $this->db->where("(userid='".$data['userid']."' AND friendsid='".$data['friendsid']."')");
        $this->db->delete('friends');
        $this->db->where("(UID='".$data['userid']."' AND FID='".$data['friendsid']."')");
        $this->db->where("notify_type","friendsRequest");
        $this->db->delete('notificationdetails');
    }
    public function seeMyProfileStatus($user_id)
    {
        $this->db->select('seeme');
        $this->db->where('userid',$user_id);
        $this->db->limit(1);
        $query = $this->db->get('user');
        return $query->row_array();
    }
    public function checkFriendOrNot($userid,$friendsid)
    {
        $this->db->select('*');
        $this->db->where(" ((userid=$userid AND friendsid=$friendsid) OR (userid=$friendsid AND friendsid=$userid)) ");
        $this->db->where('relationshipstatus', 'accepted');
        $query = $this->db->get('friends');
        $num = $query->num_rows();
        if($num)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    public function fetchForegroundStyle()
    {
        $this->db->select('*');
        $this->db->from('foregroundStyle');
        $this->db->where('status','on');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function fetchBackgroundStyle()
    {
        $this->db->select('*');
        $this->db->from('backgroundStyle');
        $this->db->where('status','on');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function totalContentParticularAlbum($page_id)
    {
        $this->db->select('pagerecordsdragdrop.album_id');
        $this->db->from('pagerecordsdragdrop');
        $this->db->where('page_id',$page_id);
        $this->db->where('album_id != 0');
        $query = $this->db->get();
        $res=$query->result_array();
        $result=array();
        foreach($res as $value)
        {
            array_push($result,$value['album_id']);

        }
        if($result)
        {

            $this->db->select('albumid,COUNT(albumid) as total');
            $this->db->group_by('albumid');
            $this->db->where_in('albumid',$result);
            $query4 = $this->db->get('albuminnerrecordsdragdrop');
            return $query4->result();

        }
        else
        {
            $obj = (object) array('total' => 0);
            return $obj;
        }
    }
    public function select_page_foreground_image($foreground_image_id)
    {
        $this->db->select("*");
        $this->db->from("pageForegroundStyle");
        $this->db->where('id',$foreground_image_id);
        $query = $this->db->get();
        return $query->result_array();
    }
}
